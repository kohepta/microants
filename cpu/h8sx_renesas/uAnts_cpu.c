﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu.c
 *
 * Implementation depends on target CPU (H8SX Renesas).
 *
 * @date 2010.05.18 new creation
 */

#include <bios/h8sx_renesas/include/bios_cpu_ext.h>
#include "uAnts_cpu_constants.h"
#include "uAnts_cpu_impl.h"
#include "uAnts_tsk.h"
#include "uAnts_inh.h"

/**
 * task activation preparation.
 * 
 * @param tsk task entity
 */
void knlcpu_task_activation_preparation(
	T_TSK * const tsk )
{
	/* create the pointer to structure of stack frame. */
	T_CPU_SAVED_STACK * sp;
	UB * start;
	
	/* stack pointer of task is decided. */
	start = (UB *)( tsk->t_stack_origin ) + tsk->t_stack_size;
	sp = (T_CPU_SAVED_STACK *)start - 1;
	
	/* initialize the stack frame to zero. */
	sp->c_er6 = 0;
 	sp->c_er5 = 0;
	sp->c_er4 = 0;
	sp->c_er3 = 0;
	sp->c_er2 = 0;
	
	/* store the knltsk_run function address to the pc. */
	sp->c_pc = (UW)knltsk_run;
	
	/* stroe this stack pointer in the task entity. */
	tsk->t_stack_pointer = (VP)sp;
}

/**
 * getting minimum size of the stack that is used by the task.
 * 
 * @return getting minimum size of the stack that is used by the task.
 */
UINT knlcpu_get_minimum_stack_size( void )
{
	return sizeof( T_CPU_SAVED_STACK );
}

/**
 * pre-process before invoking an user-interrupt handler.
 * 
 * @param intno interrupt number
 */
void knlcpu_pre_interrupt_process(
    INTNO intno )
{
	;
}

/**
 * post-process after invoking an user-interrupt handler.
 * 
 * @param intno interrupt number
 */
void knlcpu_post_interrupt_process(
    INTNO intno )
{
	;
}

/**
 * wrapper of knlinh_invoke.
 * 
 * @param intno interrupt number
 * @param current_exr current EXR value
 * @param previous_exr previous EXR value
 * @return base imask
 */
UB knlcpu_inh_invoke_wrapper(
	INTNO intno,
	UB current_exr, 
	UB previous_exr )
{
	bios_cpu_ext_set_current_imask( current_exr );
	
	knlinh_invoke( intno );
	
	bios_cpu_ext_set_current_imask( previous_exr );
	
	return bios_cpu_ext_get_base_imask();
}

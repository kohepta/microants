﻿;
; MicroAnts Real-time Kernel
; 
; This software is conforms to the specification of uITRON ver. 4.03.00
; 
;

;
; @file uAnts_cpu_dispatch.src
;
; implementations of MicroAnts CPU task dispatcher.
;
; @date 2010.06.06 new creation
;

	.CPU H8SXA
	.SECTION P, CODE, ALIGN = 2
	
	.INCLUDE "uAnts_cpu_asm_constants.inc"
	
	.IMPORT _knlsys
	.IMPORT _knlsys_stack
	.IMPORT _knltsk_current
	.IMPORT _knltsk_ready_queue
	
	.IMPORT _knlcpu_task_activation_preparation
	.IMPORT _knltex_invoke
	
	.GLOBAL _knlcpu_dispatch_from_exit_task
	.GLOBAL _knlcpu_dispatch
	.GLOBAL _knlcpu_dispatch_from_main

;
; task dispather from exit task (ext_tsk(), exd_tsk()).
;
; @param restart TRUE: restart is necessary
; FALSE: restart is not necessary
;

_knlcpu_dispatch_from_exit_task:
	; get the top of system stack. and it copy to sp.
	mov.l	#KNLSYS_STACK_ORIGIN, er1
	mov.l	@(_knlsys_stack, er1), sp
	
	; check for restart task.
	cmp.b	#1, r0l
	bne		knlcpu_dispatch_skip_restart_task
	
	; if restart of task is necessary,
	; call the task activation preparation function
	; (pass the current running task entity (er0)).
	mov.l	@_knltsk_current, er0
	jsr		@_knlcpu_task_activation_preparation
	
knlcpu_dispatch_skip_restart_task:
	; enable the dispatch.
	mov.l	#KNLSYS_DISPATCHABLE, er1
	mov.b	#1, @(_knlsys, er1)
	; jump to the knlcpu_dispatch_from_main to activate the task.
	jmp		@_knlcpu_dispatch_from_main

;
; task dispather with saving the current running task context.
;
_knlcpu_dispatch:
	; check whether dispatch is enabled or disabled.
	mov.l	#KNLSYS_DISPATCHABLE, er1
	cmp.b	#1, @(_knlsys, er1)
	; if dispatch is disabled, finish dispatch.
	bne		knlcpu_dispatch_end
	
	; check if current running task has highest priority.
	; check whether next task entity of ready queue
	; and current running task entity are same.
	cmp.l	@_knltsk_ready_queue, @_knltsk_current
	
	; when current running task has not highest priority or
	; next task entity of ready queue and current running task 
	; entity are same, finish dispatch.
	beq		knlcpu_dispatch_end
	
	; save the context to stack frame
	push.l	er2
	push.l	er3
	push.l	er4
	push.l	er5
	push.l	er6
	
	; save the stack pointer to current running task entity.
	mov.l	@_knltsk_current, er1
	mov.l	sp, @(KNLTSK_STACK_POINTER, er1)
	
	; set the system stack top to sp
	mov.l	#KNLSYS_STACK_ORIGIN, er1
	mov.l	@(_knlsys_stack, er1), sp

;
; task dispather from main().
;
_knlcpu_dispatch_from_main:
	; check for ready task in ready queue.
	mov.l	@_knltsk_ready_queue, er1
	; cmp.l knltsk_ready_queue.q_joint.j_next,
	;       knltsk_ready_queue.q_joint.j_next->j_next
	cmp.l	er1, @er1
	; if there is ready task, skip idling.
	bne		knlcpu_dispatch_skip_idling
	
	; if there is no ready task, make current running task entity to NULL.
	mov.l	#0, @_knltsk_current
	
	; keeps idling until ready task appears.
knlcpu_dispatch_idling:
	; enable all interrupts
	andc	#EXR_I_MASK_OFF, exr
	sleep
	; disable all interrupts.
	orc		#EXR_I_MASK, exr
	
	cmp.l	er1, @er1
	beq		knlcpu_dispatch_idling
	; get the highest priority task from ready queue.
	mov.l	@er1, er1
	
knlcpu_dispatch_skip_idling:
	; store the next ready task from ready queue
	; into the current running task entity.
	mov.l	er1, @_knltsk_current
	
	; restore the stack pointer from current running task entity.
	mov.l	@(KNLTSK_STACK_POINTER, er1), sp
	
	; restore the context from stack frame.
	pop.l	er6
	pop.l	er5
	pop.l	er4
	pop.l	er3
	pop.l	er2
	
knlcpu_dispatch_end:
	; invoke the current running task's exception.
	jsr		@_knltex_invoke
	
	rts

	.END

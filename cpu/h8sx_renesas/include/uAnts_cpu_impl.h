﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu_impl.h
 *
 * MicroAnts internal target CPU (H8SX renesas) dependent definitions.
 *
 * @date 2010.05.18 new creation
 */

#ifndef __UANTS_CPU_IMPL_H__
#define __UANTS_CPU_IMPL_H__

#include "uAnts_cpu.h"

/**
 * target CPU (H8SX renesas) saved stack frame structure.
 */
struct t_cpu_saved_stack
{
	/** er6 register. */
	UW c_er6;
	
	/** er5 register. */
	UW c_er5;
	
	/** er4 register. */
	UW c_er4;
	
	/** er3 register. */
	UW c_er3;
	
	/** er2 register. */
	UW c_er2;
	
	/** return address. */
	UW c_pc;
};
typedef struct t_cpu_saved_stack T_CPU_SAVED_STACK;

#endif

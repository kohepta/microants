﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu_constants.h
 * 
 * MicroAnts internal target CPU (H8SX renesas) dependent definitions.
 * 
 * @date 2010.05.18 new creation
 */
   
#ifndef __UANTS_CPU_CONSTANTS_H__
#define __UANTS_CPU_CONSTANTS_H__

#endif

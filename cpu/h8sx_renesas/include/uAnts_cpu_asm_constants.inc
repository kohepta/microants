﻿;
; MicroAnts Real-time Kernel
;
; This software is conforms to the specification of uITRON ver. 4.03.00
;

;
; @file uAnts_cpu_asm_constants.inc
;
; MicroAnts internal target CPU (H8SX renesas) dependent definitions.
;
; @date 2010.05.18 new creation
;

	.AIFDEF __UANTS_CPU_ASM_CONSTANTS_INC__
	.AELSE
	__UANTS_CPU_ASM_CONSTANTS_INC__:	.DEFINE ""
	
	; T_SYS member offset
	KNLSYS_INITIALIZED:					.DEFINE "0"		; + 1
	KNLSYS_DISPATCHABLE:				.DEFINE "1"		; + 1
	KNLSYS_NESTED_DEPTH:				.DEFINE "2"		;
	
	; T_SYS_STACK member offset
	KNLSYS_STACK_SIZE:					.DEFINE "0"		; + 2
	KNLSYS_STACK_ORIGIN:				.DEFINE "2"		;
	
	; T_TSK member offset
	KNLTSK_super_joint_next:			.DEFINE "0"		; + 4
	KNLTSK_super_joint_prev:			.DEFINE "4"		; + 4
	KNLTSK_SUPER_ID:					.DEFINE "8"		; + 2
	KNLTSK_SUPER_KIND:					.DEFINE "10"	; + 1
	KNLTSK_SUPER_ATTRIBUTE:				.DEFINE "11"	; + 2
	KNLTSK_SUPER_EXIST:					.DEFINE "13"	; + 1
	KNLTSK_EXINF:						.DEFINE "14"	; + 4
	KNLTSK_ENTRY:						.DEFINE "18"	; + 4
	KNLTSK_INITIAL_PRIORITY:			.DEFINE "22"	; + 2
	KNLTSK_BASE_PRIORITY:				.DEFINE "24"	; + 2
	KNLTSK_PRIORITY:					.DEFINE "26"	; + 2
	KNLTSK_STATE:						.DEFINE "28"	; + 2
	KNLTSK_STACK_SIZE:					.DEFINE "30"	; + 4
	KNLTSK_STACK_ORIGIN:				.DEFINE "34"	; + 4
	KNLTSK_STACK_POINTER:				.DEFINE "38"	; + 4
	KNLTSK_ACTIVATE_COUNT:				.DEFINE "42"	; + 1
	KNLTSK_WAKEUP_COUNT:				.DEFINE "43"	; + 1
	KNLTSK_SUSPEND_COUNT:				.DEFINE "44"	; + 1
	KNLTSK_WAIT:						.DEFINE "45"	; + 4
	KNLTSK_TEX:							.DEFINE "49"	;
	
	; CPU dependent
	EXR_I_MASK:							.DEFINE "H'07"
	EXR_I_MASK_OFF:						.DEFINE "H'F8"
	
	.AENDI

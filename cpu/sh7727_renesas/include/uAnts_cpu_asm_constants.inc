﻿;
; MicroAnts Real-time Kernel
;
; This software is conforms to the specification of uITRON ver. 4.03.00
;

;
; @file uAnts_cpu_asm_constants.inc
;
; MicroAnts internal target CPU (SH7727) dependent definitions.
;
; @date 2011.03.04 new creation
;

	.AIFDEF __UANTS_CPU_ASM_CONSTANTS_INC__
	.AELSE
	__UANTS_CPU_ASM_CONSTANTS_INC__:	.DEFINE ""
	
	; T_SYS member offset
	KNLSYS_INITIALIZED:					.DEFINE "0"		; + 1
	KNLSYS_DISPATCHABLE:				.DEFINE "1"		; + 1
	KNLSYS_NESTED_DEPTH:				.DEFINE "2"		;
	
	; T_SYS_STACK member offset
	KNLSYS_STACK_SIZE:					.DEFINE "0"		; + 4
	KNLSYS_STACK_ORIGIN:				.DEFINE "4"		;
	
	; T_TSK member offset
	KNLTSK_super_joint_next:			.DEFINE "0"		; + 4
	KNLTSK_super_joint_prev:			.DEFINE "4"		; + 4
	KNLTSK_SUPER_ID:					.DEFINE "8"		; + 4
	KNLTSK_SUPER_KIND:					.DEFINE "12"	; + 1 (+ padding 3)
	KNLTSK_SUPER_ATTRIBUTE:				.DEFINE "16"	; + 4
	KNLTSK_SUPER_EXIST:					.DEFINE "20"	; + 1 (+ padding 3)
	KNLTSK_EXINF:						.DEFINE "24"	; + 4
	KNLTSK_ENTRY:						.DEFINE "28"	; + 4
	KNLTSK_INITIAL_PRIORITY:			.DEFINE "32"	; + 4
	KNLTSK_BASE_PRIORITY:				.DEFINE "36"	; + 4
	KNLTSK_PRIORITY:					.DEFINE "40"	; + 4
	KNLTSK_STATE:						.DEFINE "44"	; + 4
	KNLTSK_STACK_SIZE:					.DEFINE "48"	; + 2 (+ padding 2)
	KNLTSK_STACK_ORIGIN:				.DEFINE "52"	; + 4
	KNLTSK_STACK_POINTER:				.DEFINE "56"	; + 4
	KNLTSK_ACTIVATE_COUNT:				.DEFINE "60"	; + 1 (+ padding 3)
	KNLTSK_WAKEUP_COUNT:				.DEFINE "64"	; + 1 (+ padding 3)
	KNLTSK_SUSPEND_COUNT:				.DEFINE "68"	; + 1 (+ padding 3)
	KNLTSK_WAIT:						.DEFINE "72"	; + 4
	KNLTSK_TEX:							.DEFINE "76"	;
	
	; CPU dependent
	SR_IMASK:							.DEFINE "H'000000F0"
	SR_IMASK_OFF:						.DEFINE "H'FFFFFF0F"
	
	SR_BL:								.DEFINE "H'10000000"
	SR_BL_OFF:							.DEFINE "H'EFFFFFFF"
	
	.AENDI

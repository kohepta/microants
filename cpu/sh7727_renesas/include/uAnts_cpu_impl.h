﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu_impl.h
 *
 * MicroAnts internal target CPU (SH7727) dependent definitions.
 *
 * @date 2010.03.04 new creation
 */

#ifndef __UANTS_CPU_IMPL_H__
#define __UANTS_CPU_IMPL_H__

#include "uAnts_cpu.h"

/**
 * target CPU (SH7727) saved stack frame structure.
 */
struct t_cpu_saved_stack
{
	/** 0x00 - R8. */
	UINT c_r8;
	
	/** 0x04 - R9. */
	UINT c_r9;
	
	/** 0x08 - R10. */
	UINT c_r10;
	
	/** 0x0C - R11. */
	UINT c_r11;
	
	/** 0x10 - R12. */
	UINT c_r12;
	
	/** 0x14 - R13. */
	UINT c_r13;
	
	/** 0x18 - R14. */
	UINT c_r14;
	
	/** 0x1C - SR. */
	UINT c_sr;
	
	/** 0x20 - GBR. */
	UINT c_gbr;
	
	/** 0x24 - MACH. */
	UINT c_mach;
	
	/** 0x28 - MACL. */
	UINT c_macl;
	
	/** 0x2C - PR. */
	UINT c_pr;
};
typedef struct t_cpu_saved_stack T_CPU_SAVED_STACK;

#endif

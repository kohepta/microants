﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu_constants.h
 * 
 * MicroAnts internal target CPU (SH7727) dependent definitions.
 * 
 * @date 2011.03.04 new creation
 */
   
#ifndef __UANTS_CPU_CONSTANTS_H__
#define __UANTS_CPU_CONSTANTS_H__

#define	SR_IMASK						(0x000000F0)

/** SR.MD = 特権モード */
#define SR_MD_PRIVILEGE					(0x40000000)
/** SR.RB = レジスタバンクを LDC/STC 命令でアクセスする */
#define SR_RB_BANK1_GENERIC				(0x20000000)
	
#endif

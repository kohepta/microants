﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu.c
 *
 * Implementation depends on target CPU (SH7727).
 *
 * @date 2010.03.04 new creation
 */

#include <bios/sh7727_renesas/include/bios_cpu_ext.h>
#include "uAnts_cpu_constants.h"
#include "uAnts_cpu_impl.h"
#include "uAnts_tsk.h"
#include "uAnts_inh.h"

/**
 * task activation preparation.
 * 
 * @param tsk task entity
 */
void knlcpu_task_activation_preparation(
	T_TSK * const tsk )
{
	/* create the pointer to structure of stack frame. */
	T_CPU_SAVED_STACK * sp;
	UB * start;
	
	/* stack pointer of task is decided. */
	start = (UB *)( tsk->t_stack_origin ) + tsk->t_stack_size;
	sp = (T_CPU_SAVED_STACK *)start - 1;
	
	/* initialize the stack frame to zero. */
	sp->c_r8 = 0;
	sp->c_r9 = 0;
	sp->c_r10 = 0;
	sp->c_r11 = 0;
	sp->c_r12 = 0;
	sp->c_r13 = 0;
	sp->c_r14 = 0;
	sp->c_gbr = 0;
	sp->c_mach = 0;
	sp->c_macl = 0;
	
	/*jp
	  タスクは以下の状態で起動.
	    - 特権モード
	    - レジスタ BANK1 を汎用レジスタとしてアクセス
	    - 割り込み禁止
	*/
	sp->c_sr = ( SR_MD_PRIVILEGE | SR_RB_BANK1_GENERIC | SR_IMASK );
	
	/* store the knltsk_run function address to the pr. */
	sp->c_pr = (UINT)knltsk_run;
	
	/* stroe this stack pointer in the task entity. */
	tsk->t_stack_pointer = (VP)sp;
}

/**
 * getting minimum size of the stack that is used by the task.
 * 
 * @return getting minimum size of the stack that is used by the task.
 */
UINT knlcpu_get_minimum_stack_size( void )
{
	return sizeof( T_CPU_SAVED_STACK );
}

/**
 * pre-process before invoking an user-interrupt handler.
 * 
 * @param intno interrupt number
 */
void knlcpu_pre_interrupt_process(
    INTNO intno )
{
	;
}

/**
 * post-process after invoking an user-interrupt handler.
 * 
 * @param intno interrupt number
 */
void knlcpu_post_interrupt_process(
    INTNO intno )
{
	;
}

/**
 * wrapper of knlinh_invoke.
 * 
 * @param ssr saved status register
 */
void knlcpu_inh_invoke_wrapper(
	UINT ssr  )
{
	UINT intno;
	UINT previous_imask;
	UINT current_imask;
	
	bios_intc_detect_asserted_highest_priority_interrupt(
		&intno,
		&current_imask );
	
	bios_cpu_ext_set_current_imask( current_imask );
	
	knlinh_invoke( intno );
	
	previous_imask = (UINT)( ssr & SR_IMASK ) >> 4;
	
	bios_cpu_ext_set_current_imask( previous_imask );
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_hook.c
 *
 * MicroAnts user-configurable kernel hook functions.
 *
 * @date 2010.05.17 new creation
 */

#include "uAnts_hook.h"

/**
 * pre-process before doing system-initialization.
 */
void cfghook_pre_system_initialize( void )
{
	;
}

/**
 * post-process after doing system-initialization.
 */
void cfghook_post_system_initialize( void )
{
	;
}

/**
 * pre-process before starting the system timer.
 */
void cfghook_pre_system_timer_start( void )
{
	;
}

/**
 * post-process after starting the system timer.
 */
void cfghook_post_system_timer_start( void )
{
	{
		extern initialize_user_application( void );
		
		initialize_user_application();
	}
}

/**
 * pre-process before invoking the system-timer interrupt handler.
 */
void cfghook_pre_system_timer_interrupt_process( void )
{
	;
}

/**
 * post-process after invoking the system-timer interrupt handler.
 */
void cfghook_post_system_timer_interrupt_process( void )
{
	;
}

/**
 * called when undefined interrupt occurred.
 * 
 * @param inhno interrupt handler number
 */
void cfghook_undefined_interrupt(
	INHNO inhno )
{
	;
}

/**
 * called when undefined exception occurred.
 * 
 * @param excno cpu exception handler number
 */
void cfghook_undefined_exception(
	EXCNO excno )
{
	;
}

/**
 * called when kernel shutdown.
 */
void cfghook_shutdown( void )
{
	;
}

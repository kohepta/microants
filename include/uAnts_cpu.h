﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cpu.h
 *
 * Internal definitions of MicroAnts target CPU.
 * Implementation depends on target CPU.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_CPU_H__
#define __UANTS_CPU_H__

#include "uAnts.h"

struct t_tsk;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_cpu.c */
void knlcpu_task_activation_preparation(
	struct t_tsk * const tsk );

/* uAnts_cpu.c */
UINT knlcpu_get_minimum_stack_size( void );

/* uAnts_cpu.c */
void knlcpu_pre_interrupt_process(
    INTNO intno );

/* uAnts_cpu.c */
void knlcpu_post_interrupt_process(
    INTNO intno );

/* uAnts_cpu_dispatch.xxx */
void knlcpu_dispatch( void );

/* uAnts_cpu_dispatch.xxx */
void knlcpu_dispatch_from_exit_task(
	BOOL restart);

/* uAnts_cpu_dispatch.xxx */
void knlcpu_dispatch_from_main( void );

#endif

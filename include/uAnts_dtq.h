﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dtq.h
 *
 * Internal definitions of MicroAnts data-queue.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_DTQ_H__
#define __UANTS_DTQ_H__

#include "uAnts_object.h"
#include "uAnts_tsk.h"
#include "uAnts_wait.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * class T_DTQ_SND_WAIT.
 */
struct t_dtq_snd_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** send data pointer. */
	VP w_data;
};
typedef struct t_dtq_snd_wait T_DTQ_SND_WAIT;

/**
 * class T_DTQ_RCV_WAIT.
 */
struct t_dtq_rcv_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** pointer of pointer to receive data. */
	VP * w_p_data;
};
typedef struct t_dtq_rcv_wait T_DTQ_RCV_WAIT;

/**
 * class T_DTQ.
 */
struct t_dtq
{
	/** super class. */
	T_OBJECT d_super;
	
	/** number of data. */
	UINT d_count;
	
	/** origin of buffer. */
	VP * d_origin;
	
	/** read pointer. */
	const VP * d_read;
	
	/** write pointer. */
	VP * d_write;
	
	/** Number of data that has been sent. */
	UINT d_sent_count;
	
	/** queue of tasks waiting to send. */
	T_QUEUE d_snd_task_queue;
	
	/** queue of task waiting to receive. */
	T_QUEUE d_rcv_task_queue;
};
typedef struct t_dtq T_DTQ;

/** queue of data-queues that can be used. */
extern T_QUEUE knldtq_free_queue;

/** entity array of data-queues. */
extern T_DTQ knldtq_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_dtq.c */
void knldtq_init_entities( void );

/* uAnts_dtq.c */
void knldtq_initialize(
	T_DTQ * const dtq,
	const T_CDTQ * pk_cdtq,
	VP vp );

/* uAnts_dtq.c */
void knldtq_finalize(
	T_DTQ * const dtq );

/* uAnts_dtq.c */
ER knldtq_create(
	T_DTQ * const dtq,
	const T_CDTQ * pk_cdtq );

/* uAnts_dtq.c */
ER knldtq_forcibly_send(
	ID dtqid,
	VP_INT data );

/* uAnts_dtq.c */
void knldtq_read(
	T_DTQ * const dtq,
	VP * p_data );

/* uAnts_dtq.c */
ER knldtq_receive(
    ID dtqid,
    VP_INT * p_data,
	T_EVENT * const event,
	TMO timeout );

/* uAnts_dtq.c */
ER knldtq_send(
	ID dtqid,
	VP_INT data,
	T_EVENT * const event,
	TMO timeout );

/* uAnts_dtq.c */
T_TSK * knldtq_send_to_receiver_directly(
	T_DTQ * const dtq,
	VP_INT data,
	BOOL * ready );

/* uAnts_dtq.c */
void knldtq_write(
	T_DTQ * const dtq,
	VP data );

/**
 * getting data-queue entity.
 * 
 * @param id data-queue ID
 * @return data-queue entity
 */
#if defined( __HITACHI__ )
#pragma inline( knldtq_get_entity )
#endif
UANTS_INLINE T_DTQ * knldtq_get_entity(
	ID dtqid )
{
	T_DTQ * dtq;
	
	if ( dtqid <= 0 || dtqid > UANTS_CFG_DTQ_MAX_ID )
	{
		dtq = NULL;
	}
	else
	{
		dtq = &knldtq_entities[dtqid - 1];
	}
	
	return dtq;
}

/**
 * is data-queue empty?
 * 
 * @param dtq data-queue entity
 * @retval TRUE data-queue is empty
 * @retval FALSE data-queue is not empty
 */
#if defined( __HITACHI__ )
#pragma inline( knldtq_is_empty )
#endif
UANTS_INLINE BOOL knldtq_is_empty(
	const T_DTQ * const dtq )
{
	return ( ( dtq->d_read == dtq->d_write ) && ( dtq->d_sent_count == 0 ) )
		? TRUE
		: FALSE;
}

/**
 * is data-queue full?
 * 
 * @param dtq data-queue entity
 * @retval TRUE data-queue is full
 * @retval FALSE data-queue is not full
 */
#if defined( __HITACHI__ )
#pragma inline( knldtq_is_full )
#endif
UANTS_INLINE BOOL knldtq_is_full(
	const T_DTQ * const dtq )
{
	return ( ( dtq->d_read == dtq->d_write ) && ( dtq->d_sent_count != 0 ) )
		? TRUE
		: FALSE;
}

#endif

#endif

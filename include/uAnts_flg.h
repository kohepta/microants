﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_flg.h
 *
 * Internal definitions of MicroAnts eventflag.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_FLG_H__
#define __UANTS_FLG_H__

#include "uAnts_object.h"
#include "uAnts_wait.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * class T_FLG_WAIT.
 */
struct t_flg_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** wait-flag pattern. */
	FLGPTN w_pattern;
	
	/** wait-mode: TWF_ANDW/TWF_ORW. */
	MODE w_mode;
	
	/** flag pattern, when wait released. */
	FLGPTN * w_released_pattern;
};
typedef struct t_flg_wait T_FLG_WAIT;

/**
 * class T_FLG.
 */
struct t_flg
{
	/** super class. */
	T_OBJECT f_super;
	
	/**
	 * other context executing knlflg_set()?
	 *   TRUE: executing.
	 *   FALSE: not executing.
	 */
	BOOL f_setting;
	
	/** flag pattern. */
	FLGPTN f_pattern;
	
	/**
	 * reservation flag pattern.
	 * 
	 * no-task context uses it, when f_setting is TRUE.
	 */
	FLGPTN f_reservation_pattern;
	
	/** queue of tasks waiting to specific flag pattern. */
	T_QUEUE f_task_queue;
};
typedef struct t_flg T_FLG;

/** queue of eventflags that can be used. */
extern T_QUEUE knlflg_free_queue;

/** entity array of eventflags. */
extern T_FLG knlflg_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_flg.c */
void knlflg_init_entities( void );

/* uAnts_flg.c */
void knlflg_initialize(
	T_FLG * const flg,
	const T_CFLG * pk_cflg );

/* uAnts_flg.c */
void knlflg_finalize(
	T_FLG * const flg );

/* uAnts_flg.c */
ER knlflg_create(
	T_FLG * const flg,
	const T_CFLG * pk_cflg );

/* uAnts_flg.c */
BOOL knlflg_compare(
	T_FLG * const flg,
	const T_FLG_WAIT * wait );

/* uAnts_flg.c */
ER knlflg_set(
	ID flgid,
	FLGPTN setptn );

/* uAnts_flg.c */
ER knlflg_wait(
    ID flgid,
    FLGPTN waiptn,
    MODE wfmode,
    FLGPTN * p_flgptn,
	T_EVENT * const event,
	TMO timeout );

/**
 * getting eventflag entity.
 * 
 * @param id eventflag ID
 * @return eventflag entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlflg_get_entity )
#endif
UANTS_INLINE T_FLG * knlflg_get_entity(
	ID flgid )
{
	T_FLG * flg;
	
	if ( flgid <= 0 || flgid > UANTS_CFG_FLG_MAX_ID )
	{
		flg = NULL;
	}
	else
	{
		flg = &knlflg_entities[flgid - 1];
	}
	
	return flg;
}

/**
 * getting eventflag multi-wait attribute.
 * 
 * @param flg eventflag entity
 * @retval TRUE multi-wait attribute is valid
 * @retval FALSE multi-wait attribute is invalid
 */
#if defined( __HITACHI__ )
#pragma inline( knlflg_attribute_multi_wait )
#endif
UANTS_INLINE BOOL knlflg_attribute_multi_wait(
	T_FLG * const flg )
{
	return (BOOL)( knlobject_get_attribute( &flg->f_super) & TA_WMUL );
}

/**
 * getting eventflag clear attribute.
 * 
 * @param flg eventflag entity
 * @retval TRUE clear attribute is valid
 * @retval FALSE clear attribute is invalid
 */
#if defined( __HITACHI__ )
#pragma inline( knlflg_attribute_clear )
#endif
UANTS_INLINE BOOL knlflg_attribute_clear(
	T_FLG * const flg )
{
	return (BOOL)( knlobject_get_attribute( &flg->f_super) & TA_CLR );
}

#endif

#endif

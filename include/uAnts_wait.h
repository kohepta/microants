﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_wait.h
 *
 * Internal definitions of MicroAnts wait-controller.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_WAIT_H__
#define __UANTS_WAIT_H__

#include "uAnts_event.h"

/*
 kind of wait-callback call purpose.
*/
enum t_wait_callback_purpose
{
	/**
	 * when check waitable
	 * (this code is used at critical section).
	 */
	purpose_CHECK_WAITABLE = 1,
	
	/**
	 * when waiting timeout
	 * (this code is used at critical section).
	 */
	purpose_TIMEOUT,
	
	/**
	 * when waiting released
	 * (this code is used at critical section).
	 */
	purpose_WAITING_RELEASED,
	
	/** 
	 * when wait-task priority changed
	 * (this code is used at not critical section).
	 */
	purpose_PRIORITY_CHANGED,
	
	/**
	 * when waiting terminated
	 * (this code is used at not critical section).
	 */
	purpose_TERMINATED

};
typedef enum t_wait_callback_purpose T_WAIT_CALLBACK_PURPOSE;

/**
 * class T_WAIT.
 */
struct t_wait
{
	/** reference of time-event. */
	T_EVENT * w_event;
	
	/**
	 * result of waiting.
	 *
	 *   E_OK
	 *   E_PAR
	 *   E_ILUSE
	 *   E_NOEXS
	 *   E_RLWAI
	 *   E_TMOUT
	 *   E_DLT
	 */
	ER w_result;
	
	/**
	 * waited factor.
	 *
	 *   TTW_SLP
	 *   TTW_DLY
	 *   TTW_SEM
	 *   TTW_FLG
	 *   TTW_SDTQ
	 *   TTW_RDTQ
	 *   TTW_MBX
	 *   TTW_MTX
	 *   TTW_SMBF
	 *   TTW_RMBF
	 *   TTW_CAL
	 *   TTW_ACP
	 *   TTW_RDV
	 *   TTW_MPF
	 *   TTW_MPL
	 */
	STAT w_factor;
	
	/** reference of wait-object. */
	T_OBJECT * w_object;
	
	/** queue of waiting tasks. */
	T_QUEUE * w_queue;
	
	/**
	 * wait-callback for illegal case.
	 *
	 *   1. check waitable.
	 *   2. waiting timeout.
	 *   3. waiting released.
	 *   4. wait-task priority changed.
	 *   5. waiting terminated.
	 */
	BOOL ( * w_callback )(
		const struct t_wait * const wait,
		T_WAIT_CALLBACK_PURPOSE purpose );
};
typedef struct t_wait T_WAIT;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_wait.c */
void knlwait_build(
	T_WAIT * const wait,
	T_EVENT * const event,
	STAT factor,
	T_OBJECT * const object,
	T_QUEUE * const queue,
	BOOL ( * callback )(
		const T_WAIT * const wait,
		T_WAIT_CALLBACK_PURPOSE purpose ) );

/* uAnts_wait.c */
void knlwait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/* uAnts_wait.c */
BOOL knlwait_check_waitable(
	const T_WAIT * const wait );

/**
 * getting result of waiting.
 * 
 * @param wp wait-controller entity
 * @return result of waiting
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_get_result )
#endif
UANTS_INLINE ER knlwait_get_result(
	T_WAIT * const wp )
{
	return wp->w_result;
}

/**
 * setting result of waiting.
 * 
 * @param wp wait-controller entity
 * @param result of waiting
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_set_result )
#endif
UANTS_INLINE void knlwait_set_result(
	T_WAIT * const wp,
	ER result )
{
	wp->w_result = result;
}

/**
 * getting waited factor.
 * 
 * @param wp wait-controller entity
 * @return result of waiting
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_get_factor )
#endif
UANTS_INLINE STAT knlwait_get_factor(
	T_WAIT * const wp )
{
	return wp->w_factor;
}

/**
 * getting wait-object.
 * 
 * @param wp wait-controller entity
 * @return wait-object
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_get_object )
#endif
UANTS_INLINE T_OBJECT * knlwait_get_object(
	T_WAIT * const wp )
{
	return wp->w_object;
}

/**
 * getting queue of waiting tasks.
 * 
 * @param wp wait-controller entity
 * @return queue of waiting tasks
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_get_queue )
#endif
UANTS_INLINE T_QUEUE * knlwait_get_queue(
	T_WAIT * const wp )
{
	return wp->w_queue;
}

/**
 * getting reference of time-event.
 * 
 * @param wp wait-controller entity
 * @return reference of time-event
 */
#if defined( __HITACHI__ )
#pragma inline( knlwait_get_event )
#endif
UANTS_INLINE T_EVENT * knlwait_get_event(
	T_WAIT * const wp )
{
	return wp->w_event;
}

#endif

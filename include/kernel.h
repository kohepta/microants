﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file kernel.h
 *
 * service call prototypes of MicroAnts.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __KERNEL_H__
#define __KERNEL_H__

#include <itron.h>

/*---------------------------------------------
 * task management (api/tsk/)
 *-------------------------------------------*/

/* uAnts_cre_tsk.c */
ER cre_tsk( ID tskid, T_CTSK * pk_ctsk );

/* uAnts_acre_tsk.c */
ER_ID acre_tsk( T_CTSK * pk_ctsk );

/* uAnts_del_tsk.c */
ER del_tsk( ID tskid );

/* uAnts_act_tsk.c */
ER act_tsk( ID tskid );

/* uAnts_iact_tsk.c */
ER iact_tsk( ID tskid );

/* uAnts_can_act.c */
ER_UINT can_act( ID tskid );

/* uAnts_ext_tsk.c */
void ext_tsk( void );

/* uAnts_exd_tsk.c */
void exd_tsk( void );

/* uAnts_ter_tsk.c */
ER ter_tsk( ID tskid );

/* uAnts_chg_pri.c */
ER chg_pri( ID tskid, PRI tskpri );

/* uAnts_get_pri.c */
ER get_pri( ID tskid, PRI * p_tskpri );

/* uAnts_ref_tsk.c */
ER ref_tsk( ID tskid, T_RTSK * pk_rtsk );

/* uAnts_ref_tst.c */
ER ref_tst( ID tskid, T_RTST * pk_rtst );

/*---------------------------------------------
 * task dependent synchronization (api/tsk/)
 *-------------------------------------------*/

/* uAnts_tslp_tsk.c */
ER tslp_tsk( TMO tmout );

/* uAnts_slp_tsk.c */
ER slp_tsk( void );

/* uAnts_wup_tsk.c */
ER wup_tsk( ID tskid );

/* uAnts_iwup_tsk.c */
ER iwup_tsk( ID tskid );

/* uAnts_can_wup.c */
ER_UINT can_wup( ID tskid );

/* uAnts_rel_wai.c */
ER rel_wai( ID tskid );

/* uAnts_irel_wai.c */
ER irel_wai( ID tskid );

/* uAnts_sus_tsk.c */
ER sus_tsk( ID tskid );

/* uAnts_rsm_tsk.c */
ER rsm_tsk( ID tskid );

/* uAnts_frsm_tsk.c */
ER frsm_tsk( ID tskid );

/* uAnts_dly_tsk.c */
ER dly_tsk( RELTIM dlytim );

/*---------------------------------------------
 * task exception handling (api/tex/)
 *-------------------------------------------*/

/* uAnts_ras_tex.c */
ER ras_tex( ID tskid, TEXPTN rasptn );

/* uAnts_iras_tex.c */
ER iras_tex( ID tskid, TEXPTN rasptn );

/* uAnts_def_tex.c */
ER def_tex( ID tskid, T_DTEX * pk_dtex );

/* uAnts_dis_tex.c */
ER dis_tex( void );

/* uAnts_ena_tex.c */
ER ena_tex( void );

/* uAnts_sns_tex.c */
BOOL sns_tex( void );

/* uAnts_ref_tex.c */
ER ref_tex( ID tskid, T_RTEX * pk_rtex );

/*---------------------------------------------
 * semaphore (api/sem/)
 *-------------------------------------------*/

/* uAnts_cre_sem.c */
ER cre_sem( ID semid, T_CSEM * pk_csem );

/* uAnts_acre_sem.c */
ER_ID acre_sem( T_CSEM * pk_csem );

/* uAnts_del_sem.c */
ER del_sem( ID semid );

/* uAnts_sig_sem.c */
ER sig_sem( ID semid );

/* uAnts_isig_sem.c */
ER isig_sem( ID semid );

/* uAnts_twai_sem.c */
ER twai_sem( ID semid, TMO tmout );

/* uAnts_wai_sem.c */
ER wai_sem( ID semid );

/* uAnts_pol_sem.c */
ER pol_sem( ID semid );

/* uAnts_ref_sem.c */
ER ref_sem( ID semid, T_RSEM * pk_rsem );

/*---------------------------------------------
 * eventflag (api/flg/)
 *-------------------------------------------*/

/* uAnts_cre_flg.c */
ER cre_flg( ID flgid, T_CFLG * pk_cflg );

/* uAnts_acre_flg.c */
ER_ID acre_flg( T_CFLG * pk_cflg );

/* uAnts_del_flg.c */
ER del_flg( ID flgid );

/* uAnts_set_flg.c */
ER set_flg( ID flgid, FLGPTN setptn );

/* uAnts_iset_flg.c */
ER iset_flg( ID flgid, FLGPTN setptn );

/* uAnts_clr_flg.c */
ER clr_flg( ID flgid, FLGPTN clrptn );

/* uAnts_twai_flg.c */
ER twai_flg( ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN * p_flgptn, TMO tmout );

/* uAnts_wai_flg.c */
ER wai_flg( ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN * p_flgptn );

/* uAnts_pol_flg.c */
ER pol_flg( ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN * p_flgptn );

/* uAnts_ref_flg.c */
ER ref_flg(  ID flgid, T_RFLG * pk_rflg );

/*---------------------------------------------
 * data-queue (api/dtq/)
 *-------------------------------------------*/

/* uAnts_cre_dtq.c */
ER cre_dtq( ID dtqid, T_CDTQ * pk_cdtq );

/* uAnts_acre_dtq.c */
ER_ID acre_dtq( T_CDTQ * pk_cdtq );

/* uAnts_del_dtq.c */
ER del_dtq( ID dtqid );

/* uAnts_tsnd_dtq.c */
ER tsnd_dtq( ID dtqid, VP_INT data, TMO tmout );

/* uAnts_snd_dtq.c */
ER snd_dtq( ID dtqid, VP_INT data );

/* uAnts_psnd_dtq.c */
ER psnd_dtq( ID dtqid, VP_INT data );

/* uAnts_ipsnd_dtq.c */
ER ipsnd_dtq( ID dtqid, VP_INT data );

/* uAnts_fsnd_dtq.c */
ER fsnd_dtq( ID dtqid, VP_INT data );

/* uAnts_ifsnd_dtq.c */
ER ifsnd_dtq( ID dtqid, VP_INT data );

/* uAnts_trcv_dtq.c */
ER trcv_dtq( ID dtqid, VP_INT * p_data, TMO tmout );

/* uAnts_rcv_dtq.c */
ER rcv_dtq( ID dtqid, VP_INT * p_data );

/* uAnts_prcv_dtq.c */
ER prcv_dtq( ID dtqid, VP_INT * p_data );

/* uAnts_ref_dtq.c */
ER ref_dtq( ID dtqid, T_RDTQ * pk_rdtq );

/*---------------------------------------------
 * mailbox (api/mbx/)
 *-------------------------------------------*/

/* uAnts_cre_mbx.c */
ER cre_mbx( ID mbxid, T_CMBX * pk_cmbx );

/* uAnts_acre_mbx.c */
ER_ID acre_mbx( T_CMBX * pk_cmbx );

/* uAnts_del_mbx.c */
ER del_mbx( ID mbxid );

/* uAnts_snd_mbx.c */
ER snd_mbx( ID mbxid, T_MSG * pk_msg );

/* uAnts_trcv_mbx.c */
ER trcv_mbx( ID mbxid, T_MSG * * ppk_msg, TMO tmout );

/* uAnts_rcv_mbx.c */
ER rcv_mbx( ID mbxid, T_MSG * * ppk_msg );

/* uAnts_prcv_mbx.c */
ER prcv_mbx( ID mbxid, T_MSG * * ppk_msg );

/* uAnts_ref_mbx.c */
ER ref_mbx( ID mbxid, T_RMBX * pk_rmbx );

/*---------------------------------------------
 * fixed-sized memory pool (api/mpf/)
 *-------------------------------------------*/

/* uAnts_cre_mpf.c */
ER cre_mpf( ID mpfid, T_CMPF * pk_cmpf );

/* uAnts_acre_mpf.c */
ER_ID acre_mpf( T_CMPF * pk_cmpf );

/* uAnts_del_mpf.c */
ER del_mpf( ID mpfid );

/* uAnts_tget_mpf.c */
ER tget_mpf( ID mpfid, VP * p_blk, TMO tmout );

/* uAnts_get_mpf.c */
ER get_mpf( ID mpfid, VP * p_blk );

/* uAnts_pget_mpf.c */
ER pget_mpf( ID mpfid, VP * p_blk );

/* uAnts_rel_mpf.c */
ER rel_mpf( ID mpfid, VP blk );

/* uAnts_ref_mpf.c */
ER ref_mpf( ID mpfid, T_RMPF * pk_rmpf );

/*---------------------------------------------
 * variable-sized memory pool (api/mpl/)
 *-------------------------------------------*/

/* uAnts_cre_mpl.c */
ER cre_mpl( ID mplid, T_CMPL * pk_cmpl );

/* uAnts_acre_mpl.c */
ER_ID acre_mpl( T_CMPL * pk_cmpl );

/* uAnts_del_mpl.c */
ER del_mpl( ID mplid );

/* uAnts_tget_mpl.c */
ER tget_mpl( ID mplid, UINT blksz, VP * p_blk, TMO tmout );

/* uAnts_get_mpl.c */
ER get_mpl( ID mplid, UINT blksz, VP * p_blk );

/* uAnts_pget_mpl.c */
ER pget_mpl( ID mplid, UINT blksz, VP * p_blk );

/* uAnts_rel_mpl.c */
ER rel_mpl( ID mplid, VP blf );

/* uAnts_ref_mpl.c */
ER ref_mpl( ID mplid, T_RMPL * pk_rmpl );

/*---------------------------------------------
 * system time management (api/tim)
 *-------------------------------------------*/

/* uAnts_set_tim.c */
ER set_tim( SYSTIM * p_systim );

/* uAnts_get_tim.c */
ER get_tim( SYSTIM * p_systim );

/* uAnts_isig_tim.c */
ER isig_tim( void );

/*---------------------------------------------
 * cyclic handler (api/cyc/)
 *-------------------------------------------*/

/* uAnts_cre_cyc.c */
ER cre_cyc( ID cycid, T_CCYC * pk_ccyc );

/* uAnts_acre_cyc.c */
ER_ID acre_cyc( T_CCYC * pk_ccyc );

/* uAnts_del_cyc.c */
ER del_cyc( ID cycid );

/* uAnts_sta_cyc.c */
ER sta_cyc( ID cycid );

/* uAnts_stp_cyc.c */
ER stp_cyc( ID cycid );

/* uAnts_ref_cyc.c */
ER ref_cyc( ID cycid, T_RCYC * pk_rcyc );

/*---------------------------------------------
 * alarm handler (api/alm/)
 *-------------------------------------------*/

/* uAnts_cre_alm.c */
ER cre_alm( ID almid, T_CALM * pk_calm );

/* uAnts_acre_alm.c */
ER_ID acre_alm( T_CALM * pk_calm );

/* uAnts_del_alm.c */
ER del_alm( ID almid );

/* uAnts_sta_alm.c */
ER sta_alm( ID almid, RELTIM almtim );

/* uAnts_stp_alm.c */
ER stp_alm( ID almid );

/* uAnts_ref_alm.c */
ER ref_alm( ID almid, T_RALM * pk_ralm );

/*---------------------------------------------
 * system management (api/sys/)
 *-------------------------------------------*/

/* uAnts_rot_rdq.c */
ER rot_rdq( PRI tskpri );

/* uAnts_irot_rdq.c */
ER irot_rdq( PRI tskpri );

/* uAnts_get_tid.c */
ER get_tid( ID * p_tskid );

/* uAnts_iget_tid.c */
ER iget_tid( ID * p_tskid );

/* uAnts_loc_cpu.c */
ER loc_cpu( void );

/* uAnts_ref_ver.c */
ER ref_ver( T_RVER * pk_rver );

/* uAnts_ref_cfg.c */
ER ref_cfg( T_RCFG * pk_rcfg );

/* uAnts_iloc_cpu.c */
ER iloc_cpu( void );

/* uAnts_unl_cpu.c */
ER unl_cpu( void );

/* uAnts_iunl_cpu.c */
ER iunl_cpu( void );

/* uAnts_dis_dsp.c */
ER dis_dsp( void );

/* uAnts_ena_dsp.c */
ER ena_dsp( void );

/* uAnts_sns_ctx.c */
BOOL sns_ctx( void );

/* uAnts_sns_loc.c */
BOOL sns_loc( void );

/* uAnts_sns_dsp.c */
BOOL sns_dsp( void );

/* uAnts_sns_dpn.c */
BOOL sns_dpn( void );

/*---------------------------------------------
 * interrupt management (api/inh/)
 *-------------------------------------------*/
 
/* uAnts_def_inh.c */
ER def_inh( INHNO inhno, T_DINH * pk_dinh );

/*-- (api/int/) --*/
 
/* uAnts_ena_int.c */
ER ena_int( INTNO intno );

/* uAnts_dis_int.c */
ER dis_int( INTNO intno );

/* uAnts_chg_ims.c */
ER chg_ims( IMASK imask );

/* uAnts_get_ims.c */
ER get_ims( IMASK * imask );

/*---------------------------------------------
 * cpu exception handler (api/exc/)
 *-------------------------------------------*/

/* uAnts_def_exc.c */
ER def_exc( EXCNO excno, T_DEXC * pk_dexc );

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file error.h
 *
 * error code definitions of MicroAnts.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __ERROR_H__
#define __ERROR_H__

#include <types.h>

/**
 * error codes
 */
 
/** no error. */
#define E_OK			(0)
/** system error. */
#define E_SYS			(-5)
/** no supported function. */
#define E_NOSPT			(-9)
/** reserved function code. */
#define E_RSFN			(-10)
/** reserved attribute(s). */
#define E_RSATR			(-11)
/** illegal parameter. */
#define E_PAR			(-17)
/** illegal ID number. */
#define E_ID			(-18)
/** illegal context. */
#define E_CTX			(-25)
/** memory access violation. */
#define E_MACV			(-26)
/** object access violation. */
#define E_OACV			(-27)
/** illegal service call usage. */
#define E_ILUSE			(-28) 
/** insufficient memory. */
#define E_NOMEM			(-33)
/** no allocatable ID number. */
#define E_NOID			(-34)
/** illegal object state. */
#define E_OBJ			(-41)
/** no-existent object */
#define E_NOEXS			(-42)
/** queuing overflow. */
#define E_QOVR			(-43)
/** forced release from waiting. */
#define E_RLWAI			(-49)
/** polling failure or timeout. */
#define E_TMOUT			(-50)
/** waiting object deleted. */
#define E_DLT			(-51)
/** waiting object state changed. */
#define E_CLS			(-52)
/** non-blocking call accepted. */
#define E_WBLK			(-57)
/** buffer overflow. */
#define E_BOVR			(-58)

/* generate an error code from main and sub error codes */
#define	ERCD( mercd, sercd )	( ( sercd << 8 ) | ( mercd & 0xff ) )

#ifdef _int8_
/* retrieve the main error code from an error code */
#define	MERCD( ercd )			( (ER)(B)ercd )
/* retrieve the sub error code from an error code */
#define	SERCD( ercd )			( (ER)( (B)( ercd >> 8 ) ) )
#else
#define	MERCD( ercd )			( ( ercd & 0x80 ) == 0 ? ( ercd & 0xff ) \
									: ( ercd | ~0xff) )
#define	SERCD( ercd )			( ~( ~0 >> 8 ) | ( ercd >> 8 ) )
#endif

#endif

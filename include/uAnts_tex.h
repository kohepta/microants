﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tex.h
 *
 * Internal definitions of MicroAnts task exception.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_TEX_H__
#define __UANTS_TEX_H__

#include "uAnts_object.h"

/**
 * class T_TEX.
 */
struct t_tex
{
	/** attribute: TA_HLNG/TA_ASM. */
	ATR e_attribute;
	
	/** state: TTEX_ENA/TTEX_DIS. */
	STAT e_state;
	
	/* reservation exception factor. */
	TEXPTN e_pattern;
	
	/* entry point. */
	void ( * e_entry )( TEXPTN, VP_INT );
};
typedef struct t_tex T_TEX;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_tex.c */
void knltex_define(
	T_TEX * const tex,
	const T_DTEX * pk_dtex );

/* uAnts_tex.c */
ER knltex_raise(
	ID tskid,
	TEXPTN rasptn );

/* uAnts_tex.c */
void knltex_invoke( void );

/**
 * finalize task exception entity.
 * 
 * @param tex exception entity
 */
#if defined( __HITACHI__ )
#pragma inline( knltex_finalize )
#endif
UANTS_INLINE void knltex_finalize(
	T_TEX * const tex )
{
	tex->e_pattern = 0;
	tex->e_state = TTEX_DIS;
}

#endif

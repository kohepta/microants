﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cyc.h
 *
 * Internal definitions of MicroAnts cyclic handler.
 *
 * @date 2010.05.30 new creation
 */

#ifndef __UANTS_CYC_H__
#define __UANTS_CYC_H__

#include "uAnts_event.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/**
 * class T_CYC.
 */
struct t_cyc
{
	/** super class. */
	T_EVENT c_super;
	
	/** state: TCYC_STA/TCYC_STP. */
	STAT c_state;
	
	/** extended information. */
	VP_INT c_exinf;
	
	/** entry point. */
	void ( * c_entry )( VP_INT );
	
	/** time of cycle. */
	RELTIM c_time;
	
	#if ( UANTS_CFG_TIC_NUME > 1 )
	/** time until next handler is started. */
	RELTIM c_time_next;
	
	/** adjust. */
	RELTIM c_adjust;
	#endif
	
	/** timer interrupt nested depth. */
	UB c_nested_depth;
};
typedef struct t_cyc T_CYC;

/** queue of cyclic handlers that can be used. */
extern T_QUEUE knlcyc_free_queue;

/** entity array of cyclic handlers. */
extern T_CYC knlcyc_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_cyc.c */
void knlcyc_init_entities( void );

/* uAnts_cyc.c */
void knlcyc_initialize(
	T_CYC * const cyc,
	const T_CCYC * pk_ccyc );

/* uAnts_cyc.c */
void knlcyc_finalize(
	T_CYC * const cyc );

/* uAnts_cyc.c */
ER knlcyc_create(
	T_CYC * const cyc,
	const T_CCYC * pk_ccyc );

/* uAnts_cyc.c */
void knlcyc_start(
	T_CYC * const cyc,
	RELTIM timeout );

/**
 * getting cyclic handler entity.
 * 
 * @param id cyclic handler ID
 * @return cyclic handler entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlcyc_get_entity )
#endif
UANTS_INLINE T_CYC * knlcyc_get_entity(
	ID cycid )
{
	T_CYC * cyc;
	
	if ( cycid <= 0 || cycid > UANTS_CFG_CYC_MAX_ID )
	{
		cyc = NULL;
	}
	else
	{
		cyc = &knlcyc_entities[cycid - 1];
	}
	
	return cyc;
}

/**
 * getting cyclic handler preserve phase attribute.
 * 
 * @param cyc cyclic handler entity
 * @retval TRUE preserve phase attribute is valid
 * @retval FALSE preserve phase attribute is invalid
 */
#if defined( __HITACHI__ )
#pragma inline( knlcyc_attribute_preserve_phase )
#endif
UANTS_INLINE BOOL knlcyc_attribute_preserve_phase(
	T_CYC * const cyc )
{
	return (BOOL)( knlevent_get_attribute( &cyc->c_super) & TA_PHS );
}

#if ( UANTS_CFG_TIC_NUME > 1 )
/**
 * setting time until next handler is started.
 * 
 * @param cyc cyclic handler entity
 * @param time until next handler is started.
 */
#if defined( __HITACHI__ )
#pragma inline( knlcyc_adjust_time_until_next )
#endif
UANTS_INLINE void knlcyc_adjust_time_until_next(
	T_CYC * const cyc,
	RELTIM time )
{
	cyc->c_adjust = time % UANTS_CFG_TIC_NUME;
	
	if ( cyc->c_adjust > 0 )
	{
		cyc->c_time_next = time - ( UANTS_CFG_TIC_NUME - cyc->c_adjust );
	}
	else
	{
		cyc->c_time_next = cyc->c_time;
	}
}
#endif

#endif

#endif

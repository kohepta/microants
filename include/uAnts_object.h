﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_object.h
 *
 * Internal definitions of MicroAnts object.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_OBJECT_H__
#define __UANTS_OBJECT_H__

#include "uAnts_joint.h"
#include "uAnts_queue.h"

/*
 kind of objects.
*/

/** object is invalid. */
#define KNLOBJECT_INVALID	(0)

/** object is task. */
#define KNLOBJECT_TSK		(1)

/** object is semaphore. */
#define KNLOBJECT_SEM		(2)

/** object is eventflag. */
#define KNLOBJECT_FLG		(3)

/** object is data-queue. */
#define KNLOBJECT_DTQ		(4)

/** object is mailbox. */
#define KNLOBJECT_MBX		(5)

/** object is fixed-sized memory pool. */
#define KNLOBJECT_MPF		(6)

/** object is variable-sized memory pool. */
#define KNLOBJECT_MPL		(7)

/** object is cyclic handler. */
#define KNLOBJECT_CYC		(8)

/** object is alarm handler. */
#define KNLOBJECT_ALM		(9)

/** object is interrupt handler. */
#define KNLOBJECT_INH		(10)

/** object is cpu exception handler. */
#define KNLOBJECT_EXC		(11)

/**
 * class T_OBJECT.
 *
 * base class for all classes.
 */
struct t_object
{
	/** joint. */
	T_JOINT o_joint;

	/** object ID. */
	ID o_id;

	/** object kind. */
	UB o_kind;

	/** object attribute. */
	ATR o_attribute;

	/**
	 * object exists state.
	 *   TRUE: object is exist.
	 *   FALSE: object is not exist.
	 */
	BOOL o_exist;
};
typedef struct t_object T_OBJECT;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_object.c */
void knlobject_initialize(
	T_OBJECT * const object,
	T_QUEUE * const queue,
	ATR attribute );

/* uAnts_object.c */
void knlobject_finalize(
	T_OBJECT * const object,
	T_QUEUE * const queue );

/* uAnts_object.c */
void knlobject_build(
	T_OBJECT * const object,
	ID id,
	UB kind,
	T_QUEUE * const queue );

/**
 * getting object ID.
 * 
 * @param op object entity
 * @return object ID
 */
#if defined( __HITACHI__ )
#pragma inline( knlobject_get_id )
#endif
UANTS_INLINE ID knlobject_get_id(
	T_OBJECT * const op )
{
	return op->o_id;
}

/**
 * getting object kind.
 * 
 * @param op object entity
 * @return object kind
 */
#if defined( __HITACHI__ )
#pragma inline( knlobject_get_kind )
#endif
UANTS_INLINE UB knlobject_get_kind(
	T_OBJECT * const op )
{
	return op->o_kind;
}

/**
 * getting object attribute.
 * 
 * @param op object entity
 * @return object attribute
 */
#if defined( __HITACHI__ )
#pragma inline( knlobject_get_attribute )
#endif
UANTS_INLINE ATR knlobject_get_attribute(
	T_OBJECT * const op )
{
	return op->o_attribute;
}

/**
 * getting object exists state.
 * 
 * @param op object entity
 * @retval TRUE object is exists
 * @retval FALSE object is not exists
 */
#if defined( __HITACHI__ )
#pragma inline( knlobject_exists )
#endif
UANTS_INLINE BOOL knlobject_exists(
	T_OBJECT * const op )
{
	return op->o_exist;
}

#endif

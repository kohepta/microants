﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file itron.h
 *
 * public interface of MicroAnts.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __ITRON_H__
#define __ITRON_H__

#include <types.h>
#include <constants.h>
#include <error.h>

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpl.h
 *
 * Internal definitions of MicroAnts variable-sized memory pool.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_MPL_H__
#define __UANTS_MPL_H__

#include "uAnts_object.h"
#include "uAnts_wait.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_STACK	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )

/** magic number for variable-sized memory pool's memory block. */
#define KNLMPL_MAGIC	(0x1234ABCD)

#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
#define KNLMPL_STACK_ENTITY_ID			(UANTS_CFG_MPL_MAX_ID + 1)
#else
#define KNLMPL_STACK_ENTITY_ID			(UANTS_CFG_MPL_MAX_ID)
#endif

#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
#define KNLMPL_GENERIC_ENTITY_ID		(KNLMPL_STACK_ENTITY_ID + 1)
#else
#define KNLMPL_GENERIC_ENTITY_ID		(KNLMPL_STACK_ENTITY_ID)
#endif

#define KNLMPL_MAX_ID					(KNLMPL_GENERIC_ENTITY_ID)

/**
 * class T_MPL_BLOCK_FREE.
 */
struct t_mpl_block_free
{
	/** joint. */
	T_JOINT f_joint;
	
	/** free block size. */
	UW f_size;
};
typedef struct t_mpl_block_free T_MPL_BLOCK_FREE;

/**
 * class T_MPL_BLOCK_ALLOCATED.
 */
struct t_mpl_block_allocated
{
	/** allocated block size */
	UW a_size;
	
	/** allocated memory block magic number. */
	UW a_magic;
};
typedef struct t_mpl_block_allocated T_MPL_BLOCK_ALLOCATED;

/**
 * class T_MPL_BLOCK.
 */
union t_mpl_block
{
	/** free block. */
	T_MPL_BLOCK_FREE b_free;
	
	/** allocated block. */
	T_MPL_BLOCK_ALLOCATED b_allocated;
};
typedef union t_mpl_block T_MPL_BLOCK;

/**
 * class T_MPL_WAIT.
 */
struct t_mpl_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** wait block size. */
	UW w_size;
	
	/** memory block pointer to pointer. */
	VP * w_block;
};
typedef struct t_mpl_wait T_MPL_WAIT;

/**
 * class T_MPL.
 */
struct t_mpl
{
	/** super class. */
	T_OBJECT v_super;
	
	/** size of buffer (bytes). */
	UW v_size;
	
	/** origin of buffer. */
	VP v_origin;
	
	/** free size of buffer (bytes). */
	UW v_free_size;
	
	/** maximum memory block. */
	T_MPL_BLOCK * v_max_block;
	
	/** queue of free memory blocks. */
	T_QUEUE v_block_queue;
	
	/** queue of tasks waiting to get. */
	T_QUEUE v_task_queue;
};
typedef struct t_mpl T_MPL;

/** queue of variable-sized memory pools that can be used. */
extern T_QUEUE knlmpl_free_queue;

/** entity array of variable-sized memory pools. */
extern T_MPL knlmpl_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_mpl.c */
void knlmpl_init_entities( void );

/* uAnts_mpl.c */
void knlmpl_initialize(
	T_MPL * const mpl,
	const T_CMPL * pk_cmpl,
	VP vp,
	UW vp_size );

/* uAnts_mpl.c */
VP knlmpl_allocate(
	T_MPL * const mpl,
	UW block_size,
	UW * user_area_size );

/* uAnts_mpl.c */
T_MPL_BLOCK * knlmpl_divide_block(
	T_MPL * const mpl,
	T_MPL_BLOCK * block,
	UW block_size,
	UW * user_area_size );

/* uAnts_mpl.c */
T_MPL_BLOCK * knlmpl_free(
	T_MPL * const mpl,
	VP vp );

/* uAnts_mpl.c */
BOOL knlmpl_verify_contains(
	const T_MPL * const mpl,
	const VP vp );

/* uAnts_mpl.c */
void knlmpl_finalize(
	T_MPL * const mpl );

/* uAnts_mpl.c */
ER knlmpl_create(
	T_MPL * const mpl,
	const T_CMPL * pk_cmpl );

/* uAnts_mpl.c */
void knlmpl_allocate_waiting_tasks(
	T_MPL * const mpl,
	T_MPL_BLOCK * block );

/* uAnts_mpl.c */
ER knlmpl_get(
    ID mplid,
    UW blksz,
    VP * p_blk,
	T_EVENT * const event,
	TMO timeout );

/**
 * getting variable-sized memory pool entity.
 * 
 * @param id variable-sized memory pool ID
 * @return variable-sized memory pool entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpl_get_entity )
#endif
UANTS_INLINE T_MPL * knlmpl_get_entity(
	ID mplid )
{
	T_MPL * mpl;
	
	if ( mplid <= 0 || mplid > UANTS_CFG_MPL_MAX_ID )
	{
		mpl = NULL;
	}
	else
	{
		mpl = &knlmpl_entities[mplid - 1];
	}
	
	return mpl;
}

/**
 * getting stack memory pool entity.
 * 
 * @return stack memory pool entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpl_get_stack_entity )
#endif
UANTS_INLINE T_MPL * knlmpl_get_stack_entity( void )
{
	return &knlmpl_entities[KNLMPL_STACK_ENTITY_ID - 1];
}

/**
 * getting generic memory pool entity.
 * 
 * @return generic memory pool entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpl_get_generic_entity )
#endif
UANTS_INLINE T_MPL * knlmpl_get_generic_entity( void )
{
	return &knlmpl_entities[KNLMPL_GENERIC_ENTITY_ID - 1];
}

/**
 * getting the adjusted memory block size.
 * 
 * @param block_size block size before being adjusted
 * @return adjusted block size
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpl_get_adjusted_block_size )
#endif
UANTS_INLINE UW knlmpl_get_adjusted_block_size(
	UW block_size )
{
	UW adjusted_size;
	
	adjusted_size = knlutil_maxu_32( block_size, sizeof( T_MPL_BLOCK ) );
	adjusted_size = knlutil_align_cpu( adjusted_size );
	adjusted_size += sizeof( T_MPL_BLOCK_ALLOCATED );
	
	return adjusted_size;
}

#endif

#endif

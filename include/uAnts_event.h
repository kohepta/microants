﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_event.h
 *
 * Internal definitions of MicroAnts time-event.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_EVENT_H__
#define __UANTS_EVENT_H__

#include "uAnts_object.h"
#include "uAnts_queue.h"

/**
 * class T_EVENT.
 */
struct t_event
{
	/** super class. */
	T_OBJECT e_super;
	
	/** interval until event occurs. (milliseconds) */
	RELTIM e_interval;
	
	/** event callback. */
	void ( * e_callback )( struct t_event * const );
};
typedef struct t_event T_EVENT;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_event.c */
void knlevent_initialize(
	T_EVENT * const event,
	T_QUEUE * const queue,
	ATR attribute );

/* uAnts_event.c */
void knlevent_finalize(
	T_EVENT * const event,
	T_QUEUE * const queue );

/* uAnts_event.c */
void knlevent_build(
	T_EVENT * const event,
	ID id,
	UB kind,
	T_QUEUE * const queue,
	void ( * callback )( T_EVENT * const ) );

/**
 * getting event ID.
 * 
 * @param ep event entity
 * @return event ID
 */
#if defined( __HITACHI__ )
#pragma inline( knlevent_get_id )
#endif
UANTS_INLINE ID knlevent_get_id(
	T_EVENT * const ep )
{
	return knlobject_get_id( &ep->e_super );
}

/**
 * getting event attribute.
 * 
 * @param ep event entity
 * @return event atrribute
 */
#if defined( __HITACHI__ )
#pragma inline( knlevent_get_attribute )
#endif
UANTS_INLINE ATR knlevent_get_attribute(
	T_EVENT * const ep )
{
	return knlobject_get_attribute( &ep->e_super );
}

/**
 * getting event exists state.
 * 
 * @param ep event entity
 * @retval TRUE event is exist
 * @retval FALSE event is not exist
 */
#if defined( __HITACHI__ )
#pragma inline( knlevent_exists )
#endif
UANTS_INLINE BOOL knlevent_exists(
	T_EVENT * const ep )
{
	return knlobject_exists( &ep->e_super );
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts.h
 *
 * Internal definitions of MicroAnts.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_H__
#define __UANTS_H__

#include <kernel.h>
#include <bios.h>
#include <uAnts.cfg>

/* MACRO for inline & volatile keywords */
#if defined( __GNUC__ )

/* GNU C */
#define UANTS_INLINE static __inline__

#elif defined( __CC_ARM )

/* ARM RVCT */
#define UANTS_INLINE __inline

#elif defined( __HITACHI__ )

/* Renesas C/C++ */
#define UANTS_INLINE static

#endif

#endif

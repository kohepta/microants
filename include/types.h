﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file types.h
 *
 * generic type definitions of MicroAnts.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __TYPES_H__
#define __TYPES_H__

#if !defined LOCAL
#define LOCAL		static
#endif

#if !defined EXTERN
#define EXTERN		extern
#endif

#if !defined CONST
#define CONST		const
#endif

#if !defined _bool_
#define	_bool_		char
#endif

#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
#define _int8_		char
#define _int16_		int
#define _int32_		long
#elif defined( __GNUC__ )	\
	|| defined( __CC_ARM )	\
	|| ( defined( __HITACHI__ ) && ( __HITACHI_VERSION__ >= 0x0900 ) )
#define _int8_		char
#define _int16_		short
#define _int32_		int
#define _int64_		long long
#else
#define _int8_		char
#define _int16_		short
#define _int32_		int
#endif

typedef signed _int8_ B;
typedef unsigned _int8_ UB;
typedef _int8_ VB;

typedef signed _int16_ H;
typedef unsigned _int16_ UH;
typedef _int16_ VH;

typedef signed _int32_ W;
typedef unsigned _int32_ UW;
typedef _int32_ VW;

#if defined( _int64_ )
typedef signed _int64_ D;
typedef unsigned _int64_ UD;
typedef _int64_ VD;
#endif

typedef void * VP;
typedef void ( * FP )( void );

typedef signed int INT;
typedef unsigned int UINT;

typedef _bool_ BOOL;

typedef H FN;
typedef INT ER;
typedef INT ID;
typedef UINT ATR;
typedef UINT STAT;
typedef UB MODE;
typedef INT PRI;
#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
typedef UW SIZE;
#else
typedef UINT SIZE;
#endif
typedef INT TMO;
typedef UW RELTIM;

/*
 typedef struct t_systime SYSTIM;
*/

typedef VP VP_INT;

typedef ER ER_BOOL;
typedef ER ER_ID;
typedef ER ER_UINT;

typedef UH TEXPTN;
typedef UW FLGPTN;

/*
 typedef struct t_msg T_MSG;
 typedef struct t_msg_pri T_MSG_PRI;
*/

typedef UW RDVPTN;
typedef UW RDVNO;
typedef UW OVRTIM;

typedef INT INHNO;
typedef INT INTNO;
typedef INT EXCNO;
typedef INT IMASK;

struct t_joint
{
	struct t_joint * j_next;
	
	struct t_joint * j_prev;
};
typedef struct t_joint T_JOINT;

struct t_systim
{
	UW utime;
	
	UW ltime;
};
typedef struct t_systim SYSTIM;

struct t_msg
{
	T_JOINT g_joint;
	
	UINT g_magic;
};
typedef struct t_msg T_MSG;

struct t_msg_pri
{
	T_MSG msgque;
	
	PRI msgpri;
};
typedef struct t_msg_pri T_MSG_PRI;

struct t_ctsk
{
	ATR tskatr;
	
	VP_INT exinf;
	
	FP task;
	
	PRI itskpri;
	
	SIZE stksz;
	
	VP stk;
};
typedef struct t_ctsk T_CTSK;

struct t_rtsk
{
	STAT tskstat;
	
	PRI tskpri;
	
	PRI tskbpri;
	
	STAT tskwait;
	
	ID wobjid;
	
	UH reserved;
	
	TMO lefttmo;
	
	UINT actcnt;
	
	UINT wupcnt;
	
	UINT suscnt;
};
typedef struct t_rtsk T_RTSK;

struct t_rtst
{
	STAT tskstat;
	
	STAT tskwait;
};
typedef struct t_rtst T_RTST;

struct t_dtex
{
	ATR texatr;
	
	FP texrtn;
};
typedef struct t_dtex T_DTEX;

struct t_rtex
{
	STAT texstat;
	
	TEXPTN pndptn;
};
typedef struct t_rtex T_RTEX;

struct t_csem
{
	ATR sematr;
	
	UINT isemcnt;
	
	UINT maxsem;
};
typedef struct t_csem T_CSEM;

struct t_rsem
{
	ID wtskid;
	
	UINT semcnt;
};
typedef struct t_rsem T_RSEM;

struct t_cflg
{
	ATR flgatr;
	
	FLGPTN iflgptn;
};
typedef struct t_cflg T_CFLG;

struct t_rflg
{
	ID wtskid;
	
	FLGPTN flgptn;
};
typedef struct t_rflg T_RFLG;

struct t_cdtq
{
	ATR dtqatr;
	
	UINT dtqcnt;
	
	VP dtq;
};
typedef struct t_cdtq T_CDTQ;

struct t_rdtq
{
	ID stskid;
	
	ID rtskid;
	
	UINT sdtqcnt;
};
typedef struct t_rdtq T_RDTQ;

struct t_cmbx
{
	ATR mbxatr;
	
	PRI maxmpri;
	
	VP mprihd;
};
typedef struct t_cmbx T_CMBX;

struct t_rmbx
{
	ID wtskid;
	
	T_MSG * pk_msg;
};
typedef struct t_rmbx T_RMBX;

struct t_cmtx
{
	ATR mtxatr;
	
	PRI ceilpri;
};
typedef struct t_cmtx T_CMTX;

struct t_rmtx
{
	ID htskid;
	
	ID wtskid;
};
typedef struct t_rmtx T_RMTX;

struct t_cmbf
{
	ATR mbfatr;
	
	UINT maxmsz;
	
	UINT mbfsz;
	
	VP mbf;
};
typedef struct t_cmbf T_CMBF;

struct t_rmbf
{
	ID stskid;
	
	ID rtskid;
	
	UINT smsgcnt;
	
	SIZE fmbfsz;
};
typedef struct t_rmbf T_RMBF;

struct t_cpor
{
	ATR poratr;
	
	UINT maxcmsz;
	
	UINT maxrmsz;
};
typedef struct t_cpor T_CPOR;

struct t_rpor
{
	ID ctskid;
	
	ID atskid;
};
typedef struct t_rpor T_RPOR;

struct t_rrdv
{
	ID wtskid;
};
typedef struct t_rrdv T_RRDV;

struct t_cmpf
{
	ATR mpfatr;
	
	UINT blkcnt;
	
	UINT blksz;
	
	VP mpf;
};
typedef struct t_cmpf T_CMPF;

struct t_rmpf
{
	ID wtskid;
	
	UINT fblkcnt;
};
typedef struct t_rmpf T_RMPF;

struct t_cmpl
{
	ATR mplatr;
	
	SIZE mplsz;
	
	VP mpl;
};
typedef struct t_cmpl T_CMPL;

struct t_rmpl
{
	ID wtskid;
	
	SIZE fmplsz;
	
	UINT fblksz;
	
	UINT fblkcnt;
};
typedef struct t_rmpl T_RMPL;

struct t_ccyc
{
	ATR cycatr;
	
	VP_INT exinf;
	
	FP cychdr;
	
	RELTIM cyctim;
	
	RELTIM cycphs;
};
typedef struct t_ccyc T_CCYC;

struct t_rcyc
{
	STAT cycstat;
	
	RELTIM lefttim;
};
typedef struct t_rcyc T_RCYC;

struct t_calm
{
	ATR almatr;
	
	VP_INT exinf;
	
	FP almhdr;
};
typedef struct t_calm T_CALM;

struct t_ralm
{
	STAT almstat;
	
	RELTIM lefttim;
};
typedef struct t_ralm T_RALM;

struct t_dovr
{
	ATR ovratr;
	
	FP ovrhdr;
};
typedef struct t_dovr T_DOVR;

struct t_rovr
{
	STAT ovrstat;
	
	OVRTIM leftotm;
};
typedef struct t_rovr T_ROVR;

struct t_dinh
{
	ATR inhatr;
	
	FP inthdr;
};
typedef struct t_dinh T_DINH;

struct t_cisr
{
	ATR isratr;
	
	VP_INT exinf;
	
	INTNO intno;
	
	FP isr;
};
typedef struct t_cisr T_CISR;

struct t_dsvc
{
	ATR svcatr;
	
	FP svcrtn;
};
typedef struct t_dsvc T_DSVC;

struct t_dexc
{
	ATR excatr;
	
	FP exchdr;
};
typedef struct t_dexc T_DEXC;

struct t_rcfg
{
	PRI min_tpri;
	
	PRI max_tpri;
	
	PRI min_mpri;
	
	PRI max_mpri;
	
	UINT max_actcnt;
	
	UINT max_wupcnt;
	
	UINT max_suscnt;
	
	UINT bit_texptn;
	
	UINT bit_flgptn;
	
	#if 0
	UINT bit_rdvptn;
	#endif
	
	UW tic_nume;
	
	UW tic_deno;
	
	UINT max_maxsem;
	
	ID max_tskid;
	
	ID max_semid;
	
	ID max_flgid;
	
	ID max_dtqid;
	
	ID max_mbxid;
	
	#if 0
	ID max_mtxid;
	
	ID max_mbfid;
	
	ID max_porid;
	#endif
	
	ID max_mpfid;
	
	ID max_mplid;
	
	ID max_cycid;
	
	ID max_almid;
};
typedef struct t_rcfg T_RCFG;

struct t_rver
{
	UH maker;
	
	UH prid;
	
	UH spver;
	
	UH prver;
	
	UH prno[4];
};
typedef struct t_rver T_RVER;

struct t_rexc
{
	BOOL tskexc;
	
	BOOL excctx;
	
	BOOL locsta;
	
	BOOL dspsta;
	
	BOOL dpnsta;
};
typedef struct t_rexc T_REXC;

#endif

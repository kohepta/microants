﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_util.h
 *
 * Internal definitions of MicroAnts utility.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_UTIL_H__
#define __UANTS_UTIL_H__

#include "uAnts.h"

#if defined( __HITACHI__ )
#pragma inline( knlutil_align )
#endif
UANTS_INLINE UW knlutil_align(
	UW data )
{
	return (UW)TSZ_ALIGN( data );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_is_aligned )
#endif
UANTS_INLINE BOOL knlutil_is_aligned(
	UW data )
{
#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
	return ( data & ( sizeof( INT ) - 1 ) ) ? FALSE : TRUE;
#else
	return ( data & ( sizeof( VP ) - 1 ) ) ? FALSE : TRUE;
#endif
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_align_16 )
#endif
UANTS_INLINE UH knlutil_align_16(
	UH data )
{
	return (UH)TSZ_ALIGN_16( data );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_is_aligned_16 )
#endif
UANTS_INLINE BOOL knlutil_is_aligned_16(
	UH data )
{
	return ( data & ( sizeof( UH ) - 1 ) ) ? FALSE : TRUE;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_align_32 )
#endif
UANTS_INLINE UW knlutil_align_32(
	UW data )
{
	return (UW)TSZ_ALIGN_32( data );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_is_aligned_32 )
#endif
UANTS_INLINE BOOL knlutil_is_aligned_32(
	UW data )
{
	return ( data & ( sizeof( UW ) - 1 ) ) ? FALSE : TRUE;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_align_cpu )
#endif
UANTS_INLINE UW knlutil_align_cpu(
	UW data )
{
	return (UW)TSZ_ALIGN_CPU( data );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_is_aligned_cpu )
#endif
UANTS_INLINE BOOL knlutil_is_aligned_cpu(
	UW data )
{
	return ( data & ( sizeof( UW ) - 1 ) ) ? FALSE : TRUE;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_max )
#endif
UANTS_INLINE INT knlutil_max(
	INT a,
	INT b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_maxu )
#endif
UANTS_INLINE UINT knlutil_maxu(
	UINT a,
	UINT b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_max_8 )
#endif
UANTS_INLINE B knlutil_max_8(
	B a,
	B b )
{
	return (B)( ( a > b ) ? a : b );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_maxu_8 )
#endif
UANTS_INLINE UB knlutil_maxu_8(
	UB a,
	UB b )
{
	return (UB)( ( a > b ) ? a : b );
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_max_16 )
#endif
UANTS_INLINE H knlutil_max_16(
	H a,
	H b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_maxu_16 )
#endif
UANTS_INLINE UH knlutil_maxu_16(
	UH a,
	UH b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_max_32 )
#endif
UANTS_INLINE W knlutil_max_32(
	W a,
	W b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_maxu_32 )
#endif
UANTS_INLINE UW knlutil_maxu_32(
	UW a,
	UW b )
{
	return ( a > b ) ? a : b;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_gcd )
#endif
UANTS_INLINE W knlutil_gcd(
	W a,
	W b )
{
	W t;
	
	while ( b )
	{
		t = a % b;
		a = b;
		b = t;
	}
	
	return a;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_test_bit )
#endif
UANTS_INLINE BOOL knlutil_test_bit(
	UB a,
	UB b )
{
	return ( a & b ) ? TRUE : FALSE;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_test_bit_16 )
#endif
UANTS_INLINE BOOL knlutil_test_bit_16(
	UH a,
	UH b )
{
	return ( a & b ) ? TRUE : FALSE;
}

#if defined( __HITACHI__ )
#pragma inline( knlutil_test_bit_32 )
#endif
UANTS_INLINE BOOL knlutil_test_bit_32(
	UW a,
	UW b )
{
	return ( a & b ) ? TRUE : FALSE;
}

#endif

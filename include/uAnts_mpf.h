﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpf.h
 *
 * Internal definitions of MicroAnts fixed-sized memory pool.
 *
 * @date 2010.05.17 new creation
 */
 
#ifndef __UANTS_MPF_H__
#define __UANTS_MPF_H__

#include "uAnts_object.h"
#include "uAnts_wait.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/** magic number for fixed-sized memory pool's memory block. */
#define KNLMPF_MAGIC	(0x1A2B3C4D)

/**
 * class T_MPF_BLOCK.
 */
struct t_mpf_block
{
	/** joint. */
	T_JOINT b_joint;
	
	/** magic number. */
	UINT b_magic;
};
typedef struct t_mpf_block T_MPF_BLOCK;

/**
 * class T_MPF_WAIT.
 */
struct t_mpf_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** memory block. */
	VP * w_block;
};
typedef struct t_mpf_wait T_MPF_WAIT;

/**
 * class T_MPF.
 */
struct t_mpf
{
	/** super class. */
	T_OBJECT f_super;
	
	/** count of blocks */
	UINT f_block_count;
	
	/** size per block (bytes). */
	UINT f_block_size;
	
	/** origin of buffer. */
	VP f_origin;
	
	/** queue of free block. */
	T_QUEUE f_block_queue;
	
	/** queue of tasks waiting to get. */
	T_QUEUE f_task_queue;
};
typedef struct t_mpf T_MPF;

/** queue of fixed-sized memory pools that can be used. */
extern T_QUEUE knlmpf_free_queue;

/** entity array of fixed-sized memory pools. */
extern T_MPF knlmpf_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_mpf.c */
void knlmpf_init_entities( void );

/* uAnts_mpf.c */
void knlmpf_initialize(
	T_MPF * const mpf,
	const T_CMPF * pk_cmpf,
	VP vp );

/* uAnts_mpf.c */
void knlmpf_finalize(
	T_MPF * const mpf );

/* uAnts_mpf.c */
ER knlmpf_create(
	T_MPF * const mpf,
	const T_CMPF * pk_cmpf );

/* uAnts_mpf.c */
ER knlmpf_get(
    ID mpfid,
    VP * p_blk,
	T_EVENT * const event,
	TMO timeout );

/* uAnts_mpf.c */
ER knlmpf_release(
	ID mpfid,
	VP blk );

/**
 * getting fixed-sized memory pool entity.
 * 
 * @param id fixed-sized memory pool ID
 * @return fixed-sized memory pool entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpf_get_entity )
#endif
UANTS_INLINE T_MPF * knlmpf_get_entity(
	ID mpfid )
{
	T_MPF * mpf;
	
	if ( mpfid <= 0 || mpfid > UANTS_CFG_MPF_MAX_ID )
	{
		mpf = NULL;
	}
	else
	{
		mpf = &knlmpf_entities[mpfid - 1];
	}
	
	return mpf;
}

/**
 * adding a free memory block to fixed-sized memory pool.
 * 
 * @param mpf fixed-sized memory pool entity
 * @param block free memory block
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpf_add_block )
#endif
UANTS_INLINE void knlmpf_add_block(
	T_MPF * const mpf,
	T_MPF_BLOCK * block )
{
	knlqueue_add( &mpf->f_block_queue, &block->b_joint );
	block->b_magic = (UINT)KNLMPF_MAGIC;
}

/**
 * delete a free memory block from fixed-sized memory pool.
 * 
 * @param mpf fixed-sized memory pool entity
 * @param block free memory block
 */
#if defined( __HITACHI__ )
#pragma inline( knlmpf_delete_block )
#endif
UANTS_INLINE void knlmpf_delete_block(
	T_MPF * const mpf,
	T_MPF_BLOCK * block )
{
	knlqueue_delete( &mpf->f_block_queue, &block->b_joint );
	block->b_magic = 0;
}

#endif

#endif

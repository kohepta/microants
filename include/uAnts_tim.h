﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tim.h
 *
 * Internal definitions of MicroAnts system time management.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_TIM_H__
#define __UANTS_TIM_H__

#include "uAnts_queue.h"
#include "uAnts_event.h"

/**
 * class T_TIM.
 */
struct t_tim
{
	/** system time. */
	SYSTIM t_system;
	
	/** elapsed tick. */
	RELTIM t_elapsed;
	
	#if ( UANTS_CFG_TIC_DENO > 1 )
	/** elapsed tick adjustment. */
	RELTIM t_adjust;
	#endif
	
	/** queue of time-event. */
	T_QUEUE t_event_queue;
	
	/** timer interrupt nested depth. when signal is issued. */
	UB t_nested_depth;
};
typedef struct t_tim T_TIM;

/** system time control entity. */
extern T_TIM knltim;

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_tim.c */
void knltim_initialize( void );

/* uAnts_tim.c */
void knltim_add_event(
	T_EVENT * const event,
	RELTIM timeout );

/* uAnts_tim.c */
void knltim_delete_event(
	T_EVENT * const event );

/* uAnts_tim.c */
RELTIM knltim_get_remainder_until_timeout(
	const T_EVENT * event );

/* uAnts_tim.c */
void knltim_signal( void );

/* uAnts_tim.c */
void knltim_start( void );

/* uAnts_tim.c */
void knltim_stop( void );

/* uAnts_tim.c */
void knltim_add_event_with_callback(
	T_EVENT * const event,
	RELTIM timeout,
	void ( * const callback )( T_EVENT * const event ) );

/**
 * getting system time.
 * 
 * @param[out] p_systim output system time pointer
 */
#if defined( __HITACHI__ )
#pragma inline( knltim_get )
#endif
UANTS_INLINE void knltim_get(
	SYSTIM * p_systim )
{
	p_systim->ltime = knltim.t_system.ltime;
	p_systim->utime = knltim.t_system.utime;
}

/**
 * setting system time.
 * 
 * @param[in] p_systim input system time pointer
 */
#if defined( __HITACHI__ )
#pragma inline( knltim_set )
#endif
UANTS_INLINE void knltim_set(
	SYSTIM * p_systim )
{
	knltim.t_system.ltime = p_systim->ltime;
	knltim.t_system.utime = p_systim->utime;
}

#endif

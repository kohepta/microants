﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tsk.h
 *
 * Internal definitions of MicroAnts task.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_TSK_H__
#define __UANTS_TSK_H__

#include "uAnts_object.h"
#include "uAnts_wait.h"
#include "uAnts_tex.h"

/** static root task entity ID. */
#define KNLTSK_STATIC_ROOT_ENTITY_ID			( UANTS_CFG_TSK_MAX_ID + 1 )

#define KNLTSK_MAX_ID							( KNLTSK_STATIC_ROOT_ENTITY_ID )

/**
 * class T_TSK.
 */
struct t_tsk
{
	/** super class. */
	T_OBJECT t_super;
	
	/** extended information. */
	VP_INT t_exinf;
	
	/** entry point. */
	void ( * t_entry )( VP_INT );
	
	/** initial priority. */
	PRI t_initial_priority;
	
	/** base priority. */
	PRI t_base_priority;
	
	/** current priority. */
	PRI t_priority;
	
	/**
	 * task state.
	 * 
	 *   TTS_RUN
	 *   TTS_RDY
	 *   TTS_WAI
	 *   TTS_SUS
	 *   TTS_WAS
	 *   TTS_DMT
	 */
	STAT t_state;
	
	/** size of task stack. */
	UW t_stack_size;
	
	/** top of task stack. */
	VP t_stack_origin;
	
	/** current stack pointer. */
	VP t_stack_pointer;
	
	/** count of activate request. */
	UB t_activate_count;
	
	/** count of wake up request. */ 
	UB t_wakeup_count;
	
	/** count of suspend request. */
	UB t_suspend_count;
	
	/** reference to wait-controller entity. */
	T_WAIT * t_wait;
	
	/** task exception. */
	T_TEX t_tex;
};
typedef struct t_tsk T_TSK;

/** pointer to current running task. */
extern T_TSK * knltsk_current;

/** queue of tasks that can be used. */
extern T_QUEUE knltsk_free_queue;

/** queue of ready tasks. */
extern T_QUEUE knltsk_ready_queue;

/** entity array of tasks. */
extern T_TSK knltsk_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_tsk_init_entities.c */
void knltsk_init_entities( void );

/* uAnts_tsk_initialize.c */
void knltsk_initialize(
	T_TSK * const tsk,
	const T_CTSK * pk_ctsk,
	VP stk );

/* uAnts_tsk_finalize.c */
void knltsk_finalize(
	T_TSK * const tsk );

/* uAnts_tsk_create.c */
ER knltsk_create(
	T_TSK * const tsk,
	const T_CTSK * pk_ctsk );

/* uAnts_tsk_activate.c */
ER knltsk_activate(
	ID tskid );

/* uAnts_tsk_dormant.c */
BOOL knltsk_dormant(
	T_TSK * const tsk,
	BOOL force );

/* uAnts_tsk_get_current_id.c */
ID knltsk_get_current_id( void );

/* uAnts_tsk_get_next_one_with_cpu_locking.c */
T_JOINT * knltsk_get_next_one_with_cpu_locking(
	T_TSK * const tsk,
	T_QUEUE * const queue,
	UINT * lock );

/* uAnts_tsk_notify_object_deletion.c */
void knltsk_notify_object_deletion(
	T_QUEUE * queue );

/* uAnts_tsk_quit_waiting.c */
BOOL knltsk_quit_waiting(
	T_TSK * const tsk,
	ER ercd );

/* uAnts_tsk_ready.c */
void knltsk_ready(
	T_TSK * const tsk );

/* uAnts_tsk_release_wait.c */
ER knltsk_release_wait(
	ID tskid );

/* uAnts_tsk_resume.c */
ER knltsk_resume(
	ID tskid,
	BOOL force );

/* uAnts_tsk_rotate_ready_queue.c */
ER knltsk_rotate_ready_queue(
	PRI tskpri );

/* uAnts_tsk_run.c */
void knltsk_run( void );

/* uAnts_tsk_set_priority.c */
void knltsk_set_priority(
	T_TSK * const tsk,
	PRI tskpri );

/* uAnts_tsk_sleep.c */
ER knltsk_sleep(
	TMO timeout );

/* uAnts_tsk_static_root_entry.c */
void knltsk_static_root_entry(
	VP_INT exinf );

/* uAnts_tsk_suspend.c */
ER knltsk_suspend(
	ID tskid );

/* uAnts_tsk_wait.c */
void knltsk_wait(
	T_WAIT * const wait, 
	TMO timeout );

/* uAnts_tsk_wakeup.c */
ER knltsk_wakeup(
	ID tskid );

/**
 * getting task entity.
 * 
 * @return task entity
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_get_entity )
#endif
UANTS_INLINE T_TSK * knltsk_get_entity(
	ID tskid )
{
	T_TSK * tsk;
	
	if ( tskid <= 0 || tskid > KNLTSK_MAX_ID )
	{
		tsk = NULL;
	}
	else
	{
		tsk = &knltsk_entities[tskid - 1];
	}
	
	return tsk;
}

/**
 * getting task entity (contain current running task).
 * 
 * @param id task ID
 * @return task entity
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_get_entity_self )
#endif
UANTS_INLINE T_TSK * knltsk_get_entity_self(
	ID tskid )
{
	T_TSK * tsk;
	
	if ( tskid == TSK_SELF )
	{
		tsk = knltsk_current;
	}
	else
	{
		tsk = knltsk_get_entity( tskid );
	}
	
	return tsk;
}

/**
 * getting task current priority.
 * 
 * @param tsk task entity
 * @return task current priority
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_get_priority )
#endif
UANTS_INLINE PRI knltsk_get_priority(
	T_TSK * const tsk )
{
	return tsk->t_priority;
}

/**
 * clearing task state.
 * 
 * @param tsk task entity
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_clear_state )
#endif
UANTS_INLINE void knltsk_clear_state(
	T_TSK * const tsk,
	STAT tskstat )
{
	tsk->t_state &= ~tskstat;
}

/**
 * adding state to a task.
 * 
 * @param tsk task entity
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_or_state )
#endif
UANTS_INLINE void knltsk_or_state(
	T_TSK * const tsk,
	STAT tskstat )
{
	tsk->t_state |= tskstat;
}

/**
 * testing task state.
 * 
 * @param tsk task entity
 * @param tskstat state to be tested
 * @retval TRUE state is valid
 * @retval FALSE state is invalid
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_test_state )
#endif
UANTS_INLINE BOOL knltsk_test_state(
	T_TSK * const tsk,
	STAT tskstat )
{
	return ( tsk->t_state & tskstat ) ? TRUE : FALSE;
}

/**
 * getting task state.
 * 
 * @param tsk task entity
 * @return task state
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_get_state )
#endif
UANTS_INLINE STAT knltsk_get_state(
	T_TSK * const tsk )
{
	return tsk->t_state;
}

/**
 * setting task state.
 * 
 * @param tsk task entity
 * @param tskstat new task state
 */
#if defined( __HITACHI__ )
#pragma inline( knltsk_set_state )
#endif
UANTS_INLINE void knltsk_set_state(
	T_TSK * const tsk,
	STAT tskstat )
{
	tsk->t_state = tskstat;
}

#endif

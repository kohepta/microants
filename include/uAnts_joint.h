﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_joint.h
 *
 * Internal definitions of MicroAnts joint.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_JOINT_H__
#define __UANTS_JOINT_H__

#include "uAnts.h"

/**
 * initialize joint.
 * 
 * @param jp joint
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_initialize )
#endif
UANTS_INLINE void knljoint_initialize(
	T_JOINT * const jp )
{
	jp->j_next = jp;
	jp->j_prev = jp;
}

/**
 * connect joint with next one.
 * 
 * @param jp joint
 * @param np next one
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_connect )
#endif
UANTS_INLINE void knljoint_connect(
	T_JOINT * const jp,
	T_JOINT * const np )
{
	jp->j_next = np;
	jp->j_prev = np->j_prev;
	np->j_prev->j_next = jp;
	np->j_prev = jp;
}

/**
 * re-connect joint with next one.
 * 
 * @param jp joint
 * @param np next one
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_reconnect )
#endif
UANTS_INLINE void knljoint_reconnect(
	T_JOINT * const jp,
	T_JOINT * const np )
{
	jp->j_prev->j_next = jp->j_next;
	jp->j_next->j_prev = jp->j_prev;
	
	knljoint_connect( jp, np );
}

/**
 * disconnect joint.
 * 
 * @param jp joint
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_disconnect )
#endif
UANTS_INLINE void knljoint_disconnect(
	T_JOINT * const jp )
{
	jp->j_prev->j_next = jp->j_next;
	jp->j_next->j_prev = jp->j_prev;
	jp->j_next = jp;
	jp->j_prev = jp;
}

/**
 * is joint connected?
 * 
 * @param jp joint
 * @retval TRUE joint is conncted
 * @retval FALSE joint is not connected
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_is_connected )
#endif
UANTS_INLINE BOOL knljoint_is_connected(
	const T_JOINT * const jp )
{
	return ( jp->j_next != jp ) ? TRUE : FALSE;
}

/**
 * getting joint next one.
 * 
 * @param jp joint
 * @return next one
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_next )
#endif
UANTS_INLINE T_JOINT * knljoint_next(
	const T_JOINT * const jp )
{
	return jp->j_next;
}

/**
 * getting joint prev one.
 * 
 * @param jp joint
 * @return prev one
 */
#if defined( __HITACHI__ )
#pragma inline( knljoint_prev )
#endif
UANTS_INLINE T_JOINT * knljoint_prev(
	const T_JOINT * const jp )
{
	return jp->j_prev;
}

#endif

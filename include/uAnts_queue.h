﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_queue.h
 *
 * Internal definitions of MicroAnts queue.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_QUEUE_H__
#define __UANTS_QUEUE_H__

#include "uAnts_joint.h"
#include "uAnts_sys.h"

/*
 kind of queuing order.
*/

/** priority queue. */
#define KNLQUEUE_ORDER_PRIORITY		(TRUE)

/** FIFO queue. */
#define KNLQUEUE_ORDER_FIFO			(FALSE)

/**
 * class T_QUEUE.
 */
struct t_queue
{
	/** joint. */
	T_JOINT q_joint;
	
	/** order of queue. */
	BOOL q_order;
	
	/** count of joints. */
	UH q_count;
	
	/** context nested depth. */
	UB q_nested_depth;
};
typedef struct t_queue T_QUEUE;

/**
 * initialize queue.
 * 
 * @param qp queue
 * @param order queuing order
 *   TRUE: priority, FALSE: FIFO
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_initialize )
#endif
UANTS_INLINE void knlqueue_initialize(
	T_QUEUE * const qp,
	BOOL order )
{
	knljoint_initialize( &qp->q_joint );
	qp->q_order = order;
	qp->q_count = 0;
	qp->q_nested_depth = 0;
}

/**
 * getting nested depth of last accessed context.
 * 
 * @param qp queue
 * @return nested_depth nested depth of last accessed context
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_get_nested_depth )
#endif
UANTS_INLINE UB knlqueue_get_nested_depth(
	T_QUEUE * const qp )
{
	return qp->q_nested_depth;
}

/**
 * setting nested depth of last accessed context.
 * 
 * @param qp queue
 * @param nested_depth nested depth of last accessed context
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_set_nested_depth )
#endif
UANTS_INLINE void knlqueue_set_nested_depth(
	T_QUEUE * const qp,
	UB nested_depth )
{
	qp->q_nested_depth = nested_depth;
}

/**
 * getting order of queue.
 * 
 * @param qp queue
 * @retval TRUE priority order
 * @retval FALSE FIFO order
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_get_order )
#endif
UANTS_INLINE BOOL knlqueue_get_order(
	T_QUEUE * const qp )
{
	return qp->q_order;
}

/**
 * setting order of queue.
 * 
 * @param qp queue
 * @param order queuing order
 *   TRUE: priority, FALSE: FIFO
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_set_order )
#endif
UANTS_INLINE void knlqueue_set_order(
	T_QUEUE * const qp,
	BOOL order )
{
	qp->q_order = order;
}

/**
 * getting count of joints in queue.
 * 
 * @param qp queue
 * @return count of joints in queue
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_get_count )
#endif
UANTS_INLINE UH knlqueue_get_count(
	T_QUEUE * const qp )
{
	return qp->q_count;
}

/**
 * insert joint before next one.
 * 
 * @param qp queue
 * @param jp joint (insertion object)
 * @param np next one
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_insert )
#endif
UANTS_INLINE void knlqueue_insert(
	T_QUEUE * const qp,
	T_JOINT * const jp,
	T_JOINT * const np )
{
	qp->q_nested_depth = knlsys_get_nested_depth();
	knljoint_connect( jp, np );
	qp->q_count ++;
}

/**
 * adding joint to queue.
 * 
 * @param qp queue
 * @param jp joint (addition object)
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_add )
#endif
UANTS_INLINE void knlqueue_add(
	T_QUEUE * const qp,
	T_JOINT * const jp )
{
	knlqueue_insert( qp, jp, &qp->q_joint );
}

/**
 * delete joint from queue.
 * 
 * @param qp queue
 * @param jp joint (deletion object)
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_delete )
#endif
UANTS_INLINE void knlqueue_delete(
	T_QUEUE * const qp,
	T_JOINT * const jp )
{
	qp->q_nested_depth = knlsys_get_nested_depth();
	knljoint_disconnect( jp );
	qp->q_count --;
}

/**
 * getting first joint in queue.
 * 
 * @param qp queue
 * @return first joint
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_head )
#endif
UANTS_INLINE T_JOINT * knlqueue_head(
	const T_QUEUE * const qp )
{
	return knljoint_next( &qp->q_joint );
}

/**
 * getting last joint in queue.
 * 
 * @param qp queue
 * @return last joint
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_tail )
#endif
UANTS_INLINE T_JOINT * knlqueue_tail(
	const T_QUEUE * const qp )
{
	return knljoint_prev( &qp->q_joint );
}

/**
 * are queue and joint equivalent?
 * 
 * @param qp queue
 * @param jp joint
 * @retval TRUE queue and joint are equivalent
 * @retval FALSE queue and joint are different
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_equals )
#endif
UANTS_INLINE BOOL knlqueue_equals(
	const T_QUEUE * const qp,
	const T_JOINT * const jp )
{
	return ( &qp->q_joint == jp ) ? TRUE : FALSE;
}

/**
 * is queue empty?
 * 
 * @param qp queue
 * @retval TRUE queue is empty
 * @retval FALSE queue is not empty
 */
#if defined( __HITACHI__ )
#pragma inline( knlqueue_is_empty )
#endif
UANTS_INLINE BOOL knlqueue_is_empty(
	T_QUEUE * const qp )
{
	return ( qp->q_count == 0 ) ? TRUE : FALSE;
}

#endif

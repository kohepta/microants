﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_inh.h
 *
 * Internal definitions of MicroAnts interrupt handler.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_INH_H__
#define __UANTS_INH_H__

#include "uAnts_object.h"

/**
 * class T_INH.
 */
struct t_inh
{
	/** attribute. */
	ATR i_attribute;
	
	/** entry point. */
	void ( * i_entry )( INHNO inhno, VP_INT exinf );
	
	/** extended information. */
	VP_INT i_exinf;
};
typedef struct t_inh T_INH;

/** entity array of interrupt handlers. */
extern T_INH knlinh_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_inh.c */
void knlinh_init_entities( void );

/* uAnts_inh.c */
void knlinh_define(
	INHNO inhno,
	const T_DINH * pk_dinh,
	VP_INT exinf );

/* uAnts_inh.c */
void knlinh_detect_and_invoke( void );

/* uAnts_inh.c */
void knlinh_invoke(
	INHNO inhno );

/* uAnts_inh.c */
void knlinh_undefined(
	INHNO inhno,
	VP_INT exinf );

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sys.h
 *
 * Internal definitions of MicroAnts system.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_SYS_H__
#define __UANTS_SYS_H__

#include "uAnts_cpu.h"
#include "uAnts_util.h"

/**
 * class T_SYS.
 */
struct t_sys
{
	/**
	 * system has been started?
	 *   TRUE: system has been started.
	 *   FALSE: system has not been started.
	 */
	BOOL s_initialized;
	
	/**
	 * can dispatch be done?
	 *   TRUE: dispatch can be done.
	 *   FALSE: dispatch cannot be done.
	 */
	BOOL s_dispatchable;
	
	/** interrupt/exception nested depth. */
	UB s_nested_depth;
};
typedef struct t_sys T_SYS;

/**
 * class T_SYS_STACK.
 */
struct t_sys_stack
{
	/** size of system context stack. */
	const UINT s_size;
	
	/** top of system context stack. */
	const VP s_origin;
};
typedef struct t_sys_stack T_SYS_STACK;

/** system control entity. */
extern T_SYS knlsys;

/** system context stack entity. */
extern const T_SYS_STACK knlsys_stack;

/** array of pointer to knlxxx_init_entities() for all objects. */
extern void ( * const knlsys_init_entities[] )( void );

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_sys.c */
BOOL knlsys_sense_dispatch_pending( void );

/* uAnts_sys.c */
void knlsys_shutdown( void );

/**
 * sense the current context.
 * 
 * @retval TRUE currnet context is no-task context
 * @retval FALSE currnet context is task context
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_sense_context )
#endif
UANTS_INLINE BOOL knlsys_sense_context( void )
{
	return ( knlsys.s_nested_depth > 0 ) ? TRUE : FALSE;
}

/**
 * sense the dispatchable.
 * 
 * @retval TRUE dispatch can be done
 * @retval FALSE dispatch cannot be done
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_sense_dispatchable )
#endif
UANTS_INLINE BOOL knlsys_sense_dispatchable( void )
{
	return knlsys.s_dispatchable;
}

/**
 * getting interrupt/exception nested depth.
 * 
 * @return interrupt/exception nested depth
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_get_nested_depth )
#endif
UANTS_INLINE UB knlsys_get_nested_depth( void )
{
	return knlsys.s_nested_depth;
}

/**
 * initialize interrupt/exception nested depth.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_initialize_nested_depth )
#endif
UANTS_INLINE void knlsys_initialize_nested_depth( void )
{
	knlsys.s_nested_depth = 0;
}

/**
 * increment interrupt/exception nested depth.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_increment_nested_depth )
#endif
UANTS_INLINE void knlsys_increment_nested_depth( void )
{
	knlsys.s_nested_depth ++;
}

/**
 * decrement interrupt/exception nested depth.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_decrement_nested_depth )
#endif
UANTS_INLINE void knlsys_decrement_nested_depth( void )
{
	knlsys.s_nested_depth --;
}

/**
 * system initialize.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_initialize )
#endif
UANTS_INLINE void knlsys_initialize( void )
{
	knlsys.s_initialized = TRUE;
}

/**
 * disable dispatching.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_disable_dispatch )
#endif
UANTS_INLINE void knlsys_disable_dispatch( void )
{
	knlsys.s_dispatchable = FALSE;
}

/**
 * enable dispatching.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_enable_dispatch )
#endif
UANTS_INLINE void knlsys_enable_dispatch( void )
{
	knlsys.s_dispatchable = TRUE;
}

/**
 * do dispatch with task context.
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_dispatch )
#endif
UANTS_INLINE void knlsys_dispatch( void )
{
	bios_cpu_disable_interrupts();
	
	knlcpu_dispatch();
	
	bios_cpu_enable_interrupts();
}

/**
 * locking dispatch.
 * 
 * @return state of dispatchable before locking dispatch
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_lock_dispatch )
#endif
UANTS_INLINE BOOL knlsys_lock_dispatch( void )
{
	BOOL disp;
	
	disp = knlsys.s_dispatchable;
	
	if ( !knlsys_sense_context() )
	{
		knlsys.s_dispatchable = FALSE;
	}
	
	return disp;
}

/**
 * locking dispatch.
 * trying dispatch, if dispatch can be done.
 *
 * @param disp state of dispatch before locking dispatch
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_unlock_dispatch )
#endif
UANTS_INLINE void knlsys_unlock_dispatch(
	BOOL disp )
{
	if ( !knlsys_sense_context() )
	{
		knlsys.s_dispatchable = disp;
		
		if ( !bios_cpu_sense_locked() )
		{
			knlsys_dispatch();
		}
	}
}

/**
 * setting state of dispatch.
 * 
 * @param disp state of dispatch
 */
#if defined( __HITACHI__ )
#pragma inline( knlsys_set_dispatchable )
#endif
UANTS_INLINE void knlsys_set_dispatchable(
	BOOL disp )
{
	knlsys.s_dispatchable = disp;
}

#endif

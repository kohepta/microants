﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file constants.h
 *
 * generic constant (C pre-processor macro) definitions of MicroAnts.
 *
 * @date 2010.05.13 new creation
 */

#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

/*
 generic
*/

#if !defined NULL
#define NULL				((void *)0)
#endif

#if !defined TRUE
#define TRUE				1
#endif

#if !defined FALSE
#define FALSE				0
#endif

/*
 attributes
*/

/** no attribute. */
#define TA_NULL				0x00U
/** high language interface. */
#define TA_HLNG				0x00U
/** assembly language interface. */
#define TA_ASM				0x01U
/** enqueue waiting tasks in FIFO order. */
#define TA_TFIFO			0x00U
/** enqueue waiting tasks in priority order. */
#define TA_TPRI				0x01U
/** enqueue messages in FIFO order (MBX only). */
#define TA_MFIFO			0x00U
/** enqueue messages in priority order (MBX only). */
#define TA_MPRI				0x02U
/** activate a task on cre_tsk() / acre_tsk() (TSK only). */
#define TA_ACT	 			0x02U
/** restricted task (TSK only.) */
#define TA_RSTR				0x04U
/** only the first task can wait for an eventflag (FLG only). */
#define TA_WSGL				0x00U
/** multiple tasks can wait for an eventflag (FLG only). */
#define TA_WMUL				0x02U
/** clear a task wait-pattern, when the is released (FLG only). */
#define TA_CLR	 			0x04U
/** priority inheritence protocol (MTX only). */
#define TA_INHERIT 			0x02U
/** priority ceiling protocol (MTX only). */
#define TA_CEILING 			0x03U
/** start a cyclic handler on cre_cyc() / acre_cyc() (CYC only). */
#define TA_STA	 			0x02U
/** preserve cycle phase (CYC only). */
#define TA_PHS	 			0x04U

/*
 timeout
*/

/** polling. */
#define TMO_POL 			0
/** infinite-wait. */
#define TMO_FEVR			(-1)
/** non blocking. */
#define TMO_NBLK			(-2)

/*
 wait-modes for eventflag
*/

/** AND-wait. */
#define TWF_ANDW			0x0U
/** OR-wait. */
#define TWF_ORW				0x1U

/*
 task states
*/

/** running. */
#define TTS_RUN				0x01U
/** ready. */
#define TTS_RDY				0x02U
/** waiting. */
#define TTS_WAI				0x04U
/** suspended. */
#define TTS_SUS				0x08U
/** waiting & suspended. */
#define TTS_WAS				0x0cU
/** dormant. */
#define TTS_DMT				0x10U

/*
 waited-factors
*/

/** sleeping. */
#define TTW_SLP				0x0001U
/** delaying. */
#define TTW_DLY				0x0002U
/** waiting on a semaphore. */
#define TTW_SEM				0x0004U
/** waiting for an eventflag. */
#define TTW_FLG				0x0008U
/** waiting to send a data element on a data-queue. */
#define TTW_SDTQ			0x0010U
/** waiting to receive a data element on data-queue. */
#define TTW_RDTQ			0x0020U
/** waiting to receive a massage on a mailbox. */
#define TTW_MBX				0x0040U
/** waiting to lock a mutex. */
#define TTW_MTX				0x0080U
/** waiting to send a massage on a message buffer. */
#define TTW_SMBF			0x0100U
/** waiting to receive a message on a message buffer. */
#define TTW_RMBF			0x0200U
/** wating to call rendezvous with the other task on a redezvous port. */
#define TTW_CAL				0x0400U
/** waiting to accept rendezvous with the other task on a rendezvous port. */
#define TTW_ACP				0x0800U
/** waiting for rendezvous reply. */
#define TTW_RDV				0x1000U
/** waiting to get a block from fixed-sizeed memory pool. */
#define TTW_MPF				0x2000U
/** waiting to get a block from variable-sized memory pool. */
#define TTW_MPL				0x4000U

/*
 task exception handler states
*/

/** enabled. */
#define TTEX_ENA			0x0U
/** disabled. */
#define TTEX_DIS			0x1U

/*
 cyclic handler states
*/

/** stoped. */
#define TCYC_STP			0x0U
/** started. */
#define TCYC_STA			0x1U

/*
 alarm handler states
*/

/** stopped. */
#define TALM_STP			0x0U
/** started. */
#define TALM_STA			0x1U

/*
 overrun handler states
*/

/** stopped. */
#define TOVR_STP			0x0U
/** started. */
#define TOVR_STA			0x1U


/** specify the current task ID. */
#define TSK_SELF			0

/** invalid task ID. */
#define TSK_NONE			0

/** specify the current task priority. */
#define TPRI_SELF			0

/** specify initial priority of a task. */
#define TPRI_INI			0

/** minimum task priority. */
#define TMIN_TPRI			1

/** maximum task priority. */
#define TMAX_TPRI			63

/** minimum message priority. */
#define TMIN_MPRI			1

/** maximum message priority. */
#define TMAX_MPRI			63

#define TKERNEL_MAKER		0x0000U

/** kernel product identification number. */
#define TKERNEL_PRID		0x0000U

/** kernel specification version number (uITRON ver 4.03). */
#define TKERNEL_SPVER		0x5403

/** kernel product version number. */
#define TKERNEL_PRVER		0x0000U

/** maximum count of activation requests for a task. */
#define TMAX_ACTCNT			0xffU

/** maximum count of wake up requests for a task. */
#define TMAX_WUPCNT			0xffU

/** maximum count of suspension requests for a task. */
#define TMAX_SUSCNT			0xffU

/** bit numbers of TEX. */
#define TBIT_TEXPTN			0x10U

/** bit numbers of eventflag. */
#define TBIT_FLGPTN			0x20U

/** bit numbers of rendezvous condition. */
#define TBIT_RDVPTN			0x20U

/** maximum signal count. */
#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
#define TMAX_MAXSEM			0xffffU
#else
#define TMAX_MAXSEM			0xffffffffU
#endif

/**
 * macros to compute required memory size.
 */

/** CPU register size alignment. */
#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
#define TSZ_ALIGN( sz )	\
( ( ( sz + sizeof( INT ) ) - 1U ) & ~( sizeof( INT ) - 1U ) )
#else
#define TSZ_ALIGN( sz )	\
( ( ( sz + sizeof( VP ) ) - 1U ) & ~( sizeof( VP ) - 1U ) )
#endif

/** 16-bit alignment. */
#define TSZ_ALIGN_16( sz )	\
( ( ( sz + sizeof( UH ) ) - 1U ) & ~( sizeof( UH ) - 1U ) )

/** 32-bit alignment. */
#define TSZ_ALIGN_32( sz )	\
( ( ( sz + sizeof( UW) ) - 1U ) & ~( sizeof( UW ) - 1U ) )

#if defined( UD )
/** 64-bit alignment. */
#define TSZ_ALIGN_64( sz )	\
( ( ( sz + sizeof( UD ) ) - 1U ) & ~( sizeof( UD ) - 1U ) )
#endif

/** 32-bit cpu alignment. */
#define TSZ_ALIGN_CPU( sz )	\
( ( ( sz + sizeof( UW ) ) - 1U ) & ~( sizeof( UW ) - 1U ) )

/** preferred buffer size for a data-queue. */
#define TSZ_DTQ( dtqcnt )	\
( dtqcnt * sizeof( VP ) )

/**
 * preferred buffer size for a message header for each priority
 * (not supported).
 */
#define TSZ_MPRIHD( maxmpri )	\
( maxmpri * sizeof( T_JOINT ) )

/** preferred buffer size for a message buffer. */
#define TSZ_MBF( msgcnt, msgsz )	\
( msgcnt * ( (UINT)TSZ_ALIGN( msgsz ) + sizeof( UINT ) ) )

/** preferred buffer size for a fixed-sized memory pool. */
/*
  TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UINT ) ) = sizeof( T_MPF_BLOCK )
*/
#define TSZ_MPF( blkcnt, blksz )	\
(	\
	(	\
		blkcnt * \
			(	\
				( ( TSZ_ALIGN_CPU( blksz )	\
					> TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UINT ) ) )	\
				? TSZ_ALIGN_CPU( blksz )	\
				: TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UINT ) ) )	\
					+ TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UINT ) )	\
			)	\
	)	\
)

/** preferred buffer size for a variable-sized memory pool. */
/*
  TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UW ) ) = sizeof( T_MPL_BLOCK )
  ( sizeof( UW ) + sizeof( UW ) ) = sizeof( T_MPL_BLOCK_ALLOCATED )
*/
#define TSZ_MPL( blkcnt, blksz )	\
(	\
	(	\
		blkcnt * \
			(	\
				( ( TSZ_ALIGN_CPU( blksz )	\
					> TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UW ) ) )	\
				? TSZ_ALIGN_CPU( blksz )	\
				: TSZ_ALIGN_CPU( sizeof( T_JOINT ) + sizeof( UW ) ) )	\
					+ ( sizeof( UW ) + sizeof( UW ) )	\
			)	\
	)	\
)

#endif

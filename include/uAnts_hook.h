﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_hook.h
 *
 * Internal definitions of MicroAnts hook routines.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_HOOK_H__
#define __UANTS_HOOK_H__

#include "uAnts.h"

/*---------------------------------------------
 * user-configurable kernel hook function prototypes
 *-------------------------------------------*/

/* uAnts_hook.c */
void cfghook_pre_system_initialize( void );

/* uAnts_hook.c */
void cfghook_post_system_initialize( void );

/* uAnts_hook.c */
void cfghook_pre_system_timer_start( void );

/* uAnts_hook.c */
void cfghook_post_system_timer_start( void );

/* uAnts_hook.c */
void cfghook_pre_system_timer_interrupt_process( void );

/* uAnts_hook.c */
void cfghook_post_system_timer_interrupt_process( void );

/* uAnts_hook.c */
void cfghook_undefined_interrupt(
    INHNO inhno );

/* uAnts_hook.c */
void cfghook_undefined_exception(
    EXCNO excno );

/* uAnts_hook.c */
void cfghook_shutdown( void );

#endif

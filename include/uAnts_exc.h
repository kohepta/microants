﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_exc.h
 *
 * Internal definitions of MicroAnts cpu exception handler.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_EXC_H__
#define __UANTS_EXC_H__

#include "uAnts_object.h"

#if ( UANTS_CFG_EXC_MAX_ID > 0 )

/**
 * class T_EXC.
 */
struct t_exc
{
	/** attribute. */
	ATR e_attribute;
	
	/** entry point. */
	void ( * e_entry )( EXCNO excno );
};
typedef struct t_exc T_EXC;

/** entity array of cpu exception handlers. */
extern T_EXC knlexc_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_exc.c */
void knlexc_init_entities( void );

/* uAnts_exc.c */
void knlexc_define(
	EXCNO excno,
	const T_DEXC * pk_dexc );

/* uAnts_exc.c */
void knlexc_invoke(
	EXCNO excno );

/* uAnts_exc.c */
void knlexc_undefined(
	EXCNO excno );

#endif

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_flg.h
 *
 * Internal definitions of MicroAnts semaphore.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_SEM_H__
#define __UANTS_SEM_H__

#include "uAnts_object.h"
#include "uAnts_event.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * class T_SEM.
 */
struct t_sem
{
	/** super class */
	T_OBJECT s_super;
	
	/** maximum signal count */
	UINT s_max_count;
	
	/** signal count */
	UINT s_signal_count;
	
	/** queue of tasks waiting to get */
	T_QUEUE s_task_queue;
};
typedef struct t_sem T_SEM;

/** queue of semaphores that can be used. */
extern T_QUEUE knlsem_free_queue;

/** entity array of semaphores. */
extern T_SEM knlsem_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_sem.c */
void knlsem_init_entities( void );

/* uAnts_sem.c */
void knlsem_initialize(
	T_SEM * const sem,
	const T_CSEM * pk_csem );

/* uAnts_sem.c */
void knlsem_finalize(
	T_SEM * const sem );

/* uAnts_sem.c */
ER knlsem_create(
	T_SEM * const sem,
	const T_CSEM * pk_csem );

/* uAnts_sem.c */
ER knlsem_signal(
	ID semid );

/* uAnts_sem.c */
ER knlsem_wait(
	ID semid,
	T_EVENT * const event,
	TMO timeout );

/**
 * getting semaphore entity.
 * 
 * @param id semahpore ID
 * @return semaphore entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlsem_get_entity )
#endif
UANTS_INLINE T_SEM * knlsem_get_entity(
	ID semid )
{
	T_SEM * sem;
	
	if ( semid <= 0 || semid > UANTS_CFG_SEM_MAX_ID )
	{
		sem = NULL;
	}
	else
	{
		sem = &knlsem_entities[semid - 1];
	}
	
	return sem;
}

#endif

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_alm.h
 *
 * Internal definitions of MicroAnts alarm handler.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_ALM_H__
#define __UANTS_ALM_H__

#include "uAnts_event.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0)

/**
 * class T_ALM.
 */
struct t_alm
{
	/** super class. */
	T_EVENT a_super;
	
	/** state: TALM_STA/TALM_STP. */
	STAT a_state;
	
	/** extended information. */
	VP_INT a_exinf;
	
	/** entry point. */
	void ( * a_entry )( VP_INT );
	
	/** timer interrupt nested depth. */
	UB a_nested_depth;
};
typedef struct t_alm T_ALM;

/** queue of alarm handlers that can be used. */
extern T_QUEUE knlalm_free_queue;

/** entity array of alarm handlers. */
extern T_ALM knlalm_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_alm.c */
void knlalm_init_entities( void );

/* uAnts_alm.c */
void knlalm_initialize(
	T_ALM * const alm,
	const T_CALM * pk_calm );

/* uAnts_alm.c */
void knlalm_finalize(
	T_ALM * const alm );

/* uAnts_alm.c */
ER knlalm_create(
	T_ALM * const alm,
	const T_CALM * pk_calm );

/**
 * getting alarm handler entity.
 * 
 * @param id alarm handler ID
 * @return alarm handler entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlalm_get_entity )
#endif
UANTS_INLINE T_ALM * knlalm_get_entity(
	ID almid )
{
	T_ALM * alm;
	
	if ( almid <= 0 || almid > UANTS_CFG_ALM_MAX_ID )
	{
		alm = NULL;
	}
	else
	{
		alm = &knlalm_entities[almid - 1];
	}
	
	return alm;
}

#endif

#endif

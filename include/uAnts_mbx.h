﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mbx.h
 *
 * Internal definitions of MicroAnts mailbox.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __UANTS_MBX_H__
#define __UANTS_MBX_H__

#include "uAnts_object.h"
#include "uAnts_wait.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/** magic number for message header. */
#define KNLMBX_MAGIC	(0x5A5A5A5A)

/**
 * class T_MBX_WAIT.
 */
struct t_mbx_wait
{
	/** super class. */
	T_WAIT w_super;
	
	/** pointer of pointer to message. */
	T_MSG * * w_ppk_msg;
};
typedef struct t_mbx_wait T_MBX_WAIT;

/**
 * class T_MBX.
 */
struct t_mbx
{
	/** super class. */
	T_OBJECT m_super;
	
	/** maximum message priority. */
	PRI m_max_message_priority;
	
	/** queue of messages to be sent. */
	T_QUEUE m_message_queue;
	
	/** queue of tasks waiting to receive. */
	T_QUEUE m_task_queue;
};
typedef struct t_mbx T_MBX;

/** queue of mailboxes that can be used. */
extern T_QUEUE knlmbx_free_queue;

/** entity array of mailboxes. */
extern T_MBX knlmbx_entities[];

/*---------------------------------------------
 * kernel internal function prototypes
 *-------------------------------------------*/

/* uAnts_mbx.c */
void knlmbx_init_entities( void );

/* uAnts_mbx.c */
void knlmbx_initialize(
	T_MBX * const mbx,
	const T_CMBX * pk_cmbx );

/* uAnts_mbx.c */
void knlmbx_finalize(
	T_MBX * const mbx );

/* uAnts_mbx.c */
ER knlmbx_create(
	T_MBX * const mbx,
	const T_CMBX * pk_cmbx );

/* uAnts_mbx.c */
ER knlmbx_receive(
    ID mbxid,
    T_MSG * * ppk_msg,
	T_EVENT * const event,
	TMO timeout );

/* uAnts_mbx.c */
ER knlmbx_send(
	ID mbxid,
	T_MSG * pk_msg );

/**
 * getting mailbox entity.
 * 
 * @param id mailbox ID
 * @return mailbox entity
 */
#if defined( __HITACHI__ )
#pragma inline( knlmbx_get_entity )
#endif
UANTS_INLINE T_MBX * knlmbx_get_entity(
	ID mbxid )
{
	T_MBX * mbx;
	
	if ( mbxid <= 0 || mbxid > UANTS_CFG_MBX_MAX_ID )
	{
		mbx = NULL;
	}
	else
	{
		mbx = &knlmbx_entities[mbxid - 1];
	}
	
	return mbx;
}

#endif

#endif

﻿/**
 * @file sio.c
 *
 * serial communication interface driver.
 *
 * @date 2010.08.22 new creation
 */

#include <uAnts.h>
#include <sio.h>

#if ( SIO_CFG_ENABLE )

/** SCI register base address. */
#define SCI0_BASE_ADDR	0xff80
#define SCI1_BASE_ADDR	0xff88
#define SCI2_BASE_ADDR	0xff60
#define SCI4_BASE_ADDR	0xfe90
#define SCI5_BASE_ADDR	0xf600
#define SCI6_BASE_ADDR	0xf610

/** register offset. */
#define SMR			0		/** serial mode */
#define BRR			1		/** bit rate */
#define SCR			2		/** serial control */
#define TDR			3		/** transmit data */
#define SSR			4		/** serial status */
#define RDR			5		/** receive data */
#define SCMR		6		/** smart card mode */

/** bit pattern of each register. */

/** RSR, RDR, TSR, TDR */

/** SMR */
#define CA			BIT7	/** start-stop sync  = 0 / clock sync = 1 */
#define	CHR			BIT6	/** 8bit             = 0 / 7bit       = 1 */
#define	PE			BIT5	/** Parity OFF       = 0 / Parity ON  = 1 */
#define	OE			BIT4	/** EVEN Parity      = 0 / ODD Parity = 1 */
#define	STOP		BIT3	/** 1 STOP BIT       = 0 / 2 STOP BIT = 1 */
#define MP			BIT2
#define	CKS1		BIT1
#define	CKS0		BIT0

/**
 * SMR initial value.
 *
 * BIT7 = 0：start-stop synchronous communication mode
 * BIT6 = 0：character length = 8 bit
 * BIT5 = 0：addition parity = check prohibition
 * BIT4    ：parity mode (unused)
 * BIT3 = 0：stop bit length = 1 bit
 * BIT1, 0 ：baud rate generator clock source = 
 *           SIO_CFG_SMRn_CKS (n is channel number)
 */
#define INIT_SMR	0

/** SCR */
#define TIE			BIT7
#define	RIE			BIT6
#define	TE			BIT5
#define	RE			BIT4
#define	MPIE		BIT3
#define TEIE		BIT2
#define	CKE1		BIT1
#define	CKE0		BIT0

/** SSR */
#define TDRE		BIT7
#define	RDRF		BIT6
#define	ORER		BIT5
#define	FRE			BIT4
#define	PER			BIT3
#define TEND		BIT2
#define	MPB			BIT1
#define	MPBT		BIT0

#define INDEX_SIOPORTDESC( sioportid )	( (UINT)( ( sioportid ) - 1 ) )

#define GET_SIO_PORT_DESC( sioportid )		\
	( &( sio_port_desc_table[INDEX_SIOPORTDESC( sioportid )] ) )

/**
 * serial I/O port descriptor
 */
struct t_sio_port_descriptor
{
	/** register base address. */
	UW reg_base;
	
	/** input buffer control regsiter address. */
	UH icr_addr;
	
	/** input buffer control register channel bit patter. */
	UB icr_ptn;
	
	/** baud rate */
	UH boud_rate;
	
	/** BRR setting value. */
	UB boud_brr_def;
	
	/** SMR setting value. */
	UB smr_def;
};
typedef struct t_sio_port_descriptor SIOPORTDESC;

/**
 * serial I/O port descriptor table
 */
const SIOPORTDESC sio_port_desc_table[SIO_CFG_NUM_PORT] =
{
	{
		(UW)SCI0_BASE_ADDR,
		(UH)ADDR_P2ICR,
		(UB)BIT_Pn1ICR,
		(UH)SIO_CFG_BAUD_RATE0,
		(UB)BRR0_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR0_CKS & ( CKS1 | CKS0 ) ) )
	}
#if SIO_CFG_NUM_PORT >= 2
	,{
		(UW)SCI1_BASE_ADDR,
		(UH)ADDR_P2ICR,
		(UB)BIT_Pn5ICR,
		(UH)SIO_CFG_BAUD_RATE1,
		(UB)BRR1_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR1_CKS & ( CKS1 | CKS0 ) ) )
	}
#endif
#if SIO_CFG_NUM_PORT >= 3
	,{
		(UW)SCI2_BASE_ADDR,
		(UH)ADDR_P1ICR,
		(UB)BIT_Pn1ICR,
		(UH)SIO_CFG_BAUD_RATE2,
		(UB)BRR2_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR2_CKS & ( CKS1 | CKS0 ) ) )
	}
#endif
#if SIO_CFG_NUM_PORT >= 4
	,{
		(UW)SCI4_BASE_ADDR,
		(UH)ADDR_P6ICR,
		(UB)BIT_Pn1ICR,
		(UH)SIO_CFG_BAUD_RATE4,
		(UB)BRR4_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR4_CKS & ( CKS1 | CKS0 ) ) )
	}
#endif
#if SIO_CFG_NUM_PORT >= 5
	,{
		(UW)SCI5_BASE_ADDR,
		(UH)ADDR_P1ICR,
		(UB)BIT_Pn5ICR,
		(UH)SIO_CFG_BAUD_RATE5,
		(UB)BRR5_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR5_CKS & ( CKS1 | CKS0 ) ) )
	}
#endif
#if SIO_CFG_NUM_PORT >= 6
	,{
		(UW)SCI6_BASE_ADDR,
		(UH)ADDR_PMICR,
		(UB)BIT_Pn1ICR,
		(UH)SIO_CFG_BAUD_RATE6,
		(UB)BRR6_RATE,
		(UB)( INIT_SMR | ( SIO_CFG_SMR6_CKS & ( CKS1 | CKS0 ) ) )
	}
#endif
};

UANTS_INLINE void sio_wrb(
	UW base,
	INT offset,
	UB val )
{
	bios_1653f_wrb_reg( (BIOS_VINT32)( base + offset), (BIOS_VINT8)val );
}

UANTS_INLINE VB sio_reb(
	UW base,
	INT offset )
{
	return bios_1653f_reb_reg( (BIOS_VINT32)( base + offset ) );
}

UANTS_INLINE void sio_or(
	const SIOPORTDESC * spd, 
	INT offset, 
	INT val )
{
	UB reg = sio_reb( spd->reg_base, offset );
	sio_wrb( spd->reg_base, offset, (UB)( reg | val ) );
}

UANTS_INLINE void sio_and( 
	const SIOPORTDESC * spd, 
	INT offset, 
	INT val )
{
	UB reg = sio_reb( spd->reg_base, offset );
	sio_wrb( spd->reg_base, offset, (UB)( reg & val ) );
}

static void _low_level_initialize( void );

static void _initialize_port_desc(
	const SIOPORTDESC * spd );

/**
 * initialize low level serial port.
 */
static void _low_level_initialize( void )
{
	_initialize_port_desc( GET_SIO_PORT_DESC( SIO_CFG_POL_PORTID ) );
}

/**
 * initialize serial port descriptor.
 * 
 * @param spd serial I/O port descriptor.
 */
static void _initialize_port_desc(
	const SIOPORTDESC * spd )
{
	UW i;
	
	/* disable transmit and receive. */
	sio_and( spd, SCR, ~( TE | RE ) );

	/* enable terminal input buffer. */
	bios_1653f_orb_reg( (BIOS_VINT32)spd->icr_addr, (BIOS_VINT8)spd->icr_ptn );

	/* setting bit length. */
	sio_wrb( spd->reg_base, SMR, spd->smr_def );

	/* setting baud rate. */
	sio_wrb( spd->reg_base, BRR, spd->boud_brr_def );

	/* disable interrupt. clock source = internal clock. */
	sio_and(
		spd,
		SCR,
		~( TIE | RIE | MPIE | TEIE | CKE1 | CKE0 ) );

	/* baud rate stabilization. */
	for ( i = 1000000000ul / spd->boud_rate; i > 0; i -- );
	
	/* clear error flag. */
	sio_and( spd, SSR, ~( ORER | FRE | PER ) );

	/* ensable transmit and receive. */
	sio_or( spd, SCR, ( RIE | TE | RE ) );
}

/**
 * initialize serial I/O.
 */
void sio_initialize( void )
{
	/* release module stop mode. */
#ifdef POL_MSTPCRB_SCI
	bios_1653f_andh_reg( ADDR_MSTPCRB, ~POL_MSTPCRB_SCI );
#else
	bios_1653f_andh_reg( ADDR_MSTPCRC, ~POL_MSTPCRC_SCI );
#endif
	
	/* initialize low level serial port. */
	_low_level_initialize();
}

/**
 * serial I/O putc (with polling).
 * 
 * @param c character data.
 */
void sio_putc_with_polling(
	char c )
{
	const SIOPORTDESC * spd;
	
	spd = GET_SIO_PORT_DESC( SIO_CFG_POL_PORTID );

	/* wait until TDRE becomes 1. */
	while ( !( sio_reb( spd->reg_base, SSR ) & TDRE ) );
	
	/* write byte data. */
	sio_wrb( spd->reg_base, TDR, (UB)c );

	/* clear TDRE. */
	sio_and( spd, SSR, ~TDRE );

	/* wait until TEND becomes 1. */
	while ( !( sio_reb( spd->reg_base, SSR ) & TEND ) );
}

#endif

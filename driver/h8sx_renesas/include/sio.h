﻿/**
 * @file sio.h
 *
 * serial communication interface driver header.
 *
 * @date 2010.08.22 new creation
 */

#ifndef __SIO_H__
#define __SIO_H__

#include <bios_1653f_impl.h>
#include <bios_1653f_vecno.h>
#include <sio.cfg>

/** SCI0 */
#if SIO_CFG_BAUD_RATE0 == 9600
	/** bit rate:  9600[bps] */
	#define BRR0_RATE	77
#elif SIO_CFG_BAUD_RATE0 == 19200
	/** bit rate: 19200[bps] */
	#define BRR0_RATE	38
#elif SIO_CFG_BAUD_RATE0 == 38400
	/** bit rate: 38400[bps] */
	#define BRR0_RATE	19
#endif

/** SCI1 */
#if SIO_CFG_BAUD_RATE1 == 9600
	#define BRR1_RATE	77
#elif SIO_CFG_BAUD_RATE1 == 19200
	#define BRR1_RATE	38
#elif SIO_CFG_BAUD_RATE1 == 38400
	#define BRR1_RATE	19
#endif

/** SCI2 */
#if SIO_CFG_BAUD_RATE2 == 9600
	#define BRR2_RATE	77
#elif SIO_CFG_BAUD_RATE2 == 19200
	#define BRR2_RATE	38
#elif SIO_CFG_BAUD_RATE2 == 38400
	#define BRR2_RATE	19
#endif

/** SCI4 */
#if SIO_CFG_BAUD_RATE4 == 9600
	#define BRR4_RATE	77
#elif SIO_CFG_BAUD_RATE4 == 19200
	#define BRR4_RATE	38
#elif SIO_CFG_BAUD_RATE4 == 38400
	#define BRR4_RATE	19
#endif

/** SCI5 */
#if SIO_CFG_BAUD_RATE5 == 9600
	#define BRR5_RATE	77
#elif SIO_CFG_BAUD_RATE5 == 19200
	#define BRR5_RATE	38
#elif SIO_CFG_BAUD_RATE5 == 38400
	#define BRR5_RATE	19
#endif

/** SCI6 */
#if SIO_CFG_BAUD_RATE6 == 9600
	#define BRR6_RATE	77
#elif SIO_CFG_BAUD_RATE6 == 19200
	#define BRR6_RATE	38
#elif SIO_CFG_BAUD_RATE6 == 38400
	#define BRR6_RATE	19
#endif

/**
 * sci interrupt vector number.
 */
 
/** port1 */
#define SIO1_VECNO_ERROR	BIOS_1653F_VECNO_ERI0
#define SIO1_VECNO1_IN		BIOS_1653F_VECNO_RXI0
#define SIO1_VECNO1_OUT		BIOS_1653F_VECNO_TXI0

#if SIO_CFG_NUM_PORT >= 2
/** port2 */
#define SIO2_VECNO2_ERROR	BIOS_1653F_VECNO_ERI1
#define SIO2_VECNO2_IN		BIOS_1653F_VECNO_RXI1
#define SIO2_VECNO2_OUT		BIOS_1653F_VECNO_TXI1
#endif

#if SIO_CFG_NUM_PORT >= 3
/** port3 */
#define SIO3_VECNO3_ERROR	BIOS_1653F_VECNO_ERI2
#define SIO3_VECNO3_IN		BIOS_1653F_VECNO_RXI2
#define SIO3_VECNO3_OUT		BIOS_1653F_VECNO_TXI2
#endif

#if SIO_CFG_NUM_PORT >= 4
/** port4 */
#define SIO4_VECNO4_ERROR	BIOS_1653F_VECNO_ERI4
#define SIO4_VECNO4_IN		BIOS_1653F_VECNO_RXI4
#define SIO4_VECNO4_OUT		BIOS_1653F_VECNO_TXI4
#endif

#if SIO_CFG_NUM_PORT >= 5
/** port5 */
#define SIO5_VECNO5_ERROR	BIOS_1653F_VECNO_ERI5
#define SIO5_VECNO5_IN		BIOS_1653F_VECNO_RXI5
#define SIO5_VECNO5_OUT		BIOS_1653F_VECNO_TXI5
#endif

#if SIO_CFG_NUM_PORT >= 6
/** port6 */
#define SIO6_VECNO6_ERROR	BIOS_1653F_VECNO_ERI6
#define SIO6_VECNO6_IN		BIOS_1653F_VECNO_RXI6
#define SIO6_VECNO6_OUT		BIOS_1653F_VECNO_TXI6
#endif

/**
 * module stop mode control register bit definition.
 */
#define MSTPCRB_SCI			BIT_MSTPCRB_SCI0

#if SIO_CFG_NUM_PORT > 1
#undef MSTPCRB_SCI
#endif

#if SIO_CFG_NUM_PORT == 2
#define MSTPCRB_SCI		\
	( BIT_MSTPCRB_SCI0 | BIT_MSTPCRB_SCI1 )
#endif

#if SIO_CFG_NUM_PORT == 3
#define MSTPCRB_SCI		\
	( BIT_MSTPCRB_SCI0 | BIT_MSTPCRB_SCI1 | BIT_MSTPCRB_SCI2 )
#endif

#if SIO_CFG_NUM_PORT == 4
#define MSTPCRB_SCI		\
	( BIT_MSTPCRB_SCI0 |	\
	BIT_MSTPCRB_SCI1 |		\
	BIT_MSTPCRB_SCI2 |		\
	BIT_MSTPCRB_SCI4 )
#endif

#if SIO_CFG_NUM_PORT == 5
#define MSTPCRB_SCI		\
	( BIT_MSTPCRB_SCI0 |	\
	BIT_MSTPCRB_SCI1 |		\
	BIT_MSTPCRB_SCI2 |		\
	BIT_MSTPCRB_SCI4 )
#define MSTPCRC_SCI		MSTPCRC_SCI5
#endif

#if SIO_CFG_NUM_PORT == 6
#define MSTPCRB_SCI		\
	( BIT_MSTPCRB_SCI0 |	\
	BIT_MSTPCRB_SCI1 | 		\
	BIT_MSTPCRB_SCI2 |		\
	BIT_MSTPCRB_SCI4 )
#define MSTPCRC_SCI		( BIT_MSTPCRC_SCI5 | BIT_MSTPCRC_SCI6 )
#endif

/**
 * module stop mode control register bit definition.
 * ( port only for polling )
 */
#define POL_MSTPCRB_SCI		BIT_MSTPCRB_SCI0

#if ( SIO_CFG_POL_PORTID > 1 && SIO_CFG_POL_PORTID <= SIO_CFG_NUM_PORT )
#undef POL_MSTPCRB_SCI
#endif

#if SIO_CFG_POL_PORTID == 2
#define POL_MSTPCRB_SCI		BIT_MSTPCRB_SCI1
#endif

#if SIO_CFG_POL_PORTID == 3
#define POL_MSTPCRB_SCI		BIT_MSTPCRB_SCI2
#endif

#if SIO_CFG_POL_PORTID == 4
#define POL_MSTPCRB_SCI		BIT_MSTPCRB_SCI4
#endif

#if SIO_CFG_POL_PORTID == 5
#define POL_MSTPCRC_SCI		BIT_MSTPCRC_SCI5
#endif

#if SIO_CFG_POL_PORTID == 6
#define POL_MSTPCRC_SCI		BIT_MSTPCRC_SCI6
#endif

/* sio.c */
void sio_initialize( void );

/* sio.c */
void sio_putc_with_polling(
	char c );

#endif

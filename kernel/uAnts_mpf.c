﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpf.c
 *
 * implementations of MicroAnts kernel fixed-sized memory pool functions.
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
#include "uAnts_mpl.h"
#endif
#include "uAnts_tsk.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * initialize all kernel fixed-sized memory pool entities (kernel function).
 */
void knlmpf_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlmpf_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_MPF_MAX_ID; )
	{
		T_MPF * mpf;
		
		mpf = &knlmpf_entities[id ++];
		knlobject_build(
			&mpf->f_super,
			id,
			KNLOBJECT_MPF,
			&knlmpf_free_queue );
	}
}

/**
 * initialize a fixed-sized memory pool (kernel function).
 * 
 * @param mpf fixed-sized memory pool entity
 * @param pk_cmpf fixed-sized memory pool creation information packet
 * @param vp 'fixed-sized memory pool' buffer
 */
void knlmpf_initialize(
	T_MPF * const mpf,
	const T_CMPF * pk_cmpf,
	VP vp )
{
	UINT i;
	
	if ( vp == NULL )
	{
		vp = (VP)knlutil_align_cpu( (UW)pk_cmpf->mpf );
	}
	
	knlobject_initialize(
		&mpf->f_super,
		&knlmpf_free_queue,
		pk_cmpf->mpfatr );
	knlqueue_initialize(
		&mpf->f_task_queue,
		knlutil_test_bit_16( pk_cmpf->mpfatr, TA_TPRI ) );
	knlqueue_initialize(
		&mpf->f_block_queue,
		KNLQUEUE_ORDER_FIFO );
	
	mpf->f_origin = vp;
	mpf->f_block_count = pk_cmpf->blkcnt;
	mpf->f_block_size = (UINT)knlutil_align_cpu( (UW)pk_cmpf->blksz );
	
	/* create a fixed-sized memory block queue. */
	for ( i = 0; i < pk_cmpf->blkcnt; i ++ )
	{
		T_MPF_BLOCK * block;
		
		block = (T_MPF_BLOCK *)( (UB *)vp + ( i * mpf->f_block_size ) );
		knlmpf_add_block( mpf, block );
	}
}

/**
 * finalize a fixed-sized memory pool (kernel function).
 * 
 * @param mpf fixed-sized memory pool entity
 */
void knlmpf_finalize(
	T_MPF * const mpf )
{
	knlobject_finalize( &mpf->f_super, &knlmpf_free_queue );
}

/**
 * create a new fixed-sized memory pool (kernel function).
 * 
 * @param mpf fixed-sized memory pool entity
 * @param pk_cmpf fixed-sized memory pool creation information
 * @return error code
 */
ER knlmpf_create(
	T_MPF * const mpf,
	const T_CMPF * pk_cmpf )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_cmpf == NULL )
	{
		/* illegal parameter. pk_cmpf must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_cmpf->blkcnt <= 0 )
	{
		/* memory block count must be larger than zero. */
		ercd = E_PAR;
	}
	else if ( pk_cmpf->blksz < sizeof( T_MPF_BLOCK ) )
	{
		/* memory block size is smaller than minimum. */
		ercd = E_PAR;
	}
	#if ( !UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
	else if ( pk_cmpf->mpf == NULL )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	#endif
	else if ( pk_cmpf->mpfatr & ~( TA_TFIFO | TA_TPRI ) )
	{
		/* illegal parameter. attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		VP vp;
		
		vp = NULL;
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
		if ( pk_cmpf->mpf == NULL  )
		{
			/* allocate fixed-sized memory pool buffer. */
			vp = knlmpl_allocate(
				knlmpl_get_generic_entity(),
				(UW)TSZ_MPF( pk_cmpf->blkcnt, pk_cmpf->blksz ),
				NULL );
		}
		
		if ( pk_cmpf->mpf == NULL && vp == NULL )
		{
			/* insufficient memory. */
			ercd = E_NOMEM;
		}
		else
		#endif
		{
			knlmpf_initialize( mpf, pk_cmpf, vp );
			
			ercd = E_OK;
		}
	}
	
	return ercd;
}

/**
 * get a memory block from a fixed-sized memory pool (kernel function).
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param p_blk pointer of pointer to memory block
 * @param event time-event entity
 * @param timeout time until timeout
 * @return error code
 */
ER knlmpf_get(
    ID mpfid,
    VP * p_blk,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPF * mpf;
		
		mpf = knlmpf_get_entity( mpfid );
		
		if ( mpf == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else if ( p_blk == NULL )
		{
			/* illegal parameter. p_blk must not point to null. */
			ercd = E_PAR;
		}
		else
		{
			T_MPF_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_MPF,
				&mpf->f_super,
				&mpf->f_task_queue,
				NULL );
			
			/*
			 wait-controller (fixed-sized memory pool extended).
			 copy the pointer of pointer to memory block to wait-controller.
			*/
			wait.w_block = p_blk;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpf->f_super ) )
			{
				/* fixed-sized memory pool has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else
			{
				if ( !knlqueue_is_empty( &mpf->f_block_queue ) )
				{
					/*
					 there are no tasks waiting to get
					 fixed-sized memory pool.
					*/
					
					T_MPF_BLOCK * block;
					
					/* get a memory block from a fixed-sized memory pool. */
					block = (T_MPF_BLOCK *)knlqueue_head(
						&mpf->f_block_queue );
					knlmpf_delete_block( mpf, block );
					*p_blk = block;
				}
				else
				{
					/* do wait. */
					knltsk_wait( &wait.w_super, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
		}
	}
	
	return ercd;
}

/**
 * release a memory block to a fixed-sized memory pool (kernel function).
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param blk released memory block
 * @return error code
 */
ER knlmpf_release(
	ID mpfid,
	VP blk )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPF * mpf;
		
		mpf = knlmpf_get_entity( mpfid );
		
		if ( mpf == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else if ( blk == NULL )
		{
			/* illegal parameter. blk must not point to null. */
			ercd = E_PAR;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpf->f_super ) )
			{
				/* fixed-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else if ( blk < mpf->f_origin )
			{
				/* memory block is outside fixed-sized memory pool. */
				ercd = E_PAR;
			}
			else if ( (UB *)blk >
				( (UB *)mpf->f_origin + mpf->f_block_size
				* ( mpf->f_block_count - 1 ) ) )
			{
				/* memory block is outside fixed-sized memory pool. */
				ercd = E_PAR;
			}
			else if ( ( (UW)blk - (UW)mpf->f_origin )
				% mpf->f_block_size != 0 )
			{
				/* memory block doesn't point to a block's boundary. */
				ercd = E_PAR;
			}
			else
			{
				T_MPF_BLOCK * block;
				
				block = (T_MPF_BLOCK *)blk;
				
				if ( block->b_magic == (UINT)KNLMPF_MAGIC )
				{
					/* memory block has already been released. */
					ercd = E_PAR;
				}
				else
				{
					T_TSK * tsk;
					BOOL ready;
					BIOS_UINT lock;
					
					tsk = NULL;
					ready = FALSE;
					
					/* lock CPU. */
					lock = bios_cpu_lock();
					
					if ( !knlqueue_is_empty( &mpf->f_task_queue ) )
					{
						/*
						 there are tasks waiting to get
						 fixed-sized memory pool.
						*/
						
						T_MPF_WAIT * wait;
						
						tsk = (T_TSK *)knlqueue_head( &mpf->f_task_queue );
						wait = (T_MPF_WAIT *)tsk->t_wait;
						
						/*
						 task waiting to get fixed-sized memory pool
						 got memory block.
						*/
						*wait->w_block = blk;
						
						/*jp 固定長メモリプール取得待ち状態終了処理実行. */
						ready = knltsk_quit_waiting( tsk, E_OK );
					}
					
					/* unlock CPU. */
					bios_cpu_unlock( lock );
					
					if ( ready )
					{
						/*jp 待ち状態を終了したタスクをレディ状態にする. */
						knltsk_ready( tsk );
					}
					
					if ( tsk == NULL )
					{
						/*
						 there are no tasks waiting to get
						 fixed-sized memory pool. 
						 return a memory block to a fixed-sized memory pool.
						*/
						knlmpf_add_block( mpf, block );
					}
					
					ercd = E_OK;
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

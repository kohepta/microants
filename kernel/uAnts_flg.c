﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_flg.c
 *
 * implementations of MicroAnts kernel eventflag functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

static BOOL _knlflg_check_waitable(
	const T_WAIT * const wait );

static BOOL _knlflg_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/**
 * can enter state of wait?.
 * 
 * @param wait wait-controller entity
 * @retval TRUE can enter state of wait
 * @retval FALSE can not enter state of wait
 */
static BOOL _knlflg_check_waitable(
	const T_WAIT * const wait )
{
	T_FLG * flg;
	
	flg = (T_FLG *)knlwait_get_object( wait );
	
	return !knlflg_compare( flg, (T_FLG_WAIT *)wait );
}

/**
 * individual processing in wait release.
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return status code
 */
static BOOL _knlflg_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	BOOL status;
	
	status = FALSE;
	
	switch ( purpose )
	{
	case purpose_CHECK_WAITABLE:
		status = _knlflg_check_waitable( wait );
		break;
	case purpose_TIMEOUT:
	case purpose_WAITING_RELEASED:
		{
			T_FLG * flg;
			
			flg = (T_FLG *)knlwait_get_object( wait );
			
			/*jp
			 (i)rel_wai() が呼ばれた場合, イベントフラグを待っていたタスクの
			 待ちは解除されるので, この時点でのイベントフラグパターンを待ち解
			 除時のパターンとして設定する.
			*/
			*( (T_FLG_WAIT *)wait )->w_released_pattern = flg->f_pattern;
		}
		break;
	case purpose_PRIORITY_CHANGED:
	case purpose_TERMINATED:
	default:
		break;
	}
	
	return status;
}

/**
 * initialize all kernel eventflag entities (kernel function).
 */
void knlflg_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlflg_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_FLG_MAX_ID; )
	{
		T_FLG * flg;
		
		flg = &knlflg_entities[id ++];
		knlobject_build(
			&flg->f_super,
			id,
			KNLOBJECT_FLG,
			&knlflg_free_queue );
	}
}

/**
 * initialize an eventflag (kernel function).
 * 
 * @param flg eventflag entity
 * @param pk_cflg eventflag creation information packet
 * @return 
 */
void knlflg_initialize(
	T_FLG * const flg,
	const T_CFLG * pk_cflg )
{
	knlobject_initialize(
		&flg->f_super,
		&knlflg_free_queue, 
		pk_cflg->flgatr );
	knlqueue_initialize(
		&flg->f_task_queue,
		knlutil_test_bit_16( pk_cflg->flgatr, TA_TPRI ) );
	
	flg->f_setting = FALSE;
	flg->f_pattern = pk_cflg->iflgptn;
	flg->f_reservation_pattern = 0;
}

/**
 * finalize an eventflag (kernel function).
 * 
 * @param flg eventflag entity
 */
void knlflg_finalize(
	T_FLG * const flg )
{
	knlobject_finalize( &flg->f_super, &knlflg_free_queue );
}

/**
 * create a new eventflag (kernel function).
 * 
 * @param flg eventflag entity
 * @param pk_cflg eventflag creation information packet
 * @return error code
 */
ER knlflg_create(
	T_FLG * const flg,
	const T_CFLG * pk_cflg )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_cflg == NULL )
	{
		/* illegal parameter. pk_cflg must not point to null. */
		ercd = E_PAR;
	}
    else if (pk_cflg->flgatr
    	& ~( TA_TFIFO | TA_TPRI | TA_WSGL | TA_WMUL | TA_CLR ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		BIOS_UINT lock;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		knlflg_initialize( flg, pk_cflg );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		ercd = E_OK;
	}
	
	return ercd;
}

/**
 * compare a condition with the eventflag (kernel function).
 * 
 * @param flg eventflag entity
 * @param wait wait-controller entity
 * @retval TRUE match
 * @retval FALSE not match
 */
BOOL knlflg_compare(
	T_FLG * const flg,
	const T_FLG_WAIT * wait )
{
	BOOL match;
	
	if ( ( wait->w_mode == TWF_ANDW )
		? ( ( flg->f_pattern & wait->w_pattern ) == wait->w_pattern )
		: ( ( flg->f_pattern & wait->w_pattern ) != 0 ) )
	{
		*wait->w_released_pattern = flg->f_pattern;
		
		if ( knlflg_attribute_clear( flg ) )
		{
			/* if TA_CLR is set, clear an eventflag. */
			flg->f_pattern = 0;
		}
		
		match = TRUE;
	}
	else
	{
		match = FALSE;
	}
	
	return match;
}

/**
 * set pattern to an eventflag (kernel function).
 * 
 * @param flgid eventflag ID
 * @param setptn set bit pattern
 * @return error code
 */
ER knlflg_set(
	ID flgid,
	FLGPTN setptn )
{
	ER ercd;
	T_FLG * flg;
	
	flg = knlflg_get_entity( flgid );
	
	if ( flg == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &flg->f_super ) )
		{
			/* eventflag has not been created. */
			ercd = E_NOEXS;
		}
		else
		{
			if ( setptn != 0 )
			{
				UB nested_depth;
				BIOS_UINT lock;
				BOOL iterate;
				
				/*jp 
				 スタックフレームに現在のコンテクスト
				 アクセス深度を保存.
				*/
				nested_depth = knlsys_get_nested_depth();
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( flg->f_setting )
				{
					/*jp
					 イベントフラグが既に他のコンテクストからアクセスされている
					 場合, 設定ビットパターンを予約して処理を終了し, 予約したビ
					 ットパターンの設定は現在アクセス中のコンテクストに委譲する.
					*/
					flg->f_reservation_pattern |= setptn;
					iterate = FALSE;
				}
				else
				{
					/*
					 eventflag is not accessed to be set by other context.
					 processing is continued.
					*/
					flg->f_pattern |= setptn;
					flg->f_setting = TRUE;
					iterate = TRUE;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				while ( iterate )
				{
					T_JOINT * jp;
					BOOL interrupted;
					
					/* 1: */
					
					jp = &flg->f_task_queue.q_joint;
					interrupted = FALSE;
					
					/*jp キューに現在のコンテクストアクセス深度を保存. */
					knlqueue_set_nested_depth(
						&flg->f_task_queue,
						nested_depth );
					
					do
					{
						BOOL ready;
						T_TSK * tsk;
						
						ready = FALSE;
						tsk = NULL;
						
						/* lock CPU. */
						lock = bios_cpu_lock();
						
						/*jp
						 スタックフレームに保存したアクセス深度と, キューに
						 保存したアクセス深度が異なった場合, 更に深度の高い
						 コンテクストがこのキューを改変したと見なし, キュー
						 の走査を 1: からやり直す.
						 例えば, 処理中にこのキューに格納されているタスクに
						 対して irel_wai() などを呼んだ場合.
						*/
						if ( knlqueue_get_nested_depth(
							&flg->f_task_queue ) != nested_depth )
						{
							interrupted = TRUE;
						}
						else
						{
							T_JOINT * kp;
							
							kp = knljoint_next( jp );
							
							if ( knlqueue_equals( &flg->f_task_queue, kp ) )
							{
								/* there are no waiting tasks. */
								iterate = FALSE;
							}
							else
							{
								tsk = (T_TSK *)kp;
								
								if ( knlflg_compare(
									flg,
									(T_FLG_WAIT *)tsk->t_wait ) )
								{
									/*jp
									 設定パターンと待ちパターンが一致した場合,
									 待ち状態終了処理実行.
									*/
									ready = knltsk_quit_waiting( tsk, E_OK );
									
									if ( knlflg_attribute_clear( flg ) )
									{
										iterate = FALSE;
									}
								}
								else
								{
									/*
									 when 'kp' has not been deleted from task
									 queue, 'jp' is upadated.
									*/
									jp = kp;
								}
							}
						}
						
						/* unlock CPU. */
						bios_cpu_unlock( lock );
						
						if ( ready )
						{
							/* 待ち状態を終了したタスクをレディ状態にする. */
							knltsk_ready( tsk );
						}
					}
					while ( iterate && !interrupted );
					
					/*jp
					 アクセス深度の高いコンテクストによって待ち
					 キューを変更されていない場合.
					*/
					
					/* lock CPU. */
					lock = bios_cpu_lock();
					
					if ( !interrupted && flg->f_reservation_pattern == 0 )
					{
						/*jp
						 条件 !interrupted は, 予約パターンのセットは無いの
						 だが, irel_wai などの他コンテクストにより待ちキュー
						 が変更され, パターンセットのループを抜けてしまった
						 際, そのまま処理を終了してしまうことを防ぐ為, 設け
						 てある.
						*/
						
						/*
						 there is no reservation pattern.
						 finish this function.
						*/
						flg->f_setting = FALSE;
						iterate = FALSE;
					}
					else
					{
						/*jp
						 他のコンテクストからの予約パターンある場合, 
						 予約分のパターン設定を行う為, 1: から処理を
						 やり直す.
						*/
						flg->f_pattern |= flg->f_reservation_pattern;
						flg->f_reservation_pattern = 0;
						iterate = TRUE;
					}
					
					/* unlock CPU. */
					bios_cpu_unlock( lock );
				}
			}
			
			ercd = E_OK;
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

/**
 * wait for an eventflag (kernel function).
 * 
 * @param flgid eventflag ID
 * @param waiptn wait bit pattern
 * @param wfmode wait mode
 * @param flag pattern, when wait released
 * @param event time-event entity
 * @param timeout timeout until timeout
 * @return error code
 */
ER knlflg_wait(
    ID flgid,
    FLGPTN waiptn,
    MODE wfmode,
    FLGPTN * p_flgptn,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( waiptn == 0 )
	{
		/* wait pattern must not be zero. */
		ercd = E_PAR;
	}
	else if ( p_flgptn == NULL )
	{
		/* illegal paramter. p_flgptn must not point to null. */
		ercd = E_PAR;
	}
	else if ( ( wfmode != TWF_ORW ) && ( wfmode != TWF_ANDW ) )
	{
		/* illegal mode. */
		ercd = E_PAR;
	}
	else
	{
		T_FLG * flg;
		
		flg = knlflg_get_entity( flgid );
		
		if ( flg == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			T_FLG_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_FLG,
				&flg->f_super,
				&flg->f_task_queue,
				_knlflg_wait_callback );
			
			/* wait-controller (eventflag extended). */
			wait.w_pattern = waiptn;
			wait.w_mode = wfmode;
			
			/* copy the released pattern address to wait-controller. */
			wait.w_released_pattern = p_flgptn;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &flg->f_super ) )
			{
				/* eventflag has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else if ( !knlqueue_is_empty( &flg->f_task_queue )
				&& !knlflg_attribute_multi_wait( flg ) )
			{
				/*
				 illegal service call usage.
				 TA_WMUL is not specified, and
				 other task has already been waiting for eventflag.
				*/
				knlwait_set_result( &wait.w_super, E_ILUSE );
			}
			else
			{
				BIOS_UINT lock;
				BOOL waitable;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				/* can enter state of wait? */
				waitable = _knlflg_check_waitable( &wait.w_super );
				
				if ( waitable && ( timeout == TMO_POL ) )
				{
					*p_flgptn = flg->f_pattern;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( waitable )
				{
					/* do wait. */
					knltsk_wait( &wait.w_super, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
		}
	}
	
	return ercd;
}

#endif

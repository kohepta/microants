﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpl_entities.c
 *
 * implementations of MicroAnts kernel variable-sized memory pool entities.
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_STACK	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )

/** queue of variable-sized memory pools that can be used. */
T_QUEUE knlmpl_free_queue;

/** 
 * entity array of variable-sized memory pools
 */
T_MPL knlmpl_entities[KNLMPL_MAX_ID];

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sem_entities.c
 *
 * implementations of MicroAnts kernel semafore entities.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/** queue of semafores that can be used. */
T_QUEUE knlsem_free_queue;

/** entity array of semafores. */
T_SEM knlsem_entities[UANTS_CFG_SEM_MAX_ID];

#endif

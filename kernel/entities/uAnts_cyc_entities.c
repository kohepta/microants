﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cyc_entities.c
 *
 * implementations of MicroAnts kernel cyclic handler entities.
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_cyc.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/** queue of cyclic handlers that can be used. */
T_QUEUE knlcyc_free_queue;

/** entity array of cyclic handlers. */
T_CYC knlcyc_entities[UANTS_CFG_CYC_MAX_ID];

#endif

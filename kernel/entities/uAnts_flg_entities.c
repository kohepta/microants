﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_flg_entities.c
 *
 * implementations of MicroAnts kernel eventflag entities.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/** queue of eventflags that can be used. */
T_QUEUE knlflg_free_queue;

/** entity array of eventflags. */
T_FLG knlflg_entities[UANTS_CFG_FLG_MAX_ID];

#endif

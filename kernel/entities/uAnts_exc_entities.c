﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_exc_entities.c
 *
 * implementations of MicroAnts kernel cpu exception handler entities.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_exc.h"

#if ( UANTS_CFG_EXC_MAX_ID > 0 )

/** entity array of cpu exception handlers */
T_EXC knlexc_entities[UANTS_CFG_EXC_MAX_ID];

#endif

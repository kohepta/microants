﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpf_entities.c
 *
 * implementations of MicroAnts kernel fixed-sized memory pool entities.
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/** queue of fixed-sized memory pools that can be used. */
T_QUEUE knlmpf_free_queue;

/** entity array of fixed-sized memory pools. */
T_MPF knlmpf_entities[UANTS_CFG_MPF_MAX_ID];

#endif

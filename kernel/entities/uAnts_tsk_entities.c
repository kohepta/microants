﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tsk_entities.c
 *
 * implementations of MicroAnts kernel task entities.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/** pointer to current running task. */
T_TSK * knltsk_current;

/** queue of tasks that can be used. */
T_QUEUE knltsk_free_queue;

/** queue of ready tasks. */
T_QUEUE knltsk_ready_queue;

/** entity array of tasks. */
T_TSK knltsk_entities[KNLTSK_MAX_ID];

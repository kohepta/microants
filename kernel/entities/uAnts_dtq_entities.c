﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dtq_entities.c
 *
 * implementations of MicroAnts kernel data-queue entities.
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/** queue of data-queues that can be used. */
T_QUEUE knldtq_free_queue;

/** entity array of data-queues. */
T_DTQ knldtq_entities[UANTS_CFG_DTQ_MAX_ID];

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sys_entities.c
 *
 * implementations of MicroAnts kernel system entities.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/** kernel system entity. */
T_SYS knlsys;

/** kernel system stack entity. */
const T_SYS_STACK knlsys_stack;

extern void knltsk_init_entities( void );

#if ( UANTS_CFG_SEM_MAX_ID > 0 )
extern void knlsem_init_entities( void );
#endif
#if ( UANTS_CFG_FLG_MAX_ID > 0 )
extern void knlflg_init_entities( void );
#endif
#if ( UANTS_CFG_DTQ_MAX_ID > 0 )
extern void knldtq_init_entities( void );
#endif
#if ( UANTS_CFG_MBX_MAX_ID > 0 )
extern void knlmbx_init_entities( void );
#endif
#if ( UANTS_CFG_MPF_MAX_ID > 0 )
extern void knlmpf_init_entities( void );
#endif
#if ( UANTS_CFG_MPL_MAX_ID > 0 )
extern void knlmpl_init_entities( void );
#endif
#if ( UANTS_CFG_CYC_MAX_ID > 0 )
extern void knlcyc_init_entities( void );
#endif
#if ( UANTS_CFG_ALM_MAX_ID > 0 )
extern void knlalm_init_entities( void );
#endif

extern void knlinh_init_entities( void );

#if ( UANTS_CFG_EXC_MAX_ID > 0 )
extern void knlexc_init_entities( void );
#endif

/** array of knlxxx_init_entities() functions. */
void ( * const knlsys_init_entities[] )( void ) =
{
	/* task. */
	knltsk_init_entities,
	
	#if ( UANTS_CFG_SEM_MAX_ID > 0 )
	/* semaphore. */
	knlsem_init_entities,
	#endif
	#if ( UANTS_CFG_FLG_MAX_ID > 0 )
	/* eventflag. */
	knlflg_init_entities,
	#endif
	#if ( UANTS_CFG_DTQ_MAX_ID > 0 )
	/* data-queue. */
	knldtq_init_entities,
	#endif
	#if ( UANTS_CFG_MBX_MAX_ID > 0 )
	/* mailbox. */
	knlmbx_init_entities,
	#endif
	#if ( UANTS_CFG_MPF_MAX_ID > 0 )
	/* fixed-sized memory pool. */
	knlmpf_init_entities,
	#endif
	#if ( UANTS_CFG_MPL_MAX_ID > 0 )
	/* variable-sized memory pool. */
	knlmpl_init_entities,
	#endif
	#if ( UANTS_CFG_CYC_MAX_ID > 0 )
	/* cyclic handler. */
	knlcyc_init_entities,
	#endif
	#if ( UANTS_CFG_ALM_MAX_ID > 0 )
	/* alarm handler. */
	knlalm_init_entities,
	#endif
	
	/* interrupt handler. */
	knlinh_init_entities,
	
	#if ( UANTS_CFG_EXC_MAX_ID > 0 )
	/* cpu exception handler. */
	knlexc_init_entities,
	#endif
	
	/* terminator. */
	NULL
};

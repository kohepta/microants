﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_alm_entities.c
 *
 * implementations of MicroAnts kernel alarm handler entities.
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

/** queue of alarm handlers that can be used. */
T_QUEUE knlalm_free_queue;

/** entity array of alarm handlers. */
T_ALM knlalm_entities[UANTS_CFG_ALM_MAX_ID];

#endif

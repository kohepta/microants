﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tim_entities.c
 *
 * implementations of MicroAnts kernel time management entities.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tim.h"

/** time management entity. */
T_TIM knltim;

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_inh_entities.c
 *
 * implementations of MicroAnts kernel interrupt handler entities.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_inh.h"

/** entity array of interrupt handlers. */
T_INH knlinh_entities[UANTS_CFG_INH_MAX_ID];

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mbx_entities.c
 *
 * implementations of MicroAnts kernel mailbox entities.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/** queue of mailboxes that can be used. */
T_QUEUE knlmbx_free_queue;

/** entity array of mailboxes. */
T_MBX knlmbx_entities[UANTS_CFG_MBX_MAX_ID];

#endif

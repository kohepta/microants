﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sem.c
 *
 * implementations of MicroAnts kernel semafore functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

static BOOL _knlsem_check_waitable(
	const T_WAIT * const wait );

static BOOL _knlsem_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/**
 * can enter state of wait?.
 * 
 * @param wait wait-controller entity
 * @retval TRUE can enter state of wait
 * @retval FALSE can not enter state of wait
 */
static BOOL _knlsem_check_waitable(
	const T_WAIT * const wait )
{
	T_SEM * sem;
	BOOL waitable;
	
	sem = (T_SEM *)knlwait_get_object( wait );
	
	if ( sem->s_signal_count > 0 )
	{
		sem->s_signal_count --;
		
		waitable = FALSE;
	}
	else
	{
		waitable = TRUE;
	}
	
	return waitable;
}

/**
 * individual processing in wait release.
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return status code
 */
static BOOL _knlsem_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	BOOL status;
	
	status = FALSE;
	
	switch ( purpose )
	{
	case purpose_CHECK_WAITABLE:
		status = _knlsem_check_waitable( wait );
		break;
	case purpose_TIMEOUT:
	case purpose_WAITING_RELEASED:
	case purpose_PRIORITY_CHANGED:
	case purpose_TERMINATED:
	default:
		break;
	}
	
	return status;
}

/**
 * initialize all kernel semafore entities (kernel function).
 */
void knlsem_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlsem_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_SEM_MAX_ID; )
	{
		T_SEM * sem;
		
		sem = &knlsem_entities[id ++];
		knlobject_build(
			&sem->s_super,
			id,
			KNLOBJECT_SEM,
			&knlsem_free_queue );
	}
}

/**
 * initialize a semafore (kernel function).
 * 
 * @param sem semafore entity
 * @param pk_csem semafore creation information packet
 */
void knlsem_initialize(
	T_SEM * const sem,
	const T_CSEM * pk_csem )
{
	knlobject_initialize(
		&sem->s_super,
		&knlsem_free_queue, 
		pk_csem->sematr );
	knlqueue_initialize(
		&sem->s_task_queue,
		knlutil_test_bit_16( pk_csem->sematr, TA_TPRI ) );
	
	sem->s_max_count = pk_csem->maxsem;
	sem->s_signal_count = pk_csem->isemcnt;
}

/**
 * finalize a semafore (kernel function).
 * 
 * @param sem semafore entity
 */
void knlsem_finalize(
	T_SEM * const sem )
{
	knlobject_finalize( &sem->s_super, &knlsem_free_queue );
}

/**
 * create a new semafore (kernel function).
 * 
 * @param sem semafore entitiy
 * @param pk_csem semafore criation information packet
 * @return error code
 */
ER knlsem_create(
	T_SEM * const sem,
	const T_CSEM * pk_csem )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_csem == NULL )
	{
		/* illegal parameter. pk_csem must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_csem->maxsem <= 0 )
	{
		/* max count must be larger than zero. */
		ercd = E_PAR;
	}
	else if ( pk_csem->isemcnt > pk_csem->maxsem )
	{
		/* initial count must be equal to or be smaller than max count. */
		ercd = E_PAR;
	}
	else if ( pk_csem->sematr & ~( TA_TFIFO | TA_TPRI ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		BIOS_UINT lock;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		knlsem_initialize( sem, pk_csem );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		ercd = E_OK;
	}
	
	return ercd;
}

/**
 * signal to a semafore (kernel function).
 * 
 * @param semid semafore ID
 * @return error code
 */
ER knlsem_signal(
	ID semid )
{
	T_SEM * sem;
	ER ercd;
	
	sem = knlsem_get_entity( semid );
	
	if ( sem == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &sem->s_super ) )
		{
			/* semafore has not been created. */
			ercd = E_NOEXS;
		}
		else
		{
			T_TSK * tsk;
			BOOL ready;
			BIOS_UINT lock;
			
			tsk = NULL;
			ready = FALSE;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( !knlqueue_is_empty( &sem->s_task_queue ) )
			{
				/* delete first task waiting for the semaphore. */
				tsk = (T_TSK *)knlqueue_head( &sem->s_task_queue );
				
				/*jp セマフォを待つ最初のタスクに対して待ち状態終了処理実行 */
				ready = knltsk_quit_waiting( tsk, E_OK );
				ercd = E_OK;
			}
			else
			{
				if ( sem->s_signal_count >= sem->s_max_count )
				{
					/* signal reached to the limit. */
					ercd = E_QOVR;
				}
				else
				{
					/* queuing signal. */
					sem->s_signal_count ++;
					ercd = E_OK;
				}
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( ready )
			{
				/* 待ち状態を終了したタスクをレディ状態にする. */
				knltsk_ready( tsk );
			}
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

/**
 * wait for a semafore (kernel function).
 * 
 * @param semid semafore ID
 * @param event time-event entity
 * @param time until timeout
 * @return error code
 */
ER knlsem_wait(
	ID semid,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_SEM * sem;
		
		sem = knlsem_get_entity( semid );
		
		if ( sem == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			T_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait,
				event,
				TTW_SEM,
				&sem->s_super,
				&sem->s_task_queue,
				_knlsem_wait_callback );
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &sem->s_super ) )
			{
				/* semafore has not been created. */
				knlwait_set_result( &wait, E_NOEXS );
			}
			else
			{
				BIOS_UINT lock;
				BOOL waitable;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				/* can enter state of wait? */
				waitable = _knlsem_check_waitable( &wait );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( waitable )
				{
					/* do wait. */
					knltsk_wait( &wait, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait );
		}
	}
	
	return ercd;
}

#endif

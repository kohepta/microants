﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpl.c
 *
 * implementations of MicroAnts kernel variable-sized memory pool functions.
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

static void _knlmpl_proc_waiting_release(
	T_MPL const * mpl );

static BOOL _knlmpl_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/**
 * internal procedure in waiting release.
 * 
 * @param mpl variable-sized memory pool entity
 */
static void _knlmpl_proc_waiting_release(
	T_MPL const * mpl )
{
	if ( mpl->v_max_block != NULL )
	{
		/*jp
		 メモリ解放待ちタスクの待ちがなんらかの理由で解除された場合,
		 続く解放待ちタスクに対してもメモリ割り当てを試みる.
		*/
		knlmpl_allocate_waiting_tasks(
			mpl,
			mpl->v_max_block );
	}
}

/**
 * individual processing in wait release.
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return status code
 */
static BOOL _knlmpl_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	BOOL status;
	
	status = FALSE;
	
	switch ( purpose )
	{
	case purpose_CHECK_WAITABLE:
		status = TRUE;
		break;
	case purpose_TIMEOUT:
	case purpose_WAITING_RELEASED:
		/*jp
		 タイムアウトと待ち解除時のコールバックはクリティカル
		 セクション内で呼び出されるので, CPU アンロックを行う
		  _knlmpl_proc_waiting_release() は使用できない. 
		 なので, タイムアウトと待ち解除時のメモリプール再割り
		 当ては, 待ち解除が完全に終了し, その結果 E_OK 以外を
		 取得できた際に行う.
		*/
		break;
	case purpose_PRIORITY_CHANGED:
	case purpose_TERMINATED:
		{
			T_MPL * mpl;
			
			mpl = (T_MPL *)knlwait_get_object( wait );
			
			_knlmpl_proc_waiting_release( mpl );
		}
		break;
	default:
		break;
	}
	
	return status;
}

/**
 * finalize a variable-sized memory pool (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 */
void knlmpl_finalize(
	T_MPL * const mpl )
{
	knlobject_finalize( &mpl->v_super, &knlmpl_free_queue );
}

/**
 * create a new variable-sized memory pool (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param pk_cmpl variable-sized memory pool creation information packet
 * @return error code
 */
ER knlmpl_create(
	T_MPL * const mpl,
	const T_CMPL * pk_cmpl )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_cmpl == NULL )
	{
		/* illegal parameter. pk_cmpl must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_cmpl->mplsz < sizeof( T_MPL_BLOCK ) )
	{
		/* size is smaller than minimum. */
		ercd = E_PAR;
	}
	else if
	#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
	( pk_cmpl->mpl == NULL
		&& ( knlmpl_get_adjusted_block_size( (UW)pk_cmpl->mplsz )
			> knlmpl_get_generic_entity()->v_size ) )
	{
		ercd = E_NOMEM;
	}
	#else
	( pk_cmpl->mpl == NULL )
	{
		ercd = E_PAR;
	}
	#endif
	else if ( pk_cmpl->mplatr & ~( TA_TFIFO | TA_TPRI ) )
	{
		/* attribute that doesn't exist in ITRON sepc. */
		ercd = E_RSATR;
	}
	else
	{
		VP vp;
		UW vp_size;
		
		vp = NULL;
		
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
		if ( pk_cmpl->mpl == NULL  )
		{
			/* allocate variable-sized memory pool buffer. */
			vp = knlmpl_allocate(
				knlmpl_get_generic_entity(),
				(UW)pk_cmpl->mplsz,
				&vp_size );
		}
		
		if ( pk_cmpl->mpl == NULL && vp == NULL )
		{
			/* insufficient memory. */
			ercd = E_NOMEM;
		}
		else
		#endif
		{
			knlmpl_initialize( mpl, pk_cmpl, vp, vp_size );
			
			ercd = E_OK;
		}
	}
	
	return ercd;
}

/**
 * allocate free block for waiting tasks (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param block free memory block
 */
void knlmpl_allocate_waiting_tasks(
	T_MPL * const mpl,
	T_MPL_BLOCK * block )
{
	BOOL iterate;
	
	iterate = ( block != NULL ) ? TRUE : FALSE;
	
	while ( iterate )
	{
		T_TSK * tsk;
		BOOL ready;
		UW block_size;
		BIOS_UINT lock;
		VP * p_block;
		
		tsk = NULL;
		ready = FALSE;
		block_size = 0;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		p_block = NULL;
		
		if ( iterate = !knlqueue_is_empty( &mpl->v_task_queue ) )
		{
			const T_MPL_WAIT * wait;
			
			/* get the first waiting task. */
			tsk = (T_TSK *)knlqueue_head( &mpl->v_task_queue );
			wait = (T_MPL_WAIT *)tsk->t_wait;
			
			/* get the memory block size. */
			block_size = wait->w_size;
			block_size = knlmpl_get_adjusted_block_size( block_size );
			
			if ( block_size > block->b_free.f_size )
			{
				/* free memory blocks size is insufficient. */
				iterate = FALSE;
			}
			else
			{
				p_block = wait->w_block;
				
				/*jp 取得待ち状態終了処理実行. */
				ready = knltsk_quit_waiting( tsk, E_OK );
			}
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( ready )
		{
			/*jp 取得待ち状態を終了したタスクをレディ状態にする. */
			knltsk_ready( tsk );
		}
		
		if ( p_block != NULL )
		{
			T_MPL_BLOCK * bp;
			
			/* divide the memory block for first waiting task . */
			bp = knlmpl_divide_block( mpl, block, block_size, NULL );
			
			/*jp
			 取得待ちしていたタスクが使用するメモリブロックの先頭を
			 ヘッダーサイズ分ずらす.
			*/
			*p_block = (VP)( &bp->b_allocated + 1 );
			
			if ( bp == block )
			{
				/* no more free memory block. */
				iterate = FALSE;
			}
		}
	}
}

/**
 * get a memory block from a variable-sized memory pool (kernel function).
 * 
 * @param mplid variable-sized memory pool ID
 * @param blksz memory block size
 * @param p_blk pointer of pointer to memory block
 * @param event time-event entity
 * @param timeout time until timeout
 * @return error code
 */
ER knlmpl_get(
    ID mplid,
    UW blksz,
    VP * p_blk,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPL * mpl;
		
		mpl = knlmpl_get_entity( mplid );
		
		if ( mpl == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else if ( p_blk == NULL )
		{
			/* illegal parameter. p_blk must not point to null. */
			ercd = E_PAR;
		}
		else
		{
			T_MPL_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_MPL,
				&mpl->v_super,
				&mpl->v_task_queue,
				_knlmpl_wait_callback );
			/*
			 wait-controller (variable-sized memory pool extended).
			 copy the pointer of pointer to memory block to wait-controller.
			*/
			wait.w_size = blksz;
			wait.w_block = p_blk;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpl->v_super ) )
			{
				/* variable-sized memory pool has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else if ( knlmpl_get_adjusted_block_size( blksz ) > mpl->v_size 
				|| blksz <= 0 )
			{
				/*
				 size of memory block is larger than
				 the variable-sized memory pool. or it is zero.
				*/
				knlwait_set_result( &wait.w_super, E_PAR );
			}
			else
			{
				VP blk;
				BOOL allocatable;
				BIOS_UINT lock;
				
				allocatable = FALSE;
				blk = NULL;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knlqueue_is_empty( &mpl->v_task_queue ) )
				{
					/* there is no waiting tasks. */
					allocatable = TRUE;
				}
				else
				{
					/* there is waiting tasks. */
					
					if ( knlqueue_get_order( &mpl->v_task_queue )
						== KNLQUEUE_ORDER_PRIORITY )
					{
						/* waiting tasks queue is priority order. */
						
						T_TSK * ctsk;
						T_TSK * wtsk;
						
						ctsk = knltsk_current;
						wtsk = (T_TSK *)knlqueue_head( &mpl->v_task_queue );
						
						if ( knltsk_get_priority( ctsk )
							< knltsk_get_priority( wtsk ) )
						{
							/*
							 current running task has the highest priority
							 among waiting the variable-sized memory pool.
							*/
							allocatable = TRUE;
						}
					}
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( allocatable )
				{
					/* allocate memory block. */
					blk = knlmpl_allocate( mpl, blksz, NULL );
				}
				
				if ( blk != NULL )
				{
					*p_blk = blk;
				}
				else
				{
					/* do wait. */
					knltsk_wait( &wait.w_super, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
			
			if ( ercd != E_OK )
			{
				/*
				 rel_wai() or irel_wai() have been issued by current running
				 task, otherwise wait timeout elapsed.
				*/
				
				/* lock dispatch. */
				disp = knlsys_lock_dispatch();
				
				/*
				 there is a possibility that other tasks deleted
				 the variable-sized memory pool entity.
				*/
				if ( knlobject_exists( &mpl->v_super ) )
				{
					_knlmpl_proc_waiting_release( mpl );
				}
				
				/* unlock dispatch. */
				knlsys_unlock_dispatch( disp );
			}
		}
	}
	
	return ercd;
}

#endif

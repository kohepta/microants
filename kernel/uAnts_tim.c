﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tim.c
 *
 * implementations of MicroAnts kernel time management functions.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tim.h"
#include "uAnts_hook.h"
#include "uAnts_inh.h"

static void _knltim_interrupt_handler( void );

/**
 * handler for the sytem timer interrupt.
 */
static void _knltim_interrupt_handler( void )
{
	cfghook_pre_system_timer_interrupt_process();
	
	bios_system_timer_pre_interrupt_process();
	
	/* enable interrupts. */
	bios_cpu_enable_interrupts();
	
	knltim_signal();
	
	bios_system_timer_post_interrupt_process();
	
	cfghook_post_system_timer_interrupt_process();
}

/**
 * intialize a time-management (kernel function).
 */
void knltim_initialize( void )
{
	knlqueue_initialize( &knltim.t_event_queue, KNLQUEUE_ORDER_FIFO );
	
	knltim.t_system.utime = 0;
	knltim.t_system.ltime = 0;
	knltim.t_elapsed = UANTS_CFG_TIC_NUME / UANTS_CFG_TIC_DENO;
	#if ( UANTS_CFG_TIC_DENO > 1 )
	knltim.t_adjust = UANTS_CFG_TIC_NUME % UANTS_CFG_TIC_DENO;
	#endif
	knltim.t_nested_depth = 0;
}

/**
 * add time-event to timeout queue (kernel function).
 * 
 * @param event time-event entity
 * @param timeout time until timeout
 */
void knltim_add_event(
	T_EVENT * const event,
	RELTIM timeout )
{
	knltim_add_event_with_callback( event, timeout, NULL );
}

/**
 * delete time-event from timeout queue (kernel function).
 * 
 * @param event time-event entity
 */
void knltim_delete_event(
	T_EVENT * const event )
{
	T_JOINT * jp;
	
	jp = knljoint_next( &event->e_super.o_joint );
	
	if ( !knlqueue_equals( &knltim.t_event_queue, jp ) )
	{
		/*
		 when deleted event is not the last one,
		 add it's interval to the next one.
		*/
		( (T_EVENT *)jp )->e_interval += event->e_interval;
	}
	
	event->e_interval = 0;
	
	knlqueue_delete( &knltim.t_event_queue, &event->e_super.o_joint );
	
}

/**
 * get remainder time until timeout (kernel function).
 * 
 * @param event time-event entity
 * @return remainder time until timeout
 */
RELTIM knltim_get_remainder_until_timeout(
	const T_EVENT * event )
{
	RELTIM remainder;
	T_JOINT * jp;
	
	remainder = event->e_interval;
	
	for ( jp = knlqueue_head( &knltim.t_event_queue );
		jp != &event->e_super.o_joint;
		jp = knljoint_next( jp ) )
	{
		remainder += ( (T_EVENT *)jp )->e_interval;
	}
	
	return remainder;
}

/**
 * signal a time manager from system timer interrupt (kernel function).
 */
void knltim_signal( void )
{
	BOOL iterate;
	BIOS_UINT lock;
	
	/* lock CPU. */
	lock = bios_cpu_lock();
	
	/*jp
	 knltim_signal() が処理中であれば, アクセス深度のみを
	 更新 (+1) して処理を終える.
	*/
	iterate = ( knltim.t_nested_depth == 0 ) ? TRUE : FALSE;
	
	knltim.t_nested_depth ++;
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
	
	while ( iterate )
	{
		T_JOINT * jp;
		UW remainder;
		BOOL toiterate;
		
		/* 1: */
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		#if ( UANTS_CFG_TIC_DENO > 1 )
		/*
		 udpate the elapsed time.
		 
 		 ex1). UANTS_CFG_TIC_NUME = 1, UANTS_CFG_TIC_DENO = 1
		  adjust1: 1,,, 1
		  elapsed: 1,,, 1 = 1/1
		  adjust2: 0,,, 0
		 
		 ex2). UANTS_CFG_TIC_NUME = 3, UANTS_CFG_TIC_DENO = 5
		  adjust1: 3, 6, 4, 7, 5,,, 3
		  elapsed: 0, 1, 0, 1, 1,,, 0 = 3/5
		  adjust2: 3, 1, 4, 2, 0,,, 3
		  
		 ex3). UANTS_CFG_TIC_NUME = 7, UANTS_CFG_TIC_DENO = 4
		  adjust1: 7, 10, 9, 8,,, 7
		  elapsed: 1,  2, 2, 2,,, 1 = 7/4
		  adjust2: 3,  2, 1, 0,,, 3
		*/
		knltim.t_adjust += UANTS_CFG_TIC_NUME;
		knltim.t_elapsed = knltim.t_adjust / UANTS_CFG_TIC_DENO;
		knltim.t_adjust %= UANTS_CFG_TIC_DENO;
		#endif
		
		/* update the system time. */
		if ( knltim.t_system.ltime
			> knltim.t_system.ltime + knltim.t_elapsed )
		{
			/* if the lower time is overflowed, increase the upper time. */
			knltim.t_system.utime ++;
		}
		knltim.t_system.ltime += knltim.t_elapsed;
		
		/* this loop consumes the time of time-event. */
		remainder = knltim.t_elapsed;
		for ( jp = knlqueue_head( &knltim.t_event_queue );
			!knlqueue_equals( &knltim.t_event_queue, jp ) && remainder > 0; )
		{
			T_EVENT * event;
			
			event = (T_EVENT *)jp;
			
			if ( remainder < event->e_interval )
			{
				/* time-event has not elapsed yet. finish this loop. */
				event->e_interval -= remainder;
				remainder = 0;
			}
			else
			{
				/* time-event has elapsed. */
				remainder -= event->e_interval;
				event->e_interval = 0;
				jp = knljoint_next( jp );
			}
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		/* this loop issues timeout event. */
		toiterate = TRUE;
		do
		{
			T_EVENT * event;
			
			event = NULL;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( toiterate =
				!knlqueue_is_empty( &knltim.t_event_queue ) )
			{
				event = (T_EVENT *)knlqueue_head( &knltim.t_event_queue );
				
				if ( event->e_interval > 0 )
				{
					/* time-event has not elapsed yet. finish this loop. */
					toiterate = FALSE;
				}
				else
				{
					/* time-event has elapsed. */
					knltim_delete_event( event );
				}
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			/*jp
			 このタイミングで irel_wai() 等のサービスコールがコールされる
			 可能性もあることを uAnts_wait.c: _knlwait_timeout_event() 内
			 で考慮する.
			*/
			
			if ( toiterate && event != NULL )
			{
				/* invoke the elapsed time-event handler. */
				event->e_callback( event );
			}
		}
		while ( toiterate );
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/*jp
		 アクセス深度を -1 した結果が 0 より大きい場合, 
		 knltim_signal() 処理中にシステムタイマ割り込みが
		 入ったことになるので, 再度 1: から処理を行う.
		*/
		if ( -- knltim.t_nested_depth == 0 )
		{
			/*jp アクセス深度が 0 である場合, 処理を終了する. */
			iterate = FALSE;
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
	}
}

/**
 * start the system timer (kernel function).
 */
void knltim_start( void )
{
	T_DINH dinh;
	
	dinh.inhatr = TA_HLNG;
	dinh.inthdr = _knltim_interrupt_handler;
	
	knlinh_define( UANTS_CFG_SYSTEM_TIMER_INHNO, &dinh, NULL );
	
	bios_system_timer_open( UANTS_CFG_TIC_NUME, UANTS_CFG_TIC_DENO );
}

/**
 * stop the system timer (kernel function).
 */
void knltim_stop( void )
{
	if ( bios_system_timer_close() != BIOS_ERROR_OK )
	{
		knlsys_shutdown();
	}
	
	knlinh_define( UANTS_CFG_SYSTEM_TIMER_INHNO, NULL, NULL );
}

/**
 * add time-event to timeout queue with callback (kernel function).
 * 
 * @param event time-event entity
 * @param time time until timeout
 * @param callback callback that is called when addition has completed
 */
void knltim_add_event_with_callback(
	T_EVENT * const event,
	RELTIM timeout,
	void ( * const callback )( T_EVENT * const event ) )
{
	UB nested_depth;
	BOOL found;
	
	/*jp スタックフレームに現在のコンテクストアクセス深度を保存. */
	nested_depth = knlsys_get_nested_depth();
	found = FALSE;
	
	do
	{
		BIOS_UINT lock;
		
		/* 1: */
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		if ( knlqueue_is_empty( &knltim.t_event_queue ) )
		{
			/* there is no time-event in timeout queue. */
			
			knlqueue_add( &knltim.t_event_queue, &event->e_super.o_joint );
			event->e_interval = timeout;
			
			if ( callback != NULL )
			{
				/* addition has completed. */
				callback( event );
			}
			
			/* finish this function. */
			found = TRUE;
		}
		else
		{
			/*jp
			 キューに現在のコンテクストアクセス深度を保存.
			 保存はクリティカルセクションで行う必要がある.
			*/
			knlqueue_set_nested_depth( &knltim.t_event_queue, nested_depth );
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( !found )
		{
			T_JOINT * jp;
			BOOL interrupted;
			
			jp = &knltim.t_event_queue.q_joint;
			
			interrupted = FALSE;
			
			do
			{
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knlqueue_get_nested_depth( &knltim.t_event_queue )
					!= nested_depth )
				{
					/*jp
					 スタックフレームに保存したアクセス深度と, キューに
					 保存したアクセス深度が異なった場合, 更に深度の高い
					 コンテクストがこのキューを改変したと見なし, キュー
					 の走査を 1: からやり直す.
					*/
					interrupted = TRUE;
				}
				else
				{
					jp = knljoint_next( jp );
					
					if ( knlqueue_equals( &knltim.t_event_queue, jp ) )
					{
						/*jp
						 タイムアウトキューを最後まで走査した.
						*/
						found = TRUE;
					}
					else
					{
						T_EVENT * ep;
						
						ep = (T_EVENT *)jp;
						
						if ( timeout < ep->e_interval )
						{
							/*jp
							 設定タイムアウト時間がイベントが持つ
							 インターバル未満である場合,
							 イベントが持つインターバルを設定タイムアウト
							 時間で減算.
							*/
							ep->e_interval -= timeout;
							found = TRUE;
						}
						else
						{
							/*jp
							 設定タイムアウト時間がイベントが持つ
							 インターバル以上である場合,
							 設定タイムアウト時間をイベントが持つ
							 インターバルで減算.
							*/
							timeout -= ep->e_interval;
						}
					}
				}
				
				if ( found )
				{
					/*jp
					 イベントを線形リストで管理しつつ, それぞれのイベント
					 の持つ時間は一つ前に繋がれているイベントの持つ時間と
					 の差分で管理する.
					*/
					
					/* set interval. */
					event->e_interval = timeout;
					
					/* insert a time-event to timeout queue. */
					knlqueue_insert(
						&knltim.t_event_queue, 
						&event->e_super.o_joint,
						jp );
					
					if ( callback != NULL )
					{
						/* addition has completed. */
						callback( event );
					}
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
			}
			while ( !found && !interrupted );
		}
	}
	while ( !found );
}

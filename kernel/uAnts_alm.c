﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_alm.c
 *
 * implementations of MicroAnts kernel alarm handler functions.
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

static void _knlalm_timeout_event(
	T_EVENT * const event );

/**
 * callback on timeout.
 * 
 * @param event time-event entity
 */
static void _knlalm_timeout_event(
	T_EVENT * const event )
{
	T_ALM * alm;
	BOOL iterate;
	BIOS_UINT lock;
	
	alm = (T_ALM *)event;
	
	/* lock CPU. */
	lock = bios_cpu_lock();
	
	/*jp
	 ユーザアラームハンドラが処理中であれば, アクセス深度のみを
	 更新 (+1) して処理を終える.
	 通常このケースの発生率は低いが, ユーザアラームハンドラ内で
	 ハンドラ再登録などをした場合, 少ない可能性ではあるが, 起こ
	 り得る.
	*/
	iterate = ( alm->a_nested_depth == 0 ) ? TRUE : FALSE;
	
	alm->a_nested_depth ++;
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
	
	while ( iterate )
	{
		/* 1: */
		
		if ( alm->a_state == TALM_STA )
		{
			alm->a_state = TALM_STP;
			
			/* invoke alarm handler. */
			alm->a_entry( alm->a_exinf );
		}
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/*jp
		 アクセス深度を -1 した結果が 0 より大きい場合, 
		 ユーザアラームハンドラ処理中にアラームハンドラが呼び出
		 されたことになるので, 再度 1: から処理を行う.
		*/
		if ( -- alm->a_nested_depth == 0 )
		{
			/*jp アクセス深度が 0 である場合, 処理を終了する. */
			iterate = FALSE;
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
	}
}

/**
 * initialize all kernel alarm handler entities (kernel function).
 */
void knlalm_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlalm_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_ALM_MAX_ID; )
	{
		T_ALM * alm;
		
		alm = &knlalm_entities[id ++];
		knlevent_build(
			&alm->a_super,
			id,
			KNLOBJECT_ALM,
			&knlalm_free_queue,
			_knlalm_timeout_event );
	}
}

/**
 * initialize an alarm handler (kernel function).
 * 
 * @param alm alarm handler entity
 * @param pk_calm alarm handler creation information packet
 */
void knlalm_initialize(
	T_ALM * const alm,
	const T_CALM * pk_calm )
{
	knlevent_initialize(
		&alm->a_super,
		&knlalm_free_queue,
		pk_calm->almatr );
	
	alm->a_state = TALM_STP;
	alm->a_exinf = pk_calm->exinf;
	alm->a_entry = ( void ( * )( VP_INT ) )pk_calm->almhdr;
	alm->a_nested_depth = 0;
}

/**
 * finalize an alarm handler (kernel function).
 * 
 * @param alm alarm handler entity
 */
void knlalm_finalize(
	T_ALM * const alm )
{
	knlevent_finalize( &alm->a_super, &knlalm_free_queue );
}

/**
 * create a new alarm handler (kernel function).
 * 
 * @param alm alarm handler entity
 * @param pk_calm alarm handler creation information packet
 * @return error code
 */
ER knlalm_create(
	T_ALM * const alm,
	const T_CALM * pk_calm )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_calm == NULL )
	{
		/* illegal parameter. pk_calm must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_calm->almhdr == NULL )
	{
		/* pk_calm->almhdr must not point to null. */
		ercd = E_PAR;
	}
    else if (pk_calm->almatr & ~( TA_ASM ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		knlalm_initialize( alm, pk_calm );
		
		ercd = E_OK;
	}
	
	return ercd;
}

#endif

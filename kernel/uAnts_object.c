﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_object.c
 *
 * implementations of MicroAnts object functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_object.h"

/**
 * build object.
 * 
 * @param object object entity
 * @param id object ID
 * @param kind kind of object
 * @param queue addition queue
 */
void knlobject_build(
	T_OBJECT * const object,
	ID id,
	UB kind,
	T_QUEUE * const queue )
{
	knljoint_initialize( &object->o_joint );
	object->o_id = id;
	object->o_kind = kind;
	object->o_exist = FALSE;
	
	if ( queue != NULL )
	{
		knlqueue_add( queue, &object->o_joint );
	}
}

/**
 * initialize object.
 * 
 * @param object object entity
 * @param queue deletion queue
 * @param attribute attribute of object
 */
void knlobject_initialize(
	T_OBJECT * const object,
	T_QUEUE * const queue,
	ATR attribute )
{
	knlqueue_delete( queue, &object->o_joint );
	object->o_exist = TRUE;
	object->o_attribute = attribute;
}

/**
 * finalize object.
 * 
 * @param object object entity
 * @param queue addition queue
 */
void knlobject_finalize(
	T_OBJECT * const object,
	T_QUEUE * const queue )
{
	object->o_exist = FALSE;
	object->o_attribute = TA_NULL;
	knlqueue_add( queue, &object->o_joint );
}

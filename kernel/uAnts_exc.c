﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_exc.c
 *
 * implementations of MicroAnts kernel cpu exception handler functions.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_exc.h"
#include "uAnts_hook.h"

#if ( UANTS_CFG_EXC_MAX_ID > 0 )

/**
 * initialize all kernel cpu exception handler entities (kernel function).
 */
void knlexc_init_entities( void )
{
	ID id;
	
	for ( id = 0; id < UANTS_CFG_EXC_MAX_ID; )
	{
		knlexc_define( id ++, NULL );
	}
}

/**
 * define a cpu exception handler (kernel function).
 * 
 * @param excno cpu exception handler number
 * @param pk_dexc cpu exception handler definition information packet
 */
void knlexc_define(
	EXCNO excno,
	const T_DEXC * pk_dexc )
{
	T_EXC * exc;
	ATR attribute;
	void ( * exchdr )( EXCNO );
	
	if ( pk_dexc != NULL )
	{
		attribute = pk_dexc->excatr;
		exchdr = ( void ( * )( EXCNO ) )pk_dexc->exchdr;
	}
	else
	{
		attribute = 0;
		/* register dummy cpu exception handler. */
		exchdr = knlexc_undefined;
	}
	
	exc = &knlexc_entities[excno];
	exc->e_attribute = attribute;
	exc->e_entry = exchdr;
}

/**
 * invoke a cpu exception handler (kernel function).
 * 
 * @param excno cpu exception handler number
 */
void knlexc_invoke(
	EXCNO excno )
{
	T_EXC * exc;
	
	knlsys_increment_nested_depth();
	
	exc = &knlexc_entities[excno];
	
	if ( excno <= UANTS_CFG_EXC_MAX_ID )
	{
		/* invoke a cpu exception handler. */
		exc->e_entry( excno );
	}
	else
	{
		knlexc_undefined( excno );
	}
	
	knlsys_decrement_nested_depth();
}

/**
 * dummy handler for undefined exception (kernel function).
 * 
 * @param excno cpu exception handler number
 */
void knlexc_undefined(
	EXCNO excno )
{
	cfghook_undefined_exception( excno );
}

#endif

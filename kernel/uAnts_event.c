﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_event.c
 *
 * implementations of MicroAnts time-event functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_event.h"

/**
 * build time-event.
 * 
 * @param event time-event entity
 * @param id time-event ID
 * @param kind kind of time-event
 * @param queue addition queue
 * @param callback callback, when time-event occurred
 */
void knlevent_build(
	T_EVENT * const event,
	ID id,
	UB kind,
	T_QUEUE * const queue,
	void ( * callback )( T_EVENT * const ) )
{
	knlobject_build( &event->e_super, id, kind, queue );
	event->e_interval = 0;
	event->e_callback = callback;
}

/**
 * initialize time-event.
 * 
 * @param event time-event entity
 * @param queue deletion queue
 * @param attribute attribute of time-event
 */
void knlevent_initialize(
	T_EVENT * const event,
	T_QUEUE * const queue,
	ATR attribute )
{
	knlobject_initialize( &event->e_super, queue, attribute );
}

/**
 * finalize time-event.
 * 
 * @param event time-event entity
 * @param queue addition queue
 */
void knlevent_finalize(
	T_EVENT * const event,
	T_QUEUE * const queue )
{
	event->e_interval = 0;
	knlobject_finalize( &event->e_super, queue );
}

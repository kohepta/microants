﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mbx.c
 *
 * implementations of MicroAnts kernel mailbox functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * initialize all kernel mailbox entities (kernel function).
 */
void knlmbx_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlmbx_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_MBX_MAX_ID; )
	{
		T_MBX * mbx;
		
		mbx = &knlmbx_entities[id ++];
		knlobject_build(
			&mbx->m_super,
			id,
			KNLOBJECT_MBX,
			&knlmbx_free_queue );
	}
}

/**
 * initialize a mailbox (kernel function).
 * 
 * @param mbx mailbox entity
 * @param pk_cmbx mailbox creation information packet
 */
void knlmbx_initialize(
	T_MBX * const mbx,
	const T_CMBX * pk_cmbx )
{
	knlobject_initialize(
		&mbx->m_super,
		&knlmbx_free_queue, 
		pk_cmbx->mbxatr );
	knlqueue_initialize(
		&mbx->m_task_queue,
		knlutil_test_bit_16( pk_cmbx->mbxatr, TA_TPRI ) );
	knlqueue_initialize(
		&mbx->m_message_queue,
		knlutil_test_bit_16( pk_cmbx->mbxatr, TA_MPRI ) );
	
	mbx->m_max_message_priority = pk_cmbx->maxmpri;
}

/**
 * finalize a mailbox (kernel function).
 * 
 * @param mbx mailbox entity
 */
void knlmbx_finalize(
	T_MBX * const mbx )
{
	knlobject_finalize( &mbx->m_super, &knlmbx_free_queue );
}

/**
 * create a new mailbox (kernel function).
 * 
 * @param mbx mailbox entity
 * @param pk_cmbx mailbox creation information packet
 * @return error code
 */
ER knlmbx_create(
	T_MBX * const mbx,
	const T_CMBX * pk_cmbx )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_cmbx == NULL )
	{
		/* pk_cmbx must not point to null. */
		ercd = E_PAR;
	}
	else if ( ( pk_cmbx->mbxatr & TA_MPRI )
		&& ( ( pk_cmbx->maxmpri < TMIN_MPRI )
			|| ( pk_cmbx->maxmpri > TMAX_MPRI ) ) )
	{
		/* message priority is outside effective range. */
		ercd = E_PAR;
	}
#if 0
	else if ( !( pk_cmbx->mbxatr & TA_MPRI )
		&& ( pk_cmbx->maxmpri != 0 ) )
	{
		/* if TA_MFIFO is specified, max message priority must be zero. */
		ercd = E_PAR;
	}
#endif
	else if ( pk_cmbx->mbxatr & ~( TA_TFIFO | TA_TPRI | TA_MPRI ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		knlmbx_initialize( mbx, pk_cmbx );
		
		ercd = E_OK;
	}
	
	return ercd;
}

/**
 * receive a message from a mailbox (kernel function).
 * 
 * @param mbxid mailbox ID
 * @param ppk_msg pointer of pointer to received message
 * @param event time-event entity
 * @param timeout time until timetout
 * @return error code
 */
ER knlmbx_receive(
    ID mbxid,
    T_MSG * * ppk_msg,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( ppk_msg == NULL )
	{
		/* ppk_msg must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		T_MBX * mbx;
		
		mbx = knlmbx_get_entity( mbxid );
		
		if ( mbx == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			T_MBX_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_MBX,
				&mbx->m_super,
				&mbx->m_task_queue,
				NULL );
			
			/*
			 wait-controller (mailbox extended).
			 copy the pointer of pointer to received message
			 to wait-controller.
			*/
			wait.w_ppk_msg = ppk_msg;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mbx->m_super ) )
			{
				/* mailbox has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else
			{
				if ( !knlqueue_is_empty( &mbx->m_message_queue ) )
				{
					/* there are message(s). */
					
					T_MSG * msg;
					
					/* receive message from mailbox directly. */
					msg = (T_MSG *)knlqueue_head( &mbx->m_message_queue );
					knlqueue_delete( &mbx->m_message_queue, &msg->g_joint );
					*ppk_msg = msg;
					
					/* clear message's magic number. */
					msg->g_magic = 0;
				}
				else
				{
					/* there are no message(s), then do wait. */
					knltsk_wait( &wait.w_super, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
		}
	}
	
	return ercd;
}

/**
 * send a message to a mailbox (kernel function).
 * 
 * @param mbxid mailbox ID
 * @param pk_msg message packet
 * @return error code
 */
ER knlmbx_send(
	ID mbxid,
	T_MSG * pk_msg )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_msg == NULL )
	{
		/* illegal parameter. pk_msg must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_msg->g_magic == (UINT)KNLMBX_MAGIC )
	{
		/* message has already been sent to a mailbox. */
		ercd = E_PAR;
	}
	else
	{
		T_MBX * mbx;
		
		mbx = knlmbx_get_entity( mbxid );
		
		if ( mbx == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mbx->m_super ) )
			{
				/* mailbox has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BOOL order;
				PRI msgpri;
				
				ercd = E_OK;
				
				order = knlqueue_get_order( &mbx->m_message_queue );
				
				if ( order == KNLQUEUE_ORDER_PRIORITY )
				{
					/* message queue is priority order. */
					
					msgpri = ( (T_MSG_PRI *)pk_msg )->msgpri;
					
					if ( ( msgpri <= 0 )
						|| ( msgpri > mbx->m_max_message_priority ) )
					{
						/* message priority is outside effective range. */
						ercd = E_PAR;
					}
				}
				else
				{
					msgpri = 0;
				}
				
				if ( ercd == E_OK )
				{
					BOOL ready;
					BIOS_UINT lock;
					T_TSK * tsk;
					
					ready = FALSE;
					
					/* lock CPU. */
					lock = bios_cpu_lock();
					
					if ( knlqueue_is_empty( &mbx->m_task_queue ) )
					{
						/* there are no waiting tasks. */
						tsk = NULL;
					}
					else
					{
						/* there are waiting tasks. */
						
						const T_MBX_WAIT * wait;
						
						/* send message to waiting task directly. */
						tsk = (T_TSK *)knlqueue_head( &mbx->m_task_queue );
						wait = ( T_MBX_WAIT *)tsk->t_wait;
						*wait->w_ppk_msg = pk_msg;
						
						/*jp 待ち状態終了処理実行. */
						ready = knltsk_quit_waiting( tsk, E_OK );
					}
					
					/* unlock CPU. */
					bios_cpu_unlock( lock );
					
					if ( ready )
					{
						/*jp 待ち状態を終了したタスクをレディ状態にする. */
						knltsk_ready( tsk );
					}
					
					if ( tsk == NULL )
					{
						/* queuing message to send to message queue. */
						
						T_JOINT * jp;
						
						jp = &mbx->m_message_queue.q_joint;
						
						if ( order == KNLQUEUE_ORDER_PRIORITY )
						{
							/* message queue is priority order. */
							
							if ( !knlqueue_is_empty( &mbx->m_message_queue )
								&& ( msgpri <
									( (T_MSG_PRI *)knlqueue_tail(
										&mbx->m_message_queue ) )->msgpri ) )
							{
								BOOL iterate;
								
								iterate = TRUE;
								
								do
								{
									/*
									 find appropriate insertion position
									 of message queue.
									*/
									
									const T_MSG_PRI * mp;
									
									jp = knljoint_next( jp );
									
									mp = (T_MSG_PRI *)jp;
									
									if ( msgpri < mp->msgpri )
									{
										/*
										 appropriate insertion position
										 was found.
										*/
										iterate = FALSE;
									}
								}
								while ( iterate );
							}
						}
						
						/*
						 insert the message in the appropriate position
						 of message queue.
						*/
						knlqueue_insert(
							&mbx->m_message_queue,
							&pk_msg->g_joint,
							jp );
						
						/* set magic number to message. */
						pk_msg->g_magic = (UINT)KNLMBX_MAGIC;
					}
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_start.c
 *
 * entry point of MicroAnts Kernel start function.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sys.h"
#include "uAnts_tim.h"
#include "uAnts_hook.h"

/**
 * main routine of MicroAnts kernel.
 * 
 * this function is called by startup routine.
 */
void main( void )
{
	UH i;
	UH entities;
	
	cfghook_pre_system_initialize();
	
	/*jp
	 カーネルが利用するハードウェア資源
	 (interrupt controller, system timer ,,,) を初期化.
	*/
	if ( bios_initialize() != BIOS_ERROR_OK )
	{
		knlsys_shutdown();
	}
	
	knlsys.s_initialized = FALSE;
	
	/* disable dispatch. */
	knlsys_disable_dispatch();
	
	/*jp コンテクストアクセス深度を初期化. */
	knlsys_initialize_nested_depth();
	
	/* initialize kernel time management module. */
	knltim_initialize();
	
	/* initialize all kernel module */
	for ( i = 0; knlsys_init_entities[i] != NULL; i ++ )
	{
		knlsys_init_entities[i]();
	}
	
	knlsys_initialize();
	
	cfghook_post_system_initialize();
	
	/* enable dispatch. */
	knlsys_enable_dispatch();
	
	/* start task scheduling. */
	knlcpu_dispatch_from_main();
	
	knlsys_shutdown();
}

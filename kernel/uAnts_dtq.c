﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dtq.c
 *
 * implementations of MicroAnts kernel data-queue functions.
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
#include "uAnts_mpl.h"
#endif

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

static BOOL _knldtq_rcv_check_waitable(
	const T_WAIT * const wait );

static BOOL _knldtq_rcv_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/**
 * can enter state of wait?.
 * 
 * @param wait wait-controller entity
 * @retval TRUE can enter state of wait
 * @retval FALSE can not enter state of wait
 */
static BOOL _knldtq_rcv_check_waitable(
	const T_WAIT * const wait )
{
	T_DTQ * dtq;
	BOOL waitable;
	
	dtq = (T_DTQ *)knlwait_get_object( wait );
	
	if ( dtq->d_origin != NULL
		&& !knldtq_is_empty( dtq ) )
	{
		knldtq_read( dtq, ( (T_DTQ_RCV_WAIT *)wait )->w_p_data );
		
		waitable = FALSE;
	}
	else
	{
		waitable = TRUE;
	}
	
	return waitable;
}

/**
 * individual processing in wait release.
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return status code
 */
static BOOL _knldtq_rcv_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	BOOL status;
	
	status = FALSE;
	
	switch ( purpose )
	{
	case purpose_CHECK_WAITABLE:
		status = _knldtq_rcv_check_waitable( wait );
		break;
	case purpose_TIMEOUT:
	case purpose_WAITING_RELEASED:
	case purpose_PRIORITY_CHANGED:
	case purpose_TERMINATED:
	default:
		break;
	}
	
	return status;
}

/**
 * initialize all kernel data-queue entities (kernel function).
 */
void knldtq_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knldtq_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_DTQ_MAX_ID; )
	{
		T_DTQ * dtq;
		
		dtq = &knldtq_entities[id ++];
		knlobject_build(
			&dtq->d_super,
			id,
			KNLOBJECT_DTQ,
			&knldtq_free_queue );
	}
}

/**
 * intialize a data-queue (kernel function).
 * 
 * @param dtq data-queue entity
 * @param pk_cdtq data-queue creation information packet
 * @param data-queue buffer
 */
void knldtq_initialize(
	T_DTQ * const dtq,
	const T_CDTQ * pk_cdtq,
	VP vp )
{
	knlobject_initialize(
		&dtq->d_super,
		&knldtq_free_queue, 
		pk_cdtq->dtqatr );
	knlqueue_initialize(
		&dtq->d_snd_task_queue,
		knlutil_test_bit_16( pk_cdtq->dtqatr, TA_TPRI ) );
	knlqueue_initialize( &dtq->d_rcv_task_queue, KNLQUEUE_ORDER_FIFO );
	
	dtq->d_count = pk_cdtq->dtqcnt;
	dtq->d_origin = (VP *)( ( vp == NULL ) ? pk_cdtq->dtq : vp );
	dtq->d_read = dtq->d_origin;
	dtq->d_write = dtq->d_origin;
	dtq->d_sent_count = 0;
}

/**
 * finalize a data-queue (kernel function).
 * 
 * @param dtq data-queue entity
 */
void knldtq_finalize(
	T_DTQ * const dtq )
{
	knlobject_finalize( &dtq->d_super, &knldtq_free_queue );
}

/**
 * create a new data-queue (kernel function).
 * 
 * @param dtq data-queue entity
 * @param pk_cdtq data-queue creation information packet
 * @return error code
 */
ER knldtq_create(
	T_DTQ * const dtq,
	const T_CDTQ * pk_cdtq )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_cdtq == NULL )
	{
		/* pk_cdtq must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_cdtq->dtqcnt == 0
		&& pk_cdtq->dtq != NULL )
	{
		/*
		 when data-queue buffer count(size) is zero,
		 data-queue buffer address must be null.
		*/
		ercd = E_PAR;
	}
	#if ( !UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
	else if ( pk_cdtq->dtqcnt > 0
		&& pk_cdtq->dtq == NULL )
	{
		/* illegal parameter */
		ercd = E_PAR;
	}
	#endif
	else if ( !knlutil_is_aligned( (UW)pk_cdtq->dtq ) )
	{
		/* data-queue buffer is not aligned. */
		ercd = E_PAR;
	}
	else if ( pk_cdtq->dtqatr & ~( TA_TFIFO | TA_TPRI ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		VP vp;
		
		vp = NULL;
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
		if ( pk_cdtq->dtqcnt != 0
			&& pk_cdtq->dtq == NULL  )
		{
			/* allocate data-queue buffer. */
			vp = knlmpl_allocate(
				knlmpl_get_generic_entity(),
				(UW)TSZ_DTQ( pk_cdtq->dtqcnt ),
				NULL );
		}
		
		if ( pk_cdtq->dtqcnt != 0
			&& pk_cdtq->dtq == NULL
			&& vp == NULL )
		{
			/* insufficient memory. */
			ercd = E_NOMEM;
		}
		else
		#endif
		{
			BIOS_UINT lock;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			knldtq_initialize( dtq, pk_cdtq, vp );
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			ercd = E_OK;
		}
	}
	
	return ercd;
}

/**
 * send a data forcibly to a data-queue (kernel function).
 * 
 * @param dtqid data-queue ID
 * @param data send data 
 * @return error code
 */
ER knldtq_forcibly_send(
	ID dtqid,
	VP_INT data )
{
	ER ercd;
	T_DTQ * dtq;
	
	dtq = knldtq_get_entity( dtqid );
	
	if ( dtq == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &dtq->d_super ) )
		{
			/* data-queue has not been created. */
			ercd = E_NOEXS;
		}
		else if ( dtq->d_origin == NULL )
		{
			/*jp 強制送信は容量 0 のデータキューに対しては使用不可. */
			ercd = E_ILUSE;
		}
		else
		{
			T_TSK * tsk;
			BOOL ready;
			
			tsk = NULL;
			
			/* send data to task waiting to receive data. */
			tsk = knldtq_send_to_receiver_directly( dtq, data, &ready );
			
			if ( ready )
			{
				/*jp
				 受信完了待ちタスクが存在し, その待ち状態が解除された場合,
				 そのタスクをレディ状態にする.
				*/
				knltsk_ready( tsk );
			}
			
			if ( tsk == NULL )
			{
				/* there was no task waiting to receive data. */
				
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knldtq_is_full( dtq ) )
				{
					/*
					 if data-queue buffer is full,
					 delete first data then write new one forcibly.
					*/
					knldtq_read( dtq, NULL );
				}
				
				/* write data to data-queue. */
				knldtq_write( dtq, (VP)data );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
			}
			
			ercd = E_OK;
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

/**
 * read a data from a data-queue (kernel function).
 * 
 * @param dtq data-queue entity
 * @param p_data pointer of pointer to read data
 */
void knldtq_read(
	T_DTQ * const dtq,
	VP * p_data )
{
	if ( p_data != NULL )
	{
		/* read a data from a data-queue */
		*p_data = *dtq->d_read;
	}
	
	dtq->d_read ++;
	
	if ( dtq->d_read == ( dtq->d_origin + dtq->d_count ) )
	{
		/* if read pointer reaches to the end, rotate it. */
		dtq->d_read = dtq->d_origin;
	}
	
	dtq->d_sent_count --;
}

/**
 * receive a data from a data-queue (kernel function).
 * 
 * @param dtqid data-queue ID
 * @param p_data pointer of pointer to receive data
 * @param event time-event entity
 * @param timeout time until timeout
 * @return error code
 */
ER knldtq_receive(
    ID dtqid,
    VP_INT * p_data,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( p_data == NULL )
	{
		/* illegal parameter. p_data must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		T_DTQ * dtq;
		
		dtq = knldtq_get_entity( dtqid );
		
		if ( dtq == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			T_DTQ_RCV_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_RDTQ,
				&dtq->d_super,
				&dtq->d_rcv_task_queue,
				_knldtq_rcv_wait_callback );
			
			/*
			 wait-controller (data-queue extended).
			 copy the pointer of pointer to received data to wait-controller.
			*/
			wait.w_p_data = (VP *)p_data;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &dtq->d_super ) )
			{
				/* data-queue has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else
			{
				BOOL ready;
				BOOL waitable;
				BIOS_UINT lock;
				T_TSK * tsk;
				
				ready = FALSE;
				waitable = FALSE;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( _knldtq_rcv_check_waitable( &wait.w_super ) )
				{
					/* can enter state of wait. */
					
					if ( !knlqueue_is_empty( &dtq->d_snd_task_queue ) )
					{
						/* there was task waiting to send data. */
						
						T_DTQ_SND_WAIT * wp;
						
						/* receive a data from sender task directly. */
						tsk = (T_TSK *)knlqueue_head(
							&dtq->d_snd_task_queue );
						wp = (T_DTQ_SND_WAIT *)tsk->t_wait;
						*p_data = (VP_INT)wp->w_data;
						
						/*jp 送信完了待ち状態終了処理実行. */
						ready = knltsk_quit_waiting( tsk, E_OK );
					}
					else
					{
						/* there was no task waiting to send data. */
						waitable = TRUE;
					}
				}
				else
				{
					/* can not enter state of wait. */
					
					if ( !knlqueue_is_empty( &dtq->d_snd_task_queue ) )
					{
						/* there was task waiting to send data. */
						
						T_DTQ_SND_WAIT * wp;
						
						/* write a sent data to a data-queue. */
						tsk = (T_TSK *)knlqueue_head(
							&dtq->d_snd_task_queue );
						wp = (T_DTQ_SND_WAIT *)tsk->t_wait;
						knldtq_write( dtq, wp->w_data );
						
						/*jp 送信完了待ち状態終了処理実行. */
						ready = knltsk_quit_waiting( tsk, E_OK );
					}
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( ready )
				{
					/*jp 送信待ち状態を終了したタスクをレディ状態にする. */
					knltsk_ready( tsk );
				}
				
				if ( waitable )
				{
					/* do wait. */
					knltsk_wait( &wait.w_super, timeout );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
		}
	}
	
	return ercd;
}

/**
 * send a data to a data-queue (kernel function).
 * 
 * @param dtqid data-queue ID
 * @param data send data
 * @param envent time-event entity
 * @param timeout time until timeout
 * @return error code
 */
ER knldtq_send(
	ID dtqid,
	VP_INT data,
	T_EVENT * const event,
	TMO timeout )
{
	ER ercd;
	
	if ( knlsys_sense_dispatch_pending() && timeout != TMO_POL )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_DTQ * dtq;
		
		dtq = knldtq_get_entity( dtqid );
		
		if ( dtq == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			T_DTQ_SND_WAIT wait;
			BOOL disp;
			
			/* wait-controller is builded on a stack frame. */
			knlwait_build(
				&wait.w_super,
				event,
				TTW_SDTQ,
				&dtq->d_super,
				&dtq->d_snd_task_queue,
				NULL );
			
			/*
			 wait-controller (data-queue extended).
			 copy the pointer to sent data to wait-controller.
			*/
			wait.w_data = (VP)data;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &dtq->d_super ) )
			{
				/* data-queue has not been created. */
				knlwait_set_result( &wait.w_super, E_NOEXS );
			}
			else
			{
				T_TSK * tsk;
				BOOL ready;
				
				tsk = NULL;
				
				/* send data to task waiting to receive data. */
				tsk = knldtq_send_to_receiver_directly( dtq, data, &ready );
				
				if ( ready )
				{
					/*jp
					 受信完了待ちタスクが存在し, その待ち状態が解除された場合,
					 そのタスクをレディ状態にする.
					*/
					knltsk_ready( tsk );
				}
				
				if ( tsk == NULL )
				{
					/* there was no task waiting to receive data. */
					
					BOOL waitable;
					BIOS_UINT lock;
					
					waitable = FALSE;
					
					/* lock CPU. */
					lock = bios_cpu_lock();
					
					if ( dtq->d_origin != NULL
						&& !knldtq_is_full( dtq ) )
					{
						/*
						 if data-queue buffer is can be used,
						 write a data to a data-queue.
						*/
						knldtq_write( dtq, data );
					}
					else
					{
						/* data-queue buffer is can not be used */
						waitable = TRUE;
					}
					
					/* unlock CPU. */
					bios_cpu_unlock( lock );
					
					if ( waitable )
					{
						/* do wait. */
						knltsk_wait( &wait.w_super, timeout );
					}
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
			
			/* get wait result. */
			ercd = knlwait_get_result( &wait.w_super );
		}
	}
	
	return ercd;
}

/**
 * send data to task waiting to receive data directly (kernel function).
 * 
 * @param dtq data-queue entity
 * @param data send data
 * @param ready TRUE: can make ready FALSE: can not make ready
 * @return task entity waiting to receive data
 */
T_TSK * knldtq_send_to_receiver_directly(
	T_DTQ * const dtq,
	VP_INT data,
	BOOL * ready )
{
	T_TSK * tsk;
	BIOS_UINT lock;
	
	/* lock CPU. */
	lock = bios_cpu_lock();
	
	if ( knlqueue_is_empty( &dtq->d_rcv_task_queue ) )
	{
		/* there was no task waiting to receive data. */
		
		tsk = NULL;
		*ready = FALSE;
	}
	else
	{
		/* there was task waiting to receive data. */
		
		T_DTQ_RCV_WAIT * wait;
		
		/* receive a data from sender task directly. */
		tsk = (T_TSK *)knlqueue_head( &dtq->d_rcv_task_queue );
		wait = (T_DTQ_RCV_WAIT *)tsk->t_wait;
		*wait->w_p_data = (VP)data;
		
		/*jp 受信完了待ち状態終了処理実行. */
		*ready = knltsk_quit_waiting( tsk, E_OK );
	}
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
	
	return tsk;
}

/**
 * writa a data to a data-queue (kernel function).
 * 
 * @param dtq data-queue entity
 * @param data pointer to write data
 */
void knldtq_write(
	T_DTQ * const dtq,
	VP data )
{
	/* writa a data to a data-queue. */
	*dtq->d_write ++ = data;
	
	if ( dtq->d_write == dtq->d_origin + dtq->d_count )
	{
		/* if write pointer reaches to the end, rotate it. */
		dtq->d_write = dtq->d_origin;
	}
	
	dtq->d_sent_count ++;
}

#endif

﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cyc.c
 *
 * implementations of MicroAnts kernel cyclic handler functions.
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_cyc.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

static void _knlcyc_timeout_event(
	T_EVENT * const event );

static void _knlcyc_start_event(
	T_EVENT * const event );

/**
 * callback on timeout.
 * 
 * @param event time-event entity
 */
static void _knlcyc_timeout_event(
	T_EVENT * const event )
{
	T_CYC * cyc;
	BOOL iterate;
	BIOS_UINT lock;
	
	cyc = (T_CYC *)event;
	
	/* lock CPU. */
	lock = bios_cpu_lock();
	
	/*jp
	 ユーザ周期ハンドラが処理中であれば, アクセス深度のみを
	 更新 (+1) して処理を終える.
	*/
	iterate = ( cyc->c_nested_depth == 0 ) ? TRUE : FALSE;
	
	cyc->c_nested_depth ++;
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
	
	while ( iterate )
	{
		RELTIM time;
		
		/* 1: */
		
		#if ( UANTS_CFG_TIC_NUME > 1 )
		if ( cyc->c_time > UANTS_CFG_TIC_NUME )
		{
			time = cyc->c_time_next;
			
			/* time until the next starting the handler is adjusted. */
			knlcyc_adjust_time_until_next( cyc, time );
		}
		else
		#endif
		{
			time = cyc->c_time;
		}
		
		/* add again. */
		knltim_add_event( &cyc->c_super, time );
		
		if ( cyc->c_state == TCYC_STA )
		{
			/* invoke cyclic handler. */
			cyc->c_entry( cyc->c_exinf );
		}
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/*jp
		 アクセス深度を -1 した結果が 0 より大きい場合, 
		 ユーザ周期ハンドラ処理中に周期ハンドラが呼び出されたこ
		 とになるので, 再度 1: から処理を行う.
		*/
		if ( -- cyc->c_nested_depth == 0 )
		{
			/*jp アクセス深度が 0 である場合, 処理を終了する. */
			iterate = FALSE;
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
	}
}

/**
 * callback. when the addition to timeout queue was completed, it is called.
 * 
 * @param event time-event entity
 */
static void _knlcyc_start_event(
	T_EVENT * const event )
{
	T_CYC * cyc;
	
	cyc = (T_CYC *)event;
	
	/* start a cyclic handler. */
	cyc->c_state = TCYC_STA;
}

/**
 * initialize all kernel cyclic handler entities (kernel function).
 */
void knlcyc_init_entities( void )
{
	ID id;
	
	knlqueue_initialize( &knlcyc_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < UANTS_CFG_CYC_MAX_ID; )
	{
		T_CYC * cyc;
		
		cyc = &knlcyc_entities[id ++];
		knlevent_build(
			&cyc->c_super,
			id,
			KNLOBJECT_CYC,
			&knlcyc_free_queue,
			_knlcyc_timeout_event );
	}
}

/**
 * initialize a cyclic handler (kernel function).
 * 
 * @param cyc cyclic handler entity
 * @param pk_ccyc cyclic handler creation information packet
 */
void knlcyc_initialize(
	T_CYC * const cyc,
	const T_CCYC * pk_ccyc )
{
	knlevent_initialize(
		&cyc->c_super,
		&knlcyc_free_queue,
		pk_ccyc->cycatr );
	
	cyc->c_state = TCYC_STP;
	cyc->c_exinf = pk_ccyc->exinf;
	cyc->c_entry = ( void ( * )( VP_INT ) )pk_ccyc->cychdr;
	cyc->c_time = pk_ccyc->cyctim;
	#if ( UANTS_CFG_TIC_NUME > 1 )
	cyc->c_time_next = 0;
	cyc->c_adjust = 0;
	#endif
	cyc->c_nested_depth = 0;
}

/**
 * finalize a cyclic handler (kernel function).
 * 
 * @param cyc cyclic handler entity 
 */
void knlcyc_finalize(
	T_CYC * const cyc )
{
	knlevent_finalize( &cyc->c_super, &knlcyc_free_queue );
}

/**
 * create a new cyclic handler (kernel function).
 * 
 * @param cyc cyclic handler entity
 * @param pk_ccyc cyclic handler creation information packet
 * @return error code
 */
ER knlcyc_create(
	T_CYC * const cyc,
	const T_CCYC * pk_ccyc )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_ccyc == NULL )
	{
		/* illegal parameter. pk_ccyc must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_ccyc->cychdr == NULL )
	{
		/* pk_ccyc->cychdr must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_ccyc->cyctim == 0 )
	{
		/* activation cycle must be larger than zero. */
		ercd = E_PAR;
	}
    else if (pk_ccyc->cycatr & ~( TA_ASM | TA_STA | TA_PHS ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		knlcyc_initialize( cyc, pk_ccyc );
		
		if ( pk_ccyc->cycatr & TA_STA )
		{
			knlcyc_start( cyc, pk_ccyc->cycphs );
		}
		else
		{
			if ( knlcyc_attribute_preserve_phase( cyc ) )
			{
				#if ( UANTS_CFG_TIC_NUME > 1 )
				/* set time to start the handler. */
				knlcyc_adjust_time_until_next( cyc, pk_ccyc->cycphs );
				#endif
				
				/* cyclic handler start preparation completion. */
				knltim_add_event( &cyc->c_super, pk_ccyc->cycphs );
			}
		}
		
		ercd = E_OK;
	}
	
	return ercd;
}

/**
 * start a cyclic handler (kernel function).
 * 
 * @param cyc cyclic handler entity
 * @param timeout time until timeout
 */
void knlcyc_start(
	T_CYC * const cyc,
	RELTIM timeout )
{
	#if ( UANTS_CFG_TIC_NUME > 1 )
	/* set time to start the handler. */
	knlcyc_adjust_time_until_next( cyc, timeout );
	#endif
	
	knltim_add_event_with_callback(
		&cyc->c_super, 
		timeout,
		_knlcyc_start_event );
}

#endif

﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tsk.c
 *
 * implementations of MicroAnts kernel task functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"
#include "uAnts_hook.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
#include "uAnts_mpl.h"
#endif
#include "uAnts_tim.h"

static BOOL _knltsk_check_waitable(
	const T_WAIT * const wait );

static BOOL _knltsk_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose );

/**
 * can enter state of wait?
 * 
 * @param wait wait-controller entity
 * @retval TRUE can enter state of wait
 * @retval FALSE can not enter state of wait
 */
static BOOL _knltsk_check_waitable(
	const T_WAIT * const wait )
{
	T_TSK * tsk;
	BOOL waitable;
	
	/* task is gotten from ID. */
	tsk = knltsk_get_entity(
			knlevent_get_id( knlwait_get_event( wait ) ) );
	
	if ( tsk->t_wakeup_count > 0 )
	{
		tsk->t_wakeup_count --;
		
		waitable = FALSE;
	}
	else
	{
		waitable = TRUE;
	}
	
	return waitable;
}

/**
 * individual processing in wait release.
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return status code
 */
static BOOL _knltsk_wait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	BOOL status;
	
	status = FALSE;
	
	switch ( purpose )
	{
	case purpose_CHECK_WAITABLE:
		status = _knltsk_check_waitable( wait );
		break;
	case purpose_TIMEOUT:
	case purpose_WAITING_RELEASED:
	case purpose_PRIORITY_CHANGED:
	case purpose_TERMINATED:
	default:
		break;
	}
	
	return status;
}

/**
 * initialize all kernel task entities (kernel function).
 */
void knltsk_init_entities( void )
{
	ID id;
	T_TSK * tsk;
	T_CTSK ctsk;
	
	knltsk_current = NULL;
	knlqueue_initialize( &knltsk_free_queue, KNLQUEUE_ORDER_FIFO );
	knlqueue_initialize( &knltsk_ready_queue, KNLQUEUE_ORDER_PRIORITY );
	
	for ( id = 0; id < KNLTSK_MAX_ID; )
	{
		tsk = &knltsk_entities[id ++];
		knlobject_build(
			&tsk->t_super,
			id,
			KNLOBJECT_TSK,
			&knltsk_free_queue );
	}
	
	/* get static root task entity. */
 	tsk = knltsk_get_entity( KNLTSK_STATIC_ROOT_ENTITY_ID );
	
	/* start static root task entry. */
	ctsk.tskatr = TA_HLNG | TA_ACT;
	ctsk.exinf = NULL;
 	ctsk.task = (FP)knltsk_static_root_entry;
	ctsk.itskpri = TMIN_TPRI;
	ctsk.stksz = UANTS_CFG_TSK_STATIC_INIT_ENTRY_STACK_SIZE;
	ctsk.stk = (VP)( UANTS_CFG_TSK_STATIC_INIT_ENTRY_STACK_ORIGIN );
	
	/*jp 静的ルートタスクの起床準備を行いレディ状態にする. */
	knltsk_initialize( tsk, &ctsk, NULL );
	knlcpu_task_activation_preparation( tsk );
	knltsk_ready( tsk );
}

/**
 * initialize a task (kernel function).
 * 
 * @param tsk task entity
 * @param pk_ctsk task creation information packet
 * @param stack task stack
 */
void knltsk_initialize(
	T_TSK * const tsk,
	const T_CTSK * pk_ctsk,
	VP stack )
{
	knlobject_initialize(
		&tsk->t_super,
		&knltsk_free_queue,
		pk_ctsk->tskatr );
	
	tsk->t_exinf = pk_ctsk->exinf;
	tsk->t_entry = ( void ( * )( VP_INT ) )pk_ctsk->task;
	tsk->t_initial_priority = pk_ctsk->itskpri;
	tsk->t_base_priority = tsk->t_initial_priority;
	tsk->t_priority = tsk->t_initial_priority;;
	tsk->t_stack_size = pk_ctsk->stksz;
	tsk->t_stack_origin = ( stack != NULL ) ? stack : pk_ctsk->stk;
	knltsk_set_state( tsk, TTS_DMT );
	tsk->t_activate_count = 0;
	tsk->t_wakeup_count = 0;
	tsk->t_suspend_count = 0;
	tsk->t_wait = NULL;
	
	knltex_define( &tsk->t_tex, NULL );
}

/**
 * finalize a task (kernel function).
 * 
 * @param tsk task entity
 */
void knltsk_finalize(
	T_TSK * const tsk )
{
	knlobject_finalize( &tsk->t_super, &knltsk_free_queue );
}

/**
 * create a new task (kernel function).
 * 
 * @param tsk task entity
 * @param pk_ctsk task creation information packet
 * @return error code
 */
ER knltsk_create(
	T_TSK * const tsk,
	const T_CTSK * pk_ctsk )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_ctsk == NULL )
	{
		/* illegal parameter. pk_ctsk must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_ctsk->task == NULL )
	{
		/* illegal parameter. pk_ctsk->task must not poit to null. */
		ercd = E_PAR;
	}
	else if ( pk_ctsk->itskpri < TMIN_TPRI
		|| pk_ctsk->itskpri > TMAX_TPRI )
	{
		/* initial priority is outside effective range. */
		ercd = E_PAR;
	}
	else if ( pk_ctsk->stksz < knlcpu_get_minimum_stack_size() )
	{
		/* stack size is too small. */
		ercd = E_PAR;
	}
	else if ( !knlutil_is_aligned( (UW)pk_ctsk->stksz )
		|| !knlutil_is_aligned( (UW)pk_ctsk->stk ) )
	{
		/* stack or stack size is not aligned. */
		ercd = E_PAR;
	}
	#if ( !UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
	else if ( pk_ctsk->stk == NULL )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	#endif
	else if ( pk_ctsk->tskatr & ~( TA_ASM | TA_ACT ) )
	{
		/* illegal parameter. attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		VP stack;
		
		stack = NULL;
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
		if ( pk_ctsk->stk == NULL )
		{
			/* allocate stack from stack memory pool. */
			stack = knlmpl_allocate(
				knlmpl_get_stack_entity(),
				(UW)pk_ctsk->stksz,
				NULL );
		}
		
		if ( pk_ctsk->stk == NULL && stack == NULL )
		{
			/* insufficient memory. */
			ercd = E_NOMEM;
		}
		else
		#endif
		{
			BIOS_UINT lock;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			knltsk_initialize( tsk, pk_ctsk, stack );
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( pk_ctsk->tskatr & TA_ACT )
			{
				/*jp
				 タスク生成後の状態が実行可能状態でれば,
				 タスク起床準備を行いレディ状態にする.
				*/
				knlcpu_task_activation_preparation( tsk );
				knltsk_ready( tsk );
			}
			
			ercd = E_OK;
		}
	}
	
	return ercd;
}

/**
 * activate a task (kernel function).
 * 
 * @param tskid task ID
 * @return error code
 */
ER knltsk_activate(
	ID tskid )
{
	ER ercd;
	T_TSK * tsk;
	
	/* contain current running task. */
	tsk = knltsk_get_entity_self( tskid );

	if ( tsk == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &tsk->t_super ) )
		{
			/* task has not been created. */
			ercd = E_NOEXS;
		}
		else
		{
			BOOL ready;
			BIOS_UINT lock;
			
			ready = FALSE;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( tsk->t_activate_count >= TMAX_ACTCNT )
			{
				/* activate requests reached to the limit. */
				ercd = E_QOVR;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* clear the task dormant-state. */
					knltsk_clear_state( tsk, TTS_DMT );
					/* if task has not been activate, activate the task. */
					ready = TRUE;
				}
				else
				{
					/*jp
					 タスクがすでに起床していれば, 
					 起床要求をキューイング.
					*/
					tsk->t_activate_count ++;
				}
				
				ercd = E_OK;
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( ready )
			{
				/* if task is dormant, make task ready. */
				knlcpu_task_activation_preparation( tsk );
				knltsk_ready( tsk );
			}
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

/**
 * make a task dormant (kernel function).
 * 
 * @param tsk task entity
 * @param forcibly TRUE: make forcibly dormant FALSE: not forcibly
 * @retval TRUE can restart
 * @retval FALSE can not restart
 */
BOOL knltsk_dormant(
	T_TSK * const tsk,
	BOOL forcibly )
{
	BOOL restart;
	STAT tskstat;
	
	restart = FALSE;
	
	knltex_finalize( &tsk->t_tex );
	tsk->t_wakeup_count = 0;
	tsk->t_suspend_count = 0;
	tsk->t_base_priority = tsk->t_initial_priority;
	tsk->t_priority = tsk->t_initial_priority;
	
	if ( knltsk_test_state( tsk, TTS_RDY ) )
	{
		/*jp タスクがレディ状態であるならば, レディキューから削除. */
		knltsk_clear_state( tsk, TTS_RDY );
		knlqueue_delete( &knltsk_ready_queue, &tsk->t_super.o_joint );
	}
	
	if ( forcibly )
	{
		/* make forcibly dormant. */
		tsk->t_activate_count = 0;
	}
	else
	{
		if ( tsk->t_activate_count > 0 )
		{
			/* restart is necessary. */
			tsk->t_activate_count --;
			restart = TRUE;
		}
	}
	
	if ( !restart )
	{
		knltsk_set_state( tsk, TTS_DMT );
	}
	
	return restart;
}

/**
 * get ID of the current running task (kernel function).
 * 
 * @return ID of the current running task
 */
ID knltsk_get_current_id( void )
{
	ID tskid;
	
	if ( knltsk_current != NULL )
	{
		tskid = knlobject_get_id( &knltsk_current->t_super );
	}
	else
	{
		tskid = TSK_NONE;
	}
	
	return tskid;
}

/**
 * get the next joint in a queue with CPU locking (kernel function).
 * 
 * @param tsk task entity
 * @param queue task queue
 * @param lock state of cpu locking
 * @return next joint in a queue
 */
T_JOINT * knltsk_get_next_one_with_cpu_locking(
	T_TSK * const tsk,
	T_QUEUE * const queue,
	BIOS_UINT * lock )
{
	T_JOINT * jp;
	BIOS_UINT tlock;
	
	jp = NULL;
	
	if ( knlqueue_get_order( queue ) == KNLQUEUE_ORDER_FIFO )
	{
		/* queue is FIFO order. */
		
		/* lock CPU. */
		tlock = bios_cpu_lock();
		
		/* get first joint in a queue. */
		jp = &queue->q_joint;
	}
	else
	{
		/* queue is priority order. */
		PRI tskpri;
		UB nested_depth;
		
		tskpri = tsk->t_priority;
		
		/*jp スタックフレームに現在のコンテクストアクセス深度を保存. */
		nested_depth = knlsys_get_nested_depth();
		
		do
		{
			/* 1: */
			
			/* lock CPU. */
			tlock = bios_cpu_lock();
			
			if ( knlqueue_is_empty( queue ) )
			{
				/* if queue is empty, get first joint in a queue. */
				jp = &queue->q_joint;
			}
			else
			{
				T_TSK * tp;
				
				tp = (T_TSK *)knlqueue_tail( queue );
				
				if ( tskpri >= tp->t_priority )
				{
					/*
					 if the task has the least priority in a queue,
					 get last joint in a queue.
					*/
					jp = &queue->q_joint;
				}
				else
				{
					T_JOINT * kp;
					BOOL interrupted;
					
					/*jp
					 キューに現在のコンテクストアクセス深度を保存.
					 保存はクリティカルセクションで行う必要がある.
					*/
					knlqueue_set_nested_depth( queue, nested_depth );
					
					/* unlock CPU. */
					bios_cpu_unlock( tlock );
					
					kp = &queue->q_joint;
					
					interrupted = FALSE;
					
					do
					{
						/* lock CPU. */
						tlock = bios_cpu_lock();
						
						if ( knlqueue_get_nested_depth( queue )
							!= nested_depth )
						{
							/*jp
							 スタックフレームに保存したアクセス深度と, キューに
							 保存したアクセス深度が異なった場合, 更に深度の高い
							 コンテクストがこのキューを改変したと見なし, キュー
							 の走査を 1: からやり直す.
							*/
							interrupted = TRUE;
						}
						else
						{
							kp = knljoint_next( kp );
							tp = (T_TSK *)kp;
							
							if ( tskpri < tp->t_priority )
							{
								/*jp
								 指定したタスク優先度より優先度の低いタスクを
								 発見.
								*/
								jp = kp;
							}
						}
						
						if ( jp == NULL )
						{
							/* when the search is continued, unlock CPU. */
							bios_cpu_unlock( tlock );
						}
					}
					while ( jp == NULL && !interrupted );
				}
			}
		}
		while ( jp == NULL );
	}
	
	/*jp
	 指定したタスク優先度より優先度の低いタスクを発見した場合,
	 CPU ロックをした状態で終了.
	*/
	*lock = tlock;
	
	return jp;
}

/**
 * notify the wait-object was deleted, 
 * to the all tasks waiting for it (kernel function).
 * 
 * @param queue task queue
 */
void knltsk_notify_object_deletion(
	T_QUEUE * queue )
{
	BOOL iterate;
	
	iterate = TRUE;
	
	do
	{
		T_TSK * tsk;
		BOOL ready;
		BIOS_UINT lock;
		
		tsk = NULL;
		ready = FALSE;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		if ( iterate = !knlqueue_is_empty( queue ) )
		{
			tsk = (T_TSK *)knlqueue_head( queue );
			
			/*jp 待ち状態終了処理実行. */
			ready = knltsk_quit_waiting( tsk, E_DLT );
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( ready )
		{
			/*jp 待ち状態を終了したタスクをレディ状態にする. */
			knltsk_ready( tsk );
		}
	}
	while ( iterate );
}

/**
 * quit task waiting (kernel function).
 * 
 * @param tsk task entity
 * @param ercd wait result
 * @retval TRUE can make ready
 * @retval FALSE can not make ready
 */
BOOL knltsk_quit_waiting(
	T_TSK * const tsk,
	ER ercd )
{
	BOOL ready;
	T_WAIT * wait;
	T_EVENT * event;
	
	ready = FALSE;
	
	wait = tsk->t_wait;
	
	event = knlwait_get_event( wait );
	
	/* clear state of wait. */
	knltsk_clear_state( tsk, TTS_WAI );
	
	if ( wait->w_queue != NULL )
	{
		/* delete the task from the wait queue. */
		knlqueue_delete( wait->w_queue, &tsk->t_super.o_joint );
	}
	
	if ( event != NULL
		&& knljoint_is_connected( &event->e_super.o_joint ) )
	{
		/* delete the task from the time-event (timeout) queue. */
		knltim_delete_event( event );
	}
	
	/* setting wait result. */
	knlwait_set_result( wait, ercd );
	
	tsk->t_wait = NULL;
	
	if ( !knltsk_test_state( tsk, TTS_SUS ) )
	{
		/*jp 二重待ち状態でなければ, レディ状態にすることが可能. */
		ready = TRUE;
	}
	
	return ready;
}

/**
 * make a task ready (kernel function).
 * 
 * @param tsk task entity
 */
void knltsk_ready(
	T_TSK * const tsk )
{
	BIOS_UINT lock;
	T_JOINT * jp;
	
	/* lock CPU. get appropriate insertion position of ready queue. */
	jp = knltsk_get_next_one_with_cpu_locking(
		tsk, 
		&knltsk_ready_queue,
		&lock );
	knltsk_set_state( tsk, TTS_RDY );
	
	/* insert the task in the appropriate position of ready queue. */
	knlqueue_insert( &knltsk_ready_queue, &tsk->t_super.o_joint, jp );
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
}

/**
 * release a waiting task (kernel function).
 * 
 * @param tskid task ID
 * @return error code
 */
ER knltsk_release_wait(
	ID tskid )
{
	T_TSK * tsk;
	ER ercd;
	
	/* not contain current running task. */
	tsk = knltsk_get_entity( tskid );
	
	if ( tskid == TSK_SELF || tsk == knltsk_current )
	{
		/* current running task ID is specified. */
		ercd = E_OBJ;
	}
	else if ( tsk == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &tsk->t_super ) )
		{
			/* task has not been created. */
			ercd = E_NOEXS;
		}
		else
		{
			BOOL ready;
			BIOS_UINT lock;
			
			ready = FALSE;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( !knltsk_test_state( tsk, TTS_WAI ) )
			{
				/* task is not waiting. */
				ercd = E_OBJ;
			}
			else
			{
				/*jp
				 待ち状態解放時における待ちオブジェクト個別
				 コールバック実行.
				*/
				knlwait_callback(
					tsk->t_wait,
					purpose_WAITING_RELEASED );
				
				/*jp 待ち状態終了処理実行 */
				ready = knltsk_quit_waiting( tsk, E_RLWAI );
				
				ercd = E_OK;
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( ready )
			{
				/*jp 待ち状態を終了したタスクをレディ状態にする. */
				knltsk_ready( tsk );
			}
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

/**
 * resume a task (kernel function).
 * 
 * @param tskid task ID
 * @param forcibly TRUE: resume forcibly FALSE: not forcibly
 * @return error code
 */
ER knltsk_resume(
	ID tskid,
	BOOL forcibly )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_TSK * tsk;
		
		/* not contain current running task. */
		tsk = knltsk_get_entity( tskid );
		
		if ( tskid == TSK_SELF || tsk == knltsk_current )
		{
			/* current running task ID is specified. */
			ercd = E_OBJ;
		}
		else if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BOOL ready;
				BIOS_UINT lock;
				
				ready = FALSE;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knltsk_test_state( tsk, TTS_SUS ) )
				{
					/* task has not been suspended. */
					ercd = E_OBJ;
				}
				else
				{
					if ( forcibly )
					{
						/* resume forcibly. */
						tsk->t_suspend_count = 0;
					}
					else
					{
						tsk->t_suspend_count --;
					}
					
					if ( tsk->t_suspend_count == 0 )
					{
						/* clear suspend state. */
						knltsk_clear_state( tsk, TTS_SUS );
						
						if ( !knltsk_test_state( tsk, TTS_WAI ) )
						{
							/*jp 二重待ち状態でなければ, レディ状態にする. */
							ready = TRUE;
						}
					}
					
					ercd = E_OK;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( ready )
				{
					/*jp レディ状態にする. */
					knltsk_ready( tsk );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

/**
 * rotate the ready queue (kernel function).
 * 
 * @param tskpri task priority
 * @return error code 
 */
ER knltsk_rotate_ready_queue(
	PRI tskpri )
{
	ER ercd;
	
	if ( tskpri < TMIN_TPRI || tskpri > TMAX_TPRI )
	{
		/* priority is outside effective range. */
		ercd = E_PAR;
	}
	else
	{
		UB nested_depth;
		BOOL iterate;
		BOOL disp;
		BIOS_UINT lock;
		
		/*jp スタックフレームに現在のコンテクストアクセス深度を保存. */
		nested_depth = knlsys_get_nested_depth();
		
		iterate = TRUE;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		do
		{
			/* 1: */
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( knlqueue_is_empty( &knltsk_ready_queue ) )
			{
				/* there is no ready task. finish this function. */
				iterate  = FALSE;
			}
			
			if ( tskpri < knltsk_get_priority(
				(T_TSK *)knlqueue_head( &knltsk_ready_queue ) )
				|| tskpri > knltsk_get_priority(
				(T_TSK *)knlqueue_tail( &knltsk_ready_queue ) ) )
			{
				/*
				 the specified priority is higher than the first task's
				 or, the specified priority is lower than the last task's.
				 finish this function.
				*/
				iterate = FALSE;
			}
			else
			{
				/*jp
				 キューに現在のコンテクストアクセス深度を保存.
				 保存はクリティカルセクションで行う必要がある.
				*/
				knlqueue_set_nested_depth(
					&knltsk_ready_queue,
					nested_depth );
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( iterate )
			{
				T_JOINT * jp;
				BOOL interrupted;
				
				jp = &knltsk_ready_queue.q_joint;
				interrupted = FALSE;
				
				do
				{
					T_TSK * tsk;
					
					tsk = NULL;
					
					/* lock CPU. */
					lock = bios_cpu_lock();
					
					if ( knlqueue_get_nested_depth( &knltsk_ready_queue )
						!= nested_depth )
					{
						/*jp
						 スタックフレームに保存したアクセス深度と, キューに
						 保存したアクセス深度が異なった場合, 更に深度の高い
						 コンテクストがこのキューを改変したと見なし, キュー
						 の走査を 1: からやり直す.
						*/
						interrupted = TRUE;
					}
					else
					{
						PRI pri;
						
						jp = knljoint_next( jp );
						
						/*jp
						 レディキュー内の指定優先度の先頭位置あるタスク
						 候補を取得.
						*/
						tsk = (T_TSK *)jp;
						
						pri = knltsk_get_priority( tsk );
						
						if ( tskpri != pri )
						{
							/*jp
							 レディキュー内の指定優先度の先頭位置あるタ
							 スク候補再探索.
							*/
							tsk = NULL;
							
							if ( tskpri < pri )
							{
								/*
								 there is no specified priority.
								 finish this function.
								*/
								iterate = FALSE;
							}
						}
					}
					
					/* unlock CPU. */
					bios_cpu_unlock( lock );
					
					if ( tsk != NULL )
					{
						/*jp レディキュー内の指定優先度の先頭位置を発見. */
						
						UINT chain;
						
						chain = 0;
						
						/*jp
						 レディキュー内の指定優先度の先頭位置から探索
						 を開始.
						*/
						jp = &tsk->t_super.o_joint;
						
						do
						{
							/* lock CPU. */
							lock = bios_cpu_lock();
							
							if ( knlqueue_get_nested_depth(
								&knltsk_ready_queue ) != nested_depth )
							{
								/*jp
								 スタックフレームに保存したアクセス深度と, 
								 キューに保存したアクセス深度が異なった場合, 
								 更に深度の高いコンテクストがこのキューを改変
								 したと見なし, キューの走査を 1: からやり直す.
								*/
								interrupted = TRUE;
							}
							else
							{
								jp = knljoint_next( jp );
								
								if ( knlqueue_equals(
									&knltsk_ready_queue,
									jp ) || tskpri != knltsk_get_priority(
										(T_TSK *)jp ) )
								{
									/*jp
									 キューを一周もしくは, 指定した優先度より
									 低い優先度を発見した場合, 探索を終了する.
									*/
									if ( chain > 0 )
									{
										/*jp
										 指定優先度のタスクが複数発見できた場
										 合, 指定優先度の先頭タスクを最後尾に
										 移動する.
										*/
										knljoint_reconnect(
											&tsk->t_super.o_joint,
											jp );
									}
									
									/* finish this function. */
									iterate = FALSE;
								}
								
								chain ++;
							}
							
							/* unlock CPU. */
							bios_cpu_unlock( lock );
						}
						while ( iterate && !interrupted );
					}
				}
				while ( iterate && !interrupted );
			}
		}
		while ( iterate );
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
		
		ercd = E_OK;
	}
	
	return ercd;
}

/**
 * task's base entry point (kernel function).
 */
void knltsk_run( void )
{
	/* enable interrupts. */
	bios_cpu_enable_interrupts();
	
	/* run the user entry. */
	knltsk_current->t_entry( knltsk_current->t_exinf );
	
	/* when user entry ends, invoke ext_tsk(). */
	ext_tsk();
}

/**
 * set new priority to the task (kernel function).
 * 
 * @param tsk task entity
 * @param tskpri new task priority of task
 */
void knltsk_set_priority(
	T_TSK * const tsk,
	PRI tskpri )
{
	UB nested_depth;
	BOOL found;
	T_WAIT * wait;
	
	/*jp スタックフレームに現在のコンテクストアクセス深度を保存. */
	nested_depth = knlsys_get_nested_depth();
	found = FALSE;
	
	do
	{
		BIOS_UINT lock;
		STAT tskstat;
		T_QUEUE * queue;
		
		/* 1: */
		
		wait = NULL;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/*jp
		 スタックフレームに現在のタスク状態を保存.
		 保存はクリティカルセクションで行う必要がある.
		*/
		tskstat = knltsk_get_state( tsk );
		
		if ( tskstat == TTS_RDY )
		{
			/*jp 指定されたタスクはレディキューに格納されている. */
			queue = &knltsk_ready_queue;
		}
		else if ( tskstat & TTS_WAI )
		{
			/*jp 指定されたタスクは待ちキューに格納されている. */
			queue = tsk->t_wait->w_queue;
		}
		else
		{
			queue = NULL;
		}
		
		if ( queue == NULL
			|| knlqueue_get_order( queue ) == KNLQUEUE_ORDER_FIFO )
		{
			/* 
			 task has not been equeued in a queue, 
			 or queue is FIFO oreder.
			*/
			tsk->t_priority = tskpri;
			
			/* finish this function. */
			found = TRUE;
		}
		else
		{
			/* queue is prioirty order. */
			
			T_TSK * tp;
			
			if ( ( tskstat & TTS_WAI ) 
				&& knlqueue_equals(
					queue,
					knljoint_prev( &tsk->t_super.o_joint ) ) )
			{
				/*jp
				 待ちキューの先頭タスクの優先度を変更する場合.
				 タスクが持つ待ちオブジェクトをスタックに保存する.
				*/
				wait = tsk->t_wait;
			}
			
			tp = (T_TSK *)knlqueue_tail( queue );
			
			if ( tskpri >= knltsk_get_priority( tp ) )
			{
				/*
				 if the task has the least priority in a queue,
				 reconnect task on last of a queue.
				*/
				knljoint_reconnect( &tsk->t_super.o_joint, &queue->q_joint );
				
				/* update task priority. */
				tsk->t_priority = tskpri;
				
				/* finish this function. */
				found = TRUE;
			}
			else
			{
				/*jp
				 キューに現在のコンテクストアクセス深度を保存.
				 保存はクリティカルセクションで行う必要がある.
				*/
				knlqueue_set_nested_depth( queue, nested_depth );
			}
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( !found )
		{
			T_JOINT * jp;
			BOOL interrupted;
			
			jp = &queue->q_joint;
			interrupted = FALSE;
			
			do
			{
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knlqueue_get_nested_depth( queue ) != nested_depth
					|| knltsk_get_state( tsk ) != tskstat )
				{
					/*jp
					 スタックフレームに保存したアクセス深度と, キューに
					 保存したアクセス深度が異なった場合, 更に深度の高い
					 コンテクストがこのキューを改変したと見なし, キュー
					 の走査を 1: からやり直す.
					 また, スタックフレームに保存したタスク状態とタスク
					 エンティティに記録されたタスク状態が異なる場合は, 
					 この時点でタスクが格納されているキューそのものが変
					 わっている可能性がある為, この場合もキューの走査を
					 1: からやり直す.
					*/
					interrupted = TRUE;
				}
				else
				{
					jp = knljoint_next( jp );
					
					if ( knlqueue_equals( queue, jp ) )
					{
						found = TRUE;
					}
					else
					{
						const T_TSK * tp;
						
						tp = (T_TSK *)jp;
						
						if ( tp != tsk )
						{
							if ( tskpri < knltsk_get_priority( tp ) )
							{
								/* finish this function. */
								found = TRUE;
							}
						}
					}
					
					if ( found )
					{
						/*jp
						 タスクをキュー内の指定優先度と同じ優先度を
						 持つタスク (群) の最後尾に移動する.
						*/
						knljoint_reconnect( &tsk->t_super.o_joint, jp );
						
						/* update task priority. */
						tsk->t_priority = tskpri;
					}
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
			}
			while ( !interrupted && !found );
		}
	}
	while ( !found );
	
	if ( wait != NULL )
	{
		/*jp
		 待機中の最優先タスク優先度変更時における待ちオブジェクト個別
		 コールバック実行.
		*/
		knlwait_callback( wait, purpose_PRIORITY_CHANGED );
	}
}

/**
 * current running task do sleep (kernel function).
 * 
 * @param timeout time until timeout
 * @return error code 
 */
ER knltsk_sleep(
	TMO timeout )
{
	ER ercd;
	T_EVENT event;
	
	if ( knlsys_sense_dispatch_pending() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_WAIT wait;
		BOOL disp;
		BIOS_UINT lock;
		BOOL waitable;
		
		/* wait-controller is builded on a stack frame. */
		knlwait_build(
			&wait,
			&event,
			TTW_SLP,
			NULL,
			NULL,
			_knltsk_wait_callback );
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/* can enter state of wait? */
		waitable = _knltsk_check_waitable( &wait );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( waitable )
		{
			/* do wait. */
			knltsk_wait( &wait, timeout );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
		
		/* get wait result. */
		ercd = knlwait_get_result( &wait );
	}
	
	return ercd;
}

/**
 * static root task entry (kernel function).
 * 
 * @param exinf extended information
 */
void knltsk_static_root_entry(
	VP_INT exinf )
{
	cfghook_pre_system_timer_start();
	
	/* start the system timer. */
	knltim_start();
	
	cfghook_post_system_timer_start();
	
	/* invoke ext_tsk(). */
	ext_tsk();
}

/**
 * suspend a task (kernel function).
 * 
 * @param tskid task ID
 * @return error code 
 */
ER knltsk_suspend(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( ( bios_cpu_sense_locked()
			|| !knlsys_sense_dispatchable() )
				&&  tsk == knltsk_current )
		{
			/*
			 current task specified directly.
			 illegal context.
			*/
			ercd = E_CTX;
		}
		else if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knltsk_get_state( tsk ) == TTS_DMT )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else if ( tsk->t_suspend_count >= TMAX_SUSCNT )
				{
					/* suspend requests reached to the limit. */
					ercd = E_QOVR;
				}
				else
				{
					if ( tsk->t_suspend_count == 0 )
					{
						if ( knltsk_test_state( tsk, TTS_RDY ) )
						{
							/* clear state of ready. */
							knltsk_clear_state( tsk, TTS_RDY );
							
							/* delete the task from the ready queue. */
							knlqueue_delete(
								&knltsk_ready_queue, 
								&tsk->t_super.o_joint );
						}
						
						/* adding the state of suspension to the task. */
						knltsk_or_state( tsk, TTS_SUS );
					}
					
					tsk->t_suspend_count ++;
					
					ercd = E_OK;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

/**
 * current running is made to wait (kernel function).
 * 
 * @param wait wait-controller entity
 * @param timeout time until wait release
 */
void knltsk_wait(
	T_WAIT * const wait, 
	TMO timeout )
{
	if ( timeout == TMO_POL )
	{
		/* specified polling, or wait time is zero. */
		knlwait_set_result( wait, E_TMOUT );
	}
	else
	{
		BOOL waitable;
		T_EVENT * event;
		T_JOINT * jp;
		BIOS_UINT lock;
		
		knltsk_current->t_wait = wait;
		
		waitable = FALSE;
		event = knlwait_get_event( wait );
		
		if ( event != NULL && timeout != TMO_FEVR )
		{
			/*
			 if timout are specified, 
			 insert current running task in the time-event (timeout) queue.
			*/
			knltim_add_event( event, (RELTIM)( timeout + 1 ) );
		}
		
		if ( wait->w_queue != NULL )
		{
			/* lock CPU. get appropriate insertion position of ready queue. */
			jp = knltsk_get_next_one_with_cpu_locking(
				knltsk_current,
				wait->w_queue,
				&lock );
		}
		else
		{
			jp = NULL;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
		}
		
		if ( knlwait_get_result( wait ) == E_OK )
		{
			/* timeout error has not occurred. */
			
			/* can enter state of wait? */
			waitable = knlwait_check_waitable( wait );
			
			if ( waitable )
			{
				/* can enter state of wait. */
				
				/* delete current running task from the ready queue. */
				knlqueue_delete(
					&knltsk_ready_queue,
					&knltsk_current->t_super.o_joint );
				
				if ( jp != NULL )
				{
					/* 
					 insert the task in the appropriate position
					 of wait queue.
					*/
					knlqueue_insert(
						wait->w_queue,
						&knltsk_current->t_super.o_joint,
						jp );
				}
				
				/* adding the state of wait to the current running task. */
				knltsk_set_state( knltsk_current, TTS_WAI );
			}
			else
			{
				/* waiting is canceled. */
				
				if ( event != NULL
					&& knljoint_is_connected( &event->e_super.o_joint ) )
				{
					/*
					 delete current running task from the time-event
					 (timeout) queue.
					*/
					knltim_delete_event( event );
				}
			}
		}
		
		if ( !waitable )
		{
			knltsk_current->t_wait = NULL;
		}
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
	}
}

/**
 * wake up a task (kernel function).
 * 
 * @param tskid task ID
 * @return error code 
 */
ER knltsk_wakeup(
	ID tskid )
{
	T_TSK * tsk;
	ER ercd;
	
	/* contain current running task. */
	tsk = knltsk_get_entity_self( tskid );
	
	if ( tsk == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( !knlobject_exists( &tsk->t_super ) )
		{
			/* task has not been created. */
			ercd = E_NOEXS;
		}
		else
		{
			BOOL ready;
			BIOS_UINT lock;
			
			ready = FALSE;
			
			/* lock CPU. */
			lock = bios_cpu_lock();
			
			if ( knltsk_test_state( tsk, TTS_DMT ) )
			{
				/* task is not active. */
				ercd = E_OBJ;
			}
			else
			{
				ercd = E_OK;
				
				if ( knltsk_test_state( tsk, TTS_WAI )
					&& ( knlwait_get_factor( tsk->t_wait ) == TTW_SLP ) )
				{
					/* wake up the sleeping task. */
					ready = knltsk_quit_waiting( tsk, E_OK );
				}
				else
				{
					if ( tsk->t_wakeup_count >= TMAX_WUPCNT )
					{
						/* wake up requests requests reached to the limit. */
						ercd = E_QOVR;
					}
					else
					{
						/* queuing wake up requests. */
						tsk->t_wakeup_count ++;
					}
				}
			}
			
			/* unlock CPU. */
			bios_cpu_unlock( lock );
			
			if ( ready )
			{
				/*jp 待ち状態を終了したタスクをレディ状態にする. */
				knltsk_ready( tsk );
			}
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sys.c
 *
 * implementations of MicroAnts kernel system functions.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"
#include "uAnts_hook.h"

/**
 * sense a state of dispatch (kernel function).
 * 
 * @retval TRUE dispatch is pending
 * @retval FALSE dispatch is not pending
 */
BOOL knlsys_sense_dispatch_pending( void )
{
	BOOL pending;
	
	if ( bios_cpu_sense_locked() )
	{
		/* CPU locked. */
		pending = TRUE;
	}
	else if ( knlsys_sense_context() )
	{
		/* no-task context. */
		pending = TRUE;
	}
	else if ( !knlsys_sense_dispatchable() )
	{
		/* dispatching disabled. */
		pending = TRUE;
	}
	else
	{
		pending = FALSE;
	}
	
	return pending;
}

/**
 * shutdown the kernel (kernel function).
 */
void knlsys_shutdown( void )
{
	/* disable interrupts. */
	bios_cpu_disable_interrupts();
	
	cfghook_shutdown();
	
	for ( ; ; )
	{
		;
	}
}

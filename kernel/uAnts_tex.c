﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tex.c
 *
 * implementations of MicroAnts kernel task exception functions.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * define a task exception of current running task (kernel function).
 * 
 * @param tex task exception entity
 * @param pk_dtex task exception definition information packet
 * @return error code
 */
void knltex_define(
	T_TEX * const tex,
	const T_DTEX * pk_dtex )
{
	if ( pk_dtex != NULL )
	{
		tex->e_attribute = pk_dtex->texatr;
		tex->e_entry = ( void ( * )( TEXPTN, VP_INT ) )pk_dtex->texrtn;
	}
	else
	{
		tex->e_attribute = 0;
		tex->e_pattern = 0;
		tex->e_state = TTEX_DIS;
		tex->e_entry = NULL;
	}
}

/**
 * raise a task exception (kernel function).
 * 
 * @param tskid task ID
 * @param rasptn pattern of raise exception
 * @return error code
 */
ER knltex_raise(
	ID tskid,
	TEXPTN rasptn )
{
	ER ercd;
	
	if ( rasptn == 0 )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else
				{
					T_TEX * tex;
					
					tex = &tsk->t_tex;
					
					if ( tex->e_entry == NULL )
					{
						/* task exception has not been defined. */
						ercd = E_OBJ;
					}
					else
					{
						BIOS_UINT lock;
						
						/* lock CPU. */
						lock = bios_cpu_lock();
						
						/* add pattern of raise exception. */
						tex->e_pattern |= rasptn;
						
						knlsys_set_dispatchable( disp );
						
						if ( !knlsys_sense_context() )
						{
							/* invoke task exception. */
							knlcpu_dispatch();
						}
						
						/* unlock CPU. */
						bios_cpu_unlock( lock );
						
						ercd = E_OK;
					}
				}
			}
			
			if ( ercd != E_OK )
			{
				/* unlock dispatch. */
				knlsys_unlock_dispatch( disp );
			}
		}
	}
	
	return ercd;
}

/**
 * invoke a task exception of current running task (kernel function).
 */
void knltex_invoke( void )
{
	T_TSK * tsk;
	T_TEX * tex;
	
	tsk = knltsk_current;
	tex = &tsk->t_tex;
	
	if ( tex->e_entry != NULL && tex->e_state == TTEX_ENA )
	{
		BOOL iterate;
		VP_INT exinf;
		
		iterate = TRUE;
		exinf = tsk->t_exinf;
		
		do
		{
			TEXPTN rasptn;
			
			rasptn = tex->e_pattern;
			
			if ( rasptn == 0 )
			{
				/* no task exception has been raised. finish this function. */
				iterate = FALSE;
			}
			else
			{
				/* disable task exception. */
				tex->e_state = TTEX_DIS;
				
				/* clear all task exceptions. */
				tex->e_pattern = 0;
				
				/* enable interrupts. */
				bios_cpu_enable_interrupts();
				
				/* invoke task exception. */
				tex->e_entry( rasptn, exinf );
				
				/* disable interrupts. */
				bios_cpu_disable_interrupts();
				
				/* enable task exception. */
				tex->e_state = TTEX_ENA;
			}
		}
		while ( iterate );
	}
}

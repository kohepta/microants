﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_inh.c
 *
 * implementations of MicroAnts kernel interrupt handler functions.
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_inh.h"
#include "uAnts_hook.h"

/**
 * initialize all kernel interrupt handler entities (kernel function).
 */
void knlinh_init_entities( void )
{
	ID id;
	
	for ( id = 0; id < UANTS_CFG_INH_MAX_ID; id ++ )
	{
		knlinh_define( id, NULL, NULL );
	}
}

/**
 * define an interrupt handler (kernel function).
 * 
 * @param inhno interrupt handler number
 * @param pk_dinh interrupt handler definition information packet
 * @param exinf extended information
 */
void knlinh_define(
	INHNO inhno,
	const T_DINH * pk_dinh,
	VP_INT exinf )
{
	T_INH * inh;
	ATR attribute;
	void ( * inthdr )( INHNO, VP_INT );
	
	if ( pk_dinh != NULL )
	{
		attribute = pk_dinh->inhatr;
		inthdr = ( void ( * )( INHNO, VP_INT ) )pk_dinh->inthdr;
	}
	else
	{
		attribute = 0;
		/* register dummy interrupt handler. */
		inthdr = knlinh_undefined;
	}
	
	inh = &knlinh_entities[inhno];
	inh->i_attribute = attribute;
	inh->i_entry = inthdr;
	inh->i_exinf = exinf;
}

/**
 * detect and invoke an interrupt handler (kernel function).
 *
 * @attention this function is not used now.
 */
void knlinh_detect_and_invoke( void )
{
	BOOL iterate;
	
	iterate = TRUE;
	
	do
	{
		INTNO intno;
		PRI priority;
		
		if ( bios_intc_detect_asserted_highest_priority_interrupt(
			(BIOS_UINT *)&intno,
			(BIOS_UINT *)&priority ) != BIOS_ERROR_OK )
		{
			iterate = FALSE;
		}
		else
		{
			knlinh_invoke( intno );
		}
	}
	while ( iterate );
}

/**
 * invoke an interrupt handler (kernel function).
 * 
 * @param intno interrupt number
 */
void knlinh_invoke(
	INTNO intno )
{
	T_INH * inh;
	
	knlsys_increment_nested_depth();
	
	inh = &knlinh_entities[intno];
	
	knlcpu_pre_interrupt_process( intno );
	
	if ( intno <= UANTS_CFG_INH_MAX_ID )
	{
		/* invoke an interrupt handler. */
		inh->i_entry( intno, inh->i_exinf );
	}
	else
	{
		knlinh_undefined( intno, NULL );
	}
	
	/* disable interrupts. */
	bios_cpu_disable_interrupts();
	
	knlcpu_post_interrupt_process( intno );
	
	knlsys_decrement_nested_depth();
}

/**
 * dummy handler for undefined interrupt (kernel function).
 * 
 * @param inhno interrupt handler number
 * @param exinf extended information
 */
void knlinh_undefined(
	INHNO inhno,
	VP_INT exinf )
{
	cfghook_undefined_interrupt( inhno );
	
	/* enable interrupts. */
	bios_cpu_enable_interrupts();
}

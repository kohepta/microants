﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_wait.c
 *
 * implementations of MicroAnts wait-controller functions.
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_wait.h"
#include "uAnts_tsk.h"

static void _knlwait_timeout_event(
	T_EVENT * const event );

/**
 * callback on timeout.
 *
 * @param event time-event entity
 */
static void _knlwait_timeout_event(
	T_EVENT * const event )
{
	BOOL ready;
	BIOS_UINT lock;
	T_TSK * tsk;
	
	ready = FALSE;
	
	/* lock CPU. */
	lock = bios_cpu_lock();
	
	/* task is gotten from ID. */
	tsk = knltsk_get_entity( knlevent_get_id( event ) );
	
	/*jp
	 knltim_signal() からの呼び出しよりも一瞬早く待ちが解除され
	 た場合もこのハンドラは実行されてしまうので, 既に待ち解除が
	 実行されているかを以下の条件で判定する.
	*/
	if ( tsk->t_wait != NULL )
	{
		/*jp タイムアウト時における待ちオブジェクト個別コールバックを実行. */
		knlwait_callback( tsk->t_wait, purpose_TIMEOUT );
		
		if ( knltsk_test_state( tsk, TTS_WAI ) )
		{
			/*jp
			 タスクが待ち状態である場合, 待ち状態を解除し,
			 レディ状態に遷移出来るかを取得.
			*/
			ready = knltsk_quit_waiting( tsk, E_TMOUT );
		}
		else
		{
			/*jp
			 待ち状態に入る前にタイムアウトが発生した場合,
			 タイムアウトエラーを設定のみを行う.
			*/
			knlwait_set_result( tsk->t_wait, E_TMOUT );
		}
	}
	
	/* unlock CPU. */
	bios_cpu_unlock( lock );
	
	if ( ready )
	{
		/*jp
		 待ち状態を解除されたタスクがレディ状態に遷移可能であれば,
		 レディ状態にする.
		*/
		knltsk_ready( tsk );
	}
}

/**
 * build wait-controller.
 * 
 * @param wait wait-controller entity
 * @param event time-event entity
 * @param factor waited factor
 * @param object wait object (eventflag, semafore, mailbox, ,,,)
 * @param queue wait queue
 * @param callback individual processing of wait object in wait release
 */
void knlwait_build(
	T_WAIT * const wait,
	T_EVENT * const event,
	STAT factor,
	T_OBJECT * const object,
	T_QUEUE * const queue,
	BOOL ( * callback )(
		const T_WAIT * const wait,
		T_WAIT_CALLBACK_PURPOSE purpose ) )
{
	if ( event != NULL )
	{
		knlevent_build(
			event,
			knlobject_get_id( &knltsk_current->t_super ),
			KNLOBJECT_TSK,
			NULL,
			_knlwait_timeout_event );
	}
	
	wait->w_event = event;
	wait->w_result = E_OK;
	wait->w_factor = factor;
	wait->w_object = object;
	wait->w_queue = queue;
	wait->w_callback = callback;
}

/**
 * wait-controller's callback for illegal case
 * 
 * @param wait wait-controller entity
 * @param purpose call-purpose of individual processing in wait release
 * @return result of individual processing of wait object
 */
void knlwait_callback(
	const T_WAIT * const wait,
	T_WAIT_CALLBACK_PURPOSE purpose )
{
	if ( wait->w_callback != NULL )
	{
		/*jp 待ちオブジェクト個別コールバックを実行. */
		wait->w_callback( wait, purpose );
	}
}

/**
 * can the task change to the waiting state?
 * 
 * @param wait wait-controller entity
 * @retval TRUE can change to the waiting state
 * @retval FALSE can not change to the waiting state
 */
BOOL knlwait_check_waitable(
	const T_WAIT * const wait )
{
	BOOL waitable;
	
	if ( wait->w_callback != NULL )
	{
		/* can the task change to the waiting state? */
		waitable  = wait->w_callback( wait, purpose_CHECK_WAITABLE );
	}
	else
	{
		waitable = TRUE;
	}
	
	return waitable;
}

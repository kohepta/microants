﻿/*
  MicroAnts Real-time Kernel
  
  This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_mpl_core.c
 *
 * implementations of MicroAnts kernel variable-sized memory pool
 * core functions.
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"
#include "uAnts_util.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_STACK	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )

/**
 * initialize all kernel variable-sized memory pool entities
 * (kernel function).
 */
void knlmpl_init_entities( void )
{
	ID id;
	T_CMPL cmpl;
	
	knlqueue_initialize( &knlmpl_free_queue, KNLQUEUE_ORDER_FIFO );
	
	for ( id = 0; id < KNLMPL_MAX_ID; )
	{
		T_MPL * mpl;
		
		mpl = &knlmpl_entities[id ++];
		knlobject_build(
			&mpl->v_super,
			id,
			KNLOBJECT_MPL,
			&knlmpl_free_queue );
	}
	
	#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
	/* initialize stack memory pool */
	cmpl.mplsz = (SIZE)knlutil_align_cpu( (UW)UANTS_CFG_MPL_STACK_SIZE );
	cmpl.mpl = (VP)knlutil_align_cpu( (UW)UANTS_CFG_MPL_STACK_ORIGIN );
	knlmpl_initialize( knlmpl_get_stack_entity(), &cmpl, NULL, 0 );
	#endif
	
	#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
	/* initialize generic memory pool */
	cmpl.mplsz = (SIZE)knlutil_align_cpu( (UW)UANTS_CFG_MPL_GENERIC_SIZE );
	cmpl.mpl = (VP)knlutil_align_cpu( (UW)UANTS_CFG_MPL_GENERIC_ORIGIN );
	knlmpl_initialize( knlmpl_get_generic_entity(), &cmpl, NULL, 0 );
	#endif
}

/**
 * initialize a variable-sized memory pool (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param pk_cmpl variable-sized memory pool creation information packet
 * @param vp 'variable-sized memory pool' buffer
 * @param vp_size 'variable-sized memory pool' buffer size
 *        ( effective only at vp != NULL )
 */
void knlmpl_initialize(
	T_MPL * const mpl,
	const T_CMPL * pk_cmpl,
	VP vp,
	UW vp_size )
{
	T_MPL_BLOCK * block;
	UW mplsz;
	
	if ( vp != NULL )
	{
		block = (T_MPL_BLOCK *)vp;
		mplsz = vp_size;
	}
	else
	{
		block = (T_MPL_BLOCK *)knlutil_align_cpu( (UW)pk_cmpl->mpl );
		mplsz = knlutil_align_cpu( (UW)pk_cmpl->mplsz );
		if ( block != pk_cmpl->mpl )
		{
			mplsz -= sizeof( UW );
		}
	}
	
	knlobject_initialize(
		&mpl->v_super,
		&knlmpl_free_queue,
		pk_cmpl->mplatr );
	knlqueue_initialize(
		&mpl->v_task_queue,
		knlutil_test_bit_16( pk_cmpl->mplatr, TA_TPRI ) );
	knlqueue_initialize(
		&mpl->v_block_queue,
		KNLQUEUE_ORDER_FIFO );
	
	mpl->v_size = mplsz;
	mpl->v_origin = (VP)block;
	mpl->v_max_block = block;
	
	/* mplsz contains the allocated block header area. */
	mpl->v_free_size = mplsz;
	block->b_free.f_size = mplsz;
	
	knlqueue_add( &mpl->v_block_queue, &block->b_free.f_joint );
}

/**
 * allocate a memory block from a variable-sized memory pool
 * (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param block_size memory block size
 * @param user_area_size area that can be freely used
 * @return pointer to allocated memory block
 */
VP knlmpl_allocate(
	T_MPL * const mpl,
	UW block_size,
	UW * user_area_size )
{
	T_JOINT * jp;
	VP vp;
	
	/* block size is calculated again, and add allocated block header area. */
	block_size = knlmpl_get_adjusted_block_size( block_size );
	
	if ( user_area_size != NULL )
	{
		*user_area_size = 0;
	}
	
	vp = NULL;
	
	if ( ( mpl->v_max_block != NULL )
		&& ( block_size <= mpl->v_max_block->b_free.f_size ) )
	{
		/* variable-sized memory pool can afford to be gotten. */
		
		for ( jp = knlqueue_head( &mpl->v_block_queue );
			 vp == NULL;
			 jp = knljoint_next( jp ) )
		{
			T_MPL_BLOCK * block = (T_MPL_BLOCK *)jp;
			
			if ( block_size <= block->b_free.f_size )
			{
				/* memory block of appropriate size was found. */
				
				/* divide the memory block. */
				block = knlmpl_divide_block(
					mpl, 
					block, 
					block_size, 
					user_area_size );
				
				/*jp
				 ユーザが使用するメモリブロックの先頭を
				 ヘッダーサイズ分ずらす.
				*/
				vp = (VP)( &block->b_allocated + 1 );
			}
		}
	}
	
	return vp;
}

/**
 * divide the memory block (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param block memory block
 * @param block_size memory block size
 * @param user_area_size user_area_size area that can be freely used
 * @return allocated memory block
 */
T_MPL_BLOCK * knlmpl_divide_block(
	T_MPL * const mpl,
	T_MPL_BLOCK * block,
	UW block_size,
	UW * user_area_size )
{
	T_MPL_BLOCK * bp;
	
	bp = block;
	
	/*
	 block_size >= sizeof( T_MPL_BLOCK )
		-> from knlmpl_allocate
	 block_size + sizeof( T_MPL_BLOCK_ALLOCATED ) <= bp->b_free.f_size
		-> from knlmpl_allocate
	 sizeof( T_MPL_BLOCK_ALLOCATED ) <= sizeof( T_MPL_BLOCK )
		-> from uAnts_mpl.h
	 
	 Therefore, if it is a terms and conditions above,
	 
	 sizeof( T_MPL_BLOCK ) <= block_size + sizeof( T_MPL_BLOCK_ALLOCATED )
		<= bp->b_free.f_size
	*/
	if ( ( bp->b_free.f_size - block_size ) < sizeof( T_MPL_BLOCK ) )
	{
		/*
		 this memory block is too small to divide the new block.
		 therefore not divide.
		*/
		block_size = bp->b_free.f_size;
		knlqueue_delete( &mpl->v_block_queue, &bp->b_free.f_joint );
	}
	else
	{
		/* divide the new memory block. */
		bp->b_free.f_size -= block_size;
		bp = (T_MPL_BLOCK *)( (UB *)bp + bp->b_free.f_size );
	}
	
	/* block_size contains the allocated block header area. */
	bp->b_allocated.a_size = block_size;
	bp->b_allocated.a_magic = (UW)KNLMPL_MAGIC;
	mpl->v_free_size -= block_size;
	
	if ( block == mpl->v_max_block )
	{
		const T_JOINT * jp;
		
		mpl->v_max_block = NULL;
		
		/* find the the maximum block. */
		for ( jp = knlqueue_head( &mpl->v_block_queue );
			!knlqueue_equals( &mpl->v_block_queue, jp );
			jp = knljoint_next( jp ) )
		{
			block = (T_MPL_BLOCK *)jp;
			
			if ( ( mpl->v_max_block == NULL )
				|| ( block->b_free.f_size
					> mpl->v_max_block->b_free.f_size ) )
			{
				/* upadate the maximum block. */
				mpl->v_max_block = block;
			}
		}
	}
	
	if ( user_area_size != NULL )
	{
		/* get user area size. */
		*user_area_size = bp->b_allocated.a_size
			- (UW)sizeof( T_MPL_BLOCK_ALLOCATED );
	}
	
	return bp;
}

/**
 * free a memory block to variable-sized memory pool (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param vp released memory block
 * @return free memory block where 'vp' is included
 */
T_MPL_BLOCK * knlmpl_free(
	T_MPL * const mpl,
	VP vp )
{
	T_MPL_BLOCK * block;
	T_JOINT * jp;
	
	/* get allocate block header from user used area. */
	block = (T_MPL_BLOCK *)( &( (T_MPL_BLOCK *)vp )->b_allocated - 1 );
	
	/* find the next memory block in memory address order. */
	for ( jp = knlqueue_head( &mpl->v_block_queue );
		 !knlqueue_equals( &mpl->v_block_queue, jp ) && (VP)block >= (VP)jp;
		 jp = knljoint_next( jp ) )
	{
		;
	}
	
	/* allocated block size is add to free block size. */
	block->b_free.f_size = block->b_allocated.a_size;
	
	/*
	 insert the free block in the appropriate position
	 of variable-sized memory pool queue.
	*/
	knlqueue_insert( &mpl->v_block_queue, &block->b_free.f_joint, jp );
	
	/* update total free block size. */
	mpl->v_free_size += block->b_free.f_size;
	
	/* 'jp' is already next block of released block. */
	if ( !knlqueue_equals( &mpl->v_block_queue, jp ) )
	{
		T_MPL_BLOCK * next;
		
		next = (T_MPL_BLOCK *)jp;
		if ( (VP)( (UB *)block + block->b_free.f_size ) == (VP)next )
		{
			/* combine the released block with next one. */
			block->b_free.f_size += next->b_free.f_size;
			knlqueue_delete( &mpl->v_block_queue, &next->b_free.f_joint );
		}
	}
	
	jp = knljoint_prev( &block->b_free.f_joint );
	if ( !knlqueue_equals( &mpl->v_block_queue, jp ) )
	{
		T_MPL_BLOCK * prev;
		
		prev = (T_MPL_BLOCK *)jp;
		if ( (VP)block == (VP)( (UB *)prev + prev->b_free.f_size ) )
		{
			/* combine the released block with previous one. */
			prev->b_free.f_size += block->b_free.f_size;
			knlqueue_delete( &mpl->v_block_queue, &block->b_free.f_joint );
			block = prev;
		}
	}
	
	/* update maximum block. */
	if ( ( mpl->v_max_block == NULL )
		|| ( block->b_free.f_size > mpl->v_max_block->b_free.f_size ) )
	{
		mpl->v_max_block = block;
	}
	
	return block;
}

/**
 * verify whether variable-sized memory pool contains memory block
 * (kernel function).
 * 
 * @param mpl variable-sized memory pool entity
 * @param vp memory block that user used
 * @retval TRUE contained
 * @retval FALSE not contained
 */
BOOL knlmpl_verify_contains(
	const T_MPL * const mpl,
	const VP vp )
{
	BOOL result;
	const void * end;
	
	end = (VP)( (UB *)mpl->v_origin + mpl->v_size );
	
	if ( vp < mpl->v_origin || vp >= end )
	{
		/* memory block is outside variable-sized memory pool. */
		result = FALSE;
	}
	else
	{
		T_MPL_BLOCK * block;
		
		/* get allocate block header from user used area. */
		block = (T_MPL_BLOCK *)( &( (T_MPL_BLOCK *)vp )->b_allocated - 1 );
		
		if ( block->b_allocated.a_magic == (UW)KNLMPL_MAGIC )
		{
			/* magic number is confirmed. */
			result = TRUE;
		}
		else
		{
			result = FALSE;
		}
	}
	
	return result;
}

#endif

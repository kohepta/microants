﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu_ext.h
 *
 * definitions of BIOS CPU extension.
 *
 * @date 2010.08.16 new creation
 */

#ifndef __BIOS_CPU_EXT_H__
#define __BIOS_CPU_EXT_H__

#include <bios_types.h>

#define EXR_I_MASK			(0x07)
#define EXR_I_MASK_OFF		(0xF8)

/*---------------------------------------------
 * BIOS CPU function prototypes
 *-------------------------------------------*/

/* bios_cpu_ext.c */
void bios_cpu_ext_set_current_imask(
	BIOS_UINT8 imask );

/* bios_cpu_ext.c */
BIOS_UINT8 bios_cpu_ext_get_current_imask( void );

/* bios_cpu_ext.c */
void bios_cpu_ext_initialize_imask(
	BIOS_UINT8 imask );

/* bios_cpu_ext.c */
BIOS_UINT8 bios_cpu_ext_get_base_imask( void );

#endif

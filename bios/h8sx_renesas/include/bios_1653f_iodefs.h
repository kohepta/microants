﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_1653f_iodefs.h
 *
 * definitions of BIOS H8SX 1653F I/O definitions.
 *
 * @date 2010.08.22 new creation
 */

#ifndef	__BIOS_1653F_IODEFS_H__
#define	__BIOS_1653F_IODEFS_H__

#include <bios_1653f_regs.h>

struct st_pm
{
	unsigned char DDR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
};

struct st_p1
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk3[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_p2
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[42];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ODR;
	char wk3[900];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk4[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_p3
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk3[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_p5
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk1[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
};

struct st_p6
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk3[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_pa
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk3[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_pb
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[943];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk3[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_pd
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[23];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PCR;
	char wk3[919];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk4[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_pe
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[23];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PCR;
	char wk3[919];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk4[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_pf
{
	unsigned char DDR;
	char wk1[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk2[23];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PCR;
	char wk3[6];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ODR;
	char wk4[912];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk5[15];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
};

struct st_ph
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk1[3];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
	char wk2[3];
	unsigned char DDR;
	char wk3[3];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk4[11];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PCR;
};

struct st_pi
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PORT;
	char wk1[3];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	DR;
	char wk2[3];
	unsigned char DDR;
	char wk3[3];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	ICR;
	char wk4[11];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char B7 :1;
			unsigned char B6 :1;
			unsigned char B5 :1;
			unsigned char B4 :1;
			unsigned char B3 :1;
			unsigned char B2 :1;
			unsigned char B1 :1;
			unsigned char B0 :1;
		}
		BIT;
	}
	PCR;
};

struct st_pfc
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CS7E :1;
			unsigned char CS6E :1;
			unsigned char CS5E :1;
			unsigned char CS4E :1;
			unsigned char CS3E :1;
			unsigned char CS2E :1;
			unsigned char CS1E :1;
			unsigned char CS0E :1;
		}
		BIT;
	}
	PFCR0;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CS7S :2;
			unsigned char CS6S :2;
			unsigned char CS5S :2;
		}
		BIT;
	}
	PFCR1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :1;
			unsigned char CS2S :1;
			unsigned char BSS :1;
			unsigned char BSE :1;
			unsigned char :1;
			unsigned char RDWTE :1;
			unsigned char ASOE :1;
		}
		BIT;
	}
	PFCR2;
	char wk1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char A20E :1;
			unsigned char A19E :1;
			unsigned char A18E :1;
			unsigned char A17E :1;
			unsigned char A16E :1;
		}
		BIT;
	}
	PFCR4;
	char wk2;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :1;
			unsigned char LHWROE :1;
			unsigned char :2;
			unsigned char TCLKS :1;
		}
		BIT;
	}
	PFCR6;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char DMAS3 :2;
			unsigned char DMAS2 :2;
			unsigned char DMAS1 :2;
			unsigned char DMAS0 :2;
		}
		BIT;
	}
	PFCR7;
	char wk3;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TPUMS5 :1;
			unsigned char TPUMS4 :1;
			unsigned char TPUMS3A :1;
			unsigned char TPUMS3B :1;
			unsigned char :4;
		}
		BIT;
	}
	PFCR9;
	char wk4;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char ITS11 :1;
			unsigned char ITS10 :1;
			unsigned char ITS9 :1;
			unsigned char ITS8 :1;
		}
		BIT;
	}
	PFCRB;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char ITS7 :1;
			unsigned char ITS6 :1;
			unsigned char ITS5 :1;
			unsigned char ITS4 :1;
			unsigned char ITS3 :1;
			unsigned char ITS2 :1;
			unsigned char ITS1 :1;
			unsigned char ITS0 :1;
		}
		BIT;
	}
	PFCRC;
};

struct st_intc
{
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :4;
			unsigned char SSI11 :1;
			unsigned char SSI10 :1;
			unsigned char SSI9 :1;
			unsigned char SSI8 :1;
			unsigned char SSI7 :1;
			unsigned char SSI6 :1;
			unsigned char SSI5 :1;
			unsigned char SSI4 :1;
			unsigned char SSI3 :1;
			unsigned char SSI2 :1;
			unsigned char SSI1 :1;
			unsigned char SSI0 :1;
		}
		BIT;
	}
	SSIER;
	char wk1[368];
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _IRQ0 :3;
			unsigned char :1;
			unsigned char _IRQ1 :3;
			unsigned char :1;
			unsigned char _IRQ2 :3;
			unsigned char :1;
			unsigned char _IRQ3 :3;
		}
		BIT;
	}
	IPRA;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _IRQ4 :3;
			unsigned char :1;
			unsigned char _IRQ5 :3;
			unsigned char :1;
			unsigned char _IRQ6 :3;
			unsigned char :1;
			unsigned char _IRQ7 :3;
		}
		BIT;
	}
	IPRB;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _IRQ8 :3;
			unsigned char :1;
			unsigned char _IRQ9 :3;
			unsigned char :1;
			unsigned char _IRQ10 :3;
			unsigned char :1;
			unsigned char _IRQ11 :3;
		}
		BIT;
	}
	IPRC;
	char wk2[2];
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :5;
			unsigned char _WDT :3;
		}
		BIT;
	}
	IPRE;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :5;
			unsigned char _AD :3;
			unsigned char :1;
			unsigned char _TPU0 :3;
			unsigned char :1;
			unsigned char _TPU1 :3;
		}
		BIT;
	}
	IPRF;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _TPU2 :3;
			unsigned char :1;
			unsigned char _TPU3 :3;
			unsigned char :1;
			unsigned char _TPU4 :3;
			unsigned char :1;
			unsigned char _TPU5 :3;
		}
		BIT;
	}
	IPRG;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _TMR0 :3;
			unsigned char :1;
			unsigned char _TMR1 :3;
			unsigned char :1;
			unsigned char _TMR2 :3;
			unsigned char :1;
			unsigned char _TMR3 :3;
		}
		BIT;
	}
	IPRH;
	char wk3[4];
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :8;
			unsigned char :1;
			unsigned char _SCI0 :3;
			unsigned char :1;
			unsigned char _SCI1 :3;
		}
		BIT;
	}
	IPRK;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _SCI2 :3;
			unsigned char :4;
			unsigned char :1;
			unsigned char _SCI4 :3;
		}
		BIT;
	}
	IPRL;
	char wk4[8];
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char :3;
			unsigned char :1;
			unsigned char :3;
			unsigned char :1;
			unsigned char _IIC2 :3;
			unsigned char :1;
			unsigned char _SCI5 :3;
		}
		BIT;
	}
	IPRQ;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char _SCI6 :3;
			unsigned char :1;
			unsigned char _TMR :3;
			unsigned char :1;
			unsigned char _USBINT :3;
			unsigned char :1;
			unsigned char _USB :3;
		}
		BIT;
	}
	IPRR;
	char wk5[4];
	union
	{
		unsigned long LONG;
		struct
		{
			unsigned int H;
			unsigned int L;
		}
		WORD;
		struct
		{
			unsigned char :8;
			unsigned char IRQ11SC :2;
			unsigned char IRQ10SC :2;
			unsigned char IRQ9SC :2;
			unsigned char IRQ8SC :2;
			unsigned char IRQ7SC :2;
			unsigned char IRQ6SC :2;
			unsigned char IRQ5SC :2;
			unsigned char IRQ4SC :2;
			unsigned char IRQ3SC :2;
			unsigned char IRQ2SC :2;
			unsigned char IRQ1SC :2;
			unsigned char IRQ0SC :2;
		}
		BIT;
	}
	ISCR;
	char wk6[454];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char INTM :2;
			unsigned char NMIEG :1;
		}
		BIT;
	}
	INTCR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CPUPCE :1;
			unsigned char DTCP :3;
			unsigned char IPSETE :1;
			unsigned char CPUP :3;
		}
		BIT;
	}
	CPUPCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :4;
			unsigned char IRQ11E :1;
			unsigned char IRQ10E :1;
			unsigned char IRQ9E :1;
			unsigned char IRQ8E :1;
			unsigned char IRQ7E :1;
			unsigned char IRQ6E :1;
			unsigned char IRQ5E :1;
			unsigned char IRQ4E :1;
			unsigned char IRQ3E :1;
			unsigned char IRQ2E :1;
			unsigned char IRQ1E :1;
			unsigned char IRQ0E :1;
		}
		BIT;
	}
	IER;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :4;
			unsigned char IRQ11F :1;
			unsigned char IRQ10F :1;
			unsigned char IRQ9F :1;
			unsigned char IRQ8F :1;
			unsigned char IRQ7F :1;
			unsigned char IRQ6F :1;
			unsigned char IRQ5F :1;
			unsigned char IRQ4F :1;
			unsigned char IRQ3F :1;
			unsigned char IRQ2F :1;
			unsigned char IRQ1F :1;
			unsigned char IRQ0F :1;
		}
		BIT;
	}
	ISR;
};

struct st_bsc
{
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char ABWH7 :1;
			unsigned char ABWH6 :1;
			unsigned char ABWH5 :1;
			unsigned char ABWH4 :1;
			unsigned char ABWH3 :1;
			unsigned char ABWH2 :1;
			unsigned char ABWH1 :1;
			unsigned char ABWH0 :1;
			unsigned char ABWL7 :1;
			unsigned char ABWL6 :1;
			unsigned char ABWL5 :1;
			unsigned char ABWL4 :1;
			unsigned char ABWL3 :1;
			unsigned char ABWL2 :1;
			unsigned char ABWL1 :1;
			unsigned char ABWL0 :1;
		}
		BIT;
	}
	ABWCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char AST7 :1;
			unsigned char AST6 :1;
			unsigned char AST5 :1;
			unsigned char AST4 :1;
			unsigned char AST3 :1;
			unsigned char AST2 :1;
			unsigned char AST1 :1;
			unsigned char AST0 :1;
		}
		BIT;
	}
	ASTCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char W7 :3;
			unsigned char :1;
			unsigned char W6 :3;
			unsigned char :1;
			unsigned char W5 :3;
			unsigned char :1;
			unsigned char W4 :3;
		}
		BIT;
	}
	WTCRA;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char :1;
			unsigned char W3 :3;
			unsigned char :1;
			unsigned char W2 :3;
			unsigned char :1;
			unsigned char W1 :3;
			unsigned char :1;
			unsigned char W0 :3;
		}
		BIT;
	}
	WTCRB;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char RDN7 :1;
			unsigned char RDN6 :1;
			unsigned char RDN5 :1;
			unsigned char RDN4 :1;
			unsigned char RDN3 :1;
			unsigned char RDN2 :1;
			unsigned char RDN1 :1;
			unsigned char RDN0 :1;
		}
		BIT;
	}
	RDNCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char CSXH7 :1;
			unsigned char CSXH6 :1;
			unsigned char CSXH5 :1;
			unsigned char CSXH4 :1;
			unsigned char CSXH3 :1;
			unsigned char CSXH2 :1;
			unsigned char CSXH1 :1;
			unsigned char CSXH0 :1;
			unsigned char CSXT7 :1;
			unsigned char CSXT6 :1;
			unsigned char CSXT5 :1;
			unsigned char CSXT4 :1;
			unsigned char CSXT3 :1;
			unsigned char CSXT2 :1;
			unsigned char CSXT1 :1;
			unsigned char CSXT0 :1;
		}
		BIT;
	}
	CSACR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char IDLS3 :1;
			unsigned char IDLS2 :1;
			unsigned char IDLS1 :1;
			unsigned char IDLS0 :1;
			unsigned char IDLCB :2;
			unsigned char IDLCA :2;
			unsigned char IDLSEL7 :1;
			unsigned char IDLSEL6 :1;
			unsigned char IDLSEL5 :1;
			unsigned char IDLSEL4 :1;
			unsigned char IDLSEL3 :1;
			unsigned char IDLSEL2 :1;
			unsigned char IDLSEL1 :1;
			unsigned char IDLSEL0 :1;
		}
		BIT;
	}
	IDLCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char BRLE :1;
			unsigned char BREQOE :1;
			unsigned char :4;
			unsigned char WDBE :1;
			unsigned char WAITE :1;
			unsigned char DKC :1;
		}
		BIT;
	}
	BCR1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char IBCCS :1;
			unsigned char :3;
			unsigned char PWDBE :1;
		}
		BIT;
	}
	BCR2;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char LE7 :1;
			unsigned char LE6 :1;
			unsigned char LE5 :1;
			unsigned char LE4 :1;
			unsigned char LE3 :1;
			unsigned char LE2 :1;
		}
		BIT;
	}
	ENDIANCR;
	char wk[2];
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char BCSEL7 :1;
			unsigned char BCSEL6 :1;
			unsigned char BCSEL5 :1;
			unsigned char BCSEL4 :1;
			unsigned char BCSEL3 :1;
			unsigned char BCSEL2 :1;
			unsigned char BCSEL1 :1;
			unsigned char BCSEL0 :1;
		}
		BIT;
	}
	SRAMCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char BSRM0 :1;
			unsigned char BSTS0 :3;
			unsigned char :2;
			unsigned char BSWD0 :2;
			unsigned char BSRM1 :1;
			unsigned char BSTS1 :3;
			unsigned char :2;
			unsigned char BSWD1 :2;
		}
		BIT;
	}
	BROMCR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char MPXE7 :1;
			unsigned char MPXE6 :1;
			unsigned char MPXE5 :1;
			unsigned char MPXE4 :1;
			unsigned char MPXE3 :1;
			unsigned char :3;
			unsigned char :7;
			unsigned char ADDEX :1;
		}
		BIT;
	}
	MPXCR;
};

union uni_sckcr
{
	unsigned int WORD;
	struct
	{
		unsigned char PSTOP1 :1;
		unsigned char :1;
		unsigned char POSEL1 :1;
		unsigned char :2;
		unsigned char ICK :3;
		unsigned char :1;
		unsigned char PCK :3;
		unsigned char :1;
		unsigned char BCK :3;
	}
	BIT;
};

struct st_mstp
{
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char ACSE :1;
			unsigned char :1;
			unsigned char _DMAC :1;
			unsigned char _DTC :1;
			unsigned char :2;
			unsigned char _TMR23 :1;
			unsigned char _TMR01 :1;
			unsigned char :2;
			unsigned char _DA :1;
			unsigned char :1;
			unsigned char _AD :1;
			unsigned char :2;
			unsigned char _TPU :1;
		}
		BIT;
	}
	CRA;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char _PPG :1;
			unsigned char :2;
			unsigned char _SCI4 :1;
			unsigned char :1;
			unsigned char _SCI2 :1;
			unsigned char _SCI1 :1;
			unsigned char _SCI0 :1;
			unsigned char _IIC1 :1;
			unsigned char _IIC0 :1;
		}
		BIT;
	}
	CRB;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char _SCI5 :1;
			unsigned char _SCI6 :1;
			unsigned char _TMR45 :1;
			unsigned char _TMR67 :1;
			unsigned char _USB :1;
			unsigned char _CRC :1;
			unsigned char :2;
			unsigned char :3;
			unsigned char _RAM4 :1;
			unsigned char _RAM3 :1;
			unsigned char _RAM2 :1;
			unsigned char _RAM1 :1;
			unsigned char _RAM0 :1;
		}
		BIT;
	}
	CRC;
};

union uni_wdt
{
	struct
	{
		union
		{
			unsigned char BYTE;
			struct
			{
				unsigned char OVF :1;
				unsigned char WTIT :1;
				unsigned char TME :1;
				unsigned char :2;
				unsigned char CKS :3;
			}
			BIT;
		}
		TCSR;
		unsigned char TCNT;
		char wk;
		union
		{
			unsigned char BYTE;
			struct
			{
				unsigned char WOVF :1;
				unsigned char RSTE :1;
			}
			BIT;
		}
		RSTCSR;
	}
	READ;
	struct
	{
		unsigned int TCSR;
		unsigned int RSTCSR;
	}
	WRITE;
};

struct st_tpu
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char CST5 :1;
			unsigned char CST4 :1;
			unsigned char CST3 :1;
			unsigned char CST2 :1;
			unsigned char CST1 :1;
			unsigned char CST0 :1;
		}
		BIT;
	}
	TSTR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char SYNC5 :1;
			unsigned char SYNC4 :1;
			unsigned char SYNC3 :1;
			unsigned char SYNC2 :1;
			unsigned char SYNC1 :1;
			unsigned char SYNC0 :1;
		}
		BIT;
	}
	TSYR;
};

struct st_tpu0
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CCLR :3;
			unsigned char CKEG :2;
			unsigned char TPSC :3;
		}
		BIT;
	}
	TCR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char BFB :1;
			unsigned char BFA :1;
			unsigned char MD :4;
		}
		BIT;
	}
	TMDR;
	union
	{
		unsigned int WORD;
		struct
		{
			unsigned char H;
			unsigned char L;
		}
		BYTE;
		struct
		{
			unsigned char IOB :4;
			unsigned char IOA :4;
			unsigned char IOD :4;
			unsigned char IOC :4;
		}
		BIT;
	}
	TIOR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TTGE :1;
			unsigned char :2;
			unsigned char TCIEV :1;
			unsigned char TGIED :1;
			unsigned char TGIEC :1;
			unsigned char TGIEB :1;
			unsigned char TGIEA :1;
		}
		BIT;
	}
	TIER;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char TCFV :1;
			unsigned char TGFD :1;
			unsigned char TGFC :1;
			unsigned char TGFB :1;
			unsigned char TGFA :1;
		}
		BIT;
	}
	TSR;
	unsigned int TCNT;
	unsigned int TGRA;
	unsigned int TGRB;
	unsigned int TGRC;
	unsigned int TGRD;
};

struct st_tmr
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CMIEB :1;
			unsigned char CMIEA :1;
			unsigned char OVIE :1;
			unsigned char CCLR :2;
			unsigned char CKS :3;
		}
		BIT;
	}
	TCR;
	unsigned char wk1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CMFB :1;
			unsigned char CMFA :1;
			unsigned char OVF :1;
			unsigned char ADTE :1;
			unsigned char OS_B :2;
			unsigned char OS_A :2;
		}
		BIT;
	}
	TCSR;
	unsigned char wk2;
	unsigned char TCORA;
	unsigned char wk3;
	unsigned char TCORB;
	unsigned char wk4;
	unsigned char TCNT;
	unsigned char wk5;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char TMRIS :1;
			unsigned char :1;
			unsigned char ICKS :2;
		}
		BIT;
	}
	TCCR;
};

union uni_usb_ifr0
{
	unsigned char BYTE;
	struct
	{
		unsigned char BRST :1;
		unsigned char EP1FULL :1;
		unsigned char EP2TR :1;
		unsigned char EP2EMPTY :1;
		unsigned char SETUPTS :1;
		unsigned char EP0oTS :1;
		unsigned char EP0iTR :1;
		unsigned char EP0iTS :1;
	}
	BIT;

};

union uni_usb_ifr1
{
	unsigned char BYTE;
	struct
	{
		unsigned char :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char VBUSMN :1;
		unsigned char EP3TR :1;
		unsigned char EP3TS :1;
		unsigned char VBUSF :1;
	}
	BIT;
};

union uni_usb_ifr2
{
	unsigned char BYTE;
	struct
	{
		unsigned char :1;
		unsigned char :1;
		unsigned char SURSS :1;
		unsigned char SURSF :1;
		unsigned char CFDN :1;
		unsigned char :1;
		unsigned char SETC :1;
		unsigned char SETI :1;
	}
	BIT;
};

union uni_usb_ier0
{
	unsigned char BYTE;
	struct
	{
		unsigned char BRST :1;
		unsigned char EP1FULL :1;
		unsigned char EP2TR :1;
		unsigned char EP2EMPTY :1;
		unsigned char SETUPTS :1;
		unsigned char EP0oTS :1;
		unsigned char EP0iTR :1;
		unsigned char EP0iTS :1;
	}
	BIT;
};

union uni_usb_ier1
{
	unsigned char BYTE;
	struct
	{
		unsigned char :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char EP3TR :1;
		unsigned char EP3TS :1;
		unsigned char VBUSF :1;
	}
	BIT;
};

union uni_usb_ier2
{
	unsigned char BYTE;
	struct
	{
		unsigned char SSRSME :1;
		unsigned char :1;
		unsigned char :1;
		unsigned char SURSE :1;
		unsigned char CFDN :1;
		unsigned char :1;
		unsigned char SETCE :1;
		unsigned char SETIE :1;
	}
	BIT;
};

struct st_usb
{
	union uni_usb_ifr0 IFR0;
	union uni_usb_ifr1 IFR1;
	union uni_usb_ifr2 IFR2;
	unsigned char wk0;
	union uni_usb_ier0 IER0;
	union uni_usb_ier1 IER1;
	union uni_usb_ier2 IER2;
	char wk1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char BRST :1;
			unsigned char EP1FULL :1;
			unsigned char EP2TR :1;
			unsigned char EP2EMPTY :1;
			unsigned char SETUPTS :1;
			unsigned char EP0oTS :1;
			unsigned char EP0iTR :1;
			unsigned char EP0iTS :1;
		}
		BIT;
	}
	ISR0;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :5;
			unsigned char EP3TR :1;
			unsigned char EP3TS :1;
			unsigned char VBUSF :1;
		}
		BIT;
	}
	ISR1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char SURSE :1;
			unsigned char CFDN :1;
			unsigned char :1;
			unsigned char SETCE :1;
			unsigned char SETIE :1;
		}
		BIT;
	}
	ISR2;
	char wk2;
	unsigned char EPDR0i;
	unsigned char EPDR0o;
	unsigned char EPDR0s;
	char wk3;
	unsigned char EPDR1;
	char wk4[3];
	unsigned char EPDR2;
	char wk5[3];
	unsigned char EPDR3;
	char wk6[11];
	unsigned char EPSZ0o;
	unsigned char EPSZ1;
	char wk7;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :2;
			unsigned char EP3DE :1;
			unsigned char EP2DE :1;
			unsigned char :3;
			unsigned char EP0iDE :1;
		}
		BIT;
	}
	DASTS;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :1;
			unsigned char EP3CLR :1;
			unsigned char EP1CLR :1;
			unsigned char EP2CLR :1;
			unsigned char :2;
			unsigned char EP0oCLR :1;
			unsigned char EP0iCLR :1;
		}
		BIT;
	}
	FCLR;
	char wk8;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char EP3STL :1;
			unsigned char EP2STL :1;
			unsigned char EP1STL :1;
			unsigned char EP0STL :1;
		}
		BIT;
	}
	EPSTL;
	char wk9;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :1;
			unsigned char EP3PKTE :1;
			unsigned char EP1RDFN :1;
			unsigned char EP2PKTE :1;
			unsigned char :1;
			unsigned char EP0sRDFN :1;
			unsigned char EP0oRDFN :1;
			unsigned char EP0iPKTE :1;
		}
		BIT;
	}
	TRG;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :5;
			unsigned char PULLUP_E :1;
			unsigned char EP2DMAE :1;
			unsigned char EP1DMAE :1;
		}
		BIT;
	}
	DMA;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CNFV :2;
			unsigned char INTV :2;
			unsigned char :1;
			unsigned char ALTV :3;
		}
		BIT;
	}
	CVR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char RWUPS :1;
			unsigned char RSME :1;
			unsigned char PWMD :1;
			unsigned char ASCE :1;
			unsigned char :1;
		}
		BIT;
	}
	CTLR;
	char wk10[2];
	unsigned char EPIR;
	char wk11[17];
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char PTSTE :1;
			unsigned char :3;
			unsigned char SUSPEND :1;
			unsigned char txenl :1;
			unsigned char txse0 :1;
			unsigned char txdata :1;
		}
		BIT;
	}
	TRNTREG0;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :5;
			unsigned char xver_data :1;
			unsigned char dpls :1;
			unsigned char dmns :1;
		}
		BIT;
	}
	TRNTREG1;
};

struct st_sci
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char CA :1;
			unsigned char CHR :1;
			unsigned char _PE :1;
			unsigned char OE :1;
			unsigned char STOP :1;
			unsigned char MP :1;
			unsigned char CKS :2;
		}
		BIT;
	}
	SMR;
	unsigned char BRR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TIE :1;
			unsigned char RIE :1;
			unsigned char TE :1;
			unsigned char RE :1;
			unsigned char MPIE :1;
			unsigned char TEIE :1;
			unsigned char CKE :2;
		}
		BIT;
	}
	SCR;
	unsigned char TDR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TDRE :1;
			unsigned char RDRF :1;
			unsigned char ORER :1;
			unsigned char FER :1;
			unsigned char PER :1;
			unsigned char TEND :1;
			unsigned char MPB :1;
			unsigned char MPBT :1;
		}
		BIT;
	}
	SSR;
	unsigned char RDR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :4;
			unsigned char SDIR :1;
			unsigned char SINV :1;
			unsigned char :1;
			unsigned char SMIF :1;
		}
		BIT;
	}
	SCMR;
};

struct st_ad
{
	unsigned int ADDRA;
	unsigned int ADDRB;
	unsigned int ADDRC;
	unsigned int ADDRD;
	unsigned int ADDRE;
	unsigned int ADDRF;
	unsigned int ADDRG;
	unsigned int ADDRH;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char ADF :1;
			unsigned char ADIE :1;
			unsigned char ADST :1;
			unsigned char :1;
			unsigned char CH :4;
		}
		BIT;
	}
	ADCSR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TRGS :2;
			unsigned char SCAN :2;
			unsigned char CKS :2;
		}
		BIT;
	}
	ADCR;
};

struct st_da
{
	unsigned char DADR0;
	unsigned char DADR1;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char DAOE1 :1;
			unsigned char DAOE0 :1;
			unsigned char DAE :1;
			unsigned char :5;
		}
		BIT;
	}
	DACR01;
};

struct st_iic
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char ICE :1;
			unsigned char RCVD :1;
			unsigned char MST :1;
			unsigned char TRS :1;
			unsigned char CKS :4;
		}
		BIT;
	}
	ICCRA;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char BBUSY :1;
			unsigned char SCP :1;
			unsigned char SDAO :1;
			unsigned char :1;
			unsigned char SCLO :1;
			unsigned char :1;
			unsigned char IICRST :1;
			unsigned char :1;
		}
		BIT;
	}
	ICCRB;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :1;
			unsigned char WAIT :1;
			unsigned char :2;
			unsigned char BCWP :1;
			unsigned char BC :3;
		}
		BIT;
	}
	ICMR;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TIE :1;
			unsigned char TEIE :1;
			unsigned char RIE :1;
			unsigned char NAKIE :1;
			unsigned char STIE :1;
			unsigned char ACKE :1;
			unsigned char ACKBR :1;
			unsigned char ACKBT :1;
		}
		BIT;
	}
	ICIER;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char TDRE :1;
			unsigned char TEND :1;
			unsigned char RDRF :1;
			unsigned char NACKF :1;
			unsigned char STOP :1;
			unsigned char AL :1;
			unsigned char AAS :1;
			unsigned char ADZ :1;
		}
		BIT;
	}
	ICSR;
	unsigned char SAR;
	unsigned char ICDRT;
	unsigned char ICDRR;
};

struct st_flash
{
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :3;
			unsigned char FLER :1;
			unsigned char :3;
			unsigned char SCO :1;
		}
		BIT;
	}
	FCCS;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :7;
			unsigned char PPVS :1;
		}
		BIT;
	}
	FPCS;
	union
	{
		unsigned char BYTE;
		struct
		{
			unsigned char :7;
			unsigned char EPVB :1;
		}
		BIT;
	}
	FECS;
	unsigned char wk1;
	unsigned char FKEY;
	unsigned char wk2;
	unsigned char FTDAR;
};

/** FLASH address. */
#define FLASH	( *(volatile struct st_flash *)ADD_BASE_ADDR( 0xfde8 ) )

/** PM address. */
#define PM		( *(volatile struct st_pm *)ADD_BASE_ADDR( 0xee50 ) )

/** P1 address. */
#define P1		( *(volatile struct st_p1 *)ADD_BASE_ADDR( 0xfb80 ) )

/** P2 address. */
#define P2		( *(volatile struct st_p2 *)ADD_BASE_ADDR( 0xfb81 ) )

/** P3 address. */
#define P3		( *(volatile struct st_p3 *)ADD_BASE_ADDR( 0xfb82 ) )

/** P5 address. */
#define P5		( *(volatile struct st_p5 *)ADD_BASE_ADDR( 0xfb94 ) )

/** P6 address. */
#define P6		( *(volatile struct st_p6 *)ADD_BASE_ADDR( 0xfb85 ) )

/** PA address. */
#define PA		( *(volatile struct st_pa *)ADD_BASE_ADDR( 0xfb89 ) )

/** PB address. */
#define PB		( *(volatile struct st_pb *)ADD_BASE_ADDR( 0xfb8a ) )

/** PD address. */
#define PD		( *(volatile struct st_pd *)ADD_BASE_ADDR( 0xfb8c ) )

/** PE address. */
#define PE		( *(volatile struct st_pe *)ADD_BASE_ADDR( 0xfb8d ) )

/** PF address. */
#define PF		( *(volatile struct st_pf *)ADD_BASE_ADDR( 0xfb8e ) )

/** PH address. */
#define PH		( *(volatile struct st_ph *)ADD_BASE_ADDR( 0xfba0 ) )

/** PI address. */
#define PI		( *(volatile struct st_pi *)ADD_BASE_ADDR( 0xfba1 ) )

/** PFC address. */
#define PFC		( *(volatile struct st_pfc *)ADD_BASE_ADDR( 0xfbc0 ) )

/** INTC address. */
#define INTC	( *(volatile struct st_intc *)ADD_BASE_ADDR( 0xfbce ) )

/** BSC address. */
#define BSC		( *(volatile struct st_bsc *)ADD_BASE_ADDR( 0xfd84 ) )

/** SCKCR address. */
#define SCKCR	( *(volatile union uni_sckcr *)ADD_BASE_ADDR( 0xfdc4 ) )

/** MSTP address. */
#define MSTP	( *(volatile struct st_mstp *)ADD_BASE_ADDR( 0xfdc8 ) )

/** A/D address. */
#define AD		( *(volatile struct st_ad *)ADD_BASE_ADDR( 0xff90 ) )

/** WDT address. */
#define WDT		( *(volatile union uni_wdt *)ADD_BASE_ADDR( 0xffa4 ) )

/** TPU address. */
#define TPU		( *(volatile struct st_tpu *)ADD_BASE_ADDR( 0xffbc ) )

/** TPU0 address. */
#define TPU0	( *(volatile struct st_tpu0 *)ADD_BASE_ADDR( 0xffc0 ) )

/** USB address. */
#define USB		( *(volatile struct st_usb *)ADD_BASE_ADDR( 0xee00 ) )

/** TMR0 address. */
#define TMR0	( *(volatile struct st_tmr *)ADD_BASE_ADDR( 0xffb0 ) )

/** TMR23 address. */
#define TMR1	( *(volatile struct st_tmr *)ADD_BASE_ADDR( 0xfec0 ) )

/** SCI0 address. */
#define SCI0	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xff80 ) )

/** SCI1 address. */
#define SCI1	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xff88 ) )

/** SCI2 address. */
#define SCI2	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xff60 ) )

/** SCI4 address. */
#define SCI4	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xfe90 ) )

/** SCI5 address. */
#define SCI5	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xf600 ) )

/** SCI6 address. */
#define SCI6	( *(volatile struct st_sci *)ADD_BASE_ADDR( 0xf610 ) )

/** DA address. */
#define DA		( *(volatile struct st_da *)ADD_BASE_ADDR( 0xff68 ) )

/** IIC0 address. */
#define IIC0	( *(volatile struct st_iic *)ADD_BASE_ADDR( 0xfeb0 ) )

#if 0
/** IIC1 address. */
#define IIC1	( *(volatile struct st_iic	*)ADD_BASE_ADDR( 0xfeb8 ) )
#endif

#endif

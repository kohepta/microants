﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_1653f_impl.h
 *
 * definitions of BIOS H8SX 1653F implementation.
 *
 * @date 2010.08.22 new creation
 */

#ifndef __BIOS_1653F_IMPL_H__
#define __BIOS_1653F_IMPL_H__

#include <bios.h>
#include <bios_1653f_regs.h>

BIOS_INLINE BIOS_VINT8 bios_1653f_reb_mem(
	BIOS_VP mem )
{
	BIOS_VINT8 data = *( (volatile BIOS_VINT8 *) mem );
	return data;
}

BIOS_INLINE void bios_1653f_wrb_mem(
	BIOS_VP mem,
	BIOS_VINT8 data )
{
	*( (volatile BIOS_VINT8 *) mem ) = data;
}

BIOS_INLINE BIOS_VINT16 bios_1653f_reh_mem(
	BIOS_VP mem )
{
	BIOS_VINT16 data = *( (volatile BIOS_VINT16 *) mem );
	return data;
}

BIOS_INLINE void bios_1653f_wrh_mem(
	BIOS_VP mem,
	BIOS_VINT16 data )
{
	*( (volatile BIOS_VINT16 *) mem ) = data;
}

BIOS_INLINE BIOS_VINT32 bios_1653f_rew_mem(
	BIOS_VP mem )
{
	BIOS_VINT32 data = *( (volatile BIOS_VINT32 *) mem );
	return data;
}

BIOS_INLINE void bios_1653f_wrw_mem(
	BIOS_VP mem,
	BIOS_VINT32 data )
{
	*( (volatile BIOS_VINT32 *) mem ) = data;
}

BIOS_INLINE BIOS_VINT8 bios_1653f_reb_reg(
	BIOS_VINT32 offset_addr )
{
	return bios_1653f_reb_mem( ADD_BASE_ADDR( offset_addr ) );
}

BIOS_INLINE void bios_1653f_wrb_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT8 val )
{
	bios_1653f_wrb_mem( ADD_BASE_ADDR( offset_addr ), val );
}

BIOS_INLINE BIOS_VINT16 bios_1653f_reh_reg(
	BIOS_VINT32 offset_addr )
{
	return bios_1653f_reh_mem( ADD_BASE_ADDR( offset_addr ) );
}

BIOS_INLINE void bios_1653f_wrh_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT16 val )
{
	bios_1653f_wrh_mem( ADD_BASE_ADDR( offset_addr ), val );
}

BIOS_INLINE void bios_1653f_orb_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT8 val )
{
	bios_1653f_wrb_reg(
		offset_addr,
		(BIOS_VINT8)( bios_1653f_reb_reg( offset_addr ) | val ) );
}

BIOS_INLINE void bios_1653f_andb_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT8 val )
{
	bios_1653f_wrb_reg(
		offset_addr,
		(BIOS_VINT8)( bios_1653f_reb_reg( offset_addr ) & val ) );
}

BIOS_INLINE void bios_1653f_orh_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT16 val )
{
	bios_1653f_wrh_reg(
		offset_addr,
		(BIOS_VINT16)( bios_1653f_reh_reg( offset_addr ) | val ) );
}

BIOS_INLINE void bios_1653f_andh_reg(
	BIOS_VINT32 offset_addr,
	BIOS_VINT16 val )
{
	bios_1653f_wrh_reg(
		offset_addr,
		(BIOS_VINT16)( bios_1653f_reh_reg( offset_addr ) & val ) );
}

#endif

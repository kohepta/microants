﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_1653f_regs.h
 *
 * definitions of BIOS H8SX 1653F register definitions.
 *
 * @date 2010.08.22 new creation
 */

#ifndef __BIOS_1653F_REGS_H__
#define __BIOS_1653F_REGS_H__

/** advance mode base address. */
#define BASE_ADDR		0xff0000

/** addition of base address. */
#define ADD_BASE_ADDR( addr )	\
	(BIOS_VP)( BASE_ADDR + (BIOS_UINT32)(BIOS_UINT16)( addr ) )

/** bit number definition. */
#define BIT0				0x1
#define BIT1				0x2
#define BIT2				0x4
#define BIT3				0x8
#define BIT4				0x10
#define BIT5				0x20
#define BIT6				0x40
#define BIT7				0x80
#define BIT8				0x100
#define BIT9				0x200
#define BIT10				0x400
#define BIT11				0x800
#define BIT12				0x1000
#define BIT13				0x2000
#define BIT14				0x4000
#define BIT15				0x8000

#define DUMMY				0

/**
 * module stop control register.
 */
 
/** offset address. */
#define ADDR_MSTPCRA		0xfdc8
#define ADDR_MSTPCRB		0xfdca
#define ADDR_MSTPCRC		0xfdcc

/** bit pattern of each register. */

/** MSTPCRA */
#define BIT_MSTPCRA_ACSE	BIT15
#define BIT_MSTPCRA_DMAC	BIT13
#define BIT_MSTPCRA_DTC		BIT12
#define BIT_MSTPCRA_TMR3_2	BIT9
#define BIT_MSTPCRA_TMR1_0	BIT8
#define BIT_MSTPCRA_DA		BIT5
#define BIT_MSTPCRA_AD		BIT3
#define BIT_MSTPCRA_TPU		BIT0

/** MSTPCRB */
#define BIT_MSTPCRB_PPG		BIT15
#define BIT_MSTPCRB_SCI4	BIT12
#define BIT_MSTPCRB_SCI2	BIT10
#define BIT_MSTPCRB_SCI1	BIT9
#define BIT_MSTPCRB_SCI0	BIT8
#define BIT_MSTPCRB_IIC1	BIT7
#define BIT_MSTPCRB_IIC0	BIT6

/** MSTPCRC */
#define BIT_MSTPCRC_SCI5	BIT15
#define BIT_MSTPCRC_IrDA	BIT15
#define BIT_MSTPCRC_SCI6	BIT14
#define BIT_MSTPCRC_TMR4_5	BIT13
#define BIT_MSTPCRC_TMR6_7	BIT12
#define BIT_MSTPCRC_USB		BIT11
#define BIT_MSTPCRC_CRC		BIT10
#define BIT_MSTPCRC_RAM4	BIT4	/**< 0xff_2000 - 0xff_3fff */
#define BIT_MSTPCRC_RAM3	BIT3	/**< 0xff_4000 - 0xff_5fff */
#define BIT_MSTPCRC_RAM2	BIT2	/**< 0xff_6000 - 0xff_7fff */
#define BIT_MSTPCRC_RAM1	BIT1	/**< 0xff_8000 - 0xff_9fff */
#define BIT_MSTPCRC_RAM0	BIT0	/**< 0xff_a000 - 0xff_bfff */

/**
 * interrupt controller (INTC)
 */

/** offset address. */
#define ADDR_SSIER			0xfbce
#define ADDR_IPRA    		0xfd40
#define ADDR_IPRB    		0xfd42
#define ADDR_IPRC    		0xfd44
#define ADDR_IPRE    		0xfd48
#define ADDR_IPRF    		0xfd4a
#define ADDR_IPRG    		0xfd4c
#define ADDR_IPRH    		0xfd4e
#define ADDR_IPRI    		0xfd50
#define ADDR_IPRK    		0xfd54
#define ADDR_IPRL    		0xfd56
#define ADDR_IPRQ    		0xfd60
#define ADDR_IPRR    		0xfd62
#define ADDR_ISCRH   		0xfd68
#define ADDR_ISCRL   		0xfd6a
#define ADDR_DTCERA  		0xff20
#define ADDR_DTCERB  		0xff22
#define ADDR_DTCERC  		0xff24
#define ADDR_DTCERD  		0xff26
#define ADDR_DTCERE  		0xff28
#define ADDR_DTCERG  		0xff2c
#define ADDR_DTCERH  		0xff2e
#define ADDR_DTCCR   		0xff30
#define ADDR_INTCR   		0xff32
#define ADDR_CPUPCR  		0xff33
#define ADDR_IER     		0xff34
#define ADDR_ISR     		0xff36

/** bit pattern of each register. */

/** INTCR */
#define BIT_INTM1			BIT5
#define BIT_INTM0			BIT4
#define BIT_NMIEG			BIT3

/** IPRx (x = A-C, E-I, K, L, Q, R) */
#define BIT_IPR14			BIT14
#define BIT_IPR13			BIT13
#define BIT_IPR12			BIT12
#define BIT_IPR10			BIT10
#define BIT_IPR9			BIT9
#define BIT_IPR8			BIT8
#define BIT_IPR6			BIT6
#define BIT_IPR5			BIT5
#define BIT_IPR4			BIT4
#define BIT_IPR2			BIT2
#define BIT_IPR1			BIT1
#define BIT_IPR0			BIT0

/** IER */
#define BIT_IRQ11E			BIT11
#define BIT_IRQ10E			BIT10
#define BIT_IRQ9E			BIT9
#define BIT_IRQ8E			BIT8
#define BIT_IRQ7E			BIT7
#define BIT_IRQ6E			BIT6
#define BIT_IRQ5E			BIT5
#define BIT_IRQ4E			BIT4
#define BIT_IRQ3E			BIT3
#define BIT_IRQ2E			BIT2
#define BIT_IRQ1E			BIT1
#define BIT_IRQ0E			BIT0

/** ISCRH */
#define BIT_IRQ11SR			BIT7
#define BIT_IRQ11SF			BIT6
#define BIT_IRQ10SR			BIT5
#define BIT_IRQ10SF			BIT4
#define BIT_IRQ9SR			BIT3
#define BIT_IRQ9SF			BIT2
#define BIT_IRQ8SR			BIT1
#define BIT_IRQ8SF			BIT0

/** ISCRL */
#define BIT_IRQ7SR			BIT15
#define BIT_IRQ7SF			BIT14
#define BIT_IRQ6SR			BIT13
#define BIT_IRQ6SF			BIT12
#define BIT_IRQ5SR			BIT11
#define BIT_IRQ5SF			BIT10
#define BIT_IRQ4SR			BIT9
#define BIT_IRQ4SF			BIT8
#define BIT_IRQ3SR			BIT7
#define BIT_IRQ3SF			BIT6
#define BIT_IRQ2SR			BIT5
#define BIT_IRQ2SF			BIT4
#define BIT_IRQ1SR			BIT3
#define BIT_IRQ1SF			BIT2
#define BIT_IRQ0SR			BIT1
#define BIT_IRQ0SF			BIT0

/** ISR */
#define BIT_IRQ11F			BIT11
#define BIT_IRQ10F			BIT10
#define BIT_IRQ9F			BIT9
#define BIT_IRQ8F			BIT8
#define BIT_IRQ7F			BIT7
#define BIT_IRQ6F			BIT6
#define BIT_IRQ5F			BIT5
#define BIT_IRQ4F			BIT4
#define BIT_IRQ3F			BIT3
#define BIT_IRQ2F			BIT2
#define BIT_IRQ1F			BIT1
#define BIT_IRQ0F			BIT0

/**
 *  bus controller (BSC)
 */

/** offset address. */
#define ADDR_DTCVBR			0xfd80
#define ADDR_ABWCR			0xfd84
#define ADDR_ASTCR			0xfd86
#define ADDR_WTCRA			0xfd88
#define ADDR_WTCRB			0xfd8a
#define ADDR_RDNCR			0xfd8c
#define ADDR_CSACR			0xfd8e
#define ADDR_IDLCR			0xfd90
#define ADDR_BCR1 			0xfd92
#define ADDR_BCR2 			0xfd94
#define ADDR_ENDIANCR		0xfd95
#define ADDR_SRAMCR			0xfd98
#define ADDR_BROMCR			0xfd9a
#define ADDR_MPXCR			0xfd9c
#define ADDR_RAMER			0xfd9e

/** bit pattern of each register. */

/** ABWCR */
#define BIT_ABWH7			BIT15
#define BIT_ABWH6			BIT14
#define BIT_ABWH5			BIT13
#define BIT_ABWH4			BIT12
#define BIT_ABWH3			BIT11
#define BIT_ABWH2			BIT10
#define BIT_ABWH1			BIT9
#define BIT_ABWH0			BIT8
#define BIT_ABWL7			BIT7
#define BIT_ABWL6			BIT6
#define BIT_ABWL5			BIT5
#define BIT_ABWL4			BIT4
#define BIT_ABWL3			BIT3
#define BIT_ABWL2			BIT2
#define BIT_ABWL1			BIT1
#define BIT_ABWL0			BIT0

/** ASTCR */
#define BIT_AST7			BIT15
#define BIT_AST6			BIT14
#define BIT_AST5			BIT13
#define BIT_AST4			BIT12
#define BIT_AST3			BIT11
#define BIT_AST2			BIT10
#define BIT_AST1			BIT9
#define BIT_AST0			BIT8

/** WTCRA */
#define BIT_W72				BIT14
#define BIT_W71				BIT13
#define BIT_W70				BIT12
#define BIT_W62				BIT10
#define BIT_W61				BIT9
#define BIT_W60				BIT8
#define BIT_W52				BIT6
#define BIT_W51				BIT5
#define BIT_W50				BIT4
#define BIT_W42				BIT2
#define BIT_W41				BIT1
#define BIT_W40				BIT0

/** WTCRB */				
#define BIT_W32				BIT14
#define BIT_W31				BIT13
#define BIT_W30				BIT12
#define BIT_W22				BIT10
#define BIT_W21				BIT9
#define BIT_W20				BIT8
#define BIT_W12				BIT6
#define BIT_W11				BIT5
#define BIT_W10				BIT4
#define BIT_W02				BIT2
#define BIT_W01				BIT1
#define BIT_W00				BIT0

/** RDNCR */
#define BIT_RDN7			BIT15
#define BIT_RDN6			BIT14
#define BIT_RDN5			BIT13
#define BIT_RDN4			BIT12
#define BIT_RDN3			BIT11
#define BIT_RDN2			BIT10
#define BIT_RDN1			BIT9
#define BIT_RDN0			BIT8

/** CSACR */
#define BIT_CSXH7			BIT15
#define BIT_CSXH6			BIT14
#define BIT_CSXH5			BIT13
#define BIT_CSXH4			BIT12
#define BIT_CSXH3			BIT11
#define BIT_CSXH2			BIT10
#define BIT_CSXH1			BIT9
#define BIT_CSXH0			BIT8
#define BIT_CSXT7			BIT7
#define BIT_CSXT6			BIT6
#define BIT_CSXT5			BIT5
#define BIT_CSXT4			BIT4
#define BIT_CSXT3			BIT3
#define BIT_CSXT2			BIT2
#define BIT_CSXT1			BIT1
#define BIT_CSXT0			BIT0

/** IDLCR */
#define BIT_IDLS3			BIT15
#define BIT_IDLS2			BIT14
#define BIT_IDLS1			BIT13
#define BIT_IDLS0			BIT12
#define BIT_IDLCB1			BIT11
#define BIT_IDLCB0			BIT10
#define BIT_IDLCA1			BIT9
#define BIT_IDLCA0			BIT8
#define BIT_IDLSEL7			BIT7
#define BIT_IDLSEL6			BIT6
#define BIT_IDLSEL5			BIT5
#define BIT_IDLSEL4			BIT4
#define BIT_IDLSEL3			BIT3
#define BIT_IDLSEL2			BIT2
#define BIT_IDLSEL1			BIT1
#define BIT_IDLSEL0			BIT0

/** BCR1 */
#define BIT_BRLE			BIT15
#define BIT_BREQOE			BIT14
#define BIT_WDBE			BIT9
#define BIT_WAITE			BIT8
#define BIT_DKC				BIT7

/** BCR2 */
#define BIT_IBCCS			BIT4
#define BIT_PWDIBE			BIT0

/** ENDIANCR */
#define BIT_LE7				BIT7
#define BIT_LE6				BIT6
#define BIT_LE5				BIT5
#define BIT_LE4				BIT4
#define BIT_LE3				BIT3
#define BIT_LE2				BIT2

/** SRAMCR */
#define BIT_BCSEL7			BIT15
#define BIT_BCSEL6			BIT14
#define BIT_BCSEL5			BIT13
#define BIT_BCSEL4			BIT12
#define BIT_BCSEL3			BIT11
#define BIT_BCSEL2			BIT10
#define BIT_BCSEL1			BIT9
#define BIT_BCSEL0			BIT8

/** BROMCR */
#define BIT_BSRM0			BIT15
#define BIT_BSTS02			BIT14
#define BIT_BSTS01			BIT13
#define BIT_BSTS00			BIT12
#define BIT_BSWD01			BIT9
#define BIT_BSWD00			BIT8
#define BIT_BSRM1			BIT7
#define BIT_BSTS12			BIT6
#define BIT_BSTS11			BIT5
#define BIT_BSTS10			BIT4
#define BIT_BSWD11			BIT1
#define BIT_BSWD10			BIT0

/** MPXCR */
#define BIT_MPXE7			BIT15
#define BIT_MPXE6			BIT14
#define BIT_MPXE5			BIT13
#define BIT_MPXE4			BIT12
#define BIT_MPXE3			BIT11
#define BIT_ADDEX			BIT0

/**
 *  I/O port
 */

/** offset address. */
#define ADDR_P1DDR			0xfb80
#define ADDR_P1ICR			0xfb90
#define ADDR_PORT1			0xff40
#define ADDR_P1DR			0xff50

#define ADDR_P2DDR			0xfb81
#define ADDR_P2ICR			0xfb91
#define ADDR_P2ODR			0xfbbc
#define ADDR_PORT2			0xff41
#define ADDR_P2DR			0xff51

#define ADDR_P5ICR			0xfb94
#define ADDR_PORT5			0xff44

#define ADDR_P6DDR			0xfb85
#define ADDR_P6ICR			0xfb95
#define ADDR_PORT6			0xff45
#define ADDR_P6DR			0xff55

#define ADDR_PADDR			0xfb89
#define ADDR_PAICR			0xfb99
#define ADDR_PORTA			0xff49
#define ADDR_PADR			0xff59

#define ADDR_PBDDR			0xfb8a
#define ADDR_PBICR			0xfb9a
#define ADDR_PORTB			0xff4a
#define ADDR_PBDR			0xff5a

#define ADDR_PDDDR			0xfb8c
#define ADDR_PDICR			0xfb9c
#define ADDR_PDPCR			0xfbb4
#define ADDR_PORTD			0xff4c
#define ADDR_PDDR			0xff5c

#define ADDR_PEDDR			0xfb8d
#define ADDR_PEICR			0xfb9d
#define ADDR_PEPCR			0xfbb5
#define ADDR_PORTE			0xff4d
#define ADDR_PEDR			0xff5d

#define ADDR_PFDDR			0xfb8e
#define ADDR_PFICR			0xfb9e
#define ADDR_PFPCR			0xfbb6
#define ADDR_PFODR			0xfbbd
#define ADDR_PORTF			0xff4e
#define ADDR_PFDR			0xff5e

#define ADDR_PORTH			0xfba0
#define ADDR_PHDR			0xfba4
#define ADDR_PHDDR			0xfba8
#define ADDR_PHICR			0xfbac
#define ADDR_PHPCR			0xfbb8

#define ADDR_PORTI			0xfba1
#define ADDR_PIDR			0xfba5
#define ADDR_PIDDR			0xfba9
#define ADDR_PIICR			0xfbad
#define ADDR_PIPCR			0xfbb9

#define ADDR_PMDDR			0xee50
#define ADDR_PMDR			0xee51
#define ADDR_PORTM			0xee52
#define ADDR_PMICR			0xee53

#define ADDR_PFCR0			0xfbc0
#define ADDR_PFCR1			0xfbc1
#define ADDR_PFCR2			0xfbc2
#define ADDR_PFCR4			0xfbc4
#define ADDR_PFCR6			0xfbc6
#define ADDR_PFCR7			0xfbc7
#define ADDR_PFCR9			0xfbc9
#define ADDR_PFCRB			0xfbcb
#define ADDR_PFCRC			0xfbcc

/** bit pattern of each register. */

/** PnDDR (n=1, 2, 6, A, B, D-F, H, I, M) */
#define BIT_Pn7DDR			BIT7
#define BIT_Pn6DDR			BIT6
#define BIT_Pn5DDR			BIT5
#define BIT_Pn4DDR			BIT4
#define BIT_Pn3DDR			BIT3
#define BIT_Pn2DDR			BIT2
#define BIT_Pn1DDR			BIT1
#define BIT_Pn0DDR			BIT0

/** PnDR (n=1, 2, 6, A, B, D-F, H, I, M) */
#define BIT_Pn7DR			BIT7
#define BIT_Pn6DR			BIT6
#define BIT_Pn5DR			BIT5
#define BIT_Pn4DR			BIT4
#define BIT_Pn3DR			BIT3
#define BIT_Pn2DR			BIT2
#define BIT_Pn1DR			BIT1
#define BIT_Pn0DR			BIT0

/** PORTn (n=1, 2, 5, 6, A, B, D-F, H, I, M) */
#define BIT_Pn7				BIT7
#define BIT_Pn6				BIT6
#define BIT_Pn5				BIT5
#define BIT_Pn4				BIT4
#define BIT_Pn3				BIT3
#define BIT_Pn2				BIT2
#define BIT_Pn1				BIT1
#define BIT_Pn0				BIT0

/** PnICR (n=1, 2, 5, 6, A, B, D-F, H, I, M) */
#define BIT_Pn7ICR			BIT7
#define BIT_Pn6ICR			BIT6
#define BIT_Pn5ICR			BIT5
#define BIT_Pn4ICR			BIT4
#define BIT_Pn3ICR			BIT3
#define BIT_Pn2ICR			BIT2
#define BIT_Pn1ICR			BIT1
#define BIT_Pn0ICR			BIT0

/** PnPCR (n=D-F, H, I) */
#define BIT_Pn7PCR			BIT7
#define BIT_Pn6PCR			BIT6
#define BIT_Pn5PCR			BIT5
#define BIT_Pn4PCR			BIT4
#define BIT_Pn3PCR			BIT3
#define BIT_Pn2PCR			BIT2
#define BIT_Pn1PCR			BIT1
#define BIT_Pn0PCR			BIT0

/** PnODR (n=2, F) */
#define BIT_Pn7ODR			BIT7
#define BIT_Pn6ODR			BIT6
#define BIT_Pn5ODR			BIT5
#define BIT_Pn4ODR			BIT4
#define BIT_Pn3ODR			BIT3
#define BIT_Pn2ODR			BIT2
#define BIT_Pn1ODR			BIT1
#define BIT_Pn0ODR			BIT0

/** PFCR0 */
#define BIT_CS7E			BIT7
#define BIT_CS6E			BIT6
#define BIT_CS5E			BIT5
#define BIT_CS4E			BIT4
#define BIT_CS3E			BIT3
#define BIT_CS2E			BIT2
#define BIT_CS1E			BIT1
#define BIT_CS0E			BIT0

/** PFCR1 */
#define BIT_CS7SA			BIT7
#define BIT_CS7SB			BIT6
#define BIT_CS6SA			BIT5
#define BIT_CS6SB			BIT4
#define BIT_CS5SA			BIT3
#define BIT_CS5SB			BIT2

/** PFCR2 */
#define BIT_CS2S			BIT6
#define BIT_BSS				BIT5
#define BIT_BSE				BIT4
#define BIT_RDWRE			BIT2
#define BIT_ASOE			BIT1

/** PFCR4 */
#define BIT_A23E			BIT7
#define BIT_A22E			BIT6
#define BIT_A21E			BIT5
#define BIT_A20E			BIT4
#define BIT_A19E			BIT3
#define BIT_A18E			BIT2
#define BIT_A17E			BIT1
#define BIT_A16E			BIT0

/** PFCR6 */
#define BIT_LHWROE			BIT6
#define BIT_TCLKS			BIT3

/** PFCR7 */
#define BIT_DMAS3A			BIT7
#define BIT_DMAS3B			BIT6
#define BIT_DMAS2A			BIT5
#define BIT_DMAS2B			BIT4
#define BIT_DMAS1A			BIT3
#define BIT_DMAS1B			BIT2
#define BIT_DMAS0A			BIT1
#define BIT_DMAS0B			BIT0

/** PFCR9 */
#define BIT_TPUMS5			BIT7
#define BIT_TPUMS4			BIT6
#define BIT_TPUMS3A			BIT5
#define BIT_TPUMS3B			BIT4

/** PFCRB */
#define BIT_ITS11			BIT3
#define BIT_ITS10			BIT2
#define BIT_ITS9			BIT1
#define BIT_ITS8			BIT0

/** PFCRC */
#define BIT_ITS7			BIT7
#define BIT_ITS6			BIT6
#define BIT_ITS5			BIT5
#define BIT_ITS4			BIT4
#define BIT_ITS3			BIT3
#define BIT_ITS2			BIT2
#define BIT_ITS1			BIT1
#define BIT_ITS0			BIT0

#endif

﻿;
; MicroAnts BIOS
;
; This software is conforms to the specification of uITRON ver. 4.03.00
;

;
; @file bios_cpu_asm_ext.h
;
; definitions of BIOS CPU extension (for assembler).
;
; @date 2010.08.17 new creation
;
	.AIFDEF __BIOS_CPU_ASM_EXT_H__
	.AELSE
	__BIOS_CPU_ASM_EXT_H__:		.DEFINE ""
	
	CCR_I_MASK:					.DEFINE "H'80"
	EXR_I_MASK:					.DEFINE "H'07"
	
	.AENDI

﻿;
; MicroAnts BOOT
; 
; This software is conforms to the specification of uITRON ver. 4.03.00
; 
;

;
; @file startup.src
;
; implementations of H8SX1653F reset entry.
;
; @date 2010.06.06 new creation
;

	.CPU H8SXA
	
	;jp STARTOF 演算子, SIZEOF 演算子を有効にするためのセクション宣言
	.SECTION D, DATA, ALIGN = 2
	.SECTION R, DATA, ALIGN = 2
	.SECTION B, DATA, ALIGN = 2
	
	.SECTION S, DATA, ALIGN = 2
	.RES.B H'400
	
	.SECTION H, DATA, ALIGN = 2
	.RES.B H'2800
	
	.SECTION P, CODE, ALIGN = 2
	
	.INCLUDE "bios_asm.cfg"
	.INCLUDE "bios_cpu_asm_ext.inc"
	
	.IMPORT _knlcpu_start_kernel
	.IMPORT _cpu_initialize
	.IMPORT _knlcpu_interrupt_vector_table
	
	.GLOBAL _startup
	.GLOBAL _cpu_initialize_return
	
_startup:
	; initialize system context stack pointer.
	mov.l	#BIOS_CFG_SYSTEM_STACK_ORIGIN, sp
	
	; initialize CCR.
	; interrupt control mode = 0. disable all interrupts.
	; interrupt control mode = 0, until interrupt control mode is changed.
	ldc.b	#CCR_I_MASK, ccr
	
	; initialize EXR.
	; disable all interrupts in interrupt control mode = 2.
	ldc.b	#EXR_I_MASK, exr
	
	; initialize CPU.
	jmp		@_cpu_initialize
	
_cpu_initialize_return:
	
	; clear BSS.
	mov.l	@bss_start, er0
	mov.l	@bss_end, er1
	sub.l	er2, er2
loop_clear_bss:
	mov.l	er2, @er0
	add.l	#4, er0
	cmp.l	er1, er0
	blo		loop_clear_bss				; er1 > er0
	
	; initialize constant data section.
	mov.l	@cdata_start, er0
	mov.l	@cdata_end, er1
	mov.l	@data_start, er2
loop_copy_cdata:
	mov.l	@er0+, er4	  				; *er2 ++ = *er0 ++;
	mov.l	er4, @er2
	add.l	#4, er2
	cmp.l	er1, er0
	blo		loop_copy_cdata				; er1 > er0
	
	jmp		@_knlcpu_start_kernel
	
	; just for linking.
	jmp		@_knlcpu_interrupt_vector_table
	
	.ALIGN 4
bss_start:
	.DATA.L STARTOF B
bss_end:
	.DATA.L STARTOF B + SIZEOF B
cdata_start:
	.DATA.L STARTOF D
cdata_end:
	.DATA.L STARTOF D + SIZEOF D
data_start:
	.DATA.L STARTOF R

	.END

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_intc.c
 *
 * implementations of H8SX1653F BIOS interrupt controller.
 *
 * @date 2010.05.17 new creation
 */

#include <bios_intc.h>
#include "bios_1653f_iodefs.h"

/**
 * intialize the interrupt controller.
 */
void bios_intc_initialize( void )
{
	/* interrupt control mode = 2 */
	INTC.INTCR.BIT.INTM = 0x02;
}

/**
 * finalize the interrupt controller.
 */
void bios_intc_finalize( void )
{
	;
}

/**
 * enable an interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_enable_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * disable an interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_disable_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * clear an asserted interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_clear_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * sense if an interrupt is enabled.
 * 
 * @param interrupt number
 * @param p_enabled TRUE: interrupt is enabled
 * FALSE: interrupt is not enabled
 * @return error code
 */
BIOS_ERROR bios_intc_sense_interrupt_enabled(
	BIOS_UINT intno,
	BIOS_BOOL * p_enabled )
{
	return BIOS_ERROR_OK;
}

/**
 * sense if an interrupt is asserted.
 * 
 * @param interrupt number
 * @param p_asserted TRUE: interrupt is asserted
 * FALSE: interrupt is not asserted
 * @return error code
 */
BIOS_ERROR bios_intc_sense_interrupt_asserted(
	BIOS_UINT intno,
	BIOS_BOOL * p_asserted )
{
	return BIOS_ERROR_OK;
}

/**
 * getting an interrupt priority.
 * 
 * @param intno interrupt number
 * @param p_priority pointer to current interrupt priority
 * @return error code
 */
BIOS_ERROR bios_intc_get_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT * p_priority )
{
	return BIOS_ERROR_OK;
}

/**
 * setting an interrupt priority.
 * 
 * @param intno interrupt number
 * @param priority new interrupt priority
 * @return error code
 */
BIOS_ERROR bios_intc_set_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT priority )
{
	return BIOS_ERROR_OK;
}

/**
 * getting an interrupt mode.
 * 
 * @param intno interrupt number
 * @param p_mode pointer to current interrupt mode
 * @return error code
 */
BIOS_ERROR bios_intc_get_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT * p_mode )
{
	return BIOS_ERROR_OK;
}

/**
 * setting an interrupt mode.
 * 
 * @param intno interrupt number
 * @param mode new interrupt mode
 * @return error code
 */
BIOS_ERROR bios_intc_set_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT mode )
{
	return BIOS_ERROR_OK;
}

/**
 * detect an asserted interrupt with the highest priority.
 * 
 * @param intno pointer to highest priority interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_detect_asserted_highest_priority_interrupt(
	BIOS_UINT * intno,
	BIOS_UINT * intpri )
{
	return BIOS_ERROR_OK;
}

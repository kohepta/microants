﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_board.c
 *
 * implementations of H8SX1653F BIOS board.
 *
 * @date 2010.06.06 new creation
 */

#include <bios_board.h>
#include <bios_chip.h>

/**
 * initialize the BIOS board.
 * 
 * @return error code
 */
BIOS_ERROR bios_board_initialize( void )
{
	/* initialize the chip. */
	bios_chip_initialize();
	
	return BIOS_ERROR_OK;
}

/**
 * finalize the BIOS.
 * 
 * @return error code
 */
BIOS_ERROR bios_board_finalize( void )
{
	/* finalize the chip. */
	bios_chip_finalize();
	
	return BIOS_ERROR_OK;
}

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu_ext.c
 *
 * implementations of H8SX1653F BIOS CPU extension.
 *
 * @date 2010.08.16 new creation
 */

#include "bios_cpu_ext.h"

/** 
 * current imask value.
 * 
 * interrupt of a priority level that is lower than
 * this value is not asserted.
 */
static BIOS_UINT8 current_imask;

/** 
 * base imask value.
 * 
 * in the interrupt exit processing,
 * when EXR that exists in stack is larger than
 * this value, dispatch is not done.
 */
static BIOS_UINT8 base_imask;

/**
 * set the current imask
 * 
 * @param imask current imask
 */
void bios_cpu_ext_set_current_imask(
	BIOS_UINT8 imask )
{
	current_imask = (BIOS_UINT8)( imask & EXR_I_MASK );
}

/**
 * get the current imask
 * 
 * @return current imask
 */
BIOS_UINT8 bios_cpu_ext_get_current_imask( void )
{
	return current_imask;
}

/**
 * initialize the base imask and current imask
 * 
 * @param imask imask initial value
 */
void bios_cpu_ext_initialize_imask(
	BIOS_UINT8 imask )
{
	base_imask = (BIOS_UINT8)( imask & EXR_I_MASK );
	current_imask = base_imask;
}

/**
 * get the base imask
 * 
 * @return base imask
 */
BIOS_UINT8 bios_cpu_ext_get_base_imask( void )
{
	return base_imask;
}

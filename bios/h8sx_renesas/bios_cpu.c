﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu.c
 *
 * implementations of H8SX1653F BIOS CPU.
 *
 * @date 2010.06.05 new creation
 */

#include <bios_cpu.h>
/* built-in function of processing system. */
#include <machine.h>
#include "bios_cpu_ext.h"
#include "bios_1653f_iodefs.h"

/**
 * initialize the CPU.
 */
void bios_cpu_initialize( void )
{
	/*---------------------------------
	 * initialize imask.
	 *--------------------------------*/
	bios_cpu_ext_initialize_imask( 0 );
	
	/*---------------------------------
	 * initialize clock.
	 *--------------------------------*/
	
	/*
	 on the assumption that clock = 12MHz and MD_CLK = 0
	 
	 system clock       = x4 (48MHz)
	 peripheral clock   = x2 (24MHz)
	 external bus clock = x2 (24MHz)
	*/
	SCKCR.WORD = 0x0011;
	
	/*---------------------------------
	 * initialize I/O port.
	 *--------------------------------*/
	
	/* CS3 Enable */
	PFC.PFCR0.BYTE = 0x0F;
	/* A20～A16 enable */
	PFC.PFCR4.BYTE = 0x1F;
	/* address BUS */
	PD.DDR = 0xFF;
	/* address BUS */
	PE.DDR = 0xFF;
	/* address BUS */
	PF.DDR = 0xFF;
	/* port1 */
	P1.DDR = 0x00;
	/* port2 */
	P2.DDR = 0x02;
	P2.DR.BIT.B1 = 0;
	/* portM */
	PM.DDR = 0x05;
	PM.DR.BYTE = 0x01;
	
	/*---------------------------------
	 * initialize external bus.
	 *--------------------------------*/
	
	/*
	 area2 (S-RAM)
	*/
	
	/* 16 bits bus. */
	BSC.ABWCR.BIT.ABWH2 = 0;
	BSC.ABWCR.BIT.ABWL2 = 1;
	/* 2 state access area. */
	BSC.ASTCR.BIT.AST2 = 1;
	/* byte control SRAM interface. */
	BSC.SRAMCR.BIT.BCSEL2 = 1;
	/* big endian. */
	BSC.ENDIANCR.BIT.LE2 = 0;
	/* extend address assertion period. */
	BSC.CSACR.BIT.CSXH2 = 1;

	/*
	 area3 (Azteca)
	*/
	
	/* 16 bits bus. */
	BSC.ABWCR.BIT.ABWH3 = 0;
	BSC.ABWCR.BIT.ABWL3 = 1;
	/* 3 state access area. */
	BSC.ASTCR.BIT.AST3 = 1;
	/* program wait state = 7 */
	BSC.WTCRB.BIT.W3 = 7;
	/* byte control SRAM interface. */
	BSC.SRAMCR.BIT.BCSEL3 = 1;
	BSC.MPXCR.BIT.MPXE3 = 0;
	/* big endian. */
	BSC.ENDIANCR.BIT.LE3 = 0;
	/* extend address assertion period. */
	BSC.CSACR.BIT.CSXH3 = 1;
}

/**
 * finalize the CPU.
 */
void bios_cpu_finalize( void )
{
	;
}

/**
 * disable all interrupts and return the previous interrupt mask register,
 * before doing.
 * 
 * @return interrupt mask register value
 */
BIOS_UINT bios_cpu_lock( void )
{
	BIOS_UINT lock;
	
	lock = (BIOS_UINT)get_exr();
	
	or_exr( EXR_I_MASK );
	
	return lock;
}

/**
 * restore the interrupt mask register.
 * 
 * @param lock interrupt mask register value
 */
void bios_cpu_unlock(
	BIOS_UINT lock )
{
	BIOS_UINT8 imask;
	
	imask = get_exr();
	
	imask = (BIOS_UINT8)( ( imask & EXR_I_MASK_OFF ) | (BIOS_UINT8)lock );
	
	set_exr( imask );
}

/**
 * disable all interrupts.
 */
void bios_cpu_disable_interrupts( void )
{
	or_exr( EXR_I_MASK );
}

/**
 * enable all interrupts.
 */
void bios_cpu_enable_interrupts( void )
{
	BIOS_UINT8 imask;
	
	imask = get_exr();
	
	or_exr( EXR_I_MASK );
	
	imask = (BIOS_UINT8)( ( imask & EXR_I_MASK_OFF )
		| bios_cpu_ext_get_current_imask() );
	
	set_exr( imask );
}

/**
 * sense the state of CPU locking.
 * 
 * @retval TRUE CPU is locking
 * @retval FALSE CPU is not locking
 */
BIOS_BOOL bios_cpu_sense_locked( void )
{
	BIOS_UINT8 imask;
	
	imask = (BIOS_UINT8)( get_exr() & EXR_I_MASK );
	
	return ( imask == EXR_I_MASK ) ? BIOS_TRUE : BIOS_FALSE;
}

/**
 * getting interrupt priority register value.
 * 
 * @param p_priority pointer to current interrupt priority register value
 * @return error code
 */
BIOS_ERROR bios_cpu_get_interrupt_priority(
	BIOS_UINT * p_priority )
{
	return BIOS_ERROR_OK;
}

/**
 * setting interrupt priority register.
 * 
 * @param p_priority new interrupt priority register value
 * @return error code
 */
BIOS_ERROR bios_cpu_set_interrupt_priority(
	BIOS_UINT priority )
{
	return BIOS_ERROR_OK;
}

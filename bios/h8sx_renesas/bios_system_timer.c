﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_system_timer.c
 *
 * implementations of H8SX1653F BIOS system timer.
 *
 * @date 2010.05.17 new creation
 */

#include <bios_system_timer.h>
#include "bios.cfg"
#include "bios_1653f_iodefs.h"

/**
 * initialize the system timer.
 */
void bios_system_timer_initialize( void )
{
	/* release module-stop-mode (TPU). */
	MSTP.CRA.BIT._TPU = 0;
	
	/* setting system timer interrupt priority. */
	INTC.IPRF.BIT._TPU0 = BIOS_CFG_INTERRUPT_PRIORITY_SYSTEM_TIMER;
	
	/* stop the TPU channel 0. */
	TPU.TSTR.BIT.CST0 = 0;
	
	/*jp
	 TCR の設定.
	  1). TGRA コンペアマッチでカウンタをクリア (CCLR = 1)
	  2). 立ち上がりエッジでカウント (CKEG1, 0 = 0, 1)
	  3). 内部クロック /4 でカウント (分周比設定；TPSC0 =1)
	*/
	TPU0.TCR.BIT.CCLR = 1;
	TPU0.TCR.BIT.CKEG = 1;
	TPU0.TCR.BIT.TPSC = 1;
}

/**
 * finalize the system timer.
 */
void bios_system_timer_finalize( void )
{
	bios_system_timer_close();
		
	/* enable module-stop-mode (TPU). */
	MSTP.CRA.BIT._TPU = 1;
}

/**
 * pre-process before invoking the system-timer interrupt handler.
 */
void bios_system_timer_pre_interrupt_process( void )
{
	/* clear the interrupt request of TPU channel 0. */
	TPU0.TSR.BIT.TGFA = 0;
}

/**
 * post-process before invoking the system-timer interrupt handler.
 */
void bios_system_timer_post_interrupt_process( void )
{
	;
}

/**
 * open the system timer.
 *
 * @param numerator numerator of tick
 * @param denominator denominator of tick
 * @return error code
 */
BIOS_ERROR bios_system_timer_open(
	BIOS_UINT32 numerator, 
	BIOS_UINT32 denominator )
{
	/*jp
	 TIER の設定.
	  TGIEA ビットに夜割り込み要求を許可.
	*/
	TPU0.TIER.BIT.TGIEA = 1;
	
	/*jp
	 TGR の設定.
	  TGRA レジスタ設定 (カウンタ目標値).
	  BIOS_CFG_SYSTEM_TIME_CLOCK = 1 msec
	*/
	TPU0.TGRA = (BIOS_REG16)( BIOS_CFG_SYSTEM_TIMER_CLOCK
		* numerator / denominator );
	
	/* clear the counter. */
	TPU0.TCNT = 0;
	
	/* clear the interrupt request of TPU channel 0. */
	TPU0.TSR.BIT.TGFA = 0;
	
	/* start the TPU channel 0. */
	TPU.TSTR.BIT.CST0 = 1;
	
	return BIOS_ERROR_OK;
}

/**
 * close the system timer.
 *
 * @return error code
 */
BIOS_ERROR bios_system_timer_close( void )
{
	/*jp
	 TIER の設定.
	  TGIEA ビットに夜割り込み要求を禁止.
	*/
	TPU0.TIER.BIT.TGIEA = 0;
	
	/* stop the TPU channel 0. */
	TPU.TSTR.BIT.CST0 = 0;
	
	/* clear the interrupt request of TPU channel 0. */
	TPU0.TSR.BIT.TGFA = 0;
	
	return BIOS_ERROR_OK;
}

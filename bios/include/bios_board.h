﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_board.h
 *
 * definitions of SH7727 BIOS board.
 *
 * @date 2010.03.08 new creation
 */

#ifndef __BIOS_BOARD_H__
#define __BIOS_BOARD_H__

#include <bios_types.h>

/*---------------------------------------------
 * BIOS function prototypes
 *-------------------------------------------*/

/* bios_board.c */
BIOS_ERROR bios_board_initialize( void );

/* bios_board.c */
BIOS_ERROR bios_board_finalize( void );

#endif

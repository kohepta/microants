﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_intc.h
 *
 * definitions of BIOS interrupt controller.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __BIOS_INTC_H__
#define __BIOS_INTC_H__

#include <bios_types.h>

/*---------------------------------------------
 * BIOS interrupt controller function prototypes
 *-------------------------------------------*/

/* bios_intc.c */
void bios_intc_initialize( void );

/* bios_intc.c */
void bios_intc_finalize( void );

/* bios_intc.c */
BIOS_ERROR bios_intc_enable_interrupt(
	BIOS_UINT intno );

/* bios_intc.c */
BIOS_ERROR bios_intc_disable_interrupt(
	BIOS_UINT intno );

/* bios_intc.c */
BIOS_ERROR bios_intc_clear_interrupt(
	BIOS_UINT intno );

/* bios_intc.c */
BIOS_ERROR bios_intc_sense_interrupt_enabled(
	BIOS_UINT intno,
	BIOS_BOOL * p_enabled );

/* bios_intc.c */
BIOS_ERROR bios_intc_sense_interrupt_asserted(
	BIOS_UINT intno,
	BIOS_BOOL * p_asserted );

/* bios_intc.c */
BIOS_ERROR bios_intc_get_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT * p_priority );

/* bios_intc.c */
BIOS_ERROR bios_intc_set_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT priority );

/* bios_intc.c */
BIOS_ERROR bios_intc_get_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT * p_mode );

/* bios_intc.c */
BIOS_ERROR bios_intc_set_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT mode );

/* bios_intc.c */
BIOS_ERROR bios_intc_detect_asserted_highest_priority_interrupt(
	BIOS_UINT * intno,
	BIOS_UINT * intpri );

#endif

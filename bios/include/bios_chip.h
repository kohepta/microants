﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_chip.h
 *
 * definitions of SH7727 BIOS chip.
 *
 * @date 2010.03.08 new creation
 */

#ifndef __BIOS_CHIP_H__
#define __BIOS_CHIP_H__

#include <bios_types.h>

/*---------------------------------------------
 * BIOS function prototypes
 *-------------------------------------------*/

/* bios_chip.c */
BIOS_ERROR bios_chip_initialize( void );

/* bios_chip.c */
BIOS_ERROR bios_chip_finalize( void );

#endif

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios.h
 *
 * definitions of BIOS.
 *
 * @date 2010.03.08 new creation
 */

#ifndef __BIOS_H__
#define __BIOS_H__

#include <bios_types.h>
#include <bios_cpu.h>
#include <bios_system_timer.h>
#include <bios_intc.h>

/* MACRO for inline & volatile keywords */
#if defined( __GNUC__ )

/* GNU C */
#define BIOS_INLINE static __inline__

#elif defined( __CC_ARM )

/* ARM RVCT */
#define BIOS_INLINE __inline

#elif defined( __HITACHI__ )

/* Renesas C/C++ */
#define BIOS_INLINE static

#endif

/*---------------------------------------------
 * BIOS function prototypes
 *-------------------------------------------*/

/* bios.c */
BIOS_ERROR bios_initialize( void );

/* bios.c */
BIOS_ERROR bios_finalize( void );

#endif

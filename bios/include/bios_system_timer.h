﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_system_timer.h
 *
 * definitions of BIOS system timer.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __BIOS_SYSTEM_TIMER_H__
#define __BIOS_SYSTEM_TIMER_H__

#include <bios_types.h>

/*---------------------------------------------
 * BIOS system timer function prototypes
 *-------------------------------------------*/

/* bios_system_timer.c */
void bios_system_timer_initialize( void );

/* bios_system_timer.c */
void bios_system_timer_finalize( void );

/**
 * pre-process before invoking the bios system-timer interrupt handler.
 */
void bios_system_timer_pre_interrupt_process( void );

/**
 * post-process before invoking the bios system-timer interrupt handler.
 */
void bios_system_timer_post_interrupt_process( void );

/* bios_system_timer.c */
BIOS_ERROR bios_system_timer_open(
	BIOS_UINT32 numerator, 
	BIOS_UINT32 denominator );

/* bios_system_timer.c */
BIOS_ERROR bios_system_timer_close( void );

#endif

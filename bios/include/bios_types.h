﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_types.h
 *
 * generic type definitions of BIOS.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __BIOS_TYPES_H__
#define __BIOS_TYPES_H__

#if defined( __HITACHI__ )	\
	&& ( defined( __HITACHI_H8SXA__ ) || defined( __HITACHI_H8SXX__ ) )
#define _int8_		char
#define _int16_		int
#define _int32_		long
#elif defined( __GNUC__ )	\
	|| defined( __CC_ARM )	\
	|| ( defined( __HITACHI__ ) && ( __HITACHI_VERSION__ >= 0x0900 ) )
#define _int8_		char
#define _int16_		short
#define _int32_		int
#define _int64_		long long
#else
#define _int8_		char
#define _int16_		short
#define _int32_		int
#endif

typedef signed _int8_ BIOS_INT8;
typedef unsigned _int8_ BIOS_UINT8;
typedef _int8_ BIOS_VINT8;

typedef signed _int16_ BIOS_INT16;
typedef unsigned _int16_ BIOS_UINT16;
typedef _int16_ BIOS_VINT16;

typedef signed _int32_ BIOS_INT32;
typedef unsigned _int32_ BIOS_UINT32;
typedef _int32_ BIOS_VINT32;

#if defined( _int64_ )
typedef signed _int64_ BIOS_INT64;
typedef unsigned _int64_ BIOS_UINT64;
typedef _int64_ BIOS_VINT64;
#endif

typedef void * BIOS_VP;

typedef signed int BIOS_INT;
typedef unsigned int BIOS_UINT;

typedef unsigned volatile _int8_ BIOS_REG8;

typedef unsigned volatile _int16_ BIOS_REG16;

typedef unsigned volatile _int32_ BIOS_REG32;

/* read-only 8bit register */
typedef volatile const unsigned _int8_ BIOS_REG8RO;

/* read-only 16bit register */
typedef volatile const unsigned _int16_ BIOS_REG16RO;

/* read-only 32bit register */
typedef volatile const unsigned _int32_ BIOS_REG32RO;

enum bios_bool
{
	BIOS_FALSE = 0,
	BIOS_TRUE = 1
};
typedef enum bios_bool BIOS_BOOL;

enum bios_error
{
	BIOS_ERROR_OK = 0,
	BIOS_ERROR_SYS = -5,
	BIOS_ERROR_NOSPT = -2,
	BIOS_ERROR_PAR = -3,
	BIOS_ERROR_ILUSE = -4
};
typedef enum bios_error BIOS_ERROR;

#endif

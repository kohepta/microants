﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu.h
 *
 * definitions of BIOS CPU.
 *
 * @date 2010.05.17 new creation
 */

#ifndef __BIOS_CPU_H__
#define __BIOS_CPU_H__

#include <bios_types.h>

/*---------------------------------------------
 * BIOS CPU function prototypes
 *-------------------------------------------*/

/* bios_cpu.c */
void bios_cpu_initialize( void );

/* bios_cpu.c */
void bios_cpu_finalize( void );

/* bios_cpu.c */
BIOS_UINT bios_cpu_lock( void );

/* bios_cpu.c */
void bios_cpu_unlock(
	BIOS_UINT lock );

/* bios_cpu.c */
void bios_cpu_disable_interrupts( void );

/* bios_cpu.c */
void bios_cpu_enable_interrupts( void );

/* bios_cpu.c */
BIOS_BOOL bios_cpu_sense_locked( void );

/* bios_cpu.c */
BIOS_ERROR bios_cpu_get_interrupt_priority(
	BIOS_UINT * p_priority );

/* bios_cpu.c */
BIOS_ERROR bios_cpu_set_interrupt_priority(
	BIOS_UINT priority );

#endif

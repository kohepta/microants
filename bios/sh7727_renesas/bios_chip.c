﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_chip.c
 *
 * implementations of SH7277 BIOS chip.
 *
 * @date 2011.03.02 new creation
 */

#include <bios_chip.h>
#include <bios_cpu.h>
#include <bios_intc.h>
#include <bios_system_timer.h>

/**
 * initialize the BIOS chip.
 * 
 * @return error code
 */
BIOS_ERROR bios_chip_initialize( void )
{
	/* initialize the CPU. */
	bios_cpu_initialize();
	
	/* initialize the interrupt controller. */
	bios_intc_initialize();
	
	/* initialize the system timer. */
	bios_system_timer_initialize();
	
	return BIOS_ERROR_OK;
}

/**
 * finalize the BIOS chip.
 * 
 * @return error code
 */
BIOS_ERROR bios_chip_finalize( void )
{
	/* finalize the system timer. */
	bios_system_timer_finalize();
	
	/* finalize the interrupt controller. */
	bios_intc_finalize();
	
	/* finalize the CPU. */
	bios_cpu_finalize();
	
	return BIOS_ERROR_OK;
}

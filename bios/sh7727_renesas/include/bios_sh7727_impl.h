﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_sh7727_impl.h
 *
 * definitions of BIOS SH7727 implementation.
 *
 * @date 2010.03.02 new creation
 */

#ifndef __BIOS_SH7727_IMPL_H__
#define __BIOS_SH7727_IMPL_H__

#include <bios.h>
#include <bios_sh7727_regs.h>

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_reb_mem )
#endif
BIOS_INLINE BIOS_VINT8 bios_sh7727_reb_mem(
	BIOS_VP mem )
{
	BIOS_VINT8 data = *( (volatile BIOS_VINT8 *) mem );
	return data;
}

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_wrb_mem )
#endif
BIOS_INLINE void bios_sh7727_wrb_mem(
	BIOS_VP mem,
	BIOS_VINT8 data )
{
	*( (volatile BIOS_VINT8 *) mem ) = data;
}

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_reh_mem )
#endif
BIOS_INLINE BIOS_VINT16 bios_sh7727_reh_mem(
	BIOS_VP mem )
{
	BIOS_VINT16 data = *( (volatile BIOS_VINT16 *) mem );
	return data;
}

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_wrh_mem )
#endif
BIOS_INLINE void bios_sh7727_wrh_mem(
	BIOS_VP mem,
	BIOS_VINT16 data )
{
	*( (volatile BIOS_VINT16 *) mem ) = data;
}

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_rew_mem )
#endif
BIOS_INLINE BIOS_VINT32 bios_sh7727_rew_mem(
	BIOS_VP mem )
{
	BIOS_VINT32 data = *( (volatile BIOS_VINT32 *) mem );
	return data;
}

#if defined( __HITACHI__ )
#pragma inline( bios_sh7727_wrw_mem )
#endif
BIOS_INLINE void bios_sh7727_wrw_mem(
	BIOS_VP mem,
	BIOS_VINT32 data )
{
	*( (volatile BIOS_VINT32 *) mem ) = data;
}

#endif

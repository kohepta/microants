﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_sh7727_regs.h
 *
 * definitions of BIOS SH7727 register definitions.
 *
 * @date 2010.03.02 new creation
 */

#ifndef __BIOS_SH7727_REGS_H__
#define __BIOS_SH7727_REGS_H__

/** bit number definition. */
#define BIT0				0x1
#define BIT1				0x2
#define BIT2				0x4
#define BIT3				0x8
#define BIT4				0x10
#define BIT5				0x20
#define BIT6				0x40
#define BIT7				0x80
#define BIT8				0x100
#define BIT9				0x200
#define BIT10				0x400
#define BIT11				0x800
#define BIT12				0x1000
#define BIT13				0x2000
#define BIT14				0x4000
#define BIT15				0x8000

#define DUMMY				0

#endif

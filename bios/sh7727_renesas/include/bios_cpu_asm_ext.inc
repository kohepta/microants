﻿;
; MicroAnts BIOS
;
; This software is conforms to the specification of uITRON ver. 4.03.00
;

;
; @file bios_cpu_asm_ext.inc
;
; definitions of BIOS CPU extension (for assembler).
;
; @date 2011.03.02 new creation
;
	.AIFDEF __BIOS_CPU_ASM_EXT_INC__
	.AELSE
	__BIOS_CPU_ASM_EXT_INC__:	.DEFINE ""
	
	.AENDI

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu_ext.h
 *
 * definitions of BIOS CPU extension.
 *
 * @date 2011.03.02 new creation
 */

#ifndef __BIOS_CPU_EXT_H__
#define __BIOS_CPU_EXT_H__

#include <bios_types.h>

#define BIOS_CPU_EXT_SR_IMASK				(0x0F)
#define BIOS_CPU_EXT_BIOS_SR_IMASK_OFF		(0x00)

/*---------------------------------------------
 * BIOS CPU function prototypes
 *-------------------------------------------*/

/* bios_cpu_ext.c */
void bios_cpu_ext_set_current_imask(
	BIOS_UINT8 imask );

/* bios_cpu_ext.c */
BIOS_UINT8 bios_cpu_ext_get_current_imask( void );

#endif

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_system_timer.c
 *
 * implementations of SH7727 BIOS system timer.
 *
 * @date 2011.03.03 new creation
 */

#include <bios_cpu.h>
#include <bios_system_timer.h>
#include "bios.cfg"
#include "bios_sh7727_iodefs.h"

/**
 * initialize the system timer.
 */
void bios_system_timer_initialize( void )
{
	/* setting system timer interrupt priority. */
	INTC.IPRA.BIT._TMU0 = BIOS_CFG_INTERRUPT_PRIORITY_SYSTEM_TIMER;
	
	/* stop the TMU channel 0. */
	TMU.TSTR.BIT.STR0 = 0;
	
	/*jp
	  TCR の設定.
	   アンダーフロー割り込み禁止
	   周辺クロック (24MHz) /16 でカウント (分周比設定；TPSC2,1,0 = 0,0,1)
	   ※ TMU の最高動作周波数は 2MHz
	*/
	TMU0.TCR.BIT.UNIE = 0;
	TMU0.TCR.BIT.TPSC = 1;
}

/**
 * finalize the system timer.
 */
void bios_system_timer_finalize( void )
{
	bios_system_timer_close();
}

/**
 * pre-process before invoking the system-timer interrupt handler.
 */
void bios_system_timer_pre_interrupt_process( void )
{
	/* clear the interrupt request of TMU channel 0. */
	TMU0.TCR.BIT.UNF = 0;
	
	/* enable interrupts */
	bios_cpu_enable_interrupts();
}

/**
 * post-process before invoking the system-timer interrupt handler.
 */
void bios_system_timer_post_interrupt_process( void )
{
	;
}

/**
 * open the system timer.
 *
 * @param numerator numerator of tick
 * @param denominator denominator of tick
 * @return error code
 */
BIOS_ERROR bios_system_timer_open(
	BIOS_UINT32 numerator, 
	BIOS_UINT32 denominator )
{
	/*jp
	  TCR の設定.
	    アンダーフロー割り込み許可
	*/
	TMU0.TCR.BIT.UNIE = 1;
	
	/*jp
	  TCOR の設定.
	    TCOR レジスタ設定 (カウンタ目標値).
	    BIOS_CFG_SYSTEM_TIME_CLOCK = 1 msec
	*/
	TMU0.TCOR = (BIOS_REG32)( BIOS_CFG_SYSTEM_TIMER_CLOCK
		* numerator / denominator );
	
	/* clear the counter. */
	TMU0.TCNT = TMU0.TCOR;
	
	/* clear the interrupt request of TMU channel 0. */
	TMU0.TCR.BIT.UNF = 0;
	
	/* start the TMU channel 0. */
	TMU.TSTR.BIT.STR0 = 1;
	
	return BIOS_ERROR_OK;
}

/**
 * close the system timer.
 *
 * @return error code
 */
BIOS_ERROR bios_system_timer_close( void )
{
	/*jp
	  TCR の設定.
	    アンダーフロー割り込み禁止
	*/
	TMU0.TCR.BIT.UNIE = 0;
	
	/* stop the TMU channel 0. */
	TMU.TSTR.BIT.STR0 = 0;
	
	/* clear the interrupt request of TMU channel 0. */
	TMU0.TCR.BIT.UNF = 0;
	
	return BIOS_ERROR_OK;
}

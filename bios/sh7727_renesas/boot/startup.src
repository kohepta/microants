﻿;
; MicroAnts BOOT
; 
; This software is conforms to the specification of uITRON ver. 4.03.00
; 
;

;
; @file startup.src
;
; implementations of SH7727 (MS7727CP02) reset entry.
;
; @date 2011.03.01 new creation
;

	.INCLUDE "bios_asm.cfg"
	.INCLUDE "bios_cpu_asm_ext.inc"
	
	;jp STARTOF 演算子, SIZEOF 演算子を有効にするためのセクション宣言
	.SECTION D,    DATA,  ALIGN = 4
	.SECTION R,    DATA,  ALIGN = 4
	.SECTION B,    DATA,  ALIGN = 4
	.SECTION S,    STACK, ALIGN = 4
	.SECTION H,    DATA,  ALIGN = 4
	.SECTION BOOT, CODE,  ALIGN = 4
	
	.IMPORT _knlcpu_start_kernel
	.IMPORT _knlcpu_reset_entry
	
	.EXPORT _startup
	
_startup:
	mov.l	_startup_SR_init, r0
	ldc		r0, sr
	ldc		r0, ssr
	
	mov.l	_startup_GBR_init, r0
	ldc		r0, gbr
	
	;;----------------------------------
	;; initialize CPG.
	;;----------------------------------
	mov.l	_startup_FRQCR_addr, r0
	mov.w	_startup_FRQCR_init, r1
	mov.w	r1, @r0
	
	.AIF \&BIOS_BOOT_FROM_SDRAM	EQ	0
	;;----------------------------------
	;; initialize BSC.
	;;----------------------------------
	mov.l	_startup_BCR1_addr, r0
	mov.w	_startup_BCR1_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_BCR2_addr, r0
	mov.w	_startup_BCR2_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_WCR1_addr, r0
	mov.w	_startup_WCR1_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_WCR2_addr, r0
	mov.w	_startup_WCR2_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_PCR_addr, r0
	mov.w	_startup_PCR_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_RTCNT_addr, r0
	mov.w	_startup_RTCNT_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_RTCOR_addr, r0
	mov.w	_startup_RTCOR_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_RFCR_addr, r0
	mov.w	_startup_RFCR_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_MCR_addr, r0
	mov.w	_startup_MCR_init, r1
	mov.w	r1, @r0
	
	mov.l	_startup_SDMR3_addr, r0
	mov		#0, r1
	mov.b	r1, @r0
	
	mov.l	_startup_RTCSR_addr, r0
	mov.w	_startup_RTCSR_init, r1
	mov.w	r1, @r0
	
	; wait during the fixed time until SDRAM is refreshed.
loop_bsc:
	mov.l	_startup_RFCR_addr, r0
	mov.w	@r0, r0
	extu.w	r0, r0
	mov		#7, r1
	cmp/hi	r1, r0
	bf		loop_bsc
	
	.AENDI
	
	;;----------------------------------
	;; initialize cache 1.
	;;----------------------------------
	mov.l	_startup_CCR_addr, r0
	mov.l	_startup_CCR_init, r1
	mov.l	r1, @r0
	
	; clear BSS.
	mov.l	_startup_bss_start_addr, r0
	mov.l	_startup_bss_end_addr, r1
	xor.l	r2, r2
loop_clear_bss:
	mov.l	r2, @r0
	add.l	#4, r0
	cmp/gt	r1, r0
	bf		loop_clear_bss				; r1 >= r0
	
	; initialize constant data section.
	mov.l	_startup_cdata_start_addr, r0
	mov.l	_startup_data_start_addr, r1
	mov.l	_startup_data_end_addr, r2
loop_copy_cdata:
	mov.l	@r0+, r4	  				; *r0 ++ = *r1 ++;
	mov.l	r4, @r1
	add.l	#4, r1
	cmp/gt	r2, r1
	bf		loop_copy_cdata				; r2 >= r1
	
	;;----------------------------------
	;; initialize cache 2.
	;;----------------------------------
	
	; initialize system context stack pointer.
	mov.l	_startup_system_stack_addr, r0
	mov.l	r0, r15
	
	mov.l	_startup_knlcpu_reset_entry, r0
	jmp		@r0
	nop
	
	.ALIGN 4
	
_startup_SR_init:
	.DATA.L	H'600000F0
_startup_GBR_init:
	.DATA.L	H'0
_startup_CCR_init:
	; cache disable
	.DATA.L H'0
_startup_FRQCR_addr:
	.DATA.L H'FFFFFF80
_startup_BCR1_addr:
	.DATA.L H'FFFFFF60
_startup_BCR2_addr:
	.DATA.L H'FFFFFF62
_startup_WCR1_addr:
	.DATA.L H'FFFFFF64
_startup_WCR2_addr:
	.DATA.L H'FFFFFF66
_startup_MCR_addr:
	.DATA.L H'FFFFFF68
_startup_PCR_addr:
	.DATA.L H'FFFFFF6C
_startup_RTCSR_addr:
	.DATA.L H'FFFFFF6E
_startup_RTCNT_addr:
	.DATA.L H'FFFFFF70
_startup_RTCOR_addr:
	.DATA.L H'FFFFFF72
_startup_RFCR_addr:
	.DATA.L H'FFFFFF74
_startup_SDMR3_addr:
	; write data (0x880 >> 2 = 0x220)
	.DATA.L H'FFFFE880
_startup_CCR_addr:
	.DATA.L H'FFFFFFEC
_startup_bss_start_addr:
	.DATA.L (STARTOF B)
_startup_bss_end_addr:
	.DATA.L (STARTOF B) + (SIZEOF B)
_startup_cdata_start_addr:
	.DATA.L (STARTOF D)
_startup_cdata_end_addr:
	.DATA.L (STARTOF D) + (SIZEOF D)
_startup_data_start_addr:
	.DATA.L (STARTOF R)
_startup_data_end_addr:
	.DATA.L (STARTOF R) + (SIZEOF R)
	; system stack addr
_startup_system_stack_addr:
	.DATA.L (STARTOF S) + (SIZEOF S) - H'4
	
_startup_knlcpu_reset_entry:
	.DATA.L _knlcpu_reset_entry
	
	.ALIGN 2
	
_startup_FRQCR_init:
	.DATA.W H'0112
_startup_BCR1_init:
	; area0, area2 = normal memory
	; area3 = SDRAM
	.DATA.W H'0008
_startup_BCR2_init:
	; area3 = 32 bit bus
	; area2, area4, area5, area6 = 16bit bus
	.DATA.W H'2AE0
_startup_WCR1_init:
	; use #wait signal
	; area0 = 1 idle
	; area3 = 3 idle
	; area2, area4, area5, area6 = 2 idle
	.DATA.W H'AA22
_startup_WCR2_init:
	; area0 top cycle   = 6 wait, #wait enable
	;       burst cycle = 6 state/data, #wait enable
	; area2 = 3 wait, #wait enable
	; area3 SDRAM cas latency = 2
	; area4 = 6 wait, #wait enable
	; area5 top cycle   = 6 wait, #wait enable
	;       burst cycle = 6 state/data, #wait enable
	; area6 top cycle   = 6 wait, #wait enable
	;       burst cycle = 6 state/data, #wait enable
	.DATA.W H'B6DD
_startup_MCR_init:
	; RAS precharge = default
	; RAS, CAS latency = default
	; write precharge latency = default
	; CAS before RAS refresh RAS assert = 3 cycle
	; address multiplex = 32bit bus (2M x 16bit x 4bank)
	; do refresh
	.DATA.W H'012C
_startup_PCR_init:
	.DATA.W H'0000
_startup_RTCSR_init:
	; refresh timer counter input clock = CKIO(bus clock) / 64
	.DATA.W H'A518
_startup_RTCNT_init:
	; initialize refresh timer counter
	.DATA.W H'A500
_startup_RTCOR_init:
	; refresh time constant = 0x0C
	.DATA.W H'A50C
_startup_RFCR_init:
	; intialize refresh count
	.DATA.W H'A400
	
	.END

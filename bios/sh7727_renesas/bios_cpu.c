﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu.c
 *
 * implementations of SH7727 BIOS CPU.
 *
 * @date 2011.03.02 new creation
 */

#include <bios_cpu.h>
/* built-in function of processing system. */
#include <machine.h>
#include "bios_cpu_ext.h"
#include "bios_sh7727_iodefs.h"

/**
 * initialize the CPU.
 */
void bios_cpu_initialize( void )
{
	/*---------------------------------
	 * initialize imask.
	 *--------------------------------*/
	bios_cpu_ext_set_current_imask( BIOS_CPU_EXT_BIOS_SR_IMASK_OFF );
	
	/*---------------------------------
	 * initialize clock.
	 *--------------------------------*/
}

/**
 * finalize the CPU.
 */
void bios_cpu_finalize( void )
{
	;
}

/**
 * disable all interrupts and return the previous interrupt mask register,
 * before doing.
 * 
 * @return interrupt mask register value
 */
BIOS_UINT bios_cpu_lock( void )
{
	BIOS_UINT lock;
	
	lock = (BIOS_UINT)get_imask();
	
	set_imask( BIOS_CPU_EXT_SR_IMASK );
	
	return lock;
}

/**
 * restore the interrupt mask register.
 * 
 * @param lock interrupt mask register value
 */
void bios_cpu_unlock(
	BIOS_UINT lock )
{
	set_imask( (BIOS_UINT8)lock );
}

/**
 * disable all interrupts.
 */
void bios_cpu_disable_interrupts( void )
{
	set_imask( BIOS_CPU_EXT_SR_IMASK );
}

/**
 * enable all interrupts.
 */
void bios_cpu_enable_interrupts( void )
{
	BIOS_UINT8 imask;
	
	set_imask( BIOS_CPU_EXT_SR_IMASK );
	
	imask = bios_cpu_ext_get_current_imask();
	
	set_imask( imask );
}

/**
 * sense the state of CPU locking.
 * 
 * @retval TRUE CPU is locking
 * @retval FALSE CPU is not locking
 */
BIOS_BOOL bios_cpu_sense_locked( void )
{
	BIOS_UINT8 imask;
	
	imask = (BIOS_UINT8)get_imask();
	
	return ( imask == BIOS_CPU_EXT_SR_IMASK ) ? BIOS_TRUE : BIOS_FALSE;
}

/**
 * getting interrupt priority register value.
 * 
 * @param p_priority pointer to current interrupt priority register value
 * @return error code
 */
BIOS_ERROR bios_cpu_get_interrupt_priority(
	BIOS_UINT * p_priority )
{
	return BIOS_ERROR_OK;
}

/**
 * setting interrupt priority register.
 * 
 * @param p_priority new interrupt priority register value
 * @return error code
 */
BIOS_ERROR bios_cpu_set_interrupt_priority(
	BIOS_UINT priority )
{
	return BIOS_ERROR_OK;
}

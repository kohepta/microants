﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_cpu_ext.c
 *
 * implementations of SH7727 BIOS CPU extension.
 *
 * @date 2011.03.02 new creation
 */

#include "bios_cpu_ext.h"

/** 
 * current imask value.
 * 
 * interrupt of a priority level that is lower than
 * this value is not asserted.
 */
static BIOS_UINT8 current_imask;

/**
 * set the current imask
 * 
 * @param imask current imask
 */
void bios_cpu_ext_set_current_imask(
	BIOS_UINT8 imask )
{
	current_imask = imask;
}

/**
 * get the current imask
 * 
 * @return current imask
 */
BIOS_UINT8 bios_cpu_ext_get_current_imask( void )
{
	return current_imask;
}

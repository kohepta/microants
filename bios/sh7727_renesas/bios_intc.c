﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios_intc.c
 *
 * implementations of SH7727 BIOS interrupt controller.
 *
 * @date 2011.03.03 new creation
 */

#include <bios_intc.h>
#include "bios_sh7727_vecno.h"
#include "bios_sh7727_iodefs.h"

/**
 * intialize the interrupt controller.
 */
void bios_intc_initialize( void )
{
	;
}

/**
 * finalize the interrupt controller.
 */
void bios_intc_finalize( void )
{
	;
}

/**
 * enable an interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_enable_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * disable an interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_disable_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * clear an asserted interrupt.
 * 
 * @param interrupt number
 * @return error code
 */
BIOS_ERROR bios_intc_clear_interrupt(
	BIOS_UINT intno )
{
	return BIOS_ERROR_OK;
}

/**
 * sense if an interrupt is enabled.
 * 
 * @param interrupt number
 * @param p_enabled TRUE: interrupt is enabled
 * FALSE: interrupt is not enabled
 * @return error code
 */
BIOS_ERROR bios_intc_sense_interrupt_enabled(
	BIOS_UINT intno,
	BIOS_BOOL * p_enabled )
{
	return BIOS_ERROR_OK;
}

/**
 * sense if an interrupt is asserted.
 * 
 * @param interrupt number
 * @param p_asserted TRUE: interrupt is asserted
 * FALSE: interrupt is not asserted
 * @return error code
 */
BIOS_ERROR bios_intc_sense_interrupt_asserted(
	BIOS_UINT intno,
	BIOS_BOOL * p_asserted )
{
	return BIOS_ERROR_OK;
}

/**
 * getting an interrupt priority.
 * 
 * @param intno interrupt number
 * @param p_priority pointer to current interrupt priority
 * @return error code
 */
BIOS_ERROR bios_intc_get_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT * p_priority )
{
	return BIOS_ERROR_OK;
}

/**
 * setting an interrupt priority.
 * 
 * @param intno interrupt number
 * @param priority new interrupt priority
 * @return error code
 */
BIOS_ERROR bios_intc_set_interrupt_priority(
	BIOS_UINT intno,
	BIOS_UINT priority )
{
	return BIOS_ERROR_OK;
}

/**
 * getting an interrupt mode.
 * 
 * @param intno interrupt number
 * @param p_mode pointer to current interrupt mode
 * @return error code
 */
BIOS_ERROR bios_intc_get_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT * p_mode )
{
	return BIOS_ERROR_OK;
}

/**
 * setting an interrupt mode.
 * 
 * @param intno interrupt number
 * @param mode new interrupt mode
 * @return error code
 */
BIOS_ERROR bios_intc_set_interrupt_mode(
	BIOS_UINT intno,
	BIOS_UINT mode )
{
	return BIOS_ERROR_OK;
}

/**
 * detect an asserted interrupt with the highest priority.
 * 
 * @param intno pointer to highest priority interrupt number
 * @param intpri pointer to interrupt priority
 * @return error code
 */
BIOS_ERROR bios_intc_detect_asserted_highest_priority_interrupt(
	BIOS_UINT * intno,
	BIOS_UINT * intpri )
{
	BIOS_REG32 intevt2;
	
	intevt2 = (BIOS_REG32)INTX.INTEVT2;
	
	*intno = intevt2 >> 5;
	
	switch ( *intno )
	{
	case BIOS_SH7727_VECNO_IRL_15:
	case BIOS_SH7727_VECNO_IRL_14:
	case BIOS_SH7727_VECNO_IRL_13:
	case BIOS_SH7727_VECNO_IRL_12:
	case BIOS_SH7727_VECNO_IRL_11:
	case BIOS_SH7727_VECNO_IRL_10:
	case BIOS_SH7727_VECNO_IRL_9:
	case BIOS_SH7727_VECNO_IRL_8:
	case BIOS_SH7727_VECNO_IRL_7:
	case BIOS_SH7727_VECNO_IRL_6:
	case BIOS_SH7727_VECNO_IRL_5:
	case BIOS_SH7727_VECNO_IRL_4:
	case BIOS_SH7727_VECNO_IRL_3:
	case BIOS_SH7727_VECNO_IRL_2:
	case BIOS_SH7727_VECNO_IRL_1:
		*intpri = 0x0000000F - ( *intno - BIOS_SH7727_VECNO_IRL_15 );
		break;
	
	/* INTC1 (IPRA, IPRB) */
	case BIOS_SH7727_VECNO_TMU0_TUNI0:
		*intpri = 0x0000000F & ( INTC.IPRA.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_TMU1_TUNI1:
		*intpri = 0x0000000F & ( INTC.IPRA.WORD >> 8 );
		break;
	case BIOS_SH7727_VECNO_TMU2_TUNI2:
	case BIOS_SH7727_VECNO_TMU2_TICPI2:
		*intpri = 0x0000000F & ( INTC.IPRA.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_RTC_ATI:
	case BIOS_SH7727_VECNO_RTC_PRI:
	case BIOS_SH7727_VECNO_RTC_CUI:
		*intpri = 0x0000000F & INTC.IPRA.WORD;
		break;
	case BIOS_SH7727_VECNO_SCI_ERI:
	case BIOS_SH7727_VECNO_SCI_RXI:
	case BIOS_SH7727_VECNO_SCI_TXI:
	case BIOS_SH7727_VECNO_SCI_TEI:
		*intpri = 0x0000000F & ( INTC.IPRB.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_WDT_ITI:
		*intpri = 0x0000000F & ( INTC.IPRB.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_REF_RCMI:
	case BIOS_SH7727_VECNO_REF_ROVI:
		*intpri = 0x0000000F & ( INTC.IPRB.WORD >> 8 );
		break;
	
	/* INTC2 (IPRC, IPRD, IPRE) */
	case BIOS_SH7727_VECNO_IRQ_0:
		*intpri = 0x0000000F & INTX.IPRC.WORD;
		break;
	case BIOS_SH7727_VECNO_IRQ_1:
		*intpri = 0x0000000F & ( INTX.IPRC.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_IRQ_2:
		*intpri = 0x0000000F & ( INTX.IPRC.WORD >> 8 );
		break;
	case BIOS_SH7727_VECNO_IRQ_3:
		*intpri = 0x0000000F & ( INTX.IPRC.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_IRQ_4:
		*intpri = 0x0000000F & INTX.IPRD.WORD;
		break;
	case BIOS_SH7727_VECNO_IRQ_5:
		*intpri = 0x0000000F & ( INTX.IPRD.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_PINT0_7:
		*intpri = 0x0000000F & ( INTX.IPRD.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_PINT8_15:
		*intpri = 0x0000000F & ( INTX.IPRD.WORD >> 8 );
		break;
	case BIOS_SH7727_VECNO_DMAC_DEI0:
	case BIOS_SH7727_VECNO_DMAC_DEI1:
	case BIOS_SH7727_VECNO_DMAC_DEI2:
	case BIOS_SH7727_VECNO_DMAC_DEI3:
		*intpri = 0x0000000F & ( INTX.IPRE.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_SCIF_ERI2:
	case BIOS_SH7727_VECNO_SCIF_RXI2:
	case BIOS_SH7727_VECNO_SCIF_BRI2:
	case BIOS_SH7727_VECNO_SCIF_TXI2:
		*intpri = 0x0000000F & ( INTX.IPRE.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_ADC_ADI:
		*intpri = 0x0000000F & INTX.IPRE.WORD;
		break;
	
	/* INTC3 (IPRF, IPRG) */
	case BIOS_SH7727_VECNO_LCDC_LCDCI:
		*intpri = 0x0000000F & ( INTX.IPRF.WORD >> 8 );
		break;
	case BIOS_SH7727_VECNO_PCC0:
		*intpri = 0x0000000F & ( INTX.IPRF.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_USBHI_USBHI:
		*intpri = 0x0000000F & ( INTX.IPRG.WORD >> 12 );
		break;
	case BIOS_SH7727_VECNO_USBF_USBFI0:
		*intpri = 0x0000000F & ( INTX.IPRG.WORD >> 8 );
		break;
	case BIOS_SH7727_VECNO_USBF_USBFI1:
		*intpri = 0x0000000F & ( INTX.IPRG.WORD >> 4 );
		break;
	case BIOS_SH7727_VECNO_AFEIF_AFEIFI:
		*intpri = 0x0000000F & INTX.IPRG.WORD;
		break;
	case BIOS_SH7727_VECNO_SIOF_SIFERI:
	case BIOS_SH7727_VECNO_SIOF_SIFTXI:
	case BIOS_SH7727_VECNO_SIOF_SIFRXI:
	case BIOS_SH7727_VECNO_SIOF_SIFCCI:
		*intpri = 0x0000000F & INTX.IPRF.WORD;
		break;
		
	default:
		break;
	}
	
	if ( *intno == BIOS_SH7727_VECNO_PCC0 )
	{
		BIOS_UINT16 irr3;
		BIOS_UINT i;
		
		irr3 = (BIOS_UINT16)( 0x7FFF & INTX.IRR3.WORD );
		irr3 >>= 8;
		irr3 &= 0x00FF;
		
		*intno = BIOS_SH7727_VECNO_PC0BDIR;
		
		for ( i = 0; i < 3; i ++ )
		{
			BIOS_UINT shift;
			
			shift = 1 << ( ( 3 - i ) - 1 );
			
			if ( irr3 >> shift )
			{
				irr3 >>= shift;
				*intno += shift;
			}
		}
	}
	
	return BIOS_ERROR_OK;
}

﻿/*
	MicroAnts BIOS
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file bios.c
 *
 * implementations of BIOS.
 *
 * @date 2010.06.6 new creation
 */

#include <bios.h>
#include <bios_board.h>

/**
 * initialize the BIOS.
 * 
 * @return error code
 */
BIOS_ERROR bios_initialize( void )
{
	/* initialize the board. */
	bios_board_initialize();
	
	return BIOS_ERROR_OK;
}

/**
 * finalize the BIOS.
 * 
 * @return error code
 */
BIOS_ERROR bios_finalize( void )
{
	/* finalize the board. */
	bios_board_finalize();
	
	return BIOS_ERROR_OK;
}

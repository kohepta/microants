﻿/**
 * @file logger.c
 *
 * logging library module.
 *
 * @date 2010.08.22 new creation
 */

#include <kernel.h>
#include <stdarg.h>
#include <limits.h>
#include <logger.h>
#include <logger.cfg>

#if ( LOGGER_CFG_ENABLE )

#define CONVERT_BUFLEN	( ( sizeof( _intptr_ ) * CHAR_BIT + 2 ) / 3 )

/** log buffer. */
static LOG log_buffer[TCNT_LOG_BUFFER];

/** log count. */
static UINT log_count;

/** first log position. */
static UINT log_head;

/** last log position. */
static UINT log_tail;

/** number of lost log. */
static UINT log_lost;

/** importance that should be recorded in log buffer. */
static UINT log_logmask;

/** importance that should be low-level output. */
static UINT log_lowmask;

/** native putc function pointer. */
static void ( * native_putc )( char );

/**
 * radix transformation table.
 */
static const char raddec[] = "0123456789";
static const char radhex[] = "0123456789abcdef";
static const char radHEX[] = "0123456789ABCDEF";

/** definition of default of integer type that can store pointer. */
#ifndef _intptr_
#define _intptr_		long
#endif

static void _convert_and_output( 
	unsigned _intptr_ val, 
	unsigned int radix, 
	const char * radchar,
	int width,
	int minus,
	int padzero,
	void ( * putc )( char ) );

static void _lost_log_message(
	INT lost,
	void ( * putc )( char ) );

static void _printf(
	const char * format,
	VP_INT * args,
	void ( * putc )( char ) );

static void _print_log(
	LOG *p_log,
	void ( * putc )( char ) );

/** 
 * convert and output the string
 *
 * @param val
 * @param radix
 * @param radchar
 * @param width
 * @param minus
 * @param padzero
 * @param putc putc function pointer.
 */
static void _convert_and_output( 
	unsigned _intptr_ val, 
	unsigned int radix, 
	const char * radchar,
	int width,
	int minus,
	int padzero,
	void ( * putc )( char ) )
{
	char buf[CONVERT_BUFLEN];
	int	i, j;

	i = 0;
	/* radix transformation ( numeric -> numeric character ) */
	do
	{
		/*jp 一の位の数値から基数変換し, push する. */
		buf[i ++] = radchar[val % radix];
		val /= radix;
	}
	while ( val != 0 );
	
	/*jp パディング桁数から '-' 分の一文字を減算する. */
	width -= minus;
	
	/*jp
	 符号有且つ, 0 パディング有効の場合, 
	 0 の文字列の先頭に '-' を出力する.
	*/
	if ( minus > 0 && padzero > 0 )
	{
		putc( '-' );
	}
	
	/*jp
	 0 ( or space ) パディングの桁数 > 有効数値部の桁数の場合, 
	 有効数値部の桁数が パディングの桁数に達するまで, 
	 0 ( or space ) パディングを行う.
	*/
	for ( j = i; j < width; j ++ )
	{
		putc( (char)( padzero > 0 ? '0' : ' ' ) );
	}
	
	/*jp
	 符号有且つ, 0 パディング無効 ( space パディングは有効な
	 可能性がある ) の場合, 有効数値の文字列の先頭に '-' を出力する.
	*/
	if ( minus > 0 && padzero <= 0 )
	{
		putc( '-' );
	}
	
	while ( i > 0 )
	{
		/*jp 先頭の位から pop し, 出力 */
		putc( buf[-- i] );
	}
}

/** 
 * output the message that lost the log
 *
 * @param lost number of lost log.
 * @param putc putc function pointer.
 */
static void _lost_log_message(
	INT lost,
	void ( * putc )( char ) )
{
	VP_INT lostinfo[1];

	lostinfo[0] = (VP_INT)lost;
	_printf( "%d messages are lost.", lostinfo, putc );
}

/** 
 * internal printf function
 *
 * @param format format string.
 * @param args
 * @param putc putc function pointer.
 */
static void _printf(
	const char * format,
	VP_INT * args,
	void ( * putc )( char ) )
{
	int c;
	int width;
	int padzero;
	_intptr_ val;
	const char * str;

	while ( ( c = *format ++ ) != '\0' )
	{
		/*jp % 記号以外は即出力 */
		if ( c != '%' )
		{
			putc( (char) c );
			continue;
		}

		width = padzero = 0;
		
		/*jp 桁数を表す数値文字の先頭が 0 の場合, 0 パディングを有効にする */
		if ( ( c = *format ++ ) == '0' )
		{
			padzero = 1;
			c = *format ++;
		}
		
		/*jp 0 ( or space ) パディング桁数を表す数値文字列を数値変換 */
		while ( '0' <= c && c <= '9' )
		{
			width = width * 10 + c - '0';
			c = *format ++;
		}
		
		if ( c == 'l' )
		{
			/*jp long 記号発見 */
			c = *format ++;
		}
		
		switch ( c )
		{
		case 'd':
			val = (_intptr_)( *args ++ );
			if ( val >= 0 )
			{
				/*jp 基数変換, 符号付加, パディング を行い, 出力する */
				_convert_and_output( (unsigned _intptr_)val, 10, raddec,
						width, 0, padzero, putc );
			}
			else
			{
				_convert_and_output( (unsigned _intptr_)( -val ), 10, raddec,
						width, 1, padzero, putc );
			}
			break;
		case 'u':
			val = (_intptr_)( *args ++ );
			_convert_and_output( (unsigned _intptr_)val, 10, raddec,
						width, 0, padzero, putc );
			break;
		case 'x':
		case 'p':
			val = (_intptr_)( *args ++ );
			_convert_and_output( (unsigned _intptr_)val, 16, radhex,
						width, 0, padzero, putc );
			break;
		case 'X':
			val = (_intptr_)( *args ++ );
			_convert_and_output( (unsigned _intptr_)val, 16, radHEX,
						width, 0, padzero, putc);
			break;
		case 'c':
			putc( (char)(_intptr_)( *args ++ ) );
			break;
		case 's':
			str = (const char *)( *args ++ );
			while ( (c = *str ++ ) != '\0' )
			{
				putc( (char)c );
			}
			break;
		case '%':
			putc( '%' );
			break;
		/*jp 終端を先に発見してしまった場合, 一文字読み戻す */
		case '\0':
			format --;
			break;
		default:
			break;
		}
	}
	
	putc( '\n' );
}

/** 
 * print the log
 *
 * @param p_log log information packet.
 * @param putc putc function pointer.
 */
static void _print_log(
	LOG *p_log,
	void ( * putc )( char ) )
{
	switch ( p_log->logtype )
	{
	case LOG_TYPE_COMMENT:
		_printf(
			(const char *)( p_log->loginfo[0] ),
			&( p_log->loginfo[1] ),
			putc );
		break;
	case LOG_TYPE_ASSERT:
		_printf(
			"%s:%u: Assertion `%s' failed.",
			&( p_log->loginfo[0] ),
			putc );
		break;
	}
}

/** 
 * initialize logger
 *
 * @param putc putc function pointer.
 */
void logger_initialize(
	void ( * putc )( char ) )
{
	log_count = 0;
	log_head = log_tail = 0;
	log_lost = 0;

	log_logmask = LOG_UPTO( LOG_EMERG );
	log_lowmask = LOG_UPTO( LOG_DEBUG );
	
	native_putc = putc;
}

/** 
 * finalize logger
 */
void logger_finalize( void )
{
	_printf( "-- buffered messages --", NULL, logger_putc );
	logger_flush( logger_putc );
}

/** 
 * logger printf
 *
 * @param pri
 * @param format format string.
 * @param ...
 */
void logger_printf(
	UINT pri,
	const char * format, ... )
{
	LOG	log;
	va_list ap;
	int i;
	int c;
	BOOL lflag;

	log.logtype = LOG_TYPE_COMMENT;
	log.loginfo[0] = (VP_INT)format;
	i = 1;
	
	va_start( ap, format );

	while ( ( c = *format ++ ) != '\0' && i < TMAX_LOGINFO )
	{
		/*jp % 記号以外を読み飛ばす */
		if ( c != '%' )
		{
			continue;
		}
		
		/*jp % 記号以降の数値文字を読み飛ばす */
		lflag = FALSE;
		c = *format ++;
		while ( '0' <= c && c <= '9' )
		{
			c = *format ++;
		}
		
		if ( c == 'l' )
		{
			/*jp long 記号発見 */
			lflag = TRUE;
			c = *format ++;
		}
		
		/*jp 可変引数から適切な型変換を行った値をキューイング */
		switch ( c )
		{
		case 'd':
			log.loginfo[i ++] = lflag ?
				(VP_INT)va_arg( ap, long ):
				(VP_INT)va_arg( ap, int );
			break;
		case 'u':
		case 'x':
		case 'X':
			log.loginfo[i ++] = lflag ?
				(VP_INT)va_arg( ap, unsigned long ) :
				(VP_INT)va_arg( ap, unsigned int );
			break;
		case 'p':
			log.loginfo[i ++] = (VP_INT)va_arg( ap, void * );
			break;
		case 'c':
			log.loginfo[i ++] = (VP_INT)va_arg( ap, int );
			break;
		case 's':
			log.loginfo[i ++] = (VP_INT)va_arg( ap, const char * );
			break;
		/*jp 終端を先に発見してしまった場合, 一文字読み戻す */
		case '\0':
			format --;
			break;
		default:
			break;
		}
	}
	
	va_end( ap );
	
	vwri_log( pri, &log );
}

/** 
 * logger putc.
 *
 * @param c character data.
 */
void logger_putc(
	char c )
{
	if ( native_putc != NULL )
	{
		if ( c == '\n' )
		{
			native_putc( '\r' );
		}
		
		native_putc( c );
	}
}

/** 
 * logger flush.
 *
 * @param putc putc function pointer.
 */
void logger_flush(
	void ( * putc )( char ) )
{
	LOG	log;
	INT	lostnum, n;
	void ( * tputc )( char );
	
	if ( ( tputc = putc ) == NULL )
	{
		tputc = logger_putc;
	}

	lostnum = 0;
	
	while ( ( n = vrea_log( &log ) ) >= 0 )
	{
		lostnum += n;
		
		if ( log.logtype < LOG_TYPE_COMMENT )
		{
			continue;
		}
		if ( lostnum > 0 )
		{
			_lost_log_message( lostnum, tputc );
			lostnum = 0;
		}
		
		_print_log( &log, tputc );
	}
	
	if ( lostnum > 0 )
	{
		_lost_log_message( lostnum, tputc );
	}
}

/** 
 * writing to the log buffer ( and low-level output ).
 *
 * @param pri
 * @param p_log log information packet.
 */
void vwri_log(
	UINT pri,
	LOG * p_log )
{
	BOOL ictx;
	BOOL locked;

	locked = sns_loc();
	ictx = sns_ctx();
	
	if ( !locked )
	{
		ictx ? iloc_cpu() : loc_cpu();
	}
	
	/* writing to log buffer. */
	if ( ( log_logmask & LOG_MASK( pri ) ) != 0 )
	{
		log_buffer[log_tail] = *p_log;
		log_tail ++;
		
		if ( log_tail >= TCNT_LOG_BUFFER )
		{
			log_tail = 0;
		}
		if ( log_count < TCNT_LOG_BUFFER )
		{
			log_count++;
		}
		else
		{
			log_head = log_tail;
			log_lost ++;
		}
	}

	/* low-level output. */
	if ( ( log_lowmask & LOG_MASK( pri ) ) != 0 )
	{
		_print_log( p_log, logger_putc );
	}

	if ( !locked )
	{
		ictx ? iunl_cpu() : unl_cpu();
	}
}

/**
 * reading from the log buffer.
 * 
 * @param p_log log information packet.
 * @return number of lost log.
 */
ER_UINT vrea_log(
	LOG * p_log )
{
	BOOL locked;
	BOOL ictx;
	ER_UINT ercd;

	locked = sns_loc();
	ictx = sns_ctx();
	
	if ( !locked )
	{
		ictx ? iloc_cpu() : loc_cpu();
	}
	
	if ( log_count > 0 )
	{
		*p_log = log_buffer[log_head];
		log_count --;
		log_head ++;
		
		if ( log_head >= TCNT_LOG_BUFFER )
		{
			log_head = 0;
		}
		
		ercd = (ER_UINT)log_lost;
		log_lost = 0;
	}
	else
	{
		ercd = E_OBJ;
	}
	
	if ( !locked )
	{
		ictx ? iunl_cpu() : unl_cpu();
	}
	
	return ercd;
}

/**
 * setting of importance of log information that should be output.
 * 
 * @param logmask importance that should be recorded in log buffer.
 * @param lowmask importance that should be low-level output.
 */
void vmsk_log(
	UINT logmask,
	UINT lowmask )
{
	log_logmask = LOG_UPTO( logmask );
	log_lowmask = LOG_UPTO( lowmask );
}

#endif

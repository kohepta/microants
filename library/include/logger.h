﻿/**
 * @file logger.h
 *
 * logging library module header.
 *
 * @date 2010.08.22 new creation
 */

#ifndef __LOGGER_H__
#define __LOGGER_H__

/**
 * log type.
 */
#define LOG_TYPE_INH		0x01u	/**< interrupt handler. */
#define LOG_TYPE_ISR		0x02u	/**< interrupt service routine. */
#define LOG_TYPE_CYC		0x03u	/**< cyclic handler. */
#define LOG_TYPE_EXC		0x04u	/**< cpu exception handler. */
#define LOG_TYPE_TEX		0x05u	/**< task exception routine. */
#define LOG_TYPE_TSKSTAT	0x06u	/**< task state variation. */
#define LOG_TYPE_DSP		0x07u	/**< dispatcher. */
#define LOG_TYPE_SVC		0x08u	/**< service call. */
#define LOG_TYPE_COMMENT	0x09u	/**< comment. */
#define LOG_TYPE_ASSERT		0x0au	/**< assertion. */

#define LOG_ENTER			0x00u	/**< enter/start */
#define LOG_LEAVE			0x80u	/**< leave/end */

/**
 * log importance.
 */
#define LOG_EMERG			0u		/**< emergency. */
#define LOG_ALERT			1u
#define LOG_CRIT			2u
#define LOG_ERROR			3u		/**< system error. */
#define LOG_WARNING			4u		/**< warning */
#define LOG_NOTICE			5u
#define LOG_INFO			6u
#define LOG_DEBUG			7u		/**< debug. */

/** log infomation max length. */
#define TMAX_LOGINFO		6

/** log buffer max size. */
#define TCNT_LOG_BUFFER		32

/**
 * macro to make bitmap of importance of log infromation.
 */
#define LOG_MASK( pri )		( 1u << ( pri ) )
#define LOG_UPTO( pri )		( ( 1u << ( ( pri ) + 1 ) ) - 1 )

/**
 * log data
 */
struct t_log
{
	/** log type */
	UINT logtype;
	
	/** other log information. */
	VP_INT loginfo[TMAX_LOGINFO];
};
typedef struct t_log LOG;

/* logger.c */
void logger_initialize(
	void ( * putc )( char ) );

/* logger.c */
void logger_finalize( void );

/* logger.c */
void logger_printf(
	UINT pri,
	const char * format, ... );

/* logger.c */
void logger_putc(
	char c );

/* logger.c */
void logger_flush(
	void ( * putc )( char ) );

/* logger.c */
void vwri_log(
	UINT pri,
	LOG * p_log );

/* logger.c */
ER_UINT vrea_log(
	LOG *p_log );

/* logger.c */
void vmsk_log(
	UINT logmask,
	UINT lowmask );

#endif

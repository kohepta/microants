﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dis_tex.c
 *
 * implementations of MicroAnts dis_tex().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * disable a task exception of current running task.
 * 
 * @return error code
 */
ER dis_tex( void )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_TEX * tex;
		BOOL disp;
		
		tex = &knltsk_current->t_tex;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( tex->e_entry == NULL )
		{
			/*
			 task exception has not been defined for
			 current running task.
			*/
			ercd = E_OBJ;
		}
		else
		{
			tex->e_state = TTEX_DIS;
			
			ercd = E_OK;
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

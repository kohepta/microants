﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sns_tex.c
 *
 * implementations of MicroAnts sns_tex().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * sense a task exception of current running task.
 * 
 * @retval TRUE task exception is disable
 * @retval FALSE task exception is enable
 */
BOOL sns_tex( void )
{
	BOOL status;
	
	if ( knltsk_current == NULL )
	{
		status = TRUE;
	}
	else
	{
		if ( knltsk_current->t_tex.e_state )
		{
			status = TRUE;
		}
		else
		{
			status = FALSE;
		}
	}
	
	return status;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_tex.c
 *
 * implementations of MicroAnts ref_tex().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * reference a task exception.
 * 
 * @param tskid task ID
 * @param pk_rtex task exception status packet
 * @return error code
 */
ER ref_tex(
	ID tskid,
	T_RTEX * pk_rtex )
{
	ER ercd;
	
	if ( pk_rtex == NULL )
	{
		/* illegal parameter. pk_rtex must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_TSK * tsk;
		
		if ( knlsys_sense_context() && tskid == TSK_SELF )
		{
			tsk == NULL;
		}
		else
		{
			/* contain current running task. */
			tsk = knltsk_get_entity_self( tskid );
		}
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else
				{
					const T_TEX * tex;
					
					tex = &tsk->t_tex;
					
					if ( tex->e_entry == NULL )
					{
						/* task exception has not been defined. */
						ercd = E_OBJ;
					}
					else
					{
						pk_rtex->texstat = tex->e_state;
						pk_rtex->pndptn = tex->e_pattern;
						
						ercd = E_OK;
					}
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

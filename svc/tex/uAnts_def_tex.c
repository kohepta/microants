﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_def_tex.c
 *
 * implementations of MicroAnts def_tex().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * define a task exception of current running task.
 * 
 * @param tskid task ID
 * @param pk_dtex task exception definition information packet
 * @return error code
 */
ER def_tex(
	ID tskid,
	T_DTEX * pk_dtex )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( pk_dtex != NULL  && ( pk_dtex->texrtn == NULL ) )
	{
		/* pk_dtex->texrtn must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_dtex != NULL 
		&& ( pk_dtex->texatr & ~( TA_HLNG | TA_ASM ) ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				/* define a task exception (kernel function). */
				knltex_define( &tsk->t_tex, pk_dtex );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_iras_tex.c
 *
 * implementations of MicroAnts iras_tex().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tex.h"
#include "uAnts_tsk.h"

/**
 * raise a task exception (from no-task context).
 * 
 * @param tskid task ID
 * @param rasptn pattern of raise exception
 * @return 
 */
ER iras_tex(
	ID tskid,
	TEXPTN rasptn )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( tskid == TSK_SELF )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		/* raise a task exception (kernel function). */
		ercd = knltex_raise( tskid, rasptn );
	}
	
	return ercd;
}

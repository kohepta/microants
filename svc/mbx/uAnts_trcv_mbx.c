﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_trcv_mbx.c
 *
 * implementations of MicroAnts trcv_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * receive a message from a mailbox (with timeout).
 * 
 * @param mbxid mailbox ID
 * @param ppk_msg pointer of pointer to received message
 * @param tmout time until timeout
 * @return error code
 */
ER trcv_mbx(
	ID mbxid,
	T_MSG * * ppk_msg,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/*
		 receive a message from a mailbox (kernel function).
		 (with timeout)
		*/
		ercd = knlmbx_receive( mbxid, ppk_msg, &event, tmout );
	}
	
	return ercd;
}

#endif

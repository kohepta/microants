﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_mbx.c
 *
 * implementations of MicroAnts acre_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * create a new mailbox (ID auto-assignment).
 * 
 * @param pk_cmbx mailbox creation information packet
 * @return mailbox ID (positive number) or error code
 */
ER_ID acre_mbx(
	T_CMBX * pk_cmbx )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlmbx_free_queue ) )
	{
		/* mailbox resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_MBX * mbx;
		
		mbx = (T_MBX *)knlqueue_head( &knlmbx_free_queue );
		
		/* create a new mailbox (kernel function). */
		ercd_id = knlmbx_create( mbx, pk_cmbx );
		
		if ( ercd_id == E_OK )
		{
			/* get mailbox ID. */
			ercd_id = knlobject_get_id( &mbx->m_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_mbx.c
 *
 * implementations of MicroAnts del_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * delete a mailbox.
 * 
 * @param mbxid mailbox ID
 * @return error code
 */
ER del_mbx(
	ID mbxid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MBX * mbx;
		
		mbx = knlmbx_get_entity( mbxid );
		
		if ( mbx == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mbx->m_super ) )
			{
				/* mailbox has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				/* delete all messages in a mailbox. */
				while ( !knlqueue_is_empty( &mbx->m_message_queue ) )
				{
					T_MSG * msg;
					
					msg = (T_MSG *)knlqueue_head( &mbx->m_message_queue );
					
					/* delete a massage in a queue. */
					knlqueue_delete( &mbx->m_message_queue, &msg->g_joint );
					
					/* clear magic number. */
					msg->g_magic = 0;
				}
				
				knlmbx_finalize( mbx );
				
				/*
				 notify the mailbox was deleted, to the all tasks
				 waiting for it.
				*/
				knltsk_notify_object_deletion( &mbx->m_task_queue );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

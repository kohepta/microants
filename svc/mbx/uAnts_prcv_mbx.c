﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_prcv_mbx.c
 *
 * implementations of MicroAnts prcv_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * receive a message from a mailbox (polling).
 * 
 * @param mbxid mailbox ID
 * @param ppk_msg pointer of pointer to received message 
 * @return error code
 */
ER prcv_mbx(
	ID mbxid,
	T_MSG * * ppk_msg )
{
	ER ercd;
	
	/* receive a message from a mailbox (kernel function). (polling) */
	ercd = knlmbx_receive( mbxid, ppk_msg, NULL, TMO_POL );
	
	return ercd;
}

#endif

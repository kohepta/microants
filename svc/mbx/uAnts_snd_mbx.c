﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_snd_mbx.c
 *
 * implementations of MicroAnts snd_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * send a message to a mailbox.
 * 
 * @param mbxid mailbox ID
 * @param pk_msg message packet
 * @return error code
 */
ER snd_mbx(
	ID mbxid,
	T_MSG * pk_msg )
{
	ER ercd;
	
	/* send a message to a mailbox (kernel function). */
	ercd = knlmbx_send( mbxid, pk_msg );
	
	return ercd;
}

#endif

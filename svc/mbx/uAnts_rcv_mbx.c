﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rcv_mbx.c
 *
 * implementations of MicroAnts rcv_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * receive a message from a mailbox (infinite).
 * 
 * @param mbxid mailbox ID
 * @param ppk_msg pointer of pointer to received message 
 * @return error code
 */
ER rcv_mbx(
	ID mbxid,
	T_MSG * * ppk_msg )
{
	ER ercd;
	
	/* receive a message from a mailbox (kernel function). (infinite) */
	ercd = knlmbx_receive( mbxid, ppk_msg, NULL, TMO_FEVR );
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_mbx.c
 *
 * implementations of MicroAnts cre_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * create a new mailbox.
 * 
 * @param mbxid mailbox ID
 * @param pk_cmbx mailbox creation information packet
 * @return error code
 */
ER cre_mbx(
	ID mbxid,
	T_CMBX * pk_cmbx )
{
	ER ercd;
	T_MBX * mbx;
	
	mbx = knlmbx_get_entity( mbxid );
	
	if ( mbx == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &mbx->m_super ) )
		{
			/* mailbox has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new mailbox (kernel function). */
			ercd = knlmbx_create( mbx, pk_cmbx );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

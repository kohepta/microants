﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_mbx.c
 *
 * implementations of MicroAnts ref_mbx().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_mbx.h"

#if ( UANTS_CFG_MBX_MAX_ID > 0 )

/**
 * reference a mailbox.
 * 
 * @param mbxid mailbox ID
 * @param pk_rmbx mailbox status packet
 * @return error code
 */
ER ref_mbx(
	ID mbxid,
	T_RMBX * pk_rmbx )
{
	ER ercd;
	
	if ( pk_rmbx == NULL )
	{
		/* illegal parameter. pk_rmbx must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_MBX * mbx;
		
		mbx = knlmbx_get_entity( mbxid );
		
		if ( mbx == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mbx->m_super ) )
			{
				/* mailbox has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knlqueue_is_empty( &mbx->m_task_queue ) )
				{
					pk_rmbx->wtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &mbx->m_task_queue ) );
				}
				else
				{
					pk_rmbx->wtskid = TSK_NONE;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( !knlqueue_is_empty( &mbx->m_message_queue ) )
				{
					pk_rmbx->pk_msg = 
						(T_MSG *)knlqueue_head( &mbx->m_message_queue );
				}
				else
				{
					pk_rmbx->pk_msg = NULL;
				}
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

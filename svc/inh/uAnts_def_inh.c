﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_def_inh.c
 *
 * implementations of MicroAnts def_inh().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_inh.h"

/**
 * define an interrupt handler.
 * 
 * @param inhno interrupt handler number
 * @param pk_dinh definition information packet
 * @return error code
 */
ER def_inh(
	INHNO inhno,
	T_DINH * pk_dinh )
{
	ER ercd;
	BIOS_UINT lock;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( inhno < 0 || inhno >= UANTS_CFG_INH_MAX_ID )
	{
		/* intrrupt handler number is outside effective range. */
		ercd = E_PAR;
	}
	else if ( pk_dinh != NULL && pk_dinh->inthdr == NULL )
	{
		/* pk_dinh->inthdr must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_dinh != NULL
		&& ( pk_dinh->inhatr & ~( TA_HLNG | TA_ASM ) ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/* define an interrupt handler (kernel function). */
		knlinh_define( inhno, pk_dinh, NULL );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		ercd = E_OK;
	}
	
	return ercd;
}

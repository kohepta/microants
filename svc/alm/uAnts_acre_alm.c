﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_alm.c
 *
 * implementations of MicroAnts acre_alm().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

/**
 * create a new alarm handler (ID auto-assignment).
 * 
 * @param pk_calm alarm handler creation information packet
 * @return alarm handler ID (positive number) or error code
 */
ER_ID acre_alm(
	T_CALM * pk_calm )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlalm_free_queue ) )
	{
		/* alarm handler resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_ALM * alm;
		
		alm = (T_ALM *)knlqueue_head( &knlalm_free_queue );
		
		/* create a new alarm handler (kernel function). */
		ercd_id = knlalm_create( alm, pk_calm );
		
		if ( ercd_id == E_OK )
		{
			/* get alarm handler ID. */
			ercd_id = knlevent_get_id( &alm->a_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sta_alm.c
 *
 * implementations of MicroAnts sta_alm().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

static void _knlalm_start_event(
	T_EVENT * const event );

/**
 * callback. when the addition to timeout queue was completed, it is called.
 * 
 * @param event time-event entity
 */
static void _knlalm_start_event(
	T_EVENT * const event )
{
	T_ALM * alm;
	
	alm = (T_ALM *)event;
	
	/* start an alarm handler. */
	alm->a_state = TALM_STA;
}

/**
 * start an alarm handler.
 * 
 * @param almid alarm handler ID
 * @param almtim alarm time
 * @return error code
 */
ER sta_alm(
	ID almid,
	RELTIM almtim )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_ALM * alm;
		
		alm = knlalm_get_entity( almid );
		
		if ( alm == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlevent_exists( &alm->a_super ) )
			{
				/* alarm handler has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( alm->a_state == TALM_STA )
				{
					/*
					 if alarm handler has already started,
					 stop an alarm handler.
					*/
					knltim_delete_event( &alm->a_super );
					alm->a_state = TALM_STP;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				knltim_add_event_with_callback(
					&alm->a_super, 
					almtim, 
					_knlalm_start_event );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_alm.c
 *
 * implementations of MicroAnts ref_alm().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

/**
 * reference an alarm handler.
 * 
 * @param almid alarm handler ID
 * @param pk_ralm alarm handler status packet
 * @return error code
 */
ER ref_alm(
	ID almid,
	T_RALM * pk_ralm )
{
	ER ercd;
	
	if ( pk_ralm == NULL )
	{
		/* illegal parameter. pk_ralm must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_ALM * alm;
		
		alm = knlalm_get_entity( almid );
		
		if ( alm == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlevent_exists( &alm->a_super ) )
			{
				/* alarm handler has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				pk_ralm->almstat = alm->a_state;
				
				if ( alm->a_state == TALM_STA )
				{
					pk_ralm->lefttim = 
						knltim_get_remainder_until_timeout(
							&alm->a_super );
				}
				else
				{
					pk_ralm->lefttim = 0;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

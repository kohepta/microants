﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_alm.c
 *
 * implementations of MicroAnts cre_alm().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

/**
 * create a new alarm handler.
 * 
 * @param almid alarm handler ID
 * @param pk_calm alarm handler creation information packet
 * @return error code
 */
ER cre_alm(
	ID almid,
	T_CALM * pk_calm )
{
	ER ercd;
	T_ALM * alm;
	
	alm = knlalm_get_entity( almid );
	
	if ( alm == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlevent_exists( &alm->a_super ) )
		{
			/* alarm handler has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new alarm handler (kernel function). */
			ercd = knlalm_create( alm, pk_calm );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

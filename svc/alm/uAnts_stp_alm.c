﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_stp_alm.c
 *
 * implementations of MicroAnts stp_alm().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_alm.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_ALM_MAX_ID > 0 )

/**
 * stop an alarm handler.
 * 
 * @param almid alarm handler ID
 * @return error code
 */
ER stp_alm(
	ID almid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_ALM * alm;
		
		alm = knlalm_get_entity( almid );
		
		if ( alm == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlevent_exists( &alm->a_super ) )
			{
				/* alarm handler has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( alm->a_state == TALM_STA )
				{
					knltim_delete_event( &alm->a_super );
					alm->a_state = TALM_STP;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dis_int.c
 *
 * implementations of MicroAnts dis_int().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts.h"

/**
 * disable an interrupt with interrupt number.
 * 
 * @param intno interrupt number
 * @return error code
 */
ER dis_int(
	INTNO intno )
{
	ER ercd;
	
	ercd = (ER)bios_intc_disable_interrupt( (BIOS_UINT)intno );
	
	return ercd;
}

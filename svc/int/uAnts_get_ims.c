﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_get_ims.c
 *
 * implementations of MicroAnts get_ims().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * get an interrupt mask.
 * 
 * @param p_imask pointer to interrupt mask
 * @return error code
 */
ER get_ims(
	IMASK * p_imask )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( p_imask == NULL )
	{
		/* p_imask must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		ercd = (ER)bios_cpu_get_interrupt_priority(
			(BIOS_UINT *)p_imask );
	}
	
	return ercd;
}

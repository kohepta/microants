﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_chg_ims.c
 *
 * implementations of MicroAnts chg_ims().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * change interrupt mask.
 * 
 * @param imask new interrupt mask
 * @return error code
 */
ER chg_ims(
	IMASK imask )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		ercd = (ER)bios_cpu_set_interrupt_priority( (BIOS_UINT)imask );
	}
	
	return ercd;
}

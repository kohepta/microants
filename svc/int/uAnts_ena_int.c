﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ena_int.c
 *
 * implementations of MicroAnts ena_int().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts.h"

/**
 * enable an interrupt with interrupt number.
 * 
 * @param intno interrupt number
 * @return error code
 */
ER ena_int(
	INTNO intno )
{
	ER ercd;
	
	ercd = (ER)bios_intc_enable_interrupt( (BIOS_UINT)intno );
	
	return ercd;
}

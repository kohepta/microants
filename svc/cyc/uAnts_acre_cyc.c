﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_cyc.c
 *
 * implementations of MicroAnts acre_cyc().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_cyc.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/**
 * create a new cyclic handler (ID auto-assignment).
 * 
 * @param pk_ccyc cyclic handler creation information packet
 * @return cyclic handler ID (positive number) or error code
 */
ER_ID acre_cyc(
	T_CCYC * pk_ccyc )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlcyc_free_queue ) )
	{
		/* cyclic handler resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_CYC * cyc;
		
		cyc = (T_CYC *)knlqueue_head( &knlcyc_free_queue );
		
		/* create a new cyclic handler (kernel function). */
		ercd_id = knlcyc_create( cyc, pk_ccyc );
		
		if ( ercd_id == E_OK )
		{
			/* get cyclic handler ID. */
			ercd_id = knlevent_get_id( &cyc->c_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

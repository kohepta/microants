﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_cyc.c
 *
 * implementations of MicroAnts cre_cyc().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_cyc.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/**
 * create a new cyclic handler.
 * 
 * @param cycid cyclic handler ID
 * @param pk_cycc cyclic handler creation information packet
 * @return error code
 */
ER cre_cyc(
	ID cycid,
	T_CCYC * pk_ccyc )
{
	ER ercd;
	T_CYC * cyc;
	
	cyc = knlcyc_get_entity( cycid );
	
	if ( cyc == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlevent_exists( &cyc->c_super ) )
		{
			/* cyclic handler has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a newcyclic handler (kernel function). */
			ercd = knlcyc_create( cyc, pk_ccyc );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

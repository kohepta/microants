﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_cyc.c
 *
 * implementations of MicroAnts ref_cyc().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_cyc.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/**
 * reference a cyclic handler.
 * 
 * @param cycid cyclic handler ID
 * @param pk_rcyc cyclic handler status packet
 * @return error code
 */
ER ref_cyc(
	ID cycid,
	T_RCYC * pk_rcyc )
{
	ER ercd;
	
	if ( pk_rcyc == NULL )
	{
		/* illegal parameter. pk_rcyc must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_CYC * cyc;
		
		cyc = knlcyc_get_entity( cycid );
		
		if ( cyc == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlevent_exists( &cyc->c_super ) )
			{
				/* cyclic handler has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				pk_rcyc->cycstat = cyc->c_state;
				
				if ( cyc->c_state == TCYC_STA )
				{
					pk_rcyc->lefttim = 
						knltim_get_remainder_until_timeout(
							&cyc->c_super );
				}
				else
				{
					pk_rcyc->lefttim = 0;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

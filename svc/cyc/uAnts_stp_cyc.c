﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_stp_cyc.c
 *
 * implementations of MicroAnts stp_cyc().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_cyc.h"
#include "uAnts_tim.h"

#if ( UANTS_CFG_CYC_MAX_ID > 0 )

/**
 * stop a cyclic handler.
 * 
 * @param cycid cyclic handler ID
 * @return error code
 */
ER stp_cyc(
	ID cycid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_CYC * cyc;
		
		cyc = knlcyc_get_entity( cycid );
		
		if ( cyc == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlevent_exists( &cyc->c_super ) )
			{
				/* cyclic handler has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( cyc->c_state == TCYC_STA
					&& !knlcyc_attribute_preserve_phase( cyc ) )
				{
					knltim_delete_event( &cyc->c_super );
				}
				
				/* stop a cyclic handler. */
				cyc->c_state = TCYC_STP;
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

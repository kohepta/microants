﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_mpl.c
 *
 * implementations of MicroAnts ref_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_STACK	\
	|| UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )

/**
 * reference a variable-sized memory pool.
 * 
 * @param mplid variable-sized memory pool ID
 * @param pk_rmpl variable-sized memory pool status packet
 * @return error code
 */
ER ref_mpl(
	ID mplid,
	T_RMPL * pk_rmpl )
{
	ER ercd;
	
	if ( pk_rmpl == NULL )
	{
		/* illegal parameter. pk_rmpl must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_MPL * mpl;
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK	\
			|| UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
		if ( mplid == KNLMPL_GENERIC_ENTITY_ID )
		{
			/* get generic memory pool. */
			mpl = knlmpl_get_generic_entity();
		}
		else if ( mplid == KNLMPL_STACK_ENTITY_ID )
		{
			/* get stack memory pool. */
			mpl = knlmpl_get_stack_entity();
		}
		else
		#endif
		{
			mpl = knlmpl_get_entity( mplid );
		}
		
		if ( mpl == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpl->v_super ) )
			{
				/* variable-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knlqueue_is_empty( &mpl->v_task_queue ) )
				{
					pk_rmpl->wtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &mpl->v_task_queue ) );
				}
				else
				{
					pk_rmpl->wtskid = TSK_NONE;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				pk_rmpl->fmplsz = (SIZE)mpl->v_free_size;
				
				if ( mpl->v_max_block != NULL )
				{
					pk_rmpl->fblksz = (UINT)mpl->v_max_block->b_free.f_size;
				}
				else
				{
					pk_rmpl->fblksz = 0;
				}
				
				pk_rmpl->fblkcnt = knlqueue_get_count( &mpl->v_block_queue );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_mpl.c
 *
 * implementations of MicroAnts acre_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * create a new variable-sized memory pool (ID auto-assignment).
 * 
 * @param pk_cmpl variable-sized memory pool creation information packet
 * @return variable-sized memory pool ID or error code
 */
ER_ID acre_mpl(
	T_CMPL * pk_cmpl )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlmpl_free_queue ) )
	{
		/* variable-sized memory pool resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_MPL * mpl;
		
		mpl = (T_MPL *)knlqueue_head( &knlmpl_free_queue );
		
		/* create a new variable-sized memory pool (kerenel function). */
		ercd_id = knlmpl_create( mpl, pk_cmpl );
		
		if ( ercd_id == E_OK )
		{
			/* get variable-sized memory pool ID. */
			ercd_id = knlobject_get_id( &mpl->v_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

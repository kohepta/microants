﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_mpl.c
 *
 * implementations of MicroAnts del_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * delete a variable-sized memory pool.
 * 
 * @param mplid variable-sized memory pool ID
 * @return error code
 */
ER del_mpl(
	ID mplid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPL * mpl;
		
		mpl = knlmpl_get_entity( mplid );
		
		if ( mpl == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpl->v_super ) )
			{
				/* variable-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				VP vp;
				
				vp = mpl->v_origin;
				#endif
				
				knlmpl_finalize( mpl );
				
				/*
				 notify the variable-sized memory pool was deleted,
				 to the all tasks waiting for it.
				*/
				knltsk_notify_object_deletion( &mpl->v_task_queue );
				
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				if ( vp != NULL )
				{
					T_MPL * genmpl;
					
					/* get 'variable-sized memory pool' memory pool */
					genmpl = knlmpl_get_generic_entity();
					
					if ( knlmpl_verify_contains( genmpl, vp ) )
					{
						/* 'variable-sized memory pool' buffer is freed. */
						knlmpl_free( genmpl, vp );
					}
				}
				#endif
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

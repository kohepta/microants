﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tget_mpl.c
 *
 * implementations of MicroAnts tget_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * get a memory block from a variable-sized memory pool (with timeout).
 * 
 * @param mplid variable-sized memory pool ID
 * @param blksz memory block siz
 * @param p_blk pointer of pointer to memory block
 * @param tmout time until timeout
 * @return error code
 */
ER tget_mpl(
	ID mplid,
	UINT blksz,
	VP * p_blk,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/*
		 get a memory block from a variable-sized memory pool
		 (kernel function). (with timeout)
		*/
		ercd = knlmpl_get( mplid, (UW)blksz, p_blk, &event, tmout );
	}
	
	return ercd;
}

#endif

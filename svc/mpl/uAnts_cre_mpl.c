﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_mpl.c
 *
 * implementations of MicroAnts cre_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * create a new variable-sized memory pool.
 * 
 * @param mplid variable-sized memory pool ID
 * @param pk_cmpl variable-sized memory pool creation information packet
 * @return error code
 */
ER cre_mpl(
	ID mplid,
	T_CMPL * pk_cmpl )
{
	ER ercd;
	T_MPL * mpl;
	
	mpl = knlmpl_get_entity( mplid );
	
	if ( mpl == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &mpl->v_super ) )
		{
			/* variable-sized memory pool has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new variable-sized memory pool (kernel function). */
			ercd = knlmpl_create( mpl, pk_cmpl );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

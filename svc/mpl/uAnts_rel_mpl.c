﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rel_mpl.c
 *
 * implementations of MicroAnts rel_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * release a memory block to a variable-sized memory pool.
 * 
 * @param mplid variable-sized memory pool ID
 * @param blk released memory block
 * @return error code
 */
ER rel_mpl(
	ID mplid,
	VP blk )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPL * mpl;
		
		mpl = knlmpl_get_entity( mplid );
		
		if ( mpl == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpl->v_super ) )
			{
				/* variable-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else if ( blk == NULL )
			{
				/* illegal paramter. blk must not point to null. */
				ercd = E_PAR;
			}
			else if ( !knlmpl_verify_contains( mpl, blk ) )
			{
				/*
				 memory block is not located on 
				 variable-sized memory pool.
				*/
				ercd = E_PAR;
			}
			else
			{
				/*jp
				 メモリ解放を行い, 解放されたメモリブロックを解放待ちタスク
				 に対して割り当てる.
				*/
				knlmpl_allocate_waiting_tasks(
					mpl,
					knlmpl_free( mpl, blk ) );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_pget_mpl.c
 *
 * implementations of MicroAnts pget_mpl().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_mpl.h"

#if ( UANTS_CFG_MPL_MAX_ID > 0 )

/**
 * get a memory block from a variable-sized memory pool (polling).
 * 
 * @param mplid variable-sized memory pool ID
 * @param blksz memory block size
 * @param p_blk pointer of pointer to memory block
 * @return error code
 */
ER pget_mpl(
	ID mplid,
	UINT blksz,
	VP * p_blk )
{
	ER ercd;
	
	/*
	 get a memory block from a variable-sized memory pool (kernel function).
	 (polling)
	*/
	ercd = knlmpl_get( mplid, (UW)blksz, p_blk, NULL, TMO_POL );
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_loc_cpu.c
 *
 * implementations of MicroAnts loc_cpu().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * lock CPU.
 * 
 * @return error code
 */
ER loc_cpu( void )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		bios_cpu_disable_interrupts();
		
		ercd = E_OK;
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_cfg.c
 *
 * implementations of MicroAnts ref_cfg().
 *
 * @date 2010.06.15 new creation
 */

#include "uAnts.h"
#include "uAnts_tsk.h"
#include "uAnts_mpl.h"

/**
 * reference kernel configuration.
 * 
 * @param pk_rcfg kernel configuration status packet
 * @return error code
 */
ER ref_cfg(
	T_RCFG * pk_rcfg )
{
	ER ercd;
	
	if ( pk_rcfg == NULL )
	{
		/* illegal parameter. pk_rcfg must not point to null. */
		ercd = E_PAR;
	}
	else
	{
        pk_rcfg->min_tpri = TMIN_TPRI;
        pk_rcfg->max_tpri = TMAX_TPRI;
        pk_rcfg->min_mpri = TMIN_MPRI;
        pk_rcfg->max_mpri = TMAX_MPRI;
        pk_rcfg->max_actcnt = TMAX_ACTCNT;
        pk_rcfg->max_wupcnt = TMAX_WUPCNT;
        pk_rcfg->max_suscnt = TMAX_SUSCNT;
        pk_rcfg->bit_texptn = TBIT_TEXPTN;
        pk_rcfg->bit_flgptn = TBIT_FLGPTN;
        #if 0
        pk_rcfg->bit_rdvptn = TBIT_RDVPTN;
        #endif
        pk_rcfg->tic_nume = UANTS_CFG_TIC_NUME;
        pk_rcfg->tic_deno = UANTS_CFG_TIC_DENO;
        pk_rcfg->max_maxsem = TMAX_MAXSEM;
        pk_rcfg->max_tskid = UANTS_CFG_TSK_MAX_ID;
        pk_rcfg->max_semid = UANTS_CFG_SEM_MAX_ID;
        pk_rcfg->max_flgid = UANTS_CFG_FLG_MAX_ID;
        pk_rcfg->max_dtqid = UANTS_CFG_DTQ_MAX_ID;
        pk_rcfg->max_mbxid = UANTS_CFG_MBX_MAX_ID;
        #if 0
        pk_rcfg->max_mtxid = 0;
        pk_rcfg->max_mbfid = 0;
        pk_rcfg->max_porid = 0;
        #endif
        pk_rcfg->max_mpfid = UANTS_CFG_MPF_MAX_ID;
        pk_rcfg->max_mplid = UANTS_CFG_MPL_MAX_ID;
        pk_rcfg->max_cycid = UANTS_CFG_CYC_MAX_ID;
        pk_rcfg->max_almid = UANTS_CFG_ALM_MAX_ID;
		
		ercd = E_OK;
	}
	
	return ercd;
}

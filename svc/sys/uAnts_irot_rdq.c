﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_irot_rdq.c
 *
 * implementations of MicroAnts irot_rdq().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"
#include "uAnts_tsk.h"

/**
 * rotate ready queue with a priority (from no-task context).
 * 
 * @param tskpri priority of task
 * @return error code
 */
ER irot_rdq(
	PRI tskpri )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( tskpri == TPRI_SELF )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		/* rotate ready queue with a priority (kernel function). */
		ercd = knltsk_rotate_ready_queue( tskpri );
	}
	
	return ercd;
}

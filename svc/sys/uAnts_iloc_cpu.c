﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_iloc_cpu.c
 *
 * implementations of MicroAnts iloc_cpu().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * lock CPU (from no-task context).
 * 
 * @return error code
 */
ER iloc_cpu( void )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		bios_cpu_disable_interrupts();
		
		ercd = E_OK;
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dis_dsp.c
 *
 * implementations of MicroAnts dis_dsp().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * disable dispatch.
 * 
 * @return error code
 */
ER dis_dsp( void )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* disable dispatch. */
		knlsys_disable_dispatch();
		
		ercd = E_OK;
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sns_dsp.c
 *
 * implementations of MicroAnts sns_dsp().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * sense a state of dispatch.
 * 
 * @retval TRUE dispatch is disable
 * @retval FALSE dispatch is enable
 */
BOOL sns_dsp( void )
{
	BOOL status;
	
	status = !knlsys_sense_dispatchable();
	
	return status;
}

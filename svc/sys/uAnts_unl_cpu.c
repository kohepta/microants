﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_unl_cpu.c
 *
 * implementations of MicroAnts unl_cpu().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * unlock CPU.
 * 
 * @return error code
 */
ER unl_cpu( void )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* enable interrupts. */
		bios_cpu_enable_interrupts();
		
		if ( knlsys_sense_dispatchable() )
		{
			/*jp
			 保留ディスパッチ要求があることを前提にディスパッチ
			 が保留状態でなければ, スケジューリングを行う.
			*/
			knlsys_dispatch();
		}
		
		ercd = E_OK;
	}
	
	return ercd;
}

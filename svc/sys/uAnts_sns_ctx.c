﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sns_ctx.c
 *
 * implementations of MicroAnts sns_ctx().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * sense a current context.
 * 
 * @retval TRUE current context is no-task context
 * @retval FALSE current context is task-context
 */
BOOL sns_ctx( void )
{
	BOOL status;
	
	status = knlsys_sense_context();
	
	return status;
}

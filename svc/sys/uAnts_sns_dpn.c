﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sns_dpn.c
 *
 * implementations of MicroAnts sns_dpn().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * sense a state of dispatch pending.
 * 
 * @retval TRUE dispatch is pending
 * @retval FALSE dispatch is not pending
 */
BOOL sns_dpn( void )
{
	BOOL status;
	
	status = knlsys_sense_dispatch_pending();
	
	return status;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rot_rdq.c
 *
 * implementations of MicroAnts rot_rdq().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"
#include "uAnts_tsk.h"

/**
 * rotate ready queue with a priority.
 * 
 * @param tskpri priority of task
 * @return error code
 */
ER rot_rdq(
	PRI tskpri )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		if ( tskpri == TPRI_SELF )
		{
			tskpri = knltsk_get_priority( knltsk_current );
		}
		
		/* rotate ready queue with a priority (kernel function). */
		ercd = knltsk_rotate_ready_queue( tskpri );
	}
	
	return ercd;
}

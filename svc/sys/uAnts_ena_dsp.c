﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ena_dsp.c
 *
 * implementations of MicroAnts ena_dsp().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * enable dispatch.
 * 
 * @return error code
 */
ER ena_dsp( void )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* enable dispatch. */
		knlsys_enable_dispatch();
		
		if ( !bios_cpu_sense_locked() )
		{
			/*jp
			 保留ディスパッチ要求があることを前提にディスパッチ
			 が保留状態でなければ, スケジューリングを行う.
			*/
			knlsys_dispatch();
		}
		
		ercd = E_OK;
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_get_tid.c
 *
 * implementations of MicroAnts get_tid().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"
#include "uAnts_tsk.h"

/**
 * get ID of the current running task.
 * 
 * @param p_tskid pointer to task ID
 * @return error code
 */
ER get_tid(
	ID * p_tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( p_tskid == NULL )
	{
		/* p_tskid must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		/* get ID of the current running task (kernel function). */
		*p_tskid = knltsk_get_current_id();
		
		ercd = E_OK;
	}
	
	return ercd;
}

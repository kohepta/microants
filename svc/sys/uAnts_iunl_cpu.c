﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_iunl_cpu.c
 *
 * implementations of MicroAnts iunl_cpu().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * unlock CPU (from no-task context).
 * 
 * @return error code
 */
ER iunl_cpu( void )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		bios_cpu_enable_interrupts();
		
		ercd = E_OK;
	}
	
	return ercd;
}

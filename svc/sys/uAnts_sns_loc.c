﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sns_loc.c
 *
 * implementations of MicroAnts sns_loc().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * sense a state of CPU lock.
 * 
 * @retval TRUE CPU is locked
 * @retval FALSE CPU is not locked
 */
BOOL sns_loc( void )
{
	BOOL status;
	
	status = bios_cpu_sense_locked() ? TRUE : FALSE;
	
	return status;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_ver.c
 *
 * implementations of MicroAnts ref_ver().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_sys.h"

/**
 * reference kernel version.
 * 
 * @param pk_rver kernel version status packet
 * @return error code
 */
ER ref_ver(
	T_RVER * pk_rver )
{
	ER ercd;
	
	if ( pk_rver = NULL )
	{
		/* illegal parameter. pk_rver must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		pk_rver->maker = TKERNEL_MAKER;
		pk_rver->prid = TKERNEL_PRID;
		pk_rver->spver = TKERNEL_SPVER;
		pk_rver->prver = TKERNEL_PRVER;
		pk_rver->prno[0] = 0;
		pk_rver->prno[1] = 0;
		pk_rver->prno[2] = 0;
		pk_rver->prno[3] = 0;
		
		ercd = E_OK;
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_flg.c
 *
 * implementations of MicroAnts del_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * delete an eventflag.
 * 
 * @param flgid eventflag ID
 * @return error code
 */
ER del_flg(
	ID flgid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_FLG * flg;
		
		flg = knlflg_get_entity( flgid );
		
		if ( flg == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &flg->f_super ) )
			{
				/* eventflag has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				knlflg_finalize( flg );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				/*
				 notify the eventflag was deleted, to the all tasks
				 waiting for it.
				*/
				knltsk_notify_object_deletion( &flg->f_task_queue );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

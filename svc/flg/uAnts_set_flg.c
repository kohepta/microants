﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_set_flg.c
 *
 * implementations of MicroAnts set_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * set pattern to an eventflag.
 * 
 * @param flgid eventflag ID
 * @param setptn set bit pattern
 * @return error code
 */
ER set_flg(
	ID flgid,
	FLGPTN setptn )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* set pattern to an eventflag (kernel function). */
		ercd = knlflg_set( flgid, setptn );
	}
	
	return ercd;
}

#endif

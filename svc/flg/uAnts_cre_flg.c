﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_flg.c
 *
 * implementations of MicroAnts cre_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * create a new eventflag.
 * 
 * @param flgid eventflag ID
 * @param pk_cflg eventflag creation information packet
 * @return error code
 */
ER cre_flg(
	ID flgid,
	T_CFLG * pk_cflg )
{
	ER ercd;
	T_FLG * flg;
	
	flg = knlflg_get_entity( flgid );
	
	if ( flg == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &flg->f_super ) )
		{
			/* eventflag has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new eventflag (kernel function). */
			ercd = knlflg_create( flg, pk_cflg );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_wai_flg.c
 *
 * implementations of MicroAnts wai_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * wait for an eventflag forever.
 * 
 * @param flgid eventflag ID
 * @param waiptn wait bit pattern
 * @param wfmode wait mode
 * @param flag pattern, when wait released
 * @return error code
 */
ER wai_flg(
    ID flgid,
    FLGPTN waiptn,
    MODE wfmode,
    FLGPTN * p_flgptn)
{
	ER ercd;
	
	/* wait for an eventflag forever (kernel function). */
	ercd = knlflg_wait( flgid, waiptn, wfmode, p_flgptn, NULL, TMO_FEVR );
	
	return ercd;
}

#endif

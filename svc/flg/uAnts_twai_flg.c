﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_twai_flg.c
 *
 * implementations of MicroAnts twai_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * wait for an eventflag (with timeout).
 * 
 * @param flgid eventflag ID
 * @param waiptn wait bit pattern
 * @param wfmode wait mode
 * @param flag pattern, when wait released
 * @param tmout timeout until timeout
 * @return error code
 */
ER twai_flg(
    ID flgid,
    FLGPTN waiptn,
    MODE wfmode,
    FLGPTN * p_flgptn,
    TMO tmout)
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/* wait for an eventflag (kernel function). (with timeout) */
		ercd = knlflg_wait(
			flgid, 
			waiptn, 
			wfmode, 
			p_flgptn, 
			&event, 
			tmout );
	}
	
	return ercd;
}

#endif

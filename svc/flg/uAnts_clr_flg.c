﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_clr_flg.c
 *
 * implementations of MicroAnts clr_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * clear an eventflag.
 * 
 * @param flgid eventflag ID
 * @param clrptn clear bit pattern
 * @return error code
 */
ER clr_flg(
	ID flgid,
	FLGPTN clrptn )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_FLG * flg;
		
		flg = knlflg_get_entity( flgid );
		
		if ( flg == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &flg->f_super ) )
			{
				/* eventflag has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				/* clear eventflag. */
				flg->f_pattern &= clrptn;
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

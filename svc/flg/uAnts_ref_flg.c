﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_flg.c
 *
 * implementations of MicroAnts ref_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * reference an eventflag.
 * 
 * @param flgid eventflag ID
 * @param pk_rflg eventflag status packet
 * @return error code
 */
ER ref_flg(
	ID flgid,
	T_RFLG * pk_rflg )
{
	ER ercd;
	
	if ( pk_rflg == NULL )
	{
		/* illegal parameter. pk_rflg must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_FLG * flg;
		
		flg = knlflg_get_entity( flgid );
		
		if ( flg == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &flg->f_super ) )
			{
				/* eventflag has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knlqueue_is_empty( &flg->f_task_queue ) )
				{
					pk_rflg->wtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &flg->f_task_queue ) );
				}
				else
				{
					pk_rflg->wtskid = TSK_NONE;
				}
				
				pk_rflg->flgptn = flg->f_pattern;
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_flg.c
 *
 * implementations of MicroAnts acre_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * create a new eventflag (ID auto-assignment).
 * 
 * @param pk_cflg eventflag creation information packet
 * @return eventflag ID (positive number) or error code
 */
ER_ID acre_flg(
	T_CFLG * pk_cflg )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlflg_free_queue ) )
	{
		/* eventflag resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_FLG * flg;
		
		flg = (T_FLG *)knlqueue_head( &knlflg_free_queue );
		
		/* create a new eventflag (kernel function). */
		ercd_id = knlflg_create( flg, pk_cflg );
		
		if ( ercd_id == E_OK )
		{
			/* get eventflag ID. */
			ercd_id = knlobject_get_id( &flg->f_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

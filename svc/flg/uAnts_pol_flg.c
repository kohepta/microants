﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_pol_flg.c
 *
 * implementations of MicroAnts pol_flg().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_flg.h"

#if ( UANTS_CFG_FLG_MAX_ID > 0 )

/**
 * wait for an eventflag (polling).
 * 
 * @param flgid eventflag ID
 * @param waiptn wait bit pattern
 * @param wfmode wait mode
 * @param p_flgptn flag pattern, when wait released
 * @return error code
 */
ER pol_flg(
    ID flgid,
    FLGPTN waiptn,
    MODE wfmode,
    FLGPTN * p_flgptn )
{
	ER ercd;
	
	/* wait for an eventflag (kernel function). (polling) */
	ercd = knlflg_wait( flgid, waiptn, wfmode, p_flgptn, NULL, TMO_POL );
	
	return ercd;
}

#endif

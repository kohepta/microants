﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_def_exc.c
 *
 * implementations of MicroAnts def_exc().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_exc.h"

#if ( UANTS_CFG_EXC_MAX_ID > 0 )

/**
 * define a cpu exception handler.
 * 
 * @param excno cpu exception handler number
 * @param pk_dexc cpu exception handler definition information packet
 * @return error code
 */
ER def_exc(
	EXCNO excno,
	T_DEXC * pk_dexc )
{
	ER ercd;
	BIOS_UINT lock;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( excno < 0 || excno >= UANTS_CFG_EXC_MAX_ID )
	{
		/* cpu exception handler number is outside effective range. */
		ercd = E_PAR;
	}
	else if ( pk_dexc != NULL && pk_dexc->exchdr == NULL)
	{
		/* pk_dexc->exchdr must not point to null. */
		ercd = E_PAR;
	}
	else if ( pk_dexc != NULL
		&& ( pk_dexc->excatr & ~( TA_HLNG | TA_ASM ) ) )
	{
		/* attribute that doesn't exist in ITRON spec. */
		ercd = E_RSATR;
	}
	else
	{
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/* define a cpu exception handler (kernel function). */
		knlexc_define( excno, pk_dexc );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		ercd = E_OK;
	}
	
	return ercd;
}

#endif

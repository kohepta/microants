﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_set_tim.c
 *
 * implementations of MicroAnts set_tim().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tim.h"

/**
 * set the system time.
 * 
 * @param p_systim pointer of setting system time
 * @return error code
 */
ER set_tim(
	SYSTIM * p_systim )
{
	ER ercd;
	
	if ( p_systim == NULL )
	{
		/* illegal parameter. p_systim must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		BIOS_UINT lock;
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		knltim_set( p_systim );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		ercd = E_OK;
	}
	
	return ercd;
}

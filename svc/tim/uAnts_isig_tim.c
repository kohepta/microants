﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_isig_tim.c
 *
 * implementations of MicroAnts isig_tim().
 *
 * @date 2010.05.13 new creation
 */

#include "uAnts_tim.h"

/**
 * update the system time.
 * 
 * @return error code
 */
ER isig_tim( void )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		knltim_signal();
		ercd = E_OK;
	}
	
	return ercd;
}

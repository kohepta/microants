﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_get_tim.c
 *
 * implementations of MicroAnts get_tim().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tim.h"

/**
 * get the system time.
 * 
 * @param p_systim pointer of current system time
 * @return error code
 */
ER get_tim(
	SYSTIM * p_systim )
{
	ER ercd;
	
	if ( p_systim == NULL )
	{
		/* illegal parameter. p_systim must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		knltim_get( p_systim );
		
		ercd = E_OK;
	}
	
	return ercd;
}

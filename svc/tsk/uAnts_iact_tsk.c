﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_iact_tsk.c
 *
 * implementations of MicroAnts iact_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * activate a task (from no-task context).
 * 
 * @param tskid task ID
 * @return error code
 */
ER iact_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( tskid == TSK_SELF )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		/* activate a task (kernel function). */
		ercd = knltsk_activate( tskid );
	}
	
	return ercd;
}

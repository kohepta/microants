﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rel_wai.c
 *
 * implementations of MicroAnts rel_wai().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * release a waiting task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER rel_wai(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* release a waiting task (kernel function). */
		ercd = knltsk_release_wait( tskid );
	}
	
	return ercd;
}

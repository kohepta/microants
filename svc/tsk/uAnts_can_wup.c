﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_can_wup.c
 *
 * implementations of MicroAnts can_wup().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tsk.h"

/**
 * cancel all wake up requests for a task.
 * 
 * @param tskid task ID
 * @return queued wake up request count or error code
 */
ER_UINT can_wup(
	ID tskid )
{
	ER_UINT wupcnt;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		wupcnt = E_CTX;
	}
	else
	{
		T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			wupcnt = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				wupcnt = E_NOEXS;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					wupcnt = E_OBJ;
				}
				else
				{
					wupcnt = tsk->t_wakeup_count;
					tsk->t_wakeup_count = 0;
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return wupcnt;
}

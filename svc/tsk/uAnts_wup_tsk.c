﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_wup_tsk.c
 *
 * implementations of MicroAnts wup_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * wake up a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER wup_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* wake up a task (kernel function). */
		ercd = knltsk_wakeup( tskid );
	}
	
	return ercd;
}

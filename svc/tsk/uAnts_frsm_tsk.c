﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_frsm_tsk.c
 *
 * implementations of MicroAnts frsm_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * resume a task forcibly.
 * 
 * @param tskid task ID
 * @return error code
 */
ER frsm_tsk(
	ID tskid )
{
	ER ercd;
	
	/* resume a task frocibly (kernel function). */
	ercd = knltsk_resume( tskid, TRUE );
	
	return ercd;
}

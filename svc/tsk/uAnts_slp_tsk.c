﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_slp_tsk.c
 *
 * implementations of MicroAnts slp_tsk().
 *
 * @date 2010.05.10 new creation
 */
 
#include "uAnts_tsk.h"

/**
 * current task sleeps forever.
 * 
 * @return error code
 */
ER slp_tsk( void )
{
	ER ercd;
	
	/* current task sleeps forever (kernel function). */
	ercd = knltsk_sleep( TMO_FEVR );
	
	return ercd;
}

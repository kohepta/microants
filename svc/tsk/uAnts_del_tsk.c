﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_tsk.c
 *
 * implementations of MicroAnts del_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
#include "uAnts_mpl.h"
#endif

/**
 * delete a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER del_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	if ( tskid == TSK_SELF )
	{
		/* illegal object. current running task ID is specified. */
		ercd = E_OBJ;
	}
	else
	{
		T_TSK * tsk;
		
		/* not contain current running task. */
		tsk = knltsk_get_entity( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
				VP stack;
				
				stack = tsk->t_stack_origin;
				#endif
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is active. */
					ercd = E_OBJ;
				}
				else
				{
					knltsk_finalize( tsk );
					
					ercd = E_OK;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
				if ( ercd == E_OK )
				{
					T_MPL * stkmpl;
					
					/* get stack memory pool. */
					stkmpl = knlmpl_get_stack_entity();
					
					if ( knlmpl_verify_contains( stkmpl, stack ) )
					{
						/* task stack is freed. */
						knlmpl_free( stkmpl, stack );
					}
				}
				#endif
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_tsk.c
 *
 * implementations of MicroAnts cre_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * create a new task.
 * 
 * @param tskid task ID
 * @param pk_ctsk task creation information packet
 * @return error code
 */
ER cre_tsk(
	ID tskid,
	T_CTSK * pk_ctsk )
{
	ER ercd;
	T_TSK * tsk;
	
	/* not contain current running task. */
	tsk = knltsk_get_entity( tskid );
	
	if ( tsk == NULL )
	{
		/* illegal ID. tsk must not point to null. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &tsk->t_super ) )
		{
			/* task has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new task (kernel function). */
			ercd = knltsk_create( tsk, pk_ctsk );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

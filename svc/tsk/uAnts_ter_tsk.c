﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ter_tsk.c
 *
 * implementations of MicroAnts ter_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * terminate a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER ter_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_TSK * tsk;
		
		/* not contain current running task. */
		tsk = knltsk_get_entity( tskid );
		
		if ( tskid == TSK_SELF || tsk == knltsk_current )
		{
			/* current running task ID is specified. */
			ercd = E_ILUSE;
		}
		else if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been cretated. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				T_WAIT * wait;
				BOOL restart;
				
				wait = NULL;
				restart = FALSE;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else
				{
					if ( knltsk_test_state( tsk, TTS_SUS ) )
					{
						/* clear suspend state. */
						knltsk_clear_state( tsk, TTS_SUS );
					}
					
					if ( knltsk_test_state( tsk, TTS_WAI ) )
					{
						wait = tsk->t_wait;
						
						/*jp 待ち状態終了処理実行. */
						knltsk_quit_waiting( tsk, E_OK );
					}
					
					/* make task dormant. */
					restart = knltsk_dormant( tsk, FALSE );
					
					ercd = E_OK;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				if ( wait != NULL )
				{
					/*jp 
					 待機中タスク終了時における待ちオブジェクト個別
					 コールバック実行.
					*/
					knlwait_callback( wait, purpose_TERMINATED );
				}
				
				if ( restart )
				{
					/* if there is an activate request, restart task. */
					knlcpu_task_activation_preparation( tsk );
					knltsk_ready( tsk );
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

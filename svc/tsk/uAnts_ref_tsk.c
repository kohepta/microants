﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_tsk.c
 *
 * implementations of MicroAnts ref_tsk().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tsk.h"
#include "uAnts_tim.h"

/**
 * reference a task.
 * 
 * @param tskid task ID
 * @param pk_rtsk task status packet
 * @return error code
 */
ER ref_tsk(
	ID tskid,
	T_RTSK * pk_rtsk )
{
	ER ercd;
	
	if ( pk_rtsk == NULL )
	{
		/* illegal parameter. pk_rtsk must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_TSK * tsk;
		
		if ( !knlsys_sense_context() )
		{
			/* if task-context, contain current running task. */
			tsk = knltsk_get_entity_self( tskid );
		}
		else
		{
			/* if no-task-context, not contain current running task. */
			tsk = knltsk_get_entity( tskid );
		}
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				pk_rtsk->wobjid = 0;
				pk_rtsk->tskwait = 0;
				pk_rtsk->lefttmo = 0;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( tsk == knltsk_current )
				{
					pk_rtsk->tskstat = TTS_RUN;
				}
				else
				{
					pk_rtsk->tskstat = tsk->t_state;
				}
				
				if ( knltsk_test_state( tsk, TTS_WAI ) )
				{
					const T_OBJECT * object;
					const T_EVENT * event;
					
					pk_rtsk->tskwait = knlwait_get_factor( tsk->t_wait );
					
					object = knlwait_get_object( tsk->t_wait );
					if ( object != NULL )
					{
						pk_rtsk->wobjid = knlobject_get_id( object );
					}
					
					event = tsk->t_wait->w_event;
					
					if ( event != NULL
						&& knljoint_is_connected( &event->e_super.o_joint ) )
					{
						pk_rtsk->lefttmo =
							(TMO)knltim_get_remainder_until_timeout( event );
					}
					else
					{
						pk_rtsk->lefttmo = TMO_FEVR;
					}
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				pk_rtsk->tskpri = (PRI)tsk->t_priority;
				pk_rtsk->tskbpri = (PRI)tsk->t_base_priority;
				pk_rtsk->actcnt = (UINT)tsk->t_activate_count;
				pk_rtsk->wupcnt = (UINT)tsk->t_wakeup_count;
				pk_rtsk->suscnt = (UINT)tsk->t_suspend_count;
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

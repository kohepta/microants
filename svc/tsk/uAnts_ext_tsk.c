﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ext_tsk.c
 *
 * implementations of MicroAnts ext_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"
#include "uAnts_cpu.h"

/**
 * current task exits itself.
 */
void ext_tsk( void )
{
	if ( !knlsys_sense_dispatch_pending() )
	{
		BIOS_UINT lock;
		BOOL restart;
		
		/* disable dispatch. */
		knlsys_disable_dispatch();
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/* make task dormant. */
		restart = knltsk_dormant( knltsk_current, FALSE );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		if ( restart )
		{
			/* if there is an activate request, make task ready. */
			knltsk_ready( knltsk_current );
		}
		
		/* disable interrupts. */
		bios_cpu_disable_interrupts();
		
		/* restart task scheduling. */
		knlcpu_dispatch_from_exit_task( restart );
	}
	
	knlsys_shutdown();
}

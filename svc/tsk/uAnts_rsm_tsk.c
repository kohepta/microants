﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rsm_tsk.c
 *
 * implementations of MicroAnts rsm_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * resume a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER rsm_tsk(
	ID tskid )
{
	ER ercd;
	
	/* resume a task (kernel function). */
	ercd = knltsk_resume( tskid, FALSE );
	
	return ercd;
}

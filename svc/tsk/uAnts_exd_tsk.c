﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_exd_tsk.c
 *
 * implementations of MicroAnts exd_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"
#include "uAnts_cpu.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
#include "uAnts_mpl.h"
#endif

/**
 * current task exits and deletes itself.
 */
void exd_tsk( void )
{
	if ( !knlsys_sense_dispatch_pending() )
	{
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
		VP stack;
		
		stack = knltsk_current->t_stack_origin;
		#endif
		
		/* disable dispatch. */
		knlsys_disable_dispatch();
		
		#if ( UANTS_CFG_USE_AUTO_ALLOCATE_STACK )
		{
			T_MPL * stkmpl;
			
			/* get stack memory pool. */
			stkmpl = knlmpl_get_stack_entity();
			
			if ( knlmpl_verify_contains( stkmpl, stack ) )
			{
				/* task stack is freed. */
				knlmpl_free( stkmpl, stack );
			}
		}
		#endif
		
		/* disable interrupts. */
		bios_cpu_disable_interrupts();
		
		/* make task dormant forcibly. */
		knltsk_dormant( knltsk_current, TRUE );
		
		knltsk_finalize( knltsk_current );
		
		/* restart task scheduling. */
		knlcpu_dispatch_from_exit_task( FALSE );
	}
	
	knlsys_shutdown();
}

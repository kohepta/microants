﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_get_pri.c
 *
 * implementations of MicroAnts get_pri().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tsk.h"

/**
 * get a task priority.
 * 
 * @param tskid task ID
 * @param p_tskpri priority of target task
 * @return error code
 */
ER get_pri(
	ID tskid,
	PRI * p_tskpri )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( p_tskpri == NULL )
	{
		/* illegal parameter. p_tskpri must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else
				{
					*p_tskpri = tsk->t_priority;
					
					ercd = E_OK;
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

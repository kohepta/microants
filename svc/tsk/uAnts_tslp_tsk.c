﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tslp_tsk.c
 *
 * implementations of MicroAnts tslp_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * current task sleeps (with timeout).
 * 
 * @param tmout time until timeout
 * @return error code
 */
ER tslp_tsk(
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illeagl parameter. */
		ercd = E_PAR;
	}
	else
	{
		/* current task sleeps (kernel function). (with timeout) */
		ercd = knltsk_sleep( tmout );
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_can_act.c
 *
 * implementations of MicroAnts can_act().
 *
 * @date 2010.05.30 new creation
 */

#include "uAnts_tsk.h"

/**
 * cancel all activation requests for a task.
 * 
 * @param tskid task ID
 * @return queued activation request count or error code
 */
ER_UINT can_act(
	ID tskid )
{
	ER_UINT actcnt;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		actcnt = E_CTX;
	}
	else
	{
		T_TSK * tsk;
		
		/* contain current running task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			actcnt = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				actcnt = E_NOEXS;
			}
			else
			{
				actcnt = tsk->t_activate_count;
				tsk->t_activate_count = 0;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return actcnt;
}

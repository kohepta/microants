﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_tsk.c
 *
 * implementations of MicroAnts acre_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * create a new task (ID auto-assignment).
 * 
 * @param pk_ctsk task creation information packet
 * @return task ID (positive number) or error code
 */
ER_ID acre_tsk(
	T_CTSK * pk_ctsk )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knltsk_free_queue ) )
	{
		/* task resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_TSK * tsk;
		
		tsk = (T_TSK *)knlqueue_head( &knltsk_free_queue );
		
		/* create new task (kernel function). */
		ercd_id = knltsk_create( tsk, pk_ctsk );
		
		if ( ercd_id == E_OK )
		{
			/* get task ID. */
			ercd_id = knlobject_get_id( &tsk->t_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

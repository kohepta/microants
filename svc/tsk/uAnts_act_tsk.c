﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_act_tsk.c
 *
 * implementations of MicroAnts act_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * activate a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER act_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* activate a task (kernel function). */
		ercd = knltsk_activate( tskid );
	}
	
	return ercd;
}

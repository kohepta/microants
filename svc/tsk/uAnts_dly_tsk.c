﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_dly_tsk.c
 *
 * implementations of MicroAnts dly_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"
#include "uAnts_tim.h"

/**
 * current task delays.
 * 
 * @param dlytim delay time
 * @return error code
 */
ER dly_tsk(
	RELTIM dlytim )
{
	ER ercd;
	
	if ( knlsys_sense_dispatch_pending() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_WAIT wait;
		T_EVENT event;
		BOOL disp;
		BIOS_UINT lock;
		
		/* wait-controller is builded on a stack frame. */
		knlwait_build( &wait, &event, TTW_DLY, NULL, NULL, NULL );
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		knltsk_current->t_wait = &wait;
		
		if ( dlytim > 0 )
		{
			dlytim ++;
		}
		
		/* lock CPU. */
		lock = bios_cpu_lock();
		
		/* delete the current running task from ready queue. */
		knlqueue_delete(
			&knltsk_ready_queue,
			&knltsk_current->t_super.o_joint );
		
		/* adding the state of wait to the current running task. */
		knltsk_set_state( knltsk_current, TTS_WAI );
		
		/* unlock CPU. */
		bios_cpu_unlock( lock );
		
		/*jp
		 待ち状態に入る前にタイムアウトが発生することを防ぐ為,
		 待ち状態に入ってから, タイムイベントを登録する.
		 よって, dly_tim( 0 ) が指定されてもカレントタスクは一
		 度必ず待ち状態に移行する.
		*/
		knltim_add_event( &event, dlytim );
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
		
		/* get wait result. */
		ercd = knlwait_get_result( &wait );
		
		if ( ercd == E_TMOUT )
		{
			ercd = E_OK;
		}
	}
	
	return ercd;
}

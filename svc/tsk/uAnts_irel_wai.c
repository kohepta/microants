﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_irel_wai.c
 *
 * implementations of MicroAnts irel_wai().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"


/**
 * release a waiting task (from no-task context).
 * 
 * @param tskid task ID
 * @return error code
 */
ER irel_wai(
	ID tskid )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* release a waiting task (kernel function). */
		ercd = knltsk_release_wait( tskid );
	}
	
	return ercd;
}

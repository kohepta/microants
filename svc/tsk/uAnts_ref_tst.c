﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_tst.c
 *
 * implementations of MicroAnts ref_tst().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * reference a task (convinient version).
 * 
 * @param tskid task ID
 * @param pk_rtst task status packet
 * @return error code
 */
ER ref_tst(
	ID tskid,
	T_RTST * pk_rtst )
{
	ER ercd;
	
	if ( pk_rtst == NULL )
	{
		/* illegal parameter. pk_rtst must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_TSK * tsk;
		
		if ( !knlsys_sense_context() )
		{
			/* if task-context, contain current running task. */
			tsk = knltsk_get_entity_self( tskid );
		}
		else
		{
			/* if no-task-context, not contain current running task. */
			tsk = knltsk_get_entity( tskid );
		}
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been cretated. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( tsk == knltsk_current )
				{
					pk_rtst->tskstat = TTS_RUN;
				}
				else
				{
					pk_rtst->tskstat = tsk->t_state;
				}
				
				if ( knltsk_test_state( tsk, TTS_WAI ) )
				{
					pk_rtst->tskwait = knlwait_get_factor( tsk->t_wait );
				}
				else
				{
					pk_rtst->tskwait = 0;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

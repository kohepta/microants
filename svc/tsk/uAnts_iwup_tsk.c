﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_iwup_tsk.c
 *
 * implementations of MicroAnts iwup_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * wake up a task (from no-task context).
 * 
 * @param tskid task ID
 * @return error code
 */
ER iwup_tsk(
	ID tskid )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( tskid == TSK_SELF )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		/* wake up a task (kernel function). */
		ercd = knltsk_wakeup( tskid );
	}
	
	return ercd;
}

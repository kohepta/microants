﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_chg_pri.c
 *
 * implementations of MicroAnts chg_pri().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * change a priority of a task.
 * 
 * @param tskid task ID
 * @param tskpri new priority of task
 * @return error code
 */
ER chg_pri(
	ID tskid,
	PRI tskpri )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else if ( tskpri < TPRI_INI || tskpri  > TMAX_TPRI )
	{
		/* priority is outside effective range. */
		ercd = E_PAR;
	}
	else
	{
		T_TSK * tsk;
		
		/* coutain current task. */
		tsk = knltsk_get_entity_self( tskid );
		
		if ( tsk == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &tsk->t_super ) )
			{
				/* task has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				if ( knltsk_test_state( tsk, TTS_DMT ) )
				{
					/* task is not active. */
					ercd = E_OBJ;
				}
				else
				{
					PRI newpri;
					
					if ( tskpri == TPRI_INI )
					{
						/* initialize priority. */
						newpri = tsk->t_initial_priority;
					}
					else
					{
						newpri = tskpri;
					}
					
					/* update base priority. */
					tsk->t_base_priority = newpri;
					
					/* set the new priority to task (kernel function). */
					knltsk_set_priority( tsk, newpri );
					
					ercd = E_OK;
				}
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sus_tsk.c
 *
 * implementations of MicroAnts sus_tsk().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_tsk.h"

/**
 * suspend a task.
 * 
 * @param tskid task ID
 * @return error code
 */
ER sus_tsk(
	ID tskid )
{
	ER ercd;
	
	/* suspend a task (kernel function). */
	ercd = knltsk_suspend( tskid );
	
	return ercd;
}

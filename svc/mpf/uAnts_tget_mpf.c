﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tget_mpf.c
 *
 * implementations of MicroAnts tget_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * get a memory block from a fixed-sized memory pool (with timeout).
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param p_blk pointer of pointer to memory block
 * @param tmout time until timeout
 * @return error code
 */
ER tget_mpf(
	ID mpfid,
	VP * p_blk,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/*
		 get a memory block from a fixed-sized memory pool
		 (kernel function). (with timeout)
		*/
		ercd = knlmpf_get( mpfid, p_blk, &event, tmout );
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_get_mpf.c
 *
 * implementations of MicroAnts get_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * get a memory block from a fixed-sized memory pool (infinite).
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param p_blk pointer of pointer to memory block
 * @return error code
 */
ER get_mpf(
	ID mpfid,
	VP * p_blk )
{
	ER ercd;
	
	/*
	 get a memory block from a fixed-sized memory pool
	 (kernel function).
	*/
	ercd = knlmpf_get( mpfid, p_blk, NULL, TMO_FEVR );
	
	return ercd;
}

#endif

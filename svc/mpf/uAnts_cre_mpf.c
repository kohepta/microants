﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_mpf.c
 *
 * implementations of MicroAnts cre_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * create a new fixed-sized memory pool.
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param pk_cmpf fixed-sized memory pool creation information packet
 * @return error code
 */
ER cre_mpf(
	ID mpfid,
	T_CMPF * pk_cmpf )
{
	ER ercd;
	T_MPF * mpf;
	
	mpf = knlmpf_get_entity( mpfid );
	
	if ( mpf == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &mpf->f_super ) )
		{
			/* fixed-sized memory pool has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new fixed-sized memory pool (kernel function). */
			ercd = knlmpf_create( mpf, pk_cmpf );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

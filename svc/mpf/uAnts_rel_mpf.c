﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rel_mpf.c
 *
 * implementations of MicroAnts rel_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * release a memory block to a fixed-sized memory pool.
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param blk released memory block
 * @return error code
 */
ER rel_mpf(
	ID mpfid,
	VP blk )
{
	ER ercd;
	
	/*
	 release a memory block to a fixed-sized memory pool
	 (kernel function).
	*/
	ercd = knlmpf_release( mpfid, blk );
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_mpf.c
 *
 * implementations of MicroAnts del_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"
#include "uAnts_tsk.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
#include "uAnts_mpl.h"
#endif

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * delete a fixed-sized memory pool.
 * 
 * @param mpfid fixed-sized memory pool ID
 * @return error code
 */
ER del_mpf(
	ID mpfid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_MPF * mpf;
		
		mpf = knlmpf_get_entity( mpfid );
		
		if ( mpf == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpf->f_super ) )
			{
				/* fixed-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				VP vp;
				
				vp = mpf->f_origin;
				#endif
				
				knlmpf_finalize( mpf );
				
				/*
				 notify the fixed-sized memory pool was deleted,
				 to the all tasks waiting for it.
				*/
				knltsk_notify_object_deletion( &mpf->f_task_queue );
				
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				if ( vp != NULL )
				{
					T_MPL * genmpl;
					
					/* get 'fixed-sized memory pool' memory pool */
					genmpl = knlmpl_get_generic_entity();
					
					if ( knlmpl_verify_contains( genmpl, vp ) )
					{
						/* 'fixed-sized memory pool' buffer is freed. */
						knlmpl_free( genmpl, vp );
					}
				}
				#endif
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

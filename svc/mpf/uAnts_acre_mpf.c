﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_mpf.c
 *
 * implementations of MicroAnts acre_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * create a new fixed-sized memory pool (ID auto-assignment).
 * 
 * @param pk_cmpf fixed-sized memory pool creation information packet
 * @return fixed-sized memory pool ID (positive number) or error code
 */
ER_ID acre_mpf(
	T_CMPF * pk_cmpf )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlmpf_free_queue ) )
	{
		/* fixed-sized memory pool resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_MPF * mpf;
		
		mpf = (T_MPF *)knlqueue_head( &knlmpf_free_queue );
		
		/* create a new fixed-sized memory pool (kernel function). */
		ercd_id = knlmpf_create( mpf, pk_cmpf );
		
		if ( ercd_id == E_OK )
		{
			/* get fixed-sized memory pool ID. */
			ercd_id = knlobject_get_id( &mpf->f_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_mpf.c
 *
 * implementations of MicroAnts ref_mpf().
 *
 * @date 2010.05.12 new creation
 */

#include "uAnts_mpf.h"

#if ( UANTS_CFG_MPF_MAX_ID > 0 )

/**
 * reference a fixed-sized memory pool.
 * 
 * @param mpfid fixed-sized memory pool ID
 * @param pk_rmpf fixed-sized memory pool status packet
 * @return error code
 */
ER ref_mpf(
	ID mpfid,
	T_RMPF * pk_rmpf )
{
	ER ercd;
	
	if ( pk_rmpf == NULL )
	{
		/* illegal parameter. pk_rmpf must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_MPF * mpf;
		
		mpf = knlmpf_get_entity( mpfid );
		
		if ( mpf == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &mpf->f_super ) )
			{
				/* fixed-sized memory pool has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				pk_rmpf->fblkcnt= 
					(UINT)knlqueue_get_count( &mpf->f_block_queue );
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knlqueue_is_empty( &mpf->f_task_queue ) )
				{
					pk_rmpf->wtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &mpf->f_task_queue ) );
				}
				else
				{
					pk_rmpf->wtskid = TSK_NONE;
				}
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

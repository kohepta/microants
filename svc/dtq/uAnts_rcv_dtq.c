﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_rcv_dtq.c
 *
 * implementations of rcv_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * receive a data from a data-queue (infinite).
 * 
 * @param dtqid data-queue ID
 * @param pointer of pointer to receive data
 * @return error code
 */
ER rcv_dtq(
	ID dtqid,
	VP_INT * p_data )
{
	ER ercd;
	
	/* receive a data from a data-queue (kernel function). */
	ercd = knldtq_receive( dtqid, p_data, NULL, TMO_FEVR );
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_dtq.c
 *
 * implementations of MicroAnts del_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"
#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
#include "uAnts_mpl.h"
#endif

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * delete a data-queue.
 * 
 * @param dtqid data-queue ID
 * @return error code
 */
ER del_dtq(
	ID dtqid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_DTQ * dtq;
		
		dtq = knldtq_get_entity( dtqid );
		
		if ( dtq == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &dtq->d_super ) )
			{
				/* data-queue has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				VP vp;
				
				vp = dtq->d_origin;
				#endif
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				knldtq_finalize( dtq );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				/*
				 notify the eventflag was deleted, to the all tasks
				 waiting for it.
				*/
				knltsk_notify_object_deletion( &dtq->d_snd_task_queue );
				knltsk_notify_object_deletion( &dtq->d_rcv_task_queue );
				
				#if ( UANTS_CFG_USE_AUTO_ALLOCATE_GENERIC )
				if ( vp != NULL )
				{
					T_MPL * genmpl;
					
					/* get data-queue memory pool. */
					genmpl = knlmpl_get_generic_entity();
					
					if ( knlmpl_verify_contains( genmpl, vp ) )
					{
						/* data-queue memory is freed. */
						knlmpl_free( genmpl, vp );
					}
				}
				#endif
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

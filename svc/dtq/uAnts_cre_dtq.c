﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_dtq.c
 *
 * implementations of MicroAnts cre_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * create a new data-queue.
 * 
 * @param dtqid data-queue ID
 * @param pk_cdtq data-queue creation information packet
 * @return error code
 */
ER cre_dtq(
	ID dtqid,
	T_CDTQ * pk_cdtq )
{
	ER ercd;
	T_DTQ * dtq;
	
	dtq = knldtq_get_entity( dtqid );
	
	if ( dtq == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &dtq->d_super ) )
		{
			/* data-queue has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new data-queue (kenel function). */
			ercd = knldtq_create( dtq, pk_cdtq );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

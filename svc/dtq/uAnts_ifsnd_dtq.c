﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ifsnd_dtq.c
 *
 * implementations of MicroAnts ifsnd_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * send a data forcibly to a data-queue (from no-task context).
 * 
 * @param dtqid data-queue ID
 * @param data send data 
 * @return error code
 */
ER ifsnd_dtq(
	ID dtqid,
	VP_INT data )
{
	ER ercd;
	
	if ( !knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* send a data forcibly to a data-queue (kernel function). */
		ercd = knldtq_forcibly_send( dtqid, data );
	}
	
	return ercd;
}

#endif

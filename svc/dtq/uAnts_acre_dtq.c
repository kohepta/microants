﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_dtq.c
 *
 * implementations of MicroAnts acre_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * create a new data-queue (ID auto-assignment).
 * 
 * @param pk_cdtq data-queue creation information packet
 * @return data-queue ID (positive number) or error code
 */
ER_ID acre_dtq(
	T_CDTQ * pk_cdtq )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knldtq_free_queue ) )
	{
		/* data-queue resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_DTQ * dtq;
		
		dtq = (T_DTQ *)knlqueue_head( &knldtq_free_queue );
		
		/* create a new data-queue (kernel function). */
		ercd_id = knldtq_create( dtq, pk_cdtq );
		
		if ( ercd_id == E_OK )
		{
			/* get data-queue ID. */
			ercd_id = knlobject_get_id( &dtq->d_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_tsnd_dtq.c
 *
 * implementations of MicroAnts tsnd_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * send a data to a data-queue (with timeout).
 * 
 * @param dtqid data-queue ID
 * @param data send data
 * @param tmout time until timeout
 * @return error code
 */
ER tsnd_dtq(
	ID dtqid,
	VP_INT data,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_EVENT event;
		
		/* send a data to a data-queue (kernel function). (with timeout) */
		ercd = knldtq_send( dtqid, data, &event, tmout );
	}
	
	return ercd;
}

#endif

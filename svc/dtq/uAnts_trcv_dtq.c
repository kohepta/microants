﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_trcv_dtq.c
 *
 * implementations of MicroAnts trcv_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * receive a data from a data-queue (with timeout).
 * 
 * @param dtqid data-queue ID
 * @param pointer of pointer to receive data
 * @return tmout time until timeout
 */
ER trcv_dtq(
	ID dtqid,
	VP_INT * p_data,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/*
		 receive a data from a data-queue (kernel function). 
		 (with timeout)
		*/
		ercd = knldtq_receive( dtqid, p_data, &event, tmout );
	}
	
	return ercd;
}

#endif

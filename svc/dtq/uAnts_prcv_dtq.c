﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_prcv_dtq.c
 *
 * implementations of MicroAnts prcv_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * receive a data from a data-queue (polling).
 * 
 * @param dtqid data-queue ID
 * @param p_data pointer of pointer to receive data
 * @return error code
 */
ER prcv_dtq(
	ID dtqid,
	VP_INT * p_data )
{
	ER ercd;
	
	/* receive a data from a data-queue (kernel function). (polling) */
	ercd = knldtq_receive( dtqid, p_data, NULL, TMO_POL );
	
	return ercd;
}

#endif

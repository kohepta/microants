﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_dtq.c
 *
 * implementations of MicroAnts ref_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * reference a data-queue.
 * 
 * @param dtqid data-queue ID
 * @param pk_rdtq data-queue status packet
 * @return error code
 */
ER ref_dtq(
	ID dtqid,
	T_RDTQ * pk_rdtq )
{
	ER ercd;
	
	if ( pk_rdtq == NULL )
	{
		/* illegal parameter. pk_rdtq must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_DTQ * dtq;
		
		dtq = knldtq_get_entity( dtqid );
		
		if ( dtq == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &dtq->d_super ) )
			{
				/* data-queue has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( !knlqueue_is_empty( &dtq->d_snd_task_queue ) )
				{
					pk_rdtq->stskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &dtq->d_snd_task_queue ) );
				}
				else
				{
					pk_rdtq->stskid = TSK_NONE;
				}
				
				if ( !knlqueue_is_empty( &dtq->d_rcv_task_queue ) )
				{
					pk_rdtq->rtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &dtq->d_rcv_task_queue ) );
				}
				else
				{
					pk_rdtq->rtskid = TSK_NONE;
				}
				
				pk_rdtq->sdtqcnt = dtq->d_sent_count;
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_snd_dtq.c
 *
 * implementations of MicroAnts snd_dtq().
 *
 * @date 2010.05.11 new creation
 */

#include "uAnts_dtq.h"

#if ( UANTS_CFG_DTQ_MAX_ID > 0 )

/**
 * send a data to a data-queue (infinite).
 * 
 * @param dtqid data-queue ID
 * @param data send data
 * @return error code
 */
ER snd_dtq(
	ID dtqid,
	VP_INT data )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* send a data to a data queue (kernel function). */
		ercd = knldtq_send( dtqid, data, NULL, TMO_FEVR );
	}
	
	return ercd;
}

#endif

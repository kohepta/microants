﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_cre_sem.c
 *
 * implementations of MicroAnts cre_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * create a new semafore.
 * 
 * @param semid semafore ID
 * @param pk_csem semafore creation information packet
 * @return error code
 */
ER cre_sem(
	ID semid,
	T_CSEM * pk_csem )
{
	ER ercd;
	T_SEM * sem;
	
	sem = knlsem_get_entity( semid );
	
	if ( sem == NULL )
	{
		/* illegal ID. */
		ercd = E_ID;
	}
	else
	{
		BOOL disp;
		
		/* lock dispatch. */
		disp = knlsys_lock_dispatch();
		
		if ( knlobject_exists( &sem->s_super ) )
		{
			/* semafore has already been created. */
			ercd = E_OBJ;
		}
		else
		{
			/* create a new semafore (kernel function). */
			ercd = knlsem_create( sem, pk_csem );
		}
		
		/* unlock dispatch. */
		knlsys_unlock_dispatch( disp );
	}
	
	return ercd;
}

#endif

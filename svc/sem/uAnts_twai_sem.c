﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_twai_sem.c
 *
 * implementations of MicroAnts twai_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * wait for a semafore (with timeout).
 * 
 * @param semid semafore ID
 * @param tmout time until timeout
 * @return error code
 */
ER twai_sem(
	ID semid,
	TMO tmout )
{
	ER ercd;
	
	if ( tmout < TMO_FEVR )
	{
		/* illegal parameter. */
		ercd = E_PAR;
	}
	else
	{
		T_EVENT event;
		
		/* wait for a semafore (kernel function). (with timeout) */
		ercd = knlsem_wait( semid, &event, tmout );
	}
	
	return ercd;
}

#endif

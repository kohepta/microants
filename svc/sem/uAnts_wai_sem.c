﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_wai_sem.c
 *
 * implementations of MicroAnts wai_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * wait for a semafore forever.
 * 
 * @param semid semafore ID
 * @return error code
 */
ER wai_sem(
	ID semid )
{
	ER ercd;
	
	/* wait for a semafore forever (kernel function). */
	ercd = knlsem_wait( semid, NULL, TMO_FEVR );
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_acre_sem.c
 *
 * implementations of MicroAnts acre_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * create a new semafore (ID auto-assignment).
 * 
 * @param pk_csem semafore creation information packet
 * @return semafore ID (positive number) or error code
 */
ER_ID acre_sem(
	T_CSEM * pk_csem )
{
	ER_ID ercd_id;
	BOOL disp;
	
	/* lock dispatch. */
	disp = knlsys_lock_dispatch();
	
	if ( knlqueue_is_empty( &knlsem_free_queue ) )
	{
		/* semafore resources are exhausted. */
		ercd_id = E_NOID;
	}
	else
	{
		T_SEM * sem;
		
		sem = (T_SEM *)knlqueue_head( &knlsem_free_queue );
		
		/* create a new semafore (kernel function). */
		ercd_id = knlsem_create( sem, pk_csem );
		
		if ( ercd_id == E_OK )
		{
			/* get semafore ID. */
			ercd_id = knlobject_get_id( &sem->s_super );
		}
	}
	
	/* unlock dispatch. */
	knlsys_unlock_dispatch( disp );
	
	return ercd_id;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_del_sem.c
 *
 * implementations of MicroAnts del_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"
#include "uAnts_tsk.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * delete a semafore.
 * 
 * @param semid semafore ID
 * @return error code
 */
ER del_sem(
	ID semid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		T_SEM * sem;
		
		sem = knlsem_get_entity( semid );
		
		if ( sem == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &sem->s_super ) )
			{
				/* semafore has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				knlsem_finalize( sem );
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				/*
				 notify the semafore was deleted, to the all tasks
				 waiting for it.
				*/
				knltsk_notify_object_deletion( &sem->s_task_queue );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif

﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_sig_sem.c
 *
 * implementations of MicroAnts sig_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * signal to a semafore.
 * 
 * @param semid semafore ID
 * @return error code
 */
ER sig_sem(
	ID semid )
{
	ER ercd;
	
	if ( knlsys_sense_context() )
	{
		/* illegal context. */
		ercd = E_CTX;
	}
	else
	{
		/* sigal to a semafore (kernel function). */
		ercd = knlsem_signal( semid );
	}
	
	return ercd;
}

#endif

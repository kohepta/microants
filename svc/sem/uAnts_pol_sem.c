﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_pol_sem.c
 *
 * implementations of MicroAnts pol_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * wait for a semafore (polling).
 * 
 * @param semid semafore ID
 * @return error code
 */
ER pol_sem(
	ID semid )
{
	ER ercd;
	
	/* wait for a semafore (kernel function). (polling) */
	ercd = knlsem_wait( semid, NULL, TMO_POL );
	
	return ercd;
}

#endif

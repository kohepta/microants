﻿/*
	MicroAnts Real-time Kernel
	
	This software is conforms to the specification of uITRON ver. 4.03.00
*/

/**
 * @file uAnts_ref_sem.c
 *
 * implementations of MicroAnts ref_sem().
 *
 * @date 2010.05.10 new creation
 */

#include "uAnts_sem.h"

#if ( UANTS_CFG_SEM_MAX_ID > 0 )

/**
 * reference a semafore.
 * 
 * @param semid semafore ID
 * @param pk_rsem semafore status packet
 * @return error code
 */
ER ref_sem(
	ID semid,
	T_RSEM * pk_rsem )
{
	ER ercd;
	
	if ( pk_rsem == NULL )
	{
		/* illegal parameter. pk_rsem must not point to null. */
		ercd = E_PAR;
	}
	else
	{
		const T_SEM * sem;
		
		sem = knlsem_get_entity( semid );
		
		if ( sem == NULL )
		{
			/* illegal ID. */
			ercd = E_ID;
		}
		else
		{
			BOOL disp;
			
			/* lock dispatch. */
			disp = knlsys_lock_dispatch();
			
			if ( !knlobject_exists( &sem->s_super ) )
			{
				/* semafore has not been created. */
				ercd = E_NOEXS;
			}
			else
			{
				BIOS_UINT lock;
				
				/* lock CPU. */
				lock = bios_cpu_lock();
				
				if ( knlqueue_is_empty( &sem->s_task_queue ) )
				{
					pk_rsem->wtskid = TSK_NONE;
				}
				else
				{
					pk_rsem->wtskid = knlobject_get_id(
						(T_OBJECT *)knlqueue_head( &sem->s_task_queue ) );
				}
				
				pk_rsem->semcnt = sem->s_signal_count;
				
				/* unlock CPU. */
				bios_cpu_unlock( lock );
				
				ercd = E_OK;
			}
			
			/* unlock dispatch. */
			knlsys_unlock_dispatch( disp );
		}
	}
	
	return ercd;
}

#endif
